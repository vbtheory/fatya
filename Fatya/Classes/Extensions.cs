﻿using System;
using System.Collections.Generic;

namespace Fatya.Classes
{
	public static class Extensions
	{
		public static string ToYesNo(this bool b)
		{
			return b ? "Yes" : "No";
		}

		public static string IsPlural(this int number, string word)
		{
			string plural = word.EndsWith("s") ? "es" : "s";

			return number + " " + word + (number == 1 ? "" : plural);
		}

		public static string IsPlural(this long number, string word)
		{
			string plural = word.EndsWith("s") ? "es" : "s";

			return number + " " + word + (number == 1 ? "" : plural);
		}

		public static string IsPlural(this float number, string word, string format)
		{
			return number.ToString(format) + " " + word + (number == 1 ? "" : "s");
		}

		public static int ToInt(this string number)
		{
			return Int32.Parse(number);
		}

		public static string ToLetters(this int number, bool m)
		{
			if (number == 1) return number + (m ? "er" : "ère");
			if (number >= 2) return number + "ème";

			return number.ToString();
		}

		public static string ToRelativeTime(this long ms)
		{
			if (ms < 1000) return ms.IsPlural("millisecond");
			if (ms >= 1000 && ms < 60000) return (ms / 1000F).IsPlural("second", "##.#");
			return ((ms / 1000F) / 60F).IsPlural("minute", "##.#");
		}

		public static int FindId<TSource>(this IEnumerable<TSource> source, Predicate<TSource> search)
		{
			int index = ((List<TSource>) source).FindIndex(search);

			return (index == -1 ? -2 : index)+2;
		}

	}
}
