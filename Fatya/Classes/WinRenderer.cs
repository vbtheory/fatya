﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Fatya.Classes
{

	internal class WinRenderer : ToolStripProfessionalRenderer
	{
		private ToolStripSystemRenderer _t = new ToolStripSystemRenderer();

		public WinRenderer()
			: base(new WinColors())
		{
		}

		protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
		{
			base.OnRenderMenuItemBackground(e);

			if (e.ToolStrip.IsDropDown) return;

			int opacity;

			if (e.Item.Pressed) opacity = 125;
			else if (e.Item.Selected) opacity = 100;
			else opacity = 50;

			int baseColor = !e.Item.Pressed ? 0 : 150;

			e.Graphics.Clear(e.Item.Pressed ? SystemColors.Window : Color.FromArgb(opacity, 190, 190, 190));
			e.Graphics.DrawRectangle(new Pen(Color.FromArgb(opacity + 25, baseColor, baseColor, baseColor)), 0, 0, e.Item.Width - 1, e.Item.Height - 1);
		}

		protected override void OnRenderArrow(ToolStripArrowRenderEventArgs e)
		{
			Color c = e.Item.Selected ? Color.FromArgb(25, 25, 25) : Color.FromArgb(150, 150, 150);
			Pen p = new Pen(c, 2);
			Rectangle r = new Rectangle(e.Item.Width - 12, (e.Item.Height - 10) / 2, 5, 7);

			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			e.Graphics.DrawLines(p, new[] { new Point(r.X, r.Y), new Point(r.Right, r.Y + r.Height / 2), new Point(r.X, r.Bottom) });
		}

		protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
		{
			e.Graphics.Clear(e.ToolStrip.IsDropDown?SystemColors.Window:Color.Transparent);
		}

		protected override void OnRenderSeparator(ToolStripSeparatorRenderEventArgs e)
		{
			e.Graphics.DrawLine(new Pen(Color.FromArgb(215, 215, 215), 1), e.Item.Width / 10, 2, e.Item.Width - (e.Item.Width / 10), 2);
		}

		protected override void OnRenderItemCheck(ToolStripItemImageRenderEventArgs e)
		{
			_t.DrawItemCheck(e);
		}

		protected override void OnRenderItemBackground(ToolStripItemRenderEventArgs e)
		{
			e.Graphics.DrawLine(new Pen(Color.FromArgb(5, 5, 5)), 0, e.Item.Height - 1, e.Item.Width, e.ToolStrip.Height - 1);
		}

		protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e)
		{
			if (e.ToolStrip.IsDropDown)
			{
				e.TextColor = e.Text == e.Item.Text || e.Item.Selected ? Color.FromArgb(25, 25, 25) : Color.FromArgb(150, 150, 150);
				base.OnRenderItemText(e);
			}
			else
			{
				Graphics g = e.Graphics;
				GraphicsPath blackfont = new GraphicsPath();
				SolidBrush brsh = new SolidBrush(Color.Black);

				blackfont.AddString(e.Text, new FontFamily(e.TextFont.FontFamily.Name), (int)FontStyle.Regular, e.Graphics.DpiY * e.TextFont.Size / 72,
					e.TextRectangle, new StringFormat { LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center });

				g.SmoothingMode = SmoothingMode.HighQuality;
				g.FillPath(brsh, blackfont);
			}
		}

		private class WinColors : ProfessionalColorTable
		{

			private Color baseColor = SystemColors.Window;
			private Color selectColor = Color.FromArgb(229, 229, 229);

			public override Color MenuItemPressedGradientBegin
			{
				get { return baseColor; }
			}

			public override Color MenuItemPressedGradientEnd
			{
				get { return baseColor; }
			}

			public override Color MenuItemPressedGradientMiddle
			{
				get { return baseColor; }
			}

			public override Color MenuItemSelected
			{
				get { return selectColor; }
			}

			public override Color MenuItemSelectedGradientEnd
			{
				get { return selectColor; }
			}

			public override Color MenuItemSelectedGradientBegin
			{
				get { return selectColor; }
			}

			public override Color MenuItemBorder
			{
				get { return Color.FromArgb(210, 210, 210); }
			}

			public override Color MenuBorder
			{
				get { return Color.FromArgb(200, 150, 150, 150); }
			}

			public override Color ImageMarginGradientBegin
			{
				get { return baseColor; }
			}

			public override Color ImageMarginGradientMiddle
			{
				get { return baseColor; }
			}

			public override Color ImageMarginGradientEnd
			{
				get { return baseColor; }
			}
		}
	}
}
