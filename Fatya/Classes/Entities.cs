﻿using System;
using System.Linq;

namespace Fatya.Classes
{

	public class Formation
	{

		public Formation(int id)
		{
			Id = id;
			Name = "";
			NameAbr = "";
			NYear = 2;
			CreationDate = DateTime.Now;
			Description = "";
			Icon = "";
		}

		public Formation(int id, string name, string nameAbr, int nYear, DateTime creationDate, string description, string icon)
		{
			Id = id;
			Name = name;
			NameAbr = nameAbr;
			NYear = nYear;
			CreationDate = creationDate;
			Description = description;
			Icon = icon;
		}

		public int Id { get; set; }

		public string Name { get; set; }

		public string NameAbr { get; set; }

		public int NYear { get; set; }

		public DateTime CreationDate { get; set; }

		public string Description { get; set; }

		public string Icon { get; set; }

		public override string ToString()
		{
			return Name;
		}

		public int StudentCount
		{
			get
			{
				int total = 0;

				foreach (Class c in FrmMain.Frm.Classes.Where(x => x.IdFormation == Id))
				{
					total += c.StudentCount;
				}

				return total;
			}
		}

		public int ClassCount
		{
			get { return FrmMain.Frm.Classes.Count(x => x.IdFormation == Id); }
		}

		public bool NeedsAttention
		{
			get { return StudentCount == 0 || ClassCount == 0; }
		}

	}

	public class Class
	{

		public Class(int id, DateTime startedAt, int nyear, int idFormation)
		{
			Id = id;
			StartedAt = startedAt;
			Nyear = nyear;
			IdFormation = idFormation;
		}

		public Class(int id)
		{
			Id = id;
			StartedAt = DateTime.Now;
			Nyear = 1;
			IdFormation = -1;
		}

		public int Id { get; set; }

		public DateTime StartedAt { get; set; }

		public int Nyear { get; set; }

		public int IdFormation { get; set; }

		public int StudentCount
		{
			get { return FrmMain.Frm.Students.Count(x => x.IdClass == Id); }
		}

		public string Name
		{
			get
			{
				string name = Nyear.ToLetters(false);

				//we need to find the formation name
				int i = FrmMain.Frm.Formations.FindIndex(x => x.Id == IdFormation);

				if (i == -1) name = name + " année (Pas de formation)";
				else name += " " + FrmMain.Frm.Formations[i].Name;

				return name;
			}
		}

		public bool FormationExists
		{
			get
			{
				int i = FrmMain.Frm.Formations.FindIndex(x => x.Id == IdFormation);
				
				return i != -1;
			}
		}

		public int ModuleCount
		{
			get { return FrmMain.Frm.Modules.Count(x => x.IdClass == Id); }
		}

		public bool NeedsAttention
		{
			get { return !FormationExists || ModuleCount == 0 || StudentCount == 0; }
		}
	}

	public class Module
	{
		public Module(int id)
		{
			Id = id;
			Name = "";
			MinMark = 1;
			Coefficient = 1;
			IdClass = -1;
		}

		public Module(int id, string name, int minMark, int coefficient, int idClass)
		{
			Id = id;
			Name = name;
			MinMark = minMark;
			Coefficient = coefficient;
			IdClass = idClass;
		}

		public int Id { get; set; }

		public string Name { get; set; }

		public int MinMark { get; set; }

		public int Coefficient { get; set; }

		public int IdClass { get; set; }

		public string ClassName
		{
			get
			{
				int index = FrmMain.Frm.Classes.FindIndex(x => x.Id == IdClass);

				if (index == -1) return "Pas de classe!";

				return FrmMain.Frm.Classes[index].Name;
			}
		}

		public bool ClassExists
		{
			get { return FrmMain.Frm.Classes.Any(x => x.Id == IdClass); }
		}

		public int IdFormation
		{
			get
			{
				int index = FrmMain.Frm.Classes.FindIndex(x => x.Id == IdClass);

				if (index == -1) return -1;

				return FrmMain.Frm.Classes[index].IdFormation;
			}
		}

		public bool FormationExists
		{
			get
			{
				int index = FrmMain.Frm.Classes.FindIndex(x => x.Id == IdClass);

				if (index == -1) return false;

				return FrmMain.Frm.Classes[index].FormationExists;
			}
		}

		public int SubjectCount
		{
			get { return FrmMain.Frm.Subjects.Count(x => x.IdModule == Id); }
		}

		public bool NeedsAttention
		{
			get { return !ClassExists || !FormationExists || SubjectCount == 0; }
		}

	}

    public class Subject
    {
        public Subject(int id)
        {
            Id = id;
            Name = "";
            MinMark = 1;
            Coefficient = 1;
            IdModule = -1;
            IdTeacher = -1;
        }

        public Subject(int id, string name, int minMark, int coefficient, int idModule, int idTeacher)
        {
            Id = id;
            Name = name;
            MinMark = minMark;
            Coefficient = coefficient;
            IdModule = idModule;
            IdTeacher = idTeacher;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public int MinMark { get; set; }

        public int Coefficient { get; set; }

        public int IdModule { get; set; }

        public int IdTeacher { get; set; }

        public bool TeacherExists
        {
            get
            {
                int index = FrmMain.Frm.Teachers.FindIndex(x => x.Id == IdTeacher);

                return index != -1;
            }
        }

        public string TeacherName
        {
            get
            {
                int index = FrmMain.Frm.Teachers.FindIndex(x => x.Id == IdTeacher);

                if (index == -1) return "Pas d'enseignant!";

                return FrmMain.Frm.Teachers[index].GetFullName();
            }
        }

        public string ModuleName
        {
            get
            {
                int index = FrmMain.Frm.Modules.FindIndex(x => x.Id == IdModule);

                if (index == -1) return "Pas de module!";

                return FrmMain.Frm.Modules[index].Name;
            }
        }

        public bool ModuleExists
        {
            get
            {
                int index = FrmMain.Frm.Modules.FindIndex(x => x.Id == IdModule);
                return index != -1;
            }
        }

        public int IdClass
        {
            get
            {
                int index = FrmMain.Frm.Modules.FindIndex(x => x.Id == IdModule);

                if (index == -1) return -1;

                return FrmMain.Frm.Modules[index].IdClass;
            }
        }


        public string ClassName
        {
            get
            {
                int index = FrmMain.Frm.Classes.FindIndex(x => x.Id == IdClass);

                if (index == -1) return "Pas de classe!";

                return FrmMain.Frm.Classes[index].Name;
            }
        }

        public bool ClassExists
        {
            get { return FrmMain.Frm.Classes.Any(x => x.Id == IdClass); }
        }

        public int IdFormation
        {
            get
            {
                int index = FrmMain.Frm.Classes.FindIndex(x => x.Id == IdClass);

                if (index == -1) return -1;

                return FrmMain.Frm.Classes[index].IdFormation;
            }
        }

        public bool FormationExists
        {
            get
            {
                int index = FrmMain.Frm.Classes.FindIndex(x => x.Id == IdClass);

                if (index == -1) return false;

                return FrmMain.Frm.Classes[index].FormationExists;
            }
        }

        public bool NeedsAttention
        {
            get { return !ClassExists || !FormationExists || !ModuleExists; }
        }

    }

    public class Test
    {
        public Test(int id, string name, int idSubject, DateTime datetimeStart, DateTime datetimeEnd)
        {
            Id = id;
            Name = name;
            IdSubject = idSubject;
            datetime_start = datetimeStart;
            datetime_end = datetimeEnd;
        }

        public Test(int id)
        {
            Id = id;
            Name = "";
            IdSubject = -1;
            datetime_start = DateTime.Now;
            datetime_end = DateTime.Now;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public int IdSubject { get; set; }

        public DateTime datetime_start { get; set; }

        public DateTime datetime_end { get; set; }



        public bool NeedsAttention
        {
            get { return false; //return !ClassExists || !FormationExists || !ModuleExists; }}
            }
        }

    }

	public class Teacher
	{

		public Teacher(int id)
		{
			Id = id;
			Fname = "";
			Lname = "";
			Password = "";
			Email = "";
			Bio = "";
			Telephone = "";
			Address = "";
			Cin = "";
			Nsum = "";
			Gender = Gender.M;
		}

		public Teacher(int id, string fname, string lname, string password, string email, string bio, string telephone, string address, string cin, string nsum, Gender gender)
		{
			Id = id;
			Fname = fname;
			Lname = lname;
			Password = password;
			Email = email;
			Bio = bio;
			Telephone = telephone;
			Address = address;
			Cin = cin;
			Nsum = nsum;
			Gender = gender;
		}

		#region Properties

		public int Id { get; set; }

		public string Fname { get; set; }

		public string Lname { get; set; }

		public string Password { get; set; }

		public string Email { get; set; }

		public string Bio { get; set; }

		public string Telephone { get; set; }

		public string Address { get; set; }

		public string Cin { get; set; }

		public string Nsum { get; set; }

		public Gender Gender { get; set; }

		public bool NeedsAttention
		{
			get { return SubjectCount == 0; }
		}

		public int SubjectCount
		{
			get { return FrmMain.Frm.Subjects.Count(x => x.IdTeacher == Id); }
		}

		#endregion

		public string GetFullName()
		{
			return Fname + " " + Lname.ToUpper();
		}
	}

	public class Student
	{

		public Student(int id)
		{
			Id = id;
			Fname = "";
			Lname = "";
			Password = "";
			Email = "";
			Bio = "";
			Bac = "";
			Telephone = "";
			Address = "";
			Cne = "";
			Cin = "";
			Gender = Gender.M;
			Bday = DateTime.Now;
			IdClass = -1;
		}

		public Student(int id, string fname, string lname, string password, string email, string bio, string bac,
			string telephone, string address, string cne, string cin, Gender gender, DateTime bday, int idClass)
		{
			Id = id;
			Fname = fname;
			Lname = lname;
			Password = password;
			Email = email;
			Bio = bio;
			Bac = bac;
			Telephone = telephone;
			Address = address;
			Cne = cne;
			Cin = cin;
			Gender = gender;
			Bday = bday;
			IdClass = idClass;
		}

		#region Properties

		public int Id { get; private set; }

		public string Fname { get; set; }

		public string Lname { get; set; }

		public string Password { get; set; }

		public string Email { get; set; }

		public string Bio { get; set; }

		public string Bac { get; set; }

		public string Telephone { get; set; }

		public string Address { get; set; }

		public string Cne { get; set; }

		public string Cin { get; set; }

		public Gender Gender { get; set; }

		public DateTime Bday { get; set; }

		public int IdClass { get; set; }

		public bool NeedsAttention
		{
			get { return FrmMain.Frm.Classes.FindIndex(x => x.Id == IdClass) == -1 || FrmMain.Frm.Formations.FindIndex(x => x.Id == IdFormation) == -1; }
		}

		public bool ClassExists
		{
			get { return FrmMain.Frm.Classes.FindIndex(x => x.Id == IdClass) != -1;}
		}

		public string ClassName
		{
			get
			{
				int i = FrmMain.Frm.Classes.FindIndex(x => x.Id == IdClass);

				return i == -1 ? "Non affecté!" : FrmMain.Frm.Classes[i].Name;
			}
		}

		public int IdFormation
		{
			get
			{
				int i = FrmMain.Frm.Classes.FindIndex(x => x.Id == IdClass);

				if (i == -1) return -1;

				return FrmMain.Frm.Classes[i].IdFormation;
			}
		}

		#endregion

		public string GetFullName()
		{
			return Fname + " " + Lname.ToUpper();
		}
	}

    public class Absence
    {
        public Absence(int id, AbsType type, DateTime at, bool justified)
        {
            Id = id;
            Type = type;
            At = at;
            Justified = justified;
        }

        public Absence(int id)
        {
            Id = id;
            Type = AbsType.student;
            At = DateTime.Now;
            Justified = false;
        }

        public int Id { get; set; }
        public AbsType Type { get; set; }
        public DateTime At { get; set; }
        public bool Justified  { get; set; }

        public bool NeedsAttention
        {
            get { return !Justified; }
        }
    }

    public class Mark
    {
        public Mark(int id, int idStudent, int idTest, int mark, string review)
        {
            Id = id;
            IdStudent = idStudent;
            IdTest = idTest;
            Mark_ = mark;
            Review = review;
        }

        public Mark(int id)
        {
            Id = id;
            IdStudent = -1;
            IdTest = -1;
            Mark_ = -1;
            Review = "";
        }

        public int Id { get; set; }

        public int IdStudent { get; set; }

        public int IdTest { get; set; }

        public int Mark_ { get; set; }

        public string Review { get; set; }

        public string MarkLitteral
        {
            get { return Mark_ == -1 ? "Pas de note" : Mark_.ToString(); }
        }

    }

	public class Request
	{

		public Request(int id, string description, DateTime requestedAt, DateTime doneAt, RequestType requestType, RequestState requestState, int idStudent)
		{
			Id = id;
			Description = description;
			RequestedAt = requestedAt;
			DoneAt = doneAt;
			RequestType = requestType;
			IdStudent = idStudent;
			RequestState = requestState;
		}

		public Request(int id)
		{
			Id = id;
			Description = "";
			RequestedAt = DateTime.Now;
			DoneAt = DateTime.Now;
			RequestType = RequestType.Temp;
			IdStudent = -1;
			RequestState = RequestState.None;
		}

		public int Id { get; set; }

		public string Description { get; set; }

		public DateTime RequestedAt { get; set; }

		public DateTime DoneAt { get; set; }

		public RequestType RequestType { get; set; }

		public RequestState RequestState { get; set; }

		public int IdStudent { get; set; }

		public string StudentName
		{
			get
			{
				int i = FrmMain.Frm.Students.FindIndex(x => x.Id == IdStudent);

				return i == -1 ? "Pas d'étudiant!" : FrmMain.Frm.Students[i].GetFullName();
			}
		}

		public string StudentClass
		{
			get
			{
				int i = FrmMain.Frm.Students.FindIndex(x => x.Id == IdStudent);

				return i == -1 ? "Non affecté!" : FrmMain.Frm.Students[i].ClassName;
			}
		}

		public bool StudentExists
		{
			get
			{
				int i = FrmMain.Frm.Students.FindIndex(x => x.Id == IdStudent);
				return i != -1;
			}
		}

		public int IdClass
		{
			get
			{
				int i = FrmMain.Frm.Students.FindIndex(x => x.Id == IdStudent);

				if (i == -1) return -1;

				return FrmMain.Frm.Students[i].IdClass;
			}
		}

		public bool ClassExists
		{
			get
			{
				int i = FrmMain.Frm.Classes.FindIndex(x => x.Id == IdClass);

				return i != -1;
			}
		}

		public bool NeedsAttention
		{
			get { return RequestState == RequestState.Pending; }
		}

		public string TypeLitteral()
		{
			switch (RequestType)
			{
				case RequestType.Temp:
					return "Temporaire";
			}

			return "";
		}

		public string StateLitteral()
		{
			switch (RequestState)
			{
				case RequestState.Done:
					return "Validée";
				case RequestState.Rejected:
					return "Rejetée";
				case RequestState.Pending:
					return "En attente";
			}

			return "";
		}

		public string DoneLitteral()
		{
			if (RequestState == RequestState.Done || RequestState == RequestState.Rejected)
				return DoneAt.ToString("dd MMMM yyyy");
			
			return "";
		}
	}

	public enum Gender
	{
		N = 0,
		M = 1,
		F = 2,
	}

	public enum RequestState
	{
		None = -1,
		Done = 0,
		Pending = 1,
		Rejected = 2
	}

    public enum AbsType
    {
        student,teacher
    }

	public enum RequestType
	{
		Temp//todo remove
	}

}
