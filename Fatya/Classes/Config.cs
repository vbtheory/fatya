﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Fatya.Classes
{
	public static class Config
	{

		public static List<Setting> S = new List<Setting>();

		#region Public methods

		public static void Add(object target, string propertyName, object initialValue, string category, string key)
		{
			Setting s = new Setting(target, propertyName, initialValue, category, key);
			S.Add(s);
		}

		public static void Add(object target, string propertyName, object initialValue, int max, int min, string category, string key)
		{
			Setting s = new Setting(target, propertyName, initialValue, category, key);
			S.Add(s);
			s.Max = max;
			s.Min = min;
		}

		public static object Get(string category, string key)
		{
			foreach (Setting setting in S.Where(x => x.Category == category && x.Key == key)) return setting.GetValue();
			return null;
		}

		public static void Remove(string category, string key)
		{
			foreach (Setting setting in S.Where(x => x.Category == category && x.Key == key))
			{
				S.Remove(setting);
			}
		}

		#endregion

		#region Read/Write

		public static void Read(string fileName)
		{
			if (!File.Exists(fileName)) return;

			StreamReader sr = new StreamReader(fileName); //stream reader that will read the settings

			string currentCategory = ""; //holds the category we're at

			while (!sr.EndOfStream) //goes through the file
			{
				string currentLine = sr.ReadLine(); //reads the current file

				if (string.IsNullOrEmpty(currentLine) || currentLine.Length < 3 || currentLine.StartsWith(";") ||
					currentLine.StartsWith("#"))
					continue; //checks that the line is usable

				if (currentLine.StartsWith("[") && currentLine.EndsWith("]")) //checks if the line is a category marker
				{
					currentCategory = currentLine.Substring(1, currentLine.Length - 2);
					continue;
				}

				if (!currentLine.Contains("=")) continue; //or an actual setting

				string currentKey = currentLine.Substring(0, FindFirstSpace(currentLine));

				string currentValue = GetStringAfterEquals(currentLine);

				foreach (Setting s in (S.Where(x => x.Category == currentCategory && x.Key == currentKey)))
					s.SetValue(currentValue);
			}

			sr.Close(); //closes the stream
		}

		public static void Save(string fileName) //saves the file to the previously loaded file
		{
			S = S.OrderBy(x => x.Category).ToList();

			StreamWriter sw = new StreamWriter(fileName);

			sw.WriteLine(
				"#This file is very important, please edit with caution. If anything goes wrong, the app will try to fix it with default settings.");
			sw.WriteLine();

			string lastCategory = ""; //this is the last category encountered

			foreach (Setting s in S)
			{
				if (s.Category != lastCategory) //checks if the category has never been encountered yet
				{
					if (lastCategory != "") sw.WriteLine();
					lastCategory = s.Category;
					sw.WriteLine("[" + lastCategory + "]");
				}

				sw.WriteLine(s.Key + "=" + s.GetValue());
			}

			sw.Close();
		}

		#endregion

		#region Private methods

		private static int FindFirstSpace(string s)
		{
			for (int i = 0; i < s.Length; i++)
			{
				char c = s[i];
				if (c == ' ' || c == '=') return i;
			}
			return 1;
		}

		private static string GetStringAfterEquals(string s)
		{
			bool found = false;
			for (int i = 0; i < s.Length; i++)
			{
				char c = s[i];
				if (found && c != ' ') return s.Substring(i);
				if (c == '=') found = true;
			}
			return "";
		}

		#endregion

	}

#pragma warning disable 660,661
	public class Setting
#pragma warning restore 660,661
	{

		public Type PropertyType;
		public string Category;
		public string Key;
		public PropertyInfo Property;
		public object Target;
		public int Max;
		public int Min;

		public Setting(object target, string propertyName, object initialValue, string category, string key)
		{
			PropertyInfo property = target.GetType().GetProperty(propertyName);
			if (property == null)
				throw new Exception("Object: " + target + " does not have the property: " + propertyName);
			if (!property.CanRead || !property.CanWrite)
				throw new Exception("Setting cannot be read or set: " + propertyName);

			PropertyType = property.PropertyType;
			Property = property;
			Target = target;
			Category = category;
			Key = key;
			SetValue(initialValue);
		}

		public static bool operator ==(Setting a, Setting b)
		{
			return a.Category == b.Category && a.Key == b.Key;
		}

		public static bool operator !=(Setting a, Setting b)
		{
			return a.Category != b.Category || a.Key != b.Key;
		}

		public object GetValue()
		{
			return Property.GetValue(Target, null);
		}

		public void SetValue(object value)
		{
			try
			{
				if (PropertyType == typeof(bool))
					Property.SetValue(Target, bool.Parse(value.ToString()), null);
				else if (PropertyType == typeof(string))
					Property.SetValue(Target, value.ToString(), null);
				else if (PropertyType == typeof(float))
					Property.SetValue(Target, float.Parse(value.ToString()), null);
				else if (PropertyType == typeof(int))
				{
					int temp = int.Parse(value.ToString());
					if (temp > Max) temp = Max;
					else if (temp < Min) temp = Min;

					Property.SetValue(Target, temp, null);
				}
			}
			catch {}
		}
	}
}
