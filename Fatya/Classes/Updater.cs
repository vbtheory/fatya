﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows.Forms;
using Fatya.Properties;

namespace Fatya.Classes
{

	public class Updater
	{
		#region Declarations

		public static Vers _v;

		public static string LastException = "";
		private static Vers _ignored;

		private bool _async; //this defines if the search for the version is asynchronous
		private bool _failed;
		private string _url; //this is the url from which we get the version
		private string _changelog;
		private string _download;

		#endregion

		#region Properties

		public static Vers Ignored
		{
			get { return _ignored; }
			set
			{
				_ignored = value;
			}
		}

		#endregion

		public Updater(string url, string changelog, string download, bool async = false) //constructs and gets the version
		{
			_url = url; //saves the url
			_async = async; //and the choice of sync
			_changelog = changelog;
			_download = download;
		}

		#region Private Methods

		private void GetVersion() //gets the version from the url
		{
			try
			{
				_failed = false;
				if (!IsConnected()) // if there is no valid connection, sets the default values
					throw new Exception("You are not connected to the Internet");

				WebClient wc = new WebClient(); //creates the webClient that will take care of the download

				string downloadedString = wc.DownloadString(_url); //gets the file

				string[] strings = downloadedString.Split(new[] { '.' }); //splits it into parts

				//saves each part aside
				_v = new Vers(ushort.Parse(strings[0]), ushort.Parse(strings[1]), ushort.Parse(strings[2]));

				wc.Dispose();
			}
			catch (Exception ex)
			{
				_v = new Vers(0, 0, 0);
				_failed = true;
				LastException = ex.Message;
			}
		}

		private UpdateState CheckWithVersion(string version) //checks if there are any updates available
		{
			string[] v = version.Split('.');
			Vers currentVers = new Vers(Convert.ToUInt16(v[0]), Convert.ToUInt16(v[1]), Convert.ToUInt16(v[2]));

			if (_failed) return UpdateState.Failed;

			if (_v == Ignored) return UpdateState.UpToDate;

			if (currentVers.Version < _v.Version) //is the version installed older than the one available??
			{
				return UpdateState.VersionAvailable;
			}
			if (currentVers.Version == _v.Version)
			{
				if (currentVers.SubVersion < _v.SubVersion) //is the subversion installed older than the one available??
				{
					return UpdateState.SubVersionAvailable;
				}
				if (currentVers.SubVersion == _v.SubVersion)
				{
					if (currentVers.Revision < _v.Revision) //is the revision installed older than the one available??
					{
						return UpdateState.RevisionAvailable;
					}
					if (currentVers.Revision == _v.Revision)
					{
						return UpdateState.UpToDate;
					}
				}
			}
			return UpdateState.UpToDate;
		}

		private bool IsConnected() //checks if there is any valid Internet connection available
		{
			return NetworkInterface.GetIsNetworkAvailable();
		}

		#endregion

		#region Public Methods

		public void Update(string version, bool isSilent)
		{
			if (_async)
			{
				Thread t = new Thread(x => DoUpdate(version, isSilent)) {IsBackground = true};
				t.Start();
			}
			else DoUpdate(version, isSilent);
		}

		private void DoUpdate(string version, bool isSilent)
		{
			try
			{
				Logger.Log("Searching for updates");

				GetVersion();
				UpdateState state = CheckWithVersion(version);

				string message = "";
				bool foundUpdate = true;

				switch (state)
				{
					case UpdateState.Failed:
						message = "Failed to check for updates: " + LastException;
						foundUpdate = false;
						break;
					case UpdateState.RevisionAvailable:
					case UpdateState.SubVersionAvailable:
					case UpdateState.VersionAvailable:
						message = "Fatya <b>" + _v + "</b> is available for download. ";
						break;
					case UpdateState.UpToDate:
						message = "You are up to date";
						foundUpdate = false;
						break;
				}

				Logger.Log("Update state: " + message, state == UpdateState.Failed ? LogType.Warning : LogType.Info);

				if (!foundUpdate && !isSilent && state != UpdateState.Failed) FrmMain.Frm.BeginInvoke(new MethodInvoker(() => Notifier.Show(message, "Updater", Resources.Warning)));
				else if (!foundUpdate && !isSilent && state == UpdateState.Failed) FrmMain.Frm.BeginInvoke(new MethodInvoker(() => Notifier.Show(message, "Updater", Resources.Warning, "Click here to try again", () => DoUpdate(version, false))));

				if (!foundUpdate) return;

				message += "Would you like to download it?\n\n<h1>Changelog:</h1> \n<bullet>" +
						   new WebClient().DownloadString(_changelog)
						   + "</bullet>";
				Alert a = new Alert(message, "Update", MessageBoxButtons.YesNo, MessageBoxIcon.Information, "Ignore this version");
				
				a.Btn2Text = "&Update";
				a.Btn3Text = "&Cancel";
				if (a.Show() == DialogResult.Yes)
				{
					string downloadLink = new WebClient().DownloadString(_download);
					Process.Start(downloadLink);
				}

				if (a.CheckBoxState) Ignored = _v;
			}
			catch (Exception ex)
			{
				Logger.Log("Failed to start update: " + ex.Message + " (" + ex.TargetSite.Name + ")", LogType.Error);

				if (!isSilent) FrmMain.Frm.BeginInvoke(new MethodInvoker(() => Notifier.Show("Failed to update: " + ex.Message, "Update failed", Resources.Warning, "Click here to try again", () => DoUpdate(version, false))));
			}
		}

		public static void ParseVersion(string raw)
		{
			try
			{
				string[] info = raw.Split('.');

				Vers v = new Vers(ushort.Parse(info[0]), ushort.Parse(info[1]), ushort.Parse(info[2]));

				Ignored = v;
			}
			catch (Exception ex)
			{
				Logger.Log("Failed to parse version (" + raw + "): " + ex.Message + " (" + ex.TargetSite.Name + ")", LogType.Warning);
			}
		}

		#endregion

	}

	public struct Vers
	{
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is Vers && Equals((Vers) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = Version.GetHashCode();
				hashCode = (hashCode*397) ^ SubVersion.GetHashCode();
				hashCode = (hashCode*397) ^ Revision.GetHashCode();
				return hashCode;
			}
		}

		public readonly ushort Version;
		public readonly ushort SubVersion;
		public readonly ushort Revision;

		public Vers(ushort v, ushort s, ushort r)
		{
			Version = v;
			SubVersion = s;
			Revision = r;
		}

		public Vers(string s)
		{
			string[] collection = s.Split('.');

			Version = ushort.Parse(collection[0]);
			SubVersion = ushort.Parse(collection[1]);
			Revision = ushort.Parse(collection[2]);
		}

		public bool Equals(Vers other)
		{
			return Version == other.Version && SubVersion == other.SubVersion && Revision == other.Revision;
		}

		public static bool operator ==(Vers a, Vers b)
		{
			return a.Equals(b);
		}

		public static bool operator !=(Vers a, Vers b)
		{
			return !a.Equals(b);
		}

		public override string ToString()
		{
			return Version + "." + SubVersion + "." + Revision;
		}

	}

	public enum UpdateState //defines the state of the version with the server
	{
		UpToDate,
		VersionAvailable,
		SubVersionAvailable,
		RevisionAvailable,
		Failed
	}
}