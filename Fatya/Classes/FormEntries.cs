﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Fatya.Controls;

namespace Fatya.Classes
{

	public interface IFormEntry
	{

		void SetupControls(TableLayoutPanel parent);
		object GetValue();
		bool IsValidated();
		bool IncludeInJson { get; set; }
		bool IsRequired();
		event EventHandler PropertyChanged;

	}

	public abstract class FormEntry
	{

		public string RequiredMessage { get; set; }

	}

	public class TextEntry : FormEntry, IFormEntry
	{

		private TxtBox _textbox = new TxtBox();
		private Label _label = new Label();

		private bool _isRequired;

		public TextEntry(string displayName, string defaultValue, bool isRequired, bool isMultiline = false)
		{
			IncludeInJson = true;
			_isRequired = isRequired;
			_label.Text = _label.Text = string.IsNullOrEmpty(displayName) ? "" : displayName + (isRequired ? "*:" : " :");
			_textbox.Text = defaultValue;
			_textbox.MultiLine = isMultiline;
			
			_label.AutoSize = true;
			_label.Margin = new Padding(3, 5, 3, 0);
			_label.Location = new Point(0, 3);

			if (_textbox.MultiLine) _textbox.Height *= 3;
			_textbox.TextChanged += (sender, args) => OnPropertyChanged();
			_textbox.Location = new Point(0, _label.Bottom + 3);
			_textbox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
		}

		public void SetupControls(TableLayoutPanel parent)
		{
			parent.Controls.Add(_label);
			parent.Controls.Add(_textbox);
		}

		public object GetValue()
		{
			return _textbox.Text;
		}

		public bool IsValidated()
		{
			return !_isRequired || _textbox.Text != "";
		}

		public bool IncludeInJson { get; set; }

		public bool IsRequired()
		{
			return _isRequired;
		}

		public event EventHandler PropertyChanged;

		protected virtual void OnPropertyChanged()
		{
			EventHandler handler = PropertyChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

	}

	public class RuleEntry : FormEntry, IFormEntry
	{
		RuleBox _ruleBox = new RuleBox();
		Label _label = new Label();

		private bool _isRequired;

		public RuleEntry(string displayName, string defaultValue, Validation rule, bool isRequired, int fixedlength = 0, bool isNumeric = false)
		{
			IncludeInJson = true;
			_isRequired = isRequired;
			_label.Text = _label.Text = string.IsNullOrEmpty(displayName) ? "" : displayName + (isRequired ? "*:" : " :");
			_ruleBox.ValidationMethod = rule;
			_ruleBox.FixedLength = fixedlength;
			_ruleBox.NumericOnly = isNumeric;
			_ruleBox.Text = defaultValue;

			_label.AutoSize = true;
			_label.Margin = new Padding(3, 5, 3, 0);
			_label.Location = new Point(0, 3);

			_ruleBox.TextChanged += (sender, args) => OnPropertyChanged();
			_ruleBox.Location = new Point(0, _label.Bottom + 3);
			_ruleBox.TextChanged += (sender, args) => _ruleBox.Validate();
			_ruleBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
		}

		public void SetupControls(TableLayoutPanel parent)
		{
			parent.Controls.Add(_label);
			parent.Controls.Add(_ruleBox);
		}

		public object GetValue()
		{
			return _ruleBox.IsValid ? _ruleBox.Text : "";
		}

		public bool IsValidated()
		{
			return !_isRequired || _ruleBox.IsValid;
		}

		public bool IncludeInJson { get; set; }

		public bool IsRequired()
		{
			return _isRequired;
		}

		public event EventHandler PropertyChanged;

		protected virtual void OnPropertyChanged()
		{
			EventHandler handler = PropertyChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}
	}

	public class ComboEntry : FormEntry, IFormEntry
	{
		ComboBox _comboBox = new ComboBox();
		Label _label = new Label();

		private bool _isRequired;

		public ComboEntry(string displayName, int defaultValue, bool isRequired, params object[] items)
		{
			IncludeInJson = true;
			_isRequired = isRequired;
			_label.Text = _label.Text = string.IsNullOrEmpty(displayName) ? "" : displayName + (isRequired ? "*:" : " :");
			_comboBox.Items.AddRange(items);
			_comboBox.SelectedIndex = defaultValue;

			_label.AutoSize = true;
			_label.Margin = new Padding(3, 5, 3, 0);
			_label.Location = new Point(0, 3);

			_comboBox.SelectedIndexChanged += (sender, args) => OnPropertyChanged();
			_comboBox.Location = new Point(0, _label.Bottom + 3);
			_comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			_comboBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
		}

		public void SetupControls(TableLayoutPanel parent)
		{
			parent.Controls.Add(_label);
			parent.Controls.Add(_comboBox);
		}

		public object GetValue()
		{
			return _comboBox.SelectedIndex;
		}

		public bool IsValidated()
		{
			return !_isRequired || _comboBox.SelectedIndex != -1;
		}

		public bool IncludeInJson { get; set; }

		public bool IsRequired()
		{
			return _isRequired;
		}

		public event EventHandler PropertyChanged;

		protected virtual void OnPropertyChanged()
		{
			EventHandler handler = PropertyChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}
	}

	public class IntegerEntry : FormEntry, IFormEntry
	{
		NumericUpDown _numericUpDown = new NumericUpDown();
		Label _label = new Label();

		public IntegerEntry(string displayName, int defaultValue, int min, int max)
		{
			IncludeInJson = true;
			_label.Text = _label.Text = string.IsNullOrEmpty(displayName) ? "" : displayName + " :";
			_numericUpDown.Maximum = max;
			_numericUpDown.Minimum = min;
			_numericUpDown.Value = defaultValue;

			_label.AutoSize = true;
			_label.Margin = new Padding(3, 5, 3, 0);
			_label.Location = new Point(0, 3);

			_numericUpDown.ValueChanged += (sender, args) => OnPropertyChanged();
			_numericUpDown.Location = new Point(0, _label.Bottom + 3);
			_numericUpDown.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
		}

		public void SetupControls(TableLayoutPanel parent)
		{
			parent.Controls.Add(_label);
			parent.Controls.Add(_numericUpDown);
		}

		public object GetValue()
		{
			return Convert.ToInt32(_numericUpDown.Value);
		}

		public bool IsValidated()
		{
			return true;
		}

		public bool IncludeInJson { get; set; }

		public bool IsRequired()
		{
			return false;
		}

		public event EventHandler PropertyChanged;

		protected virtual void OnPropertyChanged()
		{
			EventHandler handler = PropertyChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}
	}

	public class DateEntry : FormEntry, IFormEntry
	{
		DateTimePicker _picker = new DateTimePicker();
		Label _label = new Label();

		public DateEntry(string displayName, DateTime defaultValue)
		{
			IncludeInJson = true;
			_label.Text = _label.Text = string.IsNullOrEmpty(displayName) ? "" : displayName + " :";
			_picker.Value = defaultValue;

			_label.AutoSize = true;
			_label.Margin = new Padding(3, 5, 3, 0);
			_label.Location = new Point(0, 3);

			_picker.ValueChanged += (sender, args) => OnPropertyChanged();
			_picker.Location = new Point(0, _label.Bottom + 3);
			_picker.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
		}

		public void SetupControls(TableLayoutPanel parent)
		{
			parent.Controls.Add(_label);
			parent.Controls.Add(_picker);
		}

		public object GetValue()
		{
			return _picker.Value.ToString("d");
		}

		public bool IsValidated()
		{
			return true;
		}

		public bool IncludeInJson { get; set; }

		public bool IsRequired()
		{
			return false;
		}

		public event EventHandler PropertyChanged;

		protected virtual void OnPropertyChanged()
		{
			EventHandler handler = PropertyChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}
	}

	public class TimeEntry : FormEntry, IFormEntry
	{
		DateTimePicker _picker = new DateTimePicker();
		Label _label = new Label();

		public TimeEntry(string displayName, DateTime defaultValue)
		{
			IncludeInJson = true;
			_label.Text = _label.Text = string.IsNullOrEmpty(displayName) ? "" : displayName + " :";
			_picker.Value = defaultValue;

			_label.AutoSize = true;
			_label.Margin = new Padding(3, 5, 3, 0);
			_label.Location = new Point(0, 3);

			_picker.ValueChanged += (sender, args) => OnPropertyChanged();
			_picker.Location = new Point(0, _label.Bottom + 3);
			_picker.Format = DateTimePickerFormat.Custom;
			_picker.CustomFormat = "HH:mm";
			_picker.ShowUpDown = true;
		}

		public void SetupControls(TableLayoutPanel parent)
		{
			parent.Controls.Add(_label);
			parent.Controls.Add(_picker);
		}

		public object GetValue()
		{
			return _picker.Value.ToString("HH:mm");
		}

		public bool IsValidated()
		{
			return true;
		}

		public bool IncludeInJson { get; set; }

		public bool IsRequired()
		{
			return false;
		}

		public event EventHandler PropertyChanged;

		protected virtual void OnPropertyChanged()
		{
			EventHandler handler = PropertyChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}
	}

	public class CheckBoxEntry : FormEntry, IFormEntry
	{
		CheckBox _checkBox = new CheckBox();
		Label _label = new Label();

		private bool _isRequired;

		public CheckBoxEntry(string displayName, bool defaultValue, bool isRequired)
		{
			IncludeInJson = true;
			_checkBox.Text = displayName;
			_checkBox.Checked = defaultValue;
			_isRequired = isRequired;

			_label.AutoSize = true;
			_label.Margin = new Padding(3, 5, 3, 0);
			_label.Location = new Point(0, 3);
			_label.Text = "";

			_checkBox.AutoSize = true;
			_checkBox.CheckedChanged += (sender, args) => OnPropertyChanged();
			_checkBox.Location = new Point(0, _label.Bottom + 3);
			_checkBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
		}

		public void SetupControls(TableLayoutPanel parent)
		{
			parent.Controls.Add(_label);
			parent.Controls.Add(_checkBox);
		}

		public object GetValue()
		{
			return _checkBox.Checked;
		}

		public bool IsValidated()
		{
			return (_isRequired && _checkBox.Checked) || !_isRequired;
		}

		public bool IncludeInJson { get; set; }

		public bool IsRequired()
		{
			return _isRequired;
		}

		public event EventHandler PropertyChanged;

		protected virtual void OnPropertyChanged()
		{
			EventHandler handler = PropertyChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}
	}

	public class RadioButtonEntry : FormEntry, IFormEntry
	{
		List<RadioButton> _radios = new List<RadioButton>();
		Panel _panel = new Panel();
		Label _label = new Label();

		private bool _isRequired;

		public RadioButtonEntry(string displayName, bool isRequired, params string[] values)
		{
			IncludeInJson = true;
			int width = 0;
			foreach (string s in values)
			{
				RadioButton r = new RadioButton();

				r.Text = s;
				r.AutoSize = true;
				r.CheckedChanged += (sender, args) => OnPropertyChanged();
				r.Location = new Point(width, _label.Bottom + 3);

				width += r.Width + 5;

				_panel.Controls.Add(r);
				_radios.Add(r);
			}
			_isRequired = isRequired;

			_label.AutoSize = true;
			_label.Margin = new Padding(3, 5, 3, 0);
			_label.Location = new Point(0, 3);
			_label.Text = _label.Text = string.IsNullOrEmpty(displayName) ? "" : displayName + (isRequired ? "*:" : " :");

			_panel.AutoSize = true;
			_panel.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
		}

		public void SetupControls(TableLayoutPanel parent)
		{
			parent.Controls.Add(_label);
			parent.Controls.Add(_panel);
		}

		public object GetValue()
		{
			foreach (RadioButton button in _radios)
				if (button.Checked) return button.Text;

			return "";
		}

		public bool IsValidated()
		{
			return (_isRequired && !string.IsNullOrEmpty(((string)GetValue()))) || !_isRequired;
		}

		public bool IncludeInJson { get; set; }

		public bool IsRequired()
		{
			return _isRequired;
		}

		public event EventHandler PropertyChanged;

		protected virtual void OnPropertyChanged()
		{
			EventHandler handler = PropertyChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}
	}

	public class CaptchaEntry : FormEntry, IFormEntry
	{
		Captcha _captcha = new Captcha();
		Label _label = new Label();

		public CaptchaEntry(string displayName, CaptchaStyle s)
		{
			_captcha.CaptchaStyle = s;

			_label.AutoSize = true;
			_label.Margin = new Padding(3, 5, 3, 0);
			_label.Location = new Point(0, 3);
			_label.Text = string.IsNullOrEmpty(displayName) ? "" : displayName + " :";
		}

		public void SetupControls(TableLayoutPanel parent)
		{
			parent.Controls.Add(_label);
			parent.Controls.Add(_captcha);
		}

		public object GetValue()
		{
			return _captcha.ValidateCaptcha();
		}

		public bool IsValidated()
		{
			return _captcha.ValidateCaptcha();
		}

		public bool IncludeInJson { get; set; }

		public bool IsRequired()
		{
			return true;
		}

		public void GenerateNew()
		{
			_captcha.LoadNew(false);
		}

		public event EventHandler PropertyChanged;

	}

	public class PictureEntry : FormEntry, IFormEntry
	{
		RoundPicBox _picBox = new RoundPicBox();
		Label _label = new Label();

		public PictureEntry(Bitmap image, string name)
		{
			_picBox.Image = image;
			_picBox.Size = new Size(64, 64);
			_picBox.Anchor = AnchorStyles.Bottom | AnchorStyles.Top;

			_label.AutoSize = false;
			_label.Anchor = AnchorStyles.Left | AnchorStyles.Right;
			_label.Font = new Font(_label.Font.FontFamily, 12F);
			_label.Text = name;
		}

		public void SetupControls(TableLayoutPanel parent)
		{
			parent.Controls.Add(_picBox);
			parent.Controls.Add(_label);
		}

		public object GetValue()
		{
			return _picBox.Image;
		}

		public bool IsValidated()
		{
			return true;
		}

		public bool IncludeInJson { get; set; }

		public bool IsRequired()
		{
			return false;
		}
		
		public event EventHandler PropertyChanged;

	}

	public class LabelEntry : FormEntry, IFormEntry
	{

		private Label _value = new Label();
		private Label _label = new Label();

		public LabelEntry(string displayName, string value)
		{
			IncludeInJson = true;
			_label.Text = _label.Text = string.IsNullOrEmpty(displayName) ? "" : displayName+ ":";
			_value.Text = value;

			_label.AutoSize = true;
			_label.Margin = new Padding(3, 5, 3, 0);
			_label.Location = new Point(0, 3);

			_value.TextChanged += (sender, args) => OnPropertyChanged();
			_value.Margin = new Padding(3, 5, 3, 0);
			_value.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom;
		}

		public void SetupControls(TableLayoutPanel parent)
		{
			parent.Controls.Add(_label);
			parent.Controls.Add(_value);
		}

		public object GetValue()
		{
			return _value.Text;
		}

		public bool IsValidated()
		{
			return true;
		}

		public bool IncludeInJson { get; set; }

		public bool IsRequired()
		{
			return false;
		}

		public event EventHandler PropertyChanged;

		protected virtual void OnPropertyChanged()
		{
			EventHandler handler = PropertyChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

	}

}
