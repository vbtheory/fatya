﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Fatya.Classes
{
	static class Server
	{
		public static int Key = -1;
		private const char Delim = '-';
		private static CookieContainer _cookieContainer = new CookieContainer();
		private const string Uri = "http://25.182.120.213:8090/projects/fatya/api/";

		/// <summary>
		/// Opens a connection with the api
		/// </summary>
		/// <param name="password">Password to login</param>
		/// <returns></returns>
		public static StringRequest OpenConnection(string password)
		{
			StringRequest result = new StringRequest();

			try
			{
				if (Key != -1) throw new Exception("Connection déjà établie");  //if the connection is already open

				DictionaryRequest loginResponse = ReadJSON("api=login&api_password=" + password); //send request & receive response

				if (!string.IsNullOrEmpty(loginResponse.Exception)) throw new Exception(loginResponse.Exception); //if a problem happened in login
				if (loginResponse.Response == null) throw new Exception("Pas de réponse du serveur, veuillez vous assurer de votre connexion Internet"); //if no response was received
				if (loginResponse.Response["status"].ToLower() != "true") throw new Exception("Mot de passe érroné"); //if the password doesn't match

				Key = int.Parse(loginResponse.Response["key"]); //get the key
				
				result.Response = "Session ouverte";
			}
			catch (Exception ex)
			{
				result.Exception = ex.Message;
			}

			return result;
		}
		/// <summary>
		/// Opens a connection with wait form in the foreground
		/// </summary>
		/// <param name="password">Password to login</param>
		/// <returns></returns>
		public static StringRequest OpenConnectionAsync(string password)
		{
			FrmWait f = new FrmWait(() => OpenConnection(password)); //create new FrmWait instance
			f.ShowDialog();

			return f.StringRequest; //return the response
		}
		/// <summary>
		/// Closes the connection
		/// </summary>
		/// <returns></returns>
		public static StringRequest CloseConnection()
		{
			StringRequest result = new StringRequest();

			try
			{
				if(Key == -1) throw new Exception("Session déjà fermée"); //if the session is already closed
				
				//send request and receive response
				StringRequest logoutResponse = SendQuery("logout=true", true);

				//if an error occurs while logging out
				if (!string.IsNullOrEmpty(logoutResponse.Exception)) throw new Exception(logoutResponse.Exception); 
				//if no response is received
				if (logoutResponse.Response != "[]") throw new Exception("Pas de réponse du serveur, veuillez vous assurer de votre connexion Internet");
				//remove cookie container
				_cookieContainer = null;
				Key = -1;

				result.Response = "Session fermée";
			}
			catch (Exception ex)
			{
				result.Exception = ex.Message;
			}

			return result;
		}

		/// <summary>
		/// Reads Json response and converts it to dictionary
		/// </summary>
		/// <param name="param">Parameters to send with the request</param>
		/// <returns></returns>
		public static DictionaryRequest ReadJSON(string param)
		{
			DictionaryRequest dr = new DictionaryRequest();
			try
			{
				StringRequest sr = SendQuery(param); //sends query to server & receives response

				//if an error occurs in the request
				if (!string.IsNullOrEmpty(sr.Exception)) throw new Exception(sr.Exception);

				//creates the dictionary
				dr.Response = JsonConvert.DeserializeObject<Dictionary<string, string>>(sr.Response);

				Key = Convert.ToInt32(dr.Response["key"]);

				if (dr.Response["status"] == "false")
				{
					if(new FrmLogin().ShowDialog() != DialogResult.OK) throw new Exception("Vous êtes déconnecté!");
				}

			}
			catch (Exception ex)
			{
				dr.Exception = ex.Message;
			}
			return dr;
		}

		/// <summary>
		/// Reads Json response and converts it to ApiRequest
		/// </summary>
		/// <param name="a">api request to send</param>
		/// <returns></returns>
		public static ApiResponse ReadApiCommit(ApiRequest a)
		{
			ApiResponse ar = new ApiResponse();
			try
			{
				int previousKey = Key;
				string json = new JavaScriptSerializer().Serialize(a);
				json = Crypt(json, Key);

				string param = "api=" + json;

				DictionaryRequest dr = ReadJSONAsync(param); //sends query to server & receives response

				//if an error occurs in the request
				if (!string.IsNullOrEmpty(dr.Exception)) throw new Exception(dr.Exception);
				if (dr.Response.ContainsKey("error")) throw new Exception(dr.Response["error"]);

				//creates the dictionary
				ar.Response = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(Decrypt(dr.Response["query"], previousKey));
			}
			catch (Exception ex)
			{
				ar.Exception = ex.Message;
			}

			return ar;
		}

		/// <summary>
		/// Reads Json response and converts it to ApiRequest
		/// </summary>
		/// <param name="a">api request to send</param>
		/// <returns></returns>
		public static ApiResponseAlt ReadApiQuery(ApiRequest a)
		{
			ApiResponseAlt ar = new ApiResponseAlt();
			try
			{
				int previousKey = Key;
				string json = new JavaScriptSerializer().Serialize(a);
				json = Crypt(json, Key);

				string param = "api=" + json;

				DictionaryRequest dr = ReadJSONAsync(param); //sends query to server & receives response

				//if an error occurs in the request
				if (!string.IsNullOrEmpty(dr.Exception)) throw new Exception(dr.Exception);

				//creates the dictionary
				ar.Response = JsonConvert.DeserializeObject<JObject>(Decrypt(dr.Response["query"], previousKey));
			}
			catch (Exception ex)
			{
				ar.Exception = ex.Message;
			}

			return ar;
		}
		
		/// <summary>
		/// Reads Json response and converts it to dictionary in the background
		/// </summary>
		/// <param name="param">Parameters to send with the request</param>
		/// <returns></returns>
		public static DictionaryRequest ReadJSONAsync(string param)
		{
			FrmWait f = new FrmWait(() => ReadJSON(param)); //create FrmWait instance
			f.ShowDialog();

			return f.DictionaryRequest;
		}

		/// <summary>
		/// Sends a request to the api
		/// </summary>
		/// <param name="param">Parameters to send with the request</param>
		/// <param name="signout">If true, will throw exception when the result is []</param>
		/// <returns></returns>
		public static StringRequest SendQuery(string param, bool signout=false)
		{
			
			StringRequest r = new StringRequest();
			try
			{
				byte[] buffer = Encoding.ASCII.GetBytes(param);
				//Initialization, we use localhost, change if applicable
				HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(Uri);
				//Set the cookie container
				webReq.CookieContainer = _cookieContainer;
				//Our method is post, otherwise the buffer (postvars) would be useless
				webReq.Method = "POST";
				//We use form contentType, for the postvars.
				webReq.ContentType = "application/x-www-form-urlencoded";
				//The length of the buffer (postvars) is used as contentlength.
				webReq.ContentLength = buffer.Length;
				//We open a stream for writing the postvars
				Stream postData = webReq.GetRequestStream();
				//Now we write, and afterwards, we close. Closing is always important!
				postData.Write(buffer, 0, buffer.Length);
				postData.Close();
				//Get the response handle, we have no true response yet!
				HttpWebResponse webResp = (HttpWebResponse)webReq.GetResponse();

				//Now, we read the response (the string), and output it.
				Stream responseStream = webResp.GetResponseStream();
				//Finally, we read the stream and put it in the response
				StreamReader answer = new StreamReader(responseStream);
				r.Response = answer.ReadToEnd();

				if(r.Response == "[]" && !signout) throw new Exception("Connection échouée");
			}
			catch (Exception ex)
			{
				r.Exception = ex.Message;
			}

			return r;
		}
		/// <summary>
		/// Crypts a string with the provided key
		/// </summary>
		/// <param name="val">String to crypt</param>
		/// <param name="code">Key to crypt with</param>
		/// <returns></returns>
		public static string Crypt(string val, int code)
		{
			string response = "";
			foreach (char c in val) //iterates through every char in the string
			{
				string temp = ((int)c).ToString("x");//int
				int x = Convert.ToInt32(temp, 16);//dec
				int dec = Convert.ToInt32(x.ToString(), 16); //dec
				int final = dec * code; //add key

				response += (response != "" ? Delim.ToString() : "") + final.ToString("x");
			}

			return response;
		}

		/// <summary>
		/// Decrypts a string with the provided key
		/// </summary>
		/// <param name="val">String to decrypt</param>
		/// <param name="code">Key to decrypt with</param>
		/// <returns></returns>
		public static string Decrypt(string val, int code)
		{
			string response  = "";
			foreach (string s in val.Split(Delim)) //iterates through every char in the string
			{
				int dec = Convert.ToInt32(s, 16); //dec
				int x = dec / code; //remove key
				int hex = Convert.ToInt32(x.ToString("x"), 10);//hex
				char res = (char)hex;//char
				response = response.Insert(response.Length, res.ToString());
			}
			return response;
		}

	}

	public struct StringRequest
	{

		public string Response;
		public string Exception;

		public StringRequest(string response, string exception)
		{
			Response = response;
			Exception = exception;
		}

		public override string ToString()
		{
			return Response;
		}
	}

	public struct DictionaryRequest
	{

		public Dictionary<string, string> Response;
		public string Exception;

		public DictionaryRequest(Dictionary<string, string> response, string exception)
		{
			Response = response;
			Exception = exception;
		}

	}

	public struct ApiResponse
	{

		public Dictionary<string, List<string>> Response;
		public string Exception;

		public ApiResponse(Dictionary<string, List<string>> response, string exception)
		{
			Response = response;
			Exception = exception;
		}

	}

	public struct ApiResponseAlt
	{

		public JObject Response;
		public string Exception;

		public ApiResponseAlt(JObject response, string exception)
		{
			Response = response;
			Exception = exception;
		}

	}

	public class ApiRequest : Dictionary<string, List<Dictionary<string, string>>>
	{

		public void Add(string commandName, params KeyValuePair<string, string>[] parameters)
		{
			if (!ContainsKey(commandName))
			{
				Add(commandName, new List<Dictionary<string, string>>());
			}

			Dictionary<string, string> result = new Dictionary<string, string>();
			foreach (KeyValuePair<string, string> pair in parameters)
			{
				result.Add(pair.Key, pair.Value);
			}

			base[commandName].Add(result);
		}

	}

}
