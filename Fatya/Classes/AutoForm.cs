﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Fatya.Classes
{
	public partial class AutoForm : Form
	{

		private Dictionary<string, IFormEntry> _entries = new Dictionary<string, IFormEntry>();
		private bool _canExit;
		private bool _cancelExit;

		private bool _modified;

		public AutoForm(string title, params KeyValuePair<string, IFormEntry>[] entries)
		{
			InitializeComponent();

			MinimumSize = Size;
			MaximumSize = new Size((int)(Screen.FromControl(this).Bounds.Width * 0.5), (int)(Screen.FromControl(this).Bounds.Height * 0.7));
			TlpMain.MaximumSize = new Size(MaximumSize.Width - (Width-TlpMain.Width), MaximumSize.Height - (Height - TlpMain.Height));

			Lighter.SetHighlight(BtnOk, Highlight.Green);
			Text = title;

			foreach (KeyValuePair<string, IFormEntry> entry in entries)
				AddEntry(entry.Key, entry.Value);

			TlpMain.AutoSize = true;
		}

		public void AddEntry(string name, IFormEntry entry)
		{
			Entries.Add(name, entry);

			entry.SetupControls(TlpMain);
			entry.PropertyChanged += (sender, args) => BtnOk.Enabled = _modified = true;

			if (entry.IsRequired()) LblRequired.Visible = true;
		}

		public Dictionary<string, IFormEntry> Entries
		{
			get { return _entries; }
		}

		public void AddToDictionary(string commandName, ref Dictionary<string, Dictionary<string, string>> source)
		{
			Dictionary<string, string> inside = new Dictionary<string, string>();

			foreach (KeyValuePair<string, IFormEntry> entry in Entries)
			{
				inside.Add(entry.Key, entry.Value.GetValue().ToString());
			}

			source.Add(commandName, inside);
		}

		public string ToJSON(string commandName)
		{
			StringBuilder sb = new StringBuilder();
			JSharp writer = new JSharp(sb);

			writer.WriteOpen();
			writer.WriteValue(commandName);

			writer.WriteOpen();

			int count = 0;
			foreach (KeyValuePair<string, IFormEntry> entry in Entries.Where(entry => entry.Value.IncludeInJson))
			{
				writer.WriteValue(entry.Key, entry.Value.GetValue().ToString(), count != Entries.Count-2);
				count++;
			}
			
			writer.WriteClose();
			writer.WriteClose();

			writer.Close();

			return sb.ToString();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			bool isValid = true;
			string message = "";

			foreach (KeyValuePair<string, IFormEntry> entry in Entries)
			{
				if (entry.Value.IsValidated()) continue;

				isValid = false;
				message = ((FormEntry) entry.Value).RequiredMessage;	
				break;
			}

			if (!isValid)
			{
				Alert.Show(string.IsNullOrEmpty(message)?"Veuillez remplir tous les champs marqués par (*)":message);
				_cancelExit = true;
			}
			else
			{
				foreach (KeyValuePair<string, IFormEntry> entry in _entries.Where(entry => entry.Value is CaptchaEntry))
				{
					((CaptchaEntry) entry.Value).GenerateNew();
				}
				DialogResult = DialogResult.OK;
				_canExit = true;
				Close();
			}
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void FrmForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_cancelExit)
			{
				e.Cancel = true;
				_cancelExit = false;
				return;
			}

			if (_canExit || !_modified)
			{
				_canExit = false;
				return;
			}

			if (Alert.Show("Êtes-vous sûr de vouloir annuler?", MessageBoxButtons.YesNo) == DialogResult.No)
				e.Cancel = true;
		}

		public object GetValue(string name)
		{
			return !_entries.ContainsKey(name) ? null : _entries[name].GetValue();
		}

		public bool OkOnly
		{
			set
			{
				BtnOk.Visible = !value;
				BtnCancel.Text = value ? "OK" : "Annuler";
				Lighter.SetHighlight(BtnCancel, value?Highlight.Green:Highlight.None);
				LblRequired.Visible = !value && Entries.Values.Any(x => x.IsRequired());
			}
		}

	}

}
