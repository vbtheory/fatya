﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;

namespace Fatya.Classes
{
	class JSharp
	{

		private StringBuilder _sb;

		private string _fileName = "";
		private StreamWriter _sw;

		private int _tabbing = 0;

		public JSharp(string fileName)
		{
			_fileName = fileName;
			_sw = new StreamWriter(_fileName);
		}

		public JSharp(StringBuilder sb)
		{
			_sb = sb;
		}

		public void WriteValue(string name, string value, bool addComma)
		{
			name = "\"" + name + "\"";
			value = "\"" + value + "\"";

			Write(name+":"+value + (addComma?",":""));
		}

		public void WriteValue(string name)
		{
			name = "\"" + name + "\"";

			Write(name + ":");
		}

		public void WriteOpen()
		{
			Write("{", true);
		}

		public void WriteClose()
		{
			_tabbing--;
			Write("}");
		}

		private void Write(string value, bool addTab = false)
		{
			for (int i = 0; i < _tabbing; i++) value = "\t" + value;

			if (_sb == null) _sw.WriteLine(value);
			else _sb.AppendLine(value);

			if(addTab) _tabbing++;
		}

		public void Close()
		{
			if (_sb != null || _sw == null) return;
			
			_sw.Close();
			_sw.Dispose();
			_fileName = "";
		}

		public bool IsOpen
		{
			get { return (_fileName != "" && _sw != null) || _sb != null; }
		}
	}
}
