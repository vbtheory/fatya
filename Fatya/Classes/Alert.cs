﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Windows.Forms;

namespace Fatya.Classes
{
	public partial class Alert : Form
	{

		#region Declarations & properties

		private string[] _buttonStrings = { "&OK", "&Annuler", "&Oui", "&Non", "&Réessayer", "&Interrompre", "&Ignorer" };
		private MessageBoxIcon _icon;
		private MessageBoxButtons _buttons;

		public string Btn1Text
		{
			get { return Btn1.Text; }
			set { Btn1.Text = value; }
		}

		public string Btn2Text
		{
			get { return Btn2.Text; }
			set { Btn2.Text = value; }
		}

		public string Btn3Text
		{
			get { return Btn3.Text; }
			set { Btn3.Text = value; }
		}

		public Highlight Btn1Highlight
		{
			get { return Lighter.GetHighlight(Btn1); }
			set { Lighter.SetHighlight(Btn1, value); }
		}

		public Highlight Btn2Highlight
		{
			get { return Lighter.GetHighlight(Btn2); }
			set { Lighter.SetHighlight(Btn2, value); }
		}

		public Highlight Btn3Highlight
		{
			get { return Lighter.GetHighlight(Btn3); }
			set { Lighter.SetHighlight(Btn3, value); }
		}

		public string Message
		{
			get { return RtbMessage.Text; }
			set
			{
				RtbMessage.Text = value.Trim('\n', ' ');
				Stylize();
			}
		}

		public string Title
		{
			get { return Text; }
			set
			{
				Text = string.IsNullOrEmpty(value) ? "Fatya" : "Fatya: " + value;
				LblTitle.Text = string.IsNullOrEmpty(value) ? "Fatya" : value;
			}
		}

		public new MessageBoxIcon Icon
		{
			get { return _icon; }
			set
			{
				_icon = value;
				switch (value)
				{
					case MessageBoxIcon.Warning:
						PbxIcon.Image = SystemIcons.Warning.ToBitmap();
						break;
					case MessageBoxIcon.Error:
						PbxIcon.Image = SystemIcons.Error.ToBitmap();
						break;
					case MessageBoxIcon.None:
						PbxIcon.Image = null;
						break;
					case MessageBoxIcon.Question:
						PbxIcon.Image = SystemIcons.Question.ToBitmap();
						break;
					case MessageBoxIcon.Information:
						PbxIcon.Image = SystemIcons.Information.ToBitmap();
						break;
				}
			}
		}

		public MessageBoxButtons Buttons
		{
			get { return _buttons; }
			set
			{
				_buttons = value;
				Btn1.Visible = Btn2.Visible = Btn3.Visible = true;
				switch (value)
				{
					case MessageBoxButtons.OK:
						Btn1.Visible = false;

						Btn2.Visible = false;

						Btn3.Text = _buttonStrings[0];
						Btn3.Tag = DialogResult.OK;
						AcceptButton = Btn3;
						CancelButton = Btn3;
						Lighter.SetHighlight(Btn3, Highlight.Green);
						break;
					case MessageBoxButtons.OKCancel:
						Btn1.Visible = false;

						Btn2.Text = _buttonStrings[0];
						Btn2.Tag = DialogResult.OK;
						AcceptButton = Btn2;
						Lighter.SetHighlight(Btn2, Highlight.Green);

						Btn3.Text = _buttonStrings[1];
						Btn3.Tag = DialogResult.Cancel;
						CancelButton = Btn3;
						break;
					case MessageBoxButtons.YesNo:
						Btn1.Visible = false;

						Btn2.Text = _buttonStrings[2];
						Btn2.Tag = DialogResult.Yes;
						AcceptButton = Btn2;
						Lighter.SetHighlight(Btn2, Highlight.Green);

						Btn3.Text = _buttonStrings[3];
						Btn3.Tag = DialogResult.No;
						CancelButton = Btn3;
						break;
					case MessageBoxButtons.YesNoCancel:
						Btn1.Text = _buttonStrings[2];
						Btn1.Tag = DialogResult.Yes;
						AcceptButton = Btn1;
						Lighter.SetHighlight(Btn1, Highlight.Green);

						Btn2.Text = _buttonStrings[3];
						Btn2.Tag = DialogResult.No;
						CancelButton = Btn2;

						Btn3.Text = _buttonStrings[1];
						Btn3.Tag = DialogResult.Cancel;
						break;
					case MessageBoxButtons.RetryCancel:
						Btn1.Visible = false;

						Btn2.Text = _buttonStrings[4];
						Btn2.Tag = DialogResult.Retry;
						AcceptButton = Btn2;
						Lighter.SetHighlight(Btn2, Highlight.Green);

						Btn3.Text = _buttonStrings[1];
						Btn3.Tag = DialogResult.Cancel;
						CancelButton = Btn3;
						break;
					case MessageBoxButtons.AbortRetryIgnore:
						Btn1.Text = _buttonStrings[5];
						Btn1.Tag = DialogResult.Abort;
						CancelButton = Btn1;
						Lighter.SetHighlight(Btn1, Highlight.Red);

						Btn2.Text = _buttonStrings[4];
						Btn2.Tag = DialogResult.Retry;
						AcceptButton = Btn2;

						Btn3.Text = _buttonStrings[6];
						Btn3.Tag = DialogResult.Ignore;
						break;
				}
			}
		}

		public string CheckBoxText
		{
			get { return ChbNeverAgain.Text; }
			set
			{
				ChbNeverAgain.Text = value;
				ChbNeverAgain.Visible = !string.IsNullOrEmpty(value);
			}
		}

		public bool CheckBoxState
		{
			get { return ChbNeverAgain.Checked; }
			set { ChbNeverAgain.Checked = value; }
		}

		#endregion

		#region Constructors

		public Alert()
		{
			InitializeComponent();
		}

		public Alert(string message)
		{
			ApplyConstructor(message, "", MessageBoxIcon.Information, MessageBoxButtons.OK, "", false);
		}

		public Alert(string message, MessageBoxButtons buttons)
		{
			ApplyConstructor(message, "", MessageBoxIcon.Information, buttons, "", false);
		}

		public Alert(string message, string title, MessageBoxButtons buttons)
		{
			ApplyConstructor(message, title, MessageBoxIcon.Information, buttons, "", false);
		}

		public Alert(string message, MessageBoxButtons buttons, string checkboxText)
		{
			ApplyConstructor(message, "", MessageBoxIcon.Information, buttons, checkboxText, false);
		}

		public Alert(string message, string title, MessageBoxIcon icon)
		{
			ApplyConstructor(message, title, icon, MessageBoxButtons.OK, "", false);
		}

		public Alert(string message, string title, string checkboxText, bool defaultCheckState)
		{
			ApplyConstructor(message, title, MessageBoxIcon.Information, MessageBoxButtons.OK, checkboxText, defaultCheckState);
		}

		public Alert(string message, string title, MessageBoxButtons buttons, string checkboxText)
		{
			ApplyConstructor(message, title, MessageBoxIcon.Information, buttons, checkboxText, false);
		}

		public Alert(string message, string title, MessageBoxButtons buttons, MessageBoxIcon icon)
		{
			ApplyConstructor(message, title, icon, buttons, "", false);
		}

		public Alert(string message, string title, MessageBoxButtons buttons, MessageBoxIcon icon,
			string checkboxText)
		{
			ApplyConstructor(message, title, icon, buttons, checkboxText, false);
		}

		public Alert(string message, string title, MessageBoxButtons buttons, MessageBoxIcon icon,
			string checkboxText, bool defaultCheckState)
		{
			ApplyConstructor(message, title, icon, buttons, checkboxText, defaultCheckState);
		}

		private void ApplyConstructor(string message, string title, MessageBoxIcon icon, MessageBoxButtons buttons,
			string checkboxText, bool defaultCheckState)
		{
			InitializeComponent();

			Message = message;
			Title = title;
			Icon = icon;
			Buttons = buttons;
			CheckBoxText = checkboxText;
			CheckBoxState = defaultCheckState;
		}

		#endregion

		#region Show overloads

		public new DialogResult Show()
		{
			using (this)
			{
				ResizeAlert();
				return ShowDialog();
			}
		}

		public static DialogResult Show(string message)
		{
			return Show(message, "", MessageBoxButtons.OK, MessageBoxIcon.Information, "", false);
		}

		public static DialogResult Show(string message, string title)
		{
			return Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Information, "", false);
		}

		public static DialogResult Show(string message, MessageBoxButtons buttons)
		{
			return Show(message, "", buttons, MessageBoxIcon.Information, "", false);
		}

		public static DialogResult Show(string message, string title, MessageBoxButtons buttons)
		{
			return Show(message, title, buttons, MessageBoxIcon.Information, "", false);
		}

		public static DialogResult Show(string message, string title, MessageBoxIcon icon)
		{
			return Show(message, title, MessageBoxButtons.OK, icon, "", false);
		}

		public static DialogResult Show(string message, string title, string checkboxText, bool defaultCheckState)
		{
			return Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Information, checkboxText, defaultCheckState);
		}

		public static DialogResult Show(string message, string title, MessageBoxButtons buttons, MessageBoxIcon icon)
		{
			return Show(message, title, buttons, icon, "", false);
		}

		public static DialogResult Show(string message, string title, MessageBoxButtons buttons, MessageBoxIcon icon,
			string checkboxText)
		{
			return Show(message, title, buttons, icon, checkboxText, false);
		}

		public static void ShowTest(int times)
		{
			while (Debugger.IsAttached && times>0)
			{
				Random r = new Random();

				string message = "";
				for (int i = 0; i < r.Next(1, 250); i++)
					message += (r.Next(0, 2) == 0 || message == "" ? "" : "\n") + Path.GetRandomFileName();

				string title = Path.GetRandomFileName();

				MessageBoxIcon m;
				switch (r.Next(0, 4))
				{
					case 0:
						m = MessageBoxIcon.Warning;
						break;
					case 1:
						m = MessageBoxIcon.Error;
						break;
					case 2:
						m = MessageBoxIcon.None;
						break;
					case 3:
						m = MessageBoxIcon.Question;
						break;
					default:
						m = MessageBoxIcon.Information;
						break;
				}

				MessageBoxButtons mbb = (MessageBoxButtons)r.Next(0, 5);
				string checkbox = r.Next(0, 2) == 0 ? "" : Path.GetRandomFileName();
				bool b = r.Next(0, 1) != 0;

				Alert a = new Alert(message, title, mbb, m, checkbox, b);

				if (r.Next(0, 2) != 0) a.Btn1Text = Path.GetRandomFileName();
				if (r.Next(0, 2) != 0) a.Btn2Text = Path.GetRandomFileName();
				if (r.Next(0, 2) != 0) a.Btn3Text = Path.GetRandomFileName();

				if (r.Next(0, 2) != 0) a.Btn1Highlight = (Highlight)r.Next(0, 5);
				if (r.Next(0, 2) != 0) a.Btn2Highlight = (Highlight)r.Next(0, 5);
				if (r.Next(0, 2) != 0) a.Btn3Highlight = (Highlight)r.Next(0, 5);

				a.Show();
				times--;
			}
		}

		public static DialogResult Show(string message, string title, MessageBoxButtons buttons, MessageBoxIcon icon, 
			string checkboxText, bool defaultCheckState)
		{
			Alert a = new Alert();

			a.Message = message;
			a.Title = title;
			a.Icon = icon;
			a.Buttons = buttons;
			a.CheckBoxText = checkboxText;
			a.CheckBoxState = defaultCheckState;

			return a.Show();
		}

		#endregion

		#region Events

		private void Btn1_Click(object sender, EventArgs e)
		{
			DialogResult = (DialogResult)((Control)sender).Tag;
			Close();
		}

		private void Alert_Shown(object sender, EventArgs e)
		{
			PlaySound();
		}

		private void RtbMessage_LinkClicked(object sender, LinkClickedEventArgs e)
		{
			System.Diagnostics.Process.Start(e.LinkText);
		}

		private void PnlTop_Paint(object sender, PaintEventArgs e)
		{
			e.Graphics.DrawLine(Pens.GhostWhite, 0, PnlTop.Height - 2, PnlTop.Width, PnlTop.Height - 2);
			e.Graphics.DrawLine(Pens.Gainsboro, 0, PnlTop.Height - 1, PnlTop.Width, PnlTop.Height - 1);
		}

		#endregion

		#region Methods

		private void ResizeAlert()
		{
			float lineHeight;
			float widestLine = 0;

			using (Graphics g = RtbMessage.CreateGraphics())
			{
				lineHeight = g.MeasureString(" ", RtbMessage.Font).Height;
				foreach (string line in RtbMessage.Lines)
				{
					float lineWidth = g.MeasureString(line, RtbMessage.Font).Width;
					if (lineWidth > widestLine) widestLine = lineWidth;
				}
			}

			int widthInc = Math.Max(LblTitle.Width, -RtbMessage.Width + (int)widestLine + 10);
			int heightInc = -RtbMessage.Height + RtbMessage.Lines.Count() * (int)lineHeight + 10;

			Height += heightInc;
			PnlTop.Height += heightInc;
			RtbMessage.Height += heightInc;
			Btn1.Top += heightInc;
			Btn2.Top += heightInc;
			Btn3.Top += heightInc;
			ChbNeverAgain.Top += heightInc;

            Width += widthInc;
			PnlTop.Width += widthInc;
			RtbMessage.Width += widthInc;
			Btn1.Left += widthInc;
			Btn2.Left += widthInc;
			Btn3.Left += widthInc;

			const int maxTop = 134;

			if (Btn3.Top < maxTop) Btn3.Top = maxTop;
			if (Btn3.Left < 191) Btn3.Left = 191;

			if (Btn2.Top < maxTop) Btn2.Top = maxTop;
			if (Btn2.Left < 110) Btn2.Left = 110;

			if (Btn1.Top < maxTop) Btn1.Top = maxTop;
			if (Btn1.Left < 29) Btn1.Left = 29;

			if (ChbNeverAgain.Top < 111) ChbNeverAgain.Top = 111;

		    if (string.IsNullOrEmpty(ChbNeverAgain.Text))
		    {
		        Btn1.Top = Btn2.Top = Btn3.Top = ChbNeverAgain.Top;
		        MinimumSize = new Size(MinimumSize.Width, MinimumSize.Height - ChbNeverAgain.Height*2);
		        Height -= ChbNeverAgain.Height*2;
		    }

            Height +=  10;
		    Width += 10;
		    RtbMessage.Width += 10;
		}

		private void Stylize()
		{
			RichTextBox r = RtbMessage;

			List<Style> styles = new List<Style>();
			styles.Add(new Style("bullet", 10, r.Font));
			styles.Add(new Style("b", new Font(r.Font, FontStyle.Bold)));
			styles.Add(new Style("h1", new Font(r.Font, FontStyle.Bold)));
			styles.Add(new Style("u", new Font(r.Font, FontStyle.Underline)));
			styles.Add(new Style("i", new Font(r.Font, FontStyle.Italic)));

			r.SelectionLength = 0;

			List<int> start = new List<int>();
			List<int> end = new List<int>();
			List<Font> font = new List<Font>();
			List<int> indent = new List<int>();

			foreach (
				Style res in styles.Where(x => r.Text.Contains("<" + x.Search + ">") && r.Text.Contains("</" + x.Search + ">")))
			{
				start.Add(r.Text.IndexOf("<" + res.Search + ">", StringComparison.Ordinal));
				r.Text = r.Text.Replace("<" + res.Search + ">", "");
				end.Add(r.Text.IndexOf("</" + res.Search + ">", StringComparison.Ordinal) - r.SelectionStart);
				r.Text = r.Text.Replace("</" + res.Search + ">", "");
				font.Add(res.Font);
				indent.Add(res.Indent);
			}

			for (int index = 0; index < start.Count; index++)
			{
				int selectionStart = start[index];

				r.SelectionStart = selectionStart;
				r.SelectionLength = end[index] - selectionStart;

				r.SelectionFont = font[index];
				r.SelectionIndent = indent[index];
			}
		}

		private void PlaySound()
		{
			switch (Icon)
			{
				case MessageBoxIcon.Error:
					SystemSounds.Hand.Play();
					break;
				case MessageBoxIcon.Asterisk:
					SystemSounds.Asterisk.Play();
					break;
				case MessageBoxIcon.Exclamation:
					SystemSounds.Exclamation.Play();
					break;
				case MessageBoxIcon.Question:
					SystemSounds.Question.Play();
					break;
			}
		}

		#endregion

	}

	class Style
	{
		public Font Font;
		public int Indent;
		public string Search = "";

		public Style(string s, Font f)
		{
			Search = s;
			Font = f;
		}

		public Style(string s, int i)
		{
			Search = s;
			Indent = i;
		}

		public Style(string s, int i, Font f)
		{
			Search = s;
			Indent = i;
			Font = f;
		}
	}

}
