﻿using System.Collections.Generic;
using System.Linq;

namespace Fatya.Classes
{
	public class Filter
	{
		public bool IsActive { get; set; }

		public int Id { get; set; }

		public string Search { get; set; }

	}

	public class StudentFilter : Filter
	{

		public StudentFilter()
		{
			Search = "";
			Bac = "";
			Gender = Gender.N;
			IdClass = -2;
			IdFormation = -2;
			IdModule = -1;
			IdTeacher = -1;
			Id = -1;
		}

		public string Bac { get; set; }

		public Gender Gender { get; set; }

		public int IdClass { get; set; }

		public int IdFormation { get; set; }

		public int IdTeacher { get; set; }

		public int IdModule { get; set; }

		public bool AppliesTo(Student s)
		{
			//check first if filter is active
			if (!IsActive) return true;

			//if name filter (search) is set
			if (Search != "")
			{
				//create a list of all fields to search from
				List<string> fields = new List<string>();
				
				fields.Add(s.Bac);
				fields.Add(s.GetFullName());
				fields.Add(s.Telephone);
				fields.Add(s.Address);
				fields.Add(s.Cin);
				fields.Add(s.Cne);
				fields.Add(s.Email);

				//if none of the fields match, return false
				if(!fields.Any(x=>x.ToLower().Contains(Search.ToLower()))) return false;
			}

			//if class filter is set, compare it to student's class id
			if (IdClass == -1 && s.ClassExists) return false;
			if (IdClass > -1 && s.IdClass != IdClass) return false;

			//if formation filter is set, compare it to student's formation id
			if (IdFormation == -1 && s.IdFormation != -1) return false;
			if (IdFormation > -1 && s.IdFormation != IdFormation) return false;

			if (IdModule != -1)
			{
				int c = FrmMain.Frm.Classes.FindIndex(x => x.Id == IdModule);

				if(c == -1) return false;

				if(s.IdClass != FrmMain.Frm.Classes[c].Id) return false;
			}

			//if id filter is set, compare it student's id
			if (Id != -1 && s.Id!= Id) return false;

			//if bac filter is set, compare it student's bac
			if (Bac != "" && s.Bac != Bac) return false;

			if (IdTeacher != -1)
			{
				List<Module> modules = FrmMain.Frm.Modules.FindAll(x => x.Id == s.IdClass);

				IEnumerable<Subject> subjects = FrmMain.Frm.Subjects.Where(sub => modules.Any(m => m.Id == sub.IdModule));

				if (subjects.All(x => x.IdTeacher != IdTeacher)) return false;
			}

			//same for Gender
			if (Gender != Gender.N && s.Gender != Gender) return false;

			//if all filter apply to student, return true
			return true;
		}
	}

	public class TeacherFilter : Filter
	{

		public TeacherFilter()
		{
			Id = -1;
			IdModule = -1;
			IdClass = -1;
			IdFormation = -1;
			Gender = Gender.N;
			Search = "";
		}

		public Gender Gender { get; set; }

		public int IdClass { get; set; }

		public int IdModule { get; set; }

		public int IdFormation { get; set; }

		public bool AppliesTo(Teacher t)
		{
			if (!IsActive) return true;

			//if name filter (search) is set
			if (Search != "")
			{
				//create a list of all fields to search from
				List<string> fields = new List<string>();

				fields.Add(t.GetFullName());
				fields.Add(t.Telephone);
				fields.Add(t.Address);
				fields.Add(t.Cin);
				fields.Add(t.Nsum);
				fields.Add(t.Email);

				//if none of the fields match, return false
				if (!fields.Any(x => x.ToLower().Contains(Search.ToLower()))) return false;
			}

			if (IdClass != -1)
			{
				List<Module> modules = FrmMain.Frm.Modules.FindAll(x => x.IdClass == IdClass);

				IEnumerable<Subject> subjects = FrmMain.Frm.Subjects.Where(s => modules.Any(m => m.Id == s.IdModule));

				if(subjects.All(x=>x.IdTeacher != t.Id)) return false;
			}

			if (IdModule != -1)
			{
				IEnumerable<Subject> subjects = FrmMain.Frm.Subjects.Where(s => s.IdModule == IdModule);

				if (subjects.All(x => x.IdTeacher != t.Id)) return false;
			}

			if (IdFormation != -1)//todo: add no filter + none
			{
				List<Class> classes = FrmMain.Frm.Classes.FindAll(x => x.IdFormation == IdFormation);
				IEnumerable<Module> modules = FrmMain.Frm.Modules.Where(m => classes.Any(c => m.IdClass == c.Id));
				IEnumerable<Subject> subjects = FrmMain.Frm.Subjects.Where(s => modules.Any(m => s.IdModule == m.Id));

				if(subjects.All(x=>x.IdTeacher != t.Id)) return false;
			}

			if (Id != -1 && t.Id != Id) return false;

			if (Gender != Gender.N && t.Gender != Gender) return false;

			return true;
		}
	}

	public class FormationFilter : Filter
	{

		public FormationFilter()
		{
			Search = "";
			Duration = 0;
			Id = -1;
			IdTeacher = -1;
		}

		public int Duration { get; set; }

		public int IdTeacher { get; set; }

		public bool AppliesTo(Formation f)
		{
			//check first if filter is active
			if (!IsActive) return true;

			//if name filter (search) is set
			if (Search != "")
			{
				//create a list of all fields to search from
				List<string> fields = new List<string>();

				fields.Add(f.Name);
				fields.Add(f.Description);

				//if none of the fields match, return false
				if (!fields.Any(x => x.ToLower().Contains(Search.ToLower()))) return false;
			}

			if(Id != -1 && Id != f.Id) return false;

			//if duration filter is set, compare it formation's duration
			if (Duration != 0 && f.NYear != Duration) return false;

			if (IdTeacher != -1)
			{
				
			}

			//if all filter apply to formation, return true
			return true;
		}
	}

	public class ClassFilter : Filter
	{

		public ClassFilter()
		{
			Search = "";
			Nyear = 0;
			StudentCount = -1;
			Id = -1;
			IdFormation = -1;
			IdTeacher = -1;
		}

		public int Nyear { get; set; }

		public int StudentCount { get; set; }

		public int IdFormation { get; set; }

		public int IdTeacher { get; set; }

		public bool AppliesTo(Class c)
		{
			//check first if filter is active
			if (!IsActive) return true;

			//if name filter (search) is set
			if (Search != "")
			{
				//create a list of all fields to search from
				List<string> fields = new List<string>();

				fields.Add(c.Name);

				//if none of the fields match, return false
				if (!fields.Any(x => x.ToLower().Contains(Search.ToLower()))) return false;
			}

			if (IdTeacher != -1)
			{
				List<Module> modules = FrmMain.Frm.Modules.FindAll(x => x.Id == c.Id);

				IEnumerable<Subject> subjects = FrmMain.Frm.Subjects.Where(s => modules.Any(m => m.Id == s.IdModule));

				if (subjects.All(x => x.IdTeacher != IdTeacher)) return false;
			}

			//if nyear filter is set, compare it class' nyear
			if (Nyear != 0 && c.Nyear != Nyear) return false;

			//if StudentCount filter is set, compare it class' StudentCount
			if (StudentCount != -1 && c.StudentCount != StudentCount) return false;

			if (Id != -1 && Id != c.Id) return false;

			if (IdFormation != -1 && IdFormation != c.IdFormation) return false;

			//if all filter apply to formation, return true
			return true;
		}
	}
	
	public class RequestFilter : Filter
	{

		public RequestFilter()
		{
			Search = "";
			IdClass = -2;
			IdStudent = -2;
			IdFormation = -1;
			RequestState = RequestState.None;
		}

		public int IdClass { get; set; }

		public int IdStudent { get; set; }

		public int IdFormation { get; set; }

		public RequestState RequestState { get; set; }

		public bool AppliesTo(Request r)
		{
			//check first if filter is active
			if (!IsActive) return true;

			//if name filter (search) is set
			if (Search != "")
			{
				//create a list of all fields to search from
				List<string> fields = new List<string>();

				fields.Add(r.StudentName);
				fields.Add(r.StudentClass);
				fields.Add(r.RequestType.ToString());

				//if none of the fields match, return false
				if (!fields.Any(x => x.ToLower().Contains(Search.ToLower()))) return false;
			}

			if (IdStudent == -1 && r.StudentExists) return false;
			if(IdStudent > -1 && r.IdStudent != IdStudent) return false;

			if (IdClass == -1 && r.ClassExists) return false;
			if (IdStudent > -1 && r.IdClass != IdClass) return false;

			if (IdFormation != -1)
			{
				List<Class> classes = FrmMain.Frm.Classes.FindAll(x => x.IdFormation == IdFormation);
				IEnumerable<Student> students = FrmMain.Frm.Students.Where(s => classes.Any(c => c.Id == s.IdClass));

				if(students.All(x=>x.Id != r.IdStudent)) return false;
			}

			if (RequestState != RequestState.None && r.RequestState != RequestState) return false;
			
			//if all filter apply to formation, return true
			return true;
		}
	}

	public class ModuleFilter : Filter
	{

		public ModuleFilter()
		{
			Search = "";
			Id = -1;
			IdClass = -2;
			IdFormation = -2;
		}
		
		public int IdClass { get; set; }

		public int IdFormation { get; set; }

		public bool AppliesTo(Module r)
		{
			//check first if filter is active
			if (!IsActive) return true;

			//if name filter (search) is set
			if (Search != "")
			{
				//create a list of all fields to search from
				List<string> fields = new List<string>();

				fields.Add(r.Name);
				fields.Add(r.ClassName);

				//if none of the fields match, return false
				if (!fields.Any(x => x.ToLower().Contains(Search.ToLower()))) return false;
			}

			if (Id != -1 && r.Id != Id) return false;

			if (IdClass == -1 && r.ClassExists) return false;
			if (IdClass > -1 && r.IdClass != IdClass) return false;

			if (IdFormation == -1 && r.FormationExists) return false;
			if (IdFormation > -1 && r.IdFormation != IdFormation) return false;

			//if all filter apply to module, return true
			return true;
		}
	}

	public class SubjectFilter : Filter
	{

		public SubjectFilter()
		{
			Search = "";
			Id = -1;
			IdModule = -2;
			IdClass = -2;
			IdFormation = -2;
			IdTeacher = -2;
		}

		public int IdModule { get; set; }

		public int IdClass { get; set; }

		public int IdFormation { get; set; }

		public int IdTeacher { get; set; }

		public bool AppliesTo(Subject r)
		{
			//check first if filter is active
			if (!IsActive) return true;

			//if name filter (search) is set
			if (Search != "")
			{
				//create a list of all fields to search from
				List<string> fields = new List<string>();

				fields.Add(r.Name);
				fields.Add(r.ClassName);
				fields.Add(r.ModuleName);

				//if none of the fields match, return false
				if (!fields.Any(x => x.ToLower().Contains(Search.ToLower()))) return false;
			}

			if (Id != -1 && r.Id != Id) return false;

			if (IdModule == -1 && r.ModuleExists) return false;
			if (IdModule > -1 && r.IdModule != IdModule) return false;

			if (IdClass == -1 && r.ClassExists) return false;
			if (IdClass > -1 && r.IdClass != IdClass) return false;

			if (IdFormation == -1 && r.FormationExists) return false;
			if (IdFormation > -1 && r.IdFormation != IdFormation) return false;

			if (IdTeacher == -1 && r.TeacherExists) return false;
			if (IdTeacher > -1 && r.IdTeacher != IdTeacher) return false;

			//if all filter apply to module, return true
			return true;
		}
	}

}
