﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Threading;
using System.Windows.Forms;
using Fatya.Properties;

namespace Fatya.Controls
{
	// ReSharper disable once RedundantNameQualifier
	[System.ComponentModel.DesignerCategory("Code")]
	class Captcha: Panel
	{

		TxtBox _inputBox = new TxtBox();
		PictureBox _captcha = new PictureBox();

		private bool _isGenerating;
		private string _demanded;
		private string _display;
		private int _captchaLength = 5;
		private CaptchaStyle _captchaStyle = CaptchaStyle.Alpha;

		public Captcha()
		{
			_captcha.Size = new Size(Width-Padding.Horizontal-5, Height - _inputBox.Height - 10 - Padding.Vertical);
			_captcha.Location = new Point(3, 3);
			_captcha.SizeMode = PictureBoxSizeMode.StretchImage;
			_captcha.Anchor = AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Bottom;
			
			_inputBox.Placeholder = "Veuillez répondre à la question";
			_inputBox.Size = new Size(Width - Padding.Horizontal-5, _inputBox.Height);
			_inputBox.Location = new Point(3, _captcha.Bottom + 5);
			_inputBox.Anchor = AnchorStyles.Right | AnchorStyles.Left|AnchorStyles.Bottom;
			_inputBox.Buttons.Add(new TxtButton {Image = Resources.Reload});
			_inputBox.ButtonClicked += (sender, args) => LoadNew(true);

			Controls.Add(_captcha);
			Controls.Add(_inputBox);

			LoadNew(false);

			while (_isGenerating)
			{

			}
		}

		public bool ValidateCaptcha()
		{
			return _inputBox.Text.ToUpper() == _demanded;
		}

		public void LoadNew(bool wait)
		{
			if (_isGenerating) return;
			_isGenerating = true;

			_demanded = GenerateDemanded();

			Thread t = new Thread(x=>GenerateCaptcha(wait));
			t.IsBackground = true;
			t.Start();

			_inputBox.Text = "";
		}

		private void GenerateCaptcha(bool wait)
		{
			Font f = new Font(Font.FontFamily.Name, 12);
			if (wait)
			{
				_inputBox.Enabled = false;
				Bitmap loading = new Bitmap(_captcha.Width, _captcha.Height);
				Graphics gLoading = Graphics.FromImage(loading);
				Rectangle rLoading = new Rectangle(0, 0, loading.Width, loading.Height);

				gLoading.SmoothingMode = SmoothingMode.AntiAlias;
				gLoading.TextRenderingHint = TextRenderingHint.AntiAlias;

				gLoading.DrawImage(_captcha.Image, rLoading);
				gLoading.FillRectangle(new SolidBrush(Color.FromArgb(210, Color.White)), rLoading);
				gLoading.DrawString("Loading...", f, Brushes.Black, rLoading,
					new StringFormat {LineAlignment = StringAlignment.Center, Alignment = StringAlignment.Center});

				_captcha.Image = loading;
			}

			Random r = new Random();
			int multiplier = r.Next(2, 3);

			Bitmap b = new Bitmap(_captcha.Width / multiplier, _captcha.Height / multiplier);
			Graphics g = Graphics.FromImage(b);

			Bitmap hatch = new Bitmap(_captcha.Width*2, _captcha.Height*2);
			Graphics gHatch = Graphics.FromImage(hatch);
			gHatch.FillRectangle(new HatchBrush(HatchStyle.Cross, Color.Red, BackColor), 0,0, hatch.Width, hatch.Height);

			g.SmoothingMode = SmoothingMode.AntiAlias;
			g.TextRenderingHint = TextRenderingHint.AntiAlias;

			g.RotateTransform(r.Next(-30, 30));
			g.DrawImage(hatch, -hatch.Width / 2, -hatch.Height / 2);
			g.ResetTransform();
			g.DrawString(_display, f, new SolidBrush(Color.Black), new RectangleF(0, 0, b.Width, b.Height), new StringFormat
			{
				LineAlignment = StringAlignment.Center,
				Alignment = StringAlignment.Center,
				Trimming = StringTrimming.None
			});

			for (int i = 0; i < r.Next(5, 10); i++)
			{
				Point start = new Point(r.Next(0, b.Width), r.Next(0, b.Height));
				Point end = new Point(r.Next(0, b.Width), r.Next(0, b.Height));
				Color c = Color.FromArgb(180, r.Next(0, 255), r.Next(0, 255), r.Next(0, 255));

				g.DrawLine(new Pen(c), start, end);
			}

			if (wait) Thread.Sleep(1500);

			_captcha.Image = b;
			_inputBox.Enabled = true;

			_isGenerating = false;
		}

		private string GenerateDemanded()
		{
			string demanded = "";
			const string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
			Random r = new Random();

			switch (CaptchaStyle)
			{
				case CaptchaStyle.Numeric:
					demanded = _display = r.Next((int) Math.Pow(10, CaptchaLength), (int) Math.Pow(10, CaptchaLength+1) - 1).ToString();
					break;
				case CaptchaStyle.Alpha:
					for (int i = 0; i < CaptchaLength; i++)
						demanded = demanded.Insert(demanded.Length, allowedChars[r.Next(0, allowedChars.Length - 10)].ToString());

					_display = demanded;
					break;
				case CaptchaStyle.Equation:
					int op1 = r.Next(1, 9);
					int op2 = r.Next(1, 9);
				
					switch (r.Next(0, 3))
					{
						case 0:
							_display = op1 + " + " + op2 + "=";
							demanded = (op1 + op2).ToString();
							break;
						case 1:
							_display = op1 + " x " + op2 + "=";
							demanded = (op1 * op2).ToString();
							break;
						case 2:
							_display = op1 + " - " + op2 + "=";
							demanded = (op1 - op2).ToString();
							break;
						case 3:
							_display = op1 + " / " + op2 + "=";
							demanded = (op1 / op2).ToString();
							break;
					}
					break;
				case CaptchaStyle.AlphaNumeric:
					for (int i = 0; i < CaptchaLength; i++)
						demanded = demanded.Insert(demanded.Length, allowedChars[r.Next(0, allowedChars.Length)].ToString());

					_display = demanded;
					break;
			}

			return demanded;
		}

		[DefaultValue(typeof(CaptchaStyle), "Alpha")]
		public CaptchaStyle CaptchaStyle
		{
			get { return _captchaStyle; }
			set
			{
				_captchaStyle = value;
				LoadNew(false);
			}
		}

		public int CaptchaLength
		{
			get { return _captchaLength; }
			set
			{
				if(value == CaptchaLength || value < 3) return;

				_captchaLength = value;
				LoadNew(false);
			}
		}
	}

	public enum CaptchaStyle
	{
		Numeric, Alpha, AlphaNumeric, Equation
	}
}
