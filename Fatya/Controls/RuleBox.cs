﻿using System;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;

namespace Fatya.Controls
{
	// ReSharper disable once RedundantNameQualifier
	[System.ComponentModel.DesignerCategory("Code")]
	public class RuleBox : TxtBox
	{

		private int _fixedLength;
		private bool _isValid;
		private bool _numericOnly;
		private Validation _validationMethod = Validation.MinLength;

		public bool IsValid
		{
			get
			{
				Validate();
				return _isValid;
			}
			private set
			{
				if (NumericOnly && Text.Any(x => !char.IsNumber(x))) value = false;

				_isValid = value;
				BorderColor = !_isValid && Text !="" ? Color.Red : ForeColor;
			}
		}
		
		public Validation ValidationMethod
		{
			get { return _validationMethod; }
			set
			{
				_validationMethod = value;
				MaxLength = _validationMethod == Validation.FixedLength || ValidationMethod == Validation.MaxLength ? FixedLength : 32767;

				Validate();
			}
		}

		public int FixedLength
		{
			get { return _fixedLength; }
			set
			{
				if (value < 0) return;

				_fixedLength = value;
				MaxLength = _validationMethod == Validation.FixedLength || ValidationMethod == Validation.MaxLength ? FixedLength : 32767;

				Validate();
			}
		}

		public bool NumericOnly
		{
			get { return _numericOnly; }
			set
			{
				_numericOnly = value;
				Validate();
			}
		}

		protected override void OnLeave(EventArgs e)
		{
			base.OnLeave(e);

			Validate();
		}

		public void Validate()
		{
			switch (_validationMethod)
			{
				case Validation.Path:
					IsValid = Regex.IsMatch(Text, @"^[A-z]\:((\\([^\<\>\:\""\\\/\|\*\?\[\]\=\%\$\+\,\;\r\n\t\0])+)*(\\?))?$");
					break;
				case Validation.MinLength:
					IsValid = Text.Length > FixedLength;
					break;
				case Validation.FixedLength:
					IsValid = Text.Length == FixedLength;
					break;
				case Validation.MaxLength:
					IsValid = Text.Length < FixedLength;
					break;
				case Validation.FileName:
					IsValid = Regex.IsMatch(Text,
						@"^(?!^(PRN|AUX|CLOCK\$|NUL|CON|COM\d|LPT\d|\..*)(\..+)?$)[^\x00-\x1f\\\?\*\:\<\>\""\;\|\/]+$");
					break;
				case Validation.Email:
					IsValid = Regex.IsMatch(Text, @"^[_A-z0-9\-]+(\.[_A-z0-9-]+)*@[A-z0-9-]+(\.[A-z0-9-]+)*(\.[A-z]{2,4})$");
					break;
				case Validation.None:
					IsValid = true;
					break;
			}
		}

	}
	public enum Validation
	{
		Email,
		MinLength,
		FixedLength,
		MaxLength,
		Path,
		FileName,
		None
	}

}