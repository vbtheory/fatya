﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Fatya.Controls
{
	[System.ComponentModel.DesignerCategory("Code")]
	class RoundPicBox : PictureBox
	{

		public RoundPicBox()
		{
			SizeMode = PictureBoxSizeMode.Zoom;
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

			Rectangle r = new Rectangle(0, 0, Width, Height);
			Region rgn = new Region(r);
			GraphicsPath path = new GraphicsPath();

			path.AddEllipse(r);
			rgn.Exclude(path);

			e.Graphics.FillRegion(new SolidBrush(BackColor), rgn);

			rgn.Dispose();
			path.Dispose();
		}
	}
}
