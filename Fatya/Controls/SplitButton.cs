using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using ContentAlignment = System.Drawing.ContentAlignment;

namespace Fatya.Controls
{
	// ReSharper disable once RedundantNameQualifier
	[System.ComponentModel.DesignerCategory("Code")]
	public class SplitButton : Button
	{
		private const int SplitSectionWidth = 18;

		private static int _borderSize = SystemInformation.Border3DSize.Width*2;
		private PushButtonState _state;
		private bool _isDropDown;
		private Rectangle _dropDownRectangle;

		private bool isSplitMenuVisible;

		private ContextMenuStrip _splitMenuStrip;
		private bool _showSplit;
		private bool _skipNextOpen;

		private TextFormatFlags _textFormatFlags = TextFormatFlags.Default;
		private bool _first = true;

		public SplitButton()
		{
			AutoSize = true;
		}

		#region Properties

		[Browsable(false)]
		public override ContextMenuStrip ContextMenuStrip
		{
			get { return SplitMenuStrip; }
			set { SplitMenuStrip = value; }
		}

		[DefaultValue(null)]
		public ContextMenuStrip SplitMenuStrip
		{
			get { return _splitMenuStrip; }
			set
			{
				//remove the event handlers for the old SplitMenuStrip
				if (_splitMenuStrip != null)
				{
					_splitMenuStrip.Closing -= SplitMenuStrip_Closing;
					_splitMenuStrip.Opening -= SplitMenuStrip_Opening;
				}

				//add the event handlers for the new SplitMenuStrip
				if (value != null)
				{
					ShowSplit = true;
					value.Closing += SplitMenuStrip_Closing;
					value.Opening += SplitMenuStrip_Opening;
				}
				else
					ShowSplit = false;


				_splitMenuStrip = value;
			}
		}

		[DefaultValue(false)]
		private bool ShowSplit
		{
			set
			{
				if (value == _showSplit) return;
				_showSplit = value;
				Invalidate();

				if (Parent != null)
					Parent.PerformLayout();
			}
		}

		private PushButtonState State
		{
			get { return _state; }
			set
			{
				if (_state.Equals(value)) return;
				_state = value;
				Invalidate();
			}
		}

		public bool IsDropDown
		{
			get { return _isDropDown; }
			set
			{
				_isDropDown = value;
				Invalidate();
			}
		}

		#endregion Properties

		protected override bool IsInputKey(Keys keyData)
		{
			if (keyData.Equals(Keys.Down) && _showSplit)
				return true;

			return base.IsInputKey(keyData);
		}

		protected override void OnGotFocus(EventArgs e)
		{
			if (!_showSplit)
			{
				base.OnGotFocus(e);
				return;
			}

			if (!State.Equals(PushButtonState.Pressed) && !State.Equals(PushButtonState.Disabled))
				State = PushButtonState.Default;
		}

		protected override void OnKeyDown(KeyEventArgs kevent)
		{
			if (_showSplit)
			{
				if ((kevent.KeyCode.Equals(Keys.Down) || IsDropDown) && !isSplitMenuVisible)
					ShowContextMenuStrip();
				else if (kevent.KeyCode.Equals(Keys.Space) && kevent.Modifiers == Keys.None)
					State = PushButtonState.Pressed;
			}

			base.OnKeyDown(kevent);
		}

		protected override void OnKeyUp(KeyEventArgs kevent)
		{
			if (kevent.KeyCode.Equals(Keys.Space))
			{
				if (MouseButtons == MouseButtons.None)
					State = PushButtonState.Normal;
			}
			else if (kevent.KeyCode.Equals(Keys.Apps))
				if ((MouseButtons == MouseButtons.None || IsDropDown) && !isSplitMenuVisible)
					ShowContextMenuStrip();

			base.OnKeyUp(kevent);
		}

		protected override void OnEnabledChanged(EventArgs e)
		{
			State = Enabled ? PushButtonState.Normal : PushButtonState.Disabled;

			base.OnEnabledChanged(e);
		}

		protected override void OnLostFocus(EventArgs e)
		{
			if (!_showSplit)
			{
				base.OnLostFocus(e);
				return;
			}

			if (!State.Equals(PushButtonState.Pressed) && !State.Equals(PushButtonState.Disabled))
				State = PushButtonState.Normal;
		}

		protected override void OnMouseEnter(EventArgs e)
		{
			if (!_showSplit)
			{
				base.OnMouseEnter(e);
				return;
			}

			if (!State.Equals(PushButtonState.Pressed) && !State.Equals(PushButtonState.Disabled))
				State = PushButtonState.Hot;
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			if (!_showSplit)
			{
				base.OnMouseLeave(e);
				return;
			}

			if (!State.Equals(PushButtonState.Pressed) && !State.Equals(PushButtonState.Disabled))
				State = Focused ? PushButtonState.Default : PushButtonState.Normal;
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			if (!_showSplit)
			{
				base.OnMouseDown(e);
				return;
			}

			if (e.Button != MouseButtons.Left) return;

			if ((_dropDownRectangle.Contains(e.Location) || IsDropDown) && !isSplitMenuVisible)
				ShowContextMenuStrip();
			else
				State = PushButtonState.Pressed;
		}

		protected override void OnPaint(PaintEventArgs pevent)
		{
			base.OnPaint(pevent);

			if (!_showSplit)
				return;

			Graphics g = pevent.Graphics;
			Rectangle bounds = ClientRectangle;

			if (State != PushButtonState.Pressed && IsDefault && !Application.RenderWithVisualStyles)
			{
				Rectangle backgroundBounds = bounds;
				backgroundBounds.Inflate(-1, -1);
				ButtonRenderer.DrawButton(g, backgroundBounds, State);

				g.DrawRectangle(SystemPens.WindowFrame, 0, 0, bounds.Width - 1, bounds.Height - 1);
			}
			else
			{
				ButtonRenderer.DrawButton(g, bounds, State);
			}

			_dropDownRectangle = new Rectangle(bounds.Right - SplitSectionWidth, 0, SplitSectionWidth, bounds.Height);

			int internalBorder = _borderSize;
			Rectangle focusRect =
				new Rectangle(internalBorder - 1,
					internalBorder - 1,
					bounds.Width - (IsDropDown?2:_dropDownRectangle.Width) - internalBorder,
					bounds.Height - (internalBorder*2) + 2);

			if (RightToLeft == RightToLeft.Yes)
			{
				_dropDownRectangle.X = bounds.Left + 1;
				focusRect.X = _dropDownRectangle.Right;
				if(!IsDropDown) g.DrawLine(SystemPens.ButtonShadow, bounds.Left + SplitSectionWidth, _borderSize,
					bounds.Left + SplitSectionWidth, bounds.Bottom - _borderSize);
			}
			else
			{
				if (!IsDropDown) g.DrawLine(SystemPens.ButtonShadow, bounds.Right - SplitSectionWidth, _borderSize,
					bounds.Right - SplitSectionWidth, bounds.Bottom - _borderSize);
			}

			PaintArrow(g, _dropDownRectangle);

			PaintTextandImage(g, new Rectangle(0, 0, ClientRectangle.Width, ClientRectangle.Height));

			if (State != PushButtonState.Pressed && Focused && ShowFocusCues)
				ControlPaint.DrawFocusRectangle(g, focusRect);
		}

		private void PaintTextandImage(Graphics g, Rectangle bounds)
		{
			// Figure out where our text and image should go
			Rectangle text_rectangle;
			Rectangle image_rectangle;

			CalculateButtonTextAndImageLayout(ref bounds, out text_rectangle, out image_rectangle);

			//draw the image
			if (Image != null)
			{
				if (Enabled)
					g.DrawImage(Image, image_rectangle.X, image_rectangle.Y, Image.Width, Image.Height);
				else
					ControlPaint.DrawImageDisabled(g, Image, image_rectangle.X, image_rectangle.Y, BackColor);
			}

			// If we dont' use mnemonic, set formatFlag to NoPrefix as this will show ampersand.
			if (!UseMnemonic)
				_textFormatFlags = _textFormatFlags | TextFormatFlags.NoPrefix;
			else if (!ShowKeyboardCues)
				_textFormatFlags = _textFormatFlags | TextFormatFlags.HidePrefix;

			//draw the text
			if (string.IsNullOrEmpty(Text)) return;
			if (Enabled)
				TextRenderer.DrawText(g, Text, Font, text_rectangle, ForeColor, _textFormatFlags);
			else
				ControlPaint.DrawStringDisabled(g, Text, Font, BackColor, text_rectangle, _textFormatFlags);
		}

		private void PaintArrow(Graphics g, Rectangle dropDownRect)
		{
			Point middle = new Point(Convert.ToInt32(dropDownRect.Left + dropDownRect.Width/2) - 1,
				Convert.ToInt32(dropDownRect.Top + dropDownRect.Height/2));

			Point[] arrow =
			{
				new Point(middle.X - 2, middle.Y - 1), new Point(middle.X + 3, middle.Y - 1),
				new Point(middle.X, middle.Y + 2)
			};

			g.FillPolygon(Enabled ? SystemBrushes.WindowFrame : SystemBrushes.ButtonShadow, arrow);
		}

		public override Size GetPreferredSize(Size proposedSize)
		{
			Size preferredSize = base.GetPreferredSize(proposedSize);

			//autosize correctly for splitbuttons
			if (!_showSplit) return preferredSize;
			if (AutoSize)
				return CalculateButtonAutoSize();

			if (!string.IsNullOrEmpty(Text) &&
			    TextRenderer.MeasureText(Text, Font).Width + SplitSectionWidth > preferredSize.Width)
				return preferredSize + new Size(SplitSectionWidth + _borderSize*2, 0);

			return preferredSize;
		}

		private Size CalculateButtonAutoSize()
		{
			Size ret_size = Size.Empty;
			Size text_size = TextRenderer.MeasureText(Text, Font);
			Size image_size = Image == null ? Size.Empty : Image.Size;

			// Pad the text size
			if (Text.Length != 0)
			{
				text_size.Height += 4;
				text_size.Width += 4;
			}

			switch (TextImageRelation)
			{
				case TextImageRelation.Overlay:
					ret_size.Height = Math.Max(Text.Length == 0 ? 0 : text_size.Height, image_size.Height);
					ret_size.Width = Math.Max(text_size.Width, image_size.Width);
					break;
				case TextImageRelation.ImageAboveText:
				case TextImageRelation.TextAboveImage:
					ret_size.Height = text_size.Height + image_size.Height;
					ret_size.Width = Math.Max(text_size.Width, image_size.Width);
					break;
				case TextImageRelation.ImageBeforeText:
				case TextImageRelation.TextBeforeImage:
					ret_size.Height = Math.Max(text_size.Height, image_size.Height);
					ret_size.Width = text_size.Width + image_size.Width;
					break;
			}

			// Pad the result
			ret_size.Height += (Padding.Vertical + 6);
			ret_size.Width += (Padding.Horizontal + 6);

			//pad the splitButton arrow region
			if (_showSplit)
				ret_size.Width += SplitSectionWidth;

			return ret_size;
		}

		private void ShowContextMenuStrip()
		{
			if (_skipNextOpen)
			{
				// we were called because we're closing the context menu strip
				// when clicking the dropdown button.
				_skipNextOpen = false;
				return;
			}

			State = PushButtonState.Pressed;

			if (_splitMenuStrip == null) return;

			Point p = new Point(1, Height);

			if (PointToScreen(p).X + _splitMenuStrip.Width > Screen.FromControl(this).WorkingArea.Width)
				p.X = Width - _splitMenuStrip.Width;

			if (PointToScreen(p).Y + _splitMenuStrip.Height > Screen.FromControl(this).WorkingArea.Height)
				p.Y = 0 - _splitMenuStrip.Height;

			_splitMenuStrip.Show(this, p);

			if (!_first) return;

			_first = false;
			_splitMenuStrip.Close();
			ShowContextMenuStrip();
		}

		private void SplitMenuStrip_Opening(object sender, CancelEventArgs e)
		{
			isSplitMenuVisible = true;
		}

		private void SplitMenuStrip_Closing(object sender, ToolStripDropDownClosingEventArgs e)
		{
			isSplitMenuVisible = false;

			SetButtonDrawState();

			if (e.CloseReason == ToolStripDropDownCloseReason.AppClicked)
				_skipNextOpen = (_dropDownRectangle.Contains(PointToClient(Cursor.Position)) || IsDropDown) &&
				               MouseButtons == MouseButtons.Left;
		}

		protected override void WndProc(ref Message m)
		{
			//0x0212 == WM_EXITMENULOOP
			if (m.Msg == 0x0212)
			{
				//this message is only sent when a ContextMenu is closed (not a ContextMenuStrip)
				isSplitMenuVisible = false;
				SetButtonDrawState();
			}

			base.WndProc(ref m);
		}

		private void SetButtonDrawState()
		{
			if (Bounds.Contains(Parent.PointToClient(Cursor.Position)))
			{
				State = PushButtonState.Hot;
			}
			else if (Focused)
			{
				State = PushButtonState.Default;
			}
			else if (!Enabled)
			{
				State = PushButtonState.Disabled;
			}
			else
			{
				State = PushButtonState.Normal;
			}
		}

		#region Button Layout Calculations

		//The following layout functions were taken from Mono's Windows.Forms 
		//implementation, specifically "ThemeWin32Classic.cs", 
		//then modified to fit the context of this splitButton

		private void CalculateButtonTextAndImageLayout(ref Rectangle content_rect, out Rectangle textRectangle,
			out Rectangle imageRectangle)
		{
			Size text_size = TextRenderer.MeasureText(Text, Font, content_rect.Size, _textFormatFlags);
			Size image_size = Image == null ? Size.Empty : Image.Size;

			textRectangle = Rectangle.Empty;
			imageRectangle = Rectangle.Empty;

			switch (TextImageRelation)
			{
				case TextImageRelation.Overlay:
					// Overlay is easy, text always goes here
					textRectangle = OverlayObjectRect(ref content_rect, ref text_size, TextAlign);
						// Rectangle.Inflate(content_rect, -4, -4);

					//Offset on Windows 98 style when button is pressed
					if (_state == PushButtonState.Pressed && !Application.RenderWithVisualStyles)
						textRectangle.Offset(1, 1);

					// Image is dependent on ImageAlign
					if (Image != null)
						imageRectangle = OverlayObjectRect(ref content_rect, ref image_size, ImageAlign);

					break;
				case TextImageRelation.ImageAboveText:
					content_rect.Inflate(-4, -4);
					LayoutTextAboveOrBelowImage(content_rect, false, text_size, image_size, out textRectangle, out imageRectangle);
					break;
				case TextImageRelation.TextAboveImage:
					content_rect.Inflate(-4, -4);
					LayoutTextAboveOrBelowImage(content_rect, true, text_size, image_size, out textRectangle, out imageRectangle);
					break;
				case TextImageRelation.ImageBeforeText:
					content_rect.Inflate(-4, -4);
					LayoutTextBeforeOrAfterImage(content_rect, false, text_size, image_size, out textRectangle, out imageRectangle);
					break;
				case TextImageRelation.TextBeforeImage:
					content_rect.Inflate(-4, -4);
					LayoutTextBeforeOrAfterImage(content_rect, true, text_size, image_size, out textRectangle, out imageRectangle);
					break;
			}
		}

		private static Rectangle OverlayObjectRect(ref Rectangle container, ref Size sizeOfObject,
			ContentAlignment alignment)
		{
			int x, y;

			switch (alignment)
			{
				case ContentAlignment.TopLeft:
					x = 4;
					y = 4;
					break;
				case ContentAlignment.TopCenter:
					x = (container.Width - sizeOfObject.Width)/2;
					y = 4;
					break;
				case ContentAlignment.TopRight:
					x = container.Width - sizeOfObject.Width - 4;
					y = 4;
					break;
				case ContentAlignment.MiddleLeft:
					x = 4;
					y = (container.Height - sizeOfObject.Height)/2;
					break;
				case ContentAlignment.MiddleCenter:
					x = (container.Width - sizeOfObject.Width)/2;
					y = (container.Height - sizeOfObject.Height)/2;
					break;
				case ContentAlignment.MiddleRight:
					x = container.Width - sizeOfObject.Width - 4;
					y = (container.Height - sizeOfObject.Height)/2;
					break;
				case ContentAlignment.BottomLeft:
					x = 4;
					y = container.Height - sizeOfObject.Height - 4;
					break;
				case ContentAlignment.BottomCenter:
					x = (container.Width - sizeOfObject.Width)/2;
					y = container.Height - sizeOfObject.Height - 4;
					break;
				case ContentAlignment.BottomRight:
					x = container.Width - sizeOfObject.Width - 4;
					y = container.Height - sizeOfObject.Height - 4;
					break;
				default:
					x = 4;
					y = 4;
					break;
			}

			return new Rectangle(x, y, sizeOfObject.Width, sizeOfObject.Height);
		}

		private void LayoutTextBeforeOrAfterImage(Rectangle totalArea, bool textFirst, Size textSize, Size imageSize,
			out Rectangle textRect, out Rectangle imageRect)
		{
			int element_spacing = 0; // Spacing between the Text and the Image
			int total_width = textSize.Width + element_spacing + imageSize.Width;

			if (!textFirst)
				element_spacing += 2;

			// If the text is too big, chop it down to the size we have available to it
			if (total_width > totalArea.Width)
			{
				textSize.Width = totalArea.Width - element_spacing - imageSize.Width;
				total_width = totalArea.Width;
			}

			int excess_width = totalArea.Width - total_width;
			int offset = 0;

			Rectangle final_text_rect;
			Rectangle final_image_rect;

			HorizontalAlignment h_text = GetHorizontalAlignment(TextAlign);
			HorizontalAlignment h_image = GetHorizontalAlignment(ImageAlign);

			if (h_image == HorizontalAlignment.Left)
				offset = 0;
			else if (h_image == HorizontalAlignment.Right && h_text == HorizontalAlignment.Right)
				offset = excess_width;
			else if (h_image == HorizontalAlignment.Center &&
			         (h_text == HorizontalAlignment.Left || h_text == HorizontalAlignment.Center))
				offset += excess_width/3;
			else
				offset += 2*(excess_width/3);

			if (textFirst)
			{
				final_text_rect = new Rectangle(totalArea.Left + offset, AlignInRectangle(totalArea, textSize, TextAlign).Top,
					textSize.Width, textSize.Height);
				final_image_rect = new Rectangle(final_text_rect.Right + element_spacing,
					AlignInRectangle(totalArea, imageSize, ImageAlign).Top, imageSize.Width, imageSize.Height);
			}
			else
			{
				final_image_rect = new Rectangle(totalArea.Left + offset, AlignInRectangle(totalArea, imageSize, ImageAlign).Top,
					imageSize.Width, imageSize.Height);
				final_text_rect = new Rectangle(final_image_rect.Right + element_spacing,
					AlignInRectangle(totalArea, textSize, TextAlign).Top, textSize.Width, textSize.Height);
			}

			textRect = final_text_rect;
			imageRect = final_image_rect;
		}

		private void LayoutTextAboveOrBelowImage(Rectangle totalArea, bool textFirst, Size textSize, Size imageSize,
			out Rectangle textRect, out Rectangle imageRect)
		{
			int element_spacing = 0; // Spacing between the Text and the Image
			int total_height = textSize.Height + element_spacing + imageSize.Height;

			if (textFirst)
				element_spacing += 2;

			if (textSize.Width > totalArea.Width)
				textSize.Width = totalArea.Width;

			// If the there isn't enough room and we're text first, cut out the image
			if (total_height > totalArea.Height && textFirst)
			{
				imageSize = Size.Empty;
				total_height = totalArea.Height;
			}

			int excess_height = totalArea.Height - total_height;
			int offset = 0;

			Rectangle final_text_rect;
			Rectangle final_image_rect;

			VerticalAlignment v_text = GetVerticalAlignment(TextAlign);
			VerticalAlignment v_image = GetVerticalAlignment(ImageAlign);

			if (v_image == VerticalAlignment.Top)
				offset = 0;
			else if (v_image == VerticalAlignment.Bottom && v_text == VerticalAlignment.Bottom)
				offset = excess_height;
			else if (v_image == VerticalAlignment.Center && (v_text == VerticalAlignment.Top || v_text == VerticalAlignment.Center))
				offset += excess_height/3;
			else
				offset += 2*(excess_height/3);

			if (textFirst)
			{
				final_text_rect = new Rectangle(AlignInRectangle(totalArea, textSize, TextAlign).Left, totalArea.Top + offset,
					textSize.Width, textSize.Height);
				final_image_rect = new Rectangle(AlignInRectangle(totalArea, imageSize, ImageAlign).Left,
					final_text_rect.Bottom + element_spacing, imageSize.Width, imageSize.Height);
			}
			else
			{
				final_image_rect = new Rectangle(AlignInRectangle(totalArea, imageSize, ImageAlign).Left, totalArea.Top + offset,
					imageSize.Width, imageSize.Height);
				final_text_rect = new Rectangle(AlignInRectangle(totalArea, textSize, TextAlign).Left,
					final_image_rect.Bottom + element_spacing, textSize.Width, textSize.Height);

				if (final_text_rect.Bottom > totalArea.Bottom)
					final_text_rect.Y = totalArea.Top;
			}

			textRect = final_text_rect;
			imageRect = final_image_rect;
		}

		private static HorizontalAlignment GetHorizontalAlignment(ContentAlignment align)
		{
			switch (align)
			{
				case ContentAlignment.BottomLeft:
				case ContentAlignment.MiddleLeft:
				case ContentAlignment.TopLeft:
					return HorizontalAlignment.Left;
				case ContentAlignment.BottomCenter:
				case ContentAlignment.MiddleCenter:
				case ContentAlignment.TopCenter:
					return HorizontalAlignment.Center;
				case ContentAlignment.BottomRight:
				case ContentAlignment.MiddleRight:
				case ContentAlignment.TopRight:
					return HorizontalAlignment.Right;
			}

			return HorizontalAlignment.Left;
		}

		private static VerticalAlignment GetVerticalAlignment(ContentAlignment align)
		{
			switch (align)
			{
				case ContentAlignment.TopLeft:
				case ContentAlignment.TopCenter:
				case ContentAlignment.TopRight:
					return VerticalAlignment.Top;
				case ContentAlignment.MiddleLeft:
				case ContentAlignment.MiddleCenter:
				case ContentAlignment.MiddleRight:
					return VerticalAlignment.Center;
				case ContentAlignment.BottomLeft:
				case ContentAlignment.BottomCenter:
				case ContentAlignment.BottomRight:
					return VerticalAlignment.Bottom;
			}

			return VerticalAlignment.Top;
		}

		private static Rectangle AlignInRectangle(Rectangle outer, Size inner, ContentAlignment align)
		{
			int x = 0;
			int y = 0;

			switch (align)
			{
				case ContentAlignment.TopLeft:
				case ContentAlignment.MiddleLeft:
				case ContentAlignment.BottomLeft:
					x = outer.X;
					break;
				case ContentAlignment.TopCenter:
				case ContentAlignment.MiddleCenter:
				case ContentAlignment.BottomCenter:
					x = Math.Max(outer.X + ((outer.Width - inner.Width)/2), outer.Left);
					break;
				case ContentAlignment.TopRight:
				case ContentAlignment.MiddleRight:
				case ContentAlignment.BottomRight:
					x = outer.Right - inner.Width;
					break;
			}
			switch (align)
			{
				case ContentAlignment.TopRight:
				case ContentAlignment.TopLeft:
				case ContentAlignment.TopCenter:
					y = outer.Y;
					break;
				case ContentAlignment.MiddleRight:
				case ContentAlignment.MiddleLeft:
				case ContentAlignment.MiddleCenter:
					y = outer.Y + (outer.Height - inner.Height)/2;
					break;
				case ContentAlignment.BottomLeft:
				case ContentAlignment.BottomRight:
				case ContentAlignment.BottomCenter:
					y = outer.Bottom - inner.Height;
					break;
			}

			return new Rectangle(x, y, Math.Min(inner.Width, outer.Width), Math.Min(inner.Height, outer.Height));
		}

		#endregion Button Layout Calculations
	}
}