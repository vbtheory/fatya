﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Threading;
using System.Windows.Forms;
using Fatya.Properties;

namespace Fatya.Controls
{
// ReSharper disable once RedundantNameQualifier
	[System.ComponentModel.DesignerCategory("DesignCategory.Code")]
	[DefaultEvent("SelectedIndexChanged"), DefaultProperty("Items")]
	class Header : Control
	{

		private BindingList<HeaderItem> _items;

		private int _selectedIndex = -1;
		private int entered = -1;
		private int down = -1;
		private Bitmap noise;

		private Bitmap easter;

		public void RunEaster()
		{
			Thread t = new Thread(x =>
			{
				if(easter != null) return;
				Stopwatch s = new Stopwatch();
				s.Start();

				List<Bitmap> b = new List<Bitmap>();

				for (int i = 0; i < 20; i++)
					b.Add(GenerateNoise(150));
				
				while (s.ElapsedMilliseconds<10000)
				{
					foreach (Bitmap bmp in b)
					{
						easter = bmp;
						Invalidate();
					}
				}
				easter = null;
				Invalidate();
			});

			t.IsBackground = true;
			t.Start();
		}

		public Header()
		{
			noise = GenerateNoise();
			SetStyle(ControlStyles.DoubleBuffer| ControlStyles.ResizeRedraw, true); //double buffered to avoid flicker
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			Rectangle bounds = new Rectangle(-1, -1, Width+1, Height+1);

			//set quality settings
			e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
			e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

			Color back = Color.FromArgb(51, 51, 51);
			Color fore = Color.FromArgb(153, 153, 153);

			LinearGradientBrush l = new LinearGradientBrush(new Point(0, 0), new Point(0, Height), back, Color.FromArgb(75, 75, 75));

			e.Graphics.FillRectangle(l, bounds);

			if (!DesignMode) 
				using (TextureBrush brush = new TextureBrush(easter??noise, WrapMode.Tile))
					e.Graphics.FillRectangle(brush, bounds);

			for (int i = 0; i < Items.Count; i++)//iterate through all header items
			{
				HeaderItem h = Items[i];
				Rectangle temp = ItemRectangle(i); // this is the rectangle where it will be drawn
				SizeF s = e.Graphics.MeasureString(h.Text, Font);
				RectangleF text = new RectangleF(temp.X + (temp.Width - s.Width)/2F, temp.Y + (temp.Height + s.Height)/2, s.Width, s.Height);
				RectangleF img = new RectangleF(temp.X + (temp.Width - 40) / 2, (temp.Height - 40) / 2F - text.Height, 40, 40);
				RectangleF notification = new RectangleF(img.Right - 15, img.Bottom - 15, 15, 15);
				Color fill = fore;
				Bitmap bmp = h.Image;

				if (i == SelectedIndex) fill = Color.FromArgb(29, 215, 178);
				else if (i == down) fill = Color.FromArgb(22, 164, 136);
				else if (i == entered) fill = Color.FromArgb(96, 222, 197);

				if (bmp != null)
				{
					if (i == entered || i == down || i == SelectedIndex || h.Notify) bmp = Fill(h.Image, fill);
					e.Graphics.DrawImage(bmp, img); //draw image
				}

				if (h.Notify) e.Graphics.DrawImage(Resources.Warning, notification);

				if (h.StartGroup && i != 0)
					e.Graphics.DrawLine(new Pen(Color.FromArgb(81, 81, 81)), temp.X, temp.Y, temp.X, temp.Bottom);

				e.Graphics.DrawString(h.Text, Font, new SolidBrush(fill), text);
			}

			e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(138, 37, 0)), 0, Height * 0.95F, Width, Height * .05F);

			base.OnPaint(e);
		}

		public Bitmap GenerateNoise(int op = 50)
		{
			Bitmap finalBmp = new Bitmap(100, 100);
			Random r = new Random();

			for (int x = 0; x < finalBmp.Width; x++)
			{
				for (int y = 0; y < finalBmp.Height; y++)
				{
					int num = r.Next(0, 55);
					finalBmp.SetPixel(x, y, Color.FromArgb(op, num, num, num));
				}
			}

			return finalBmp;
		}

		public Bitmap Fill(Bitmap origin, Color fill)
		{
			Bitmap bmp = new Bitmap(origin);

			for (int x = 0; x < bmp.Width; x++)
			{
				for (int y = 0; y < bmp.Height; y++)
				{
					if (bmp.GetPixel(x, y).A == 0) continue;
					bmp.SetPixel(x, y, Color.FromArgb(bmp.GetPixel(x, y).A, fill));
				}
			}

			return bmp;
		}

		protected override void OnMouseClick(MouseEventArgs e)
		{
			base.OnMouseClick(e);

			if (e.Button != MouseButtons.Left) return;

			for (int i = 0; i < Items.Count; i++) //iterate through all items
			{
				if (!ClickRectangle(i).Contains(e.Location)) continue; // if cursor is not inside the item rectangle do nothing

				if (!Items[i].IsButton) SelectedIndex = i; //if the item is not a button, change index
				else OnButtonClicked(i, Items[i]); // else trigger button clicked

				down = -1;
				entered = i;
				Invalidate();
				return;
			}

			down = -1;
			entered = -1;
			Invalidate();
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);

			for (int i = 0; i < Items.Count; i++)
			{
				if (down != -1 || !ClickRectangle(i).Contains(e.Location)) continue;

				entered = i;
				Invalidate();
				return;
			}

			entered = -1;
			Invalidate();
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.OnMouseDown(e);

			if (e.Button != MouseButtons.Left) return;

			for (int i = 0; i < Items.Count; i++)
			{
				if (!ClickRectangle(i).Contains(e.Location)) continue;
				down = i;
				entered = -1;
				Invalidate();
				return;
			}
			
			down = -1;
			entered = -1;
			Invalidate();
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			down = entered = -1;
			Invalidate();
			base.OnMouseUp(e);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			down = entered = -1;
			Invalidate();
			base.OnMouseLeave(e);
		}

		private Rectangle ItemRectangle(int i)
		{
			int width = (Width - 20) / Items.Count;

			//calculate the item rectangle out of previous variables
			return new Rectangle(10 + (Width - width * Items.Count) / 2 + width * i, 0, width, Height);
		}

		private RectangleF ClickRectangle(int i)
		{
			Rectangle temp = ItemRectangle(i);
			SizeF text = CreateGraphics().MeasureString(Items[i].Text, Font);
			SizeF hitBox = new SizeF(Math.Max(text.Width, 40) + 40, Height*0.9F);

			return new RectangleF(temp.X + (temp.Width-hitBox.Width)/2F, temp.Y, hitBox.Width, hitBox.Height);
		}

		[Browsable(false), Description("The index of the current selected item")]
		public int SelectedIndex
		{
			get { return _selectedIndex; }
			set
			{
				if(_selectedIndex == value || value <-1 || value >Items.Count-1 || Items[value].IsButton) return;

				_selectedIndex = value;
				OnSelectedIndexChanged(value, SelectedIndex == -1?null:Items[value]);
				Invalidate();
			}
		}

		[Browsable(false), Description("The item that is currently selected")]
		public HeaderItem SelectedItem
		{
			get
			{
				if (SelectedIndex < 0 || SelectedIndex > Items.Count - 1) return null;
				return Items[SelectedIndex];
			}
			set
			{
				int i = -1;
				
				for (int index = 0; index < Items.Count; index++)
				{
					HeaderItem item = Items[index];

					if (item != value) continue;

					i = index;
					break;
				}
				
				if (i != -1) SelectedIndex = i;
			}
		}
		
		[Description("Header items contained by this header"), DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public BindingList<HeaderItem> Items
		{
			get
			{
				if (_items != null) return _items;

				_items = new BindingList<HeaderItem>();
				_items.ListChanged += (sender, args) =>
				{
					if (SelectedIndex == -1 && Items.Count > 0) SelectedIndex = 0;
					else if (SelectedIndex > Items.Count) SelectedIndex = Items.Count - 1;
					Invalidate();
				};
				return _items;
			}
		}

		[Description("Triggered when an item that is not a button is clicked")]
		public event EventHandler<HeaderEventArgs> SelectedIndexChanged;

		protected virtual void OnSelectedIndexChanged(int i, HeaderItem h)
		{
			EventHandler<HeaderEventArgs> handler = SelectedIndexChanged;
			if (handler != null) handler(this, new HeaderEventArgs(i, h));
		}
		[Description("Trigged when a button is clicked")]
		public event EventHandler<HeaderEventArgs> ButtonClicked;

		protected virtual void OnButtonClicked(int i, HeaderItem h)
		{
			EventHandler<HeaderEventArgs> handler = ButtonClicked;
			if (handler != null) handler(this, new HeaderEventArgs(i, h));
		}

		[Browsable(false), Description("Returns the text of the current item")]
		public override string Text {
			get { return SelectedIndex == -1 ? "" : Items[SelectedIndex].Text; }}
	}

	[Serializable, DesignTimeVisible(false)]
	public class HeaderItem : Component, INotifyPropertyChanged
	{
		private string _text = "";
		private Bitmap _image;
		private bool _startGroup;
		private bool _isButton;
		private bool _notify;
		/// <summary>
		/// Gets or sets the image
		/// </summary>
		public Bitmap Image
		{
			get { return _image; }
			set
			{
				_image = value;
				OnPropertyChanged("Image");
			}
		}
		/// <summary>
		/// Gets or sets the item text
		/// </summary>
		[DefaultValue("")]
		public string Text
		{
			get { return _text; }
			set
			{
				_text = value;
				OnPropertyChanged("Text");
			}
		}
		/// <summary>
		/// If true, a separator will be set before the item
		/// </summary>
		[DefaultValue(false)]
		public bool StartGroup
		{
			get { return _startGroup; }
			set
			{
				_startGroup = value;
				OnPropertyChanged("StartGroup");
			}
		}
		/// <summary>
		/// If true, clicking the item will trigger ButtonClicked and won't change the index
		/// </summary>
		[DefaultValue(false)]
		public bool IsButton
		{
			get { return _isButton; }
			set
			{
				_isButton = value;
				OnPropertyChanged("IsButton");
			}
		}
		/// <summary>
		/// Gets or sets if notifications are shown
		/// </summary>
		[Browsable(false), DefaultValue(false)]
		public bool Notify
		{
			get { return _notify; }
			set
			{
				_notify = value;
				OnPropertyChanged("Notify");
			}
		}
		[field:NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}
	}

	public class HeaderEventArgs : EventArgs
	{

		private int _selectedIndex;
		private HeaderItem _item;

		public HeaderEventArgs(int selectedIndex, HeaderItem item)
		{
			_selectedIndex = selectedIndex;
			_item = item;
		}
		/// <summary>
		/// Gets the item that triggered the event
		/// </summary>
		public HeaderItem Item
		{
			get { return _item; }
		}
		/// <summary>
		/// Get the index of the item that triggered the event
		/// </summary>
		public int SelectedIndex
		{
			get { return _selectedIndex; }
		}

	}

}
