﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Media;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Fatya.Classes;
using Timer = System.Windows.Forms.Timer;

namespace Fatya.Controls
{

	// ReSharper disable once RedundantNameQualifier
	[System.ComponentModel.DesignerCategory("Code")]
	[DefaultEvent("TextChanged"), DefaultProperty("Text")]
	public class TxtBox : Control
	{

		#region Fields

		private bool _raiseChanged;
		private bool _caretVisible;
		private int _textPad;
		private int _lineIndex;
		private int _wordSelectionStart;
		private int _wordSelectionEnd;
		private TxtBoxSelectionMode _selectionMode;
		private ContextMenuStrip _cms = new ContextMenuStrip();
		private List<Command> _undoList = new List<Command>();
		private List<Command> _redoList = new List<Command>();
		private AutoCompleteDropDown _drop = new AutoCompleteDropDown();
		private Timer _blinker = new Timer();
		private Timer _delayed = new Timer();

		private char _passwordChar;
		private bool _hideSelection;
		private bool _readOnly;
		private bool _showButtons = true;
		private bool _enabledRightClickMenu = true;
		private int _caretPosition;
		private int _maxLength = 32767;
		private int _selectionHead = -1;
		private string _placeholder;
		private Color _selectionForeColor = Color.Black;
		private Color _disabledText = SystemColors.GrayText;
		private Color _inactiveSelection = SystemColors.InactiveCaption;
		private Color _caretColor = Color.Black;
		private Color _borderColor = Color.Black;
		private Color _selectionColor = SystemColors.Highlight;
		private Color _placeholderColor = Color.FromArgb(180, Color.Gray);
		private Color _focusedPlaceholderColor = Color.Gray;
		private Color _buttonBackColor = Color.Transparent;
		private Color _buttonBorderColor = Color.Transparent;
		private StringCollection _autoCompleteCustomSource;
		private BindingList<TxtButton> _buttons;
		private ButtonStyle _buttonStyle = ButtonStyle.None;
		private AutoCompleteSource _autoCompleteSource = AutoCompleteSource.CustomSource;
		private AutoCompleteMode _autoCompleteMode = AutoCompleteMode.SuggestAppend;
		private ButtonBorderStyle _borderLineStyle = ButtonBorderStyle.Solid;
		private TxtBoxBorderStyle _borderStyle = TxtBoxBorderStyle.FixedSingle;
		private bool _multiLine;

		#endregion

		public TxtBox()
		{
			SetStyle(ControlStyles.DoubleBuffer | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor, true);
			Size = new Size(150, (int)(CreateGraphics().MeasureString(" ", Font).Height * 1.5));
			Cursor = Cursors.IBeam;
			Padding = new Padding(5, 0, 5, 0);
			EnabledRightClickMenu = true;
			BackColor = SystemColors.Window;

			Invalidate();

			if (DesignMode) return;

			_cms.Items.Add("Undo", null, (sender, args) => OnKeyDown(new KeyEventArgs(Keys.Z | Keys.Control)));
			_cms.Items.Add("Redo", null, (sender, args) => OnKeyDown(new KeyEventArgs(Keys.Z | Keys.Control | Keys.Shift)));
			_cms.Items.Add(new ToolStripSeparator());
			_cms.Items.Add("Copy", null, (sender, args) => OnKeyDown(new KeyEventArgs(Keys.C | Keys.Control)));
			_cms.Items.Add("Cut", null, (sender, args) => OnKeyDown(new KeyEventArgs(Keys.X | Keys.Control)));
			_cms.Items.Add("Paste", null, (sender, args) => OnKeyDown(new KeyEventArgs(Keys.V | Keys.Control)));
			_cms.Items.Add("Delete", null, (sender, args) => OnKeyDown(new KeyEventArgs(Keys.Back)));
			_cms.Items.Add(new ToolStripSeparator());
			_cms.Items.Add("Select all", null, (sender, args) => OnKeyDown(new KeyEventArgs(Keys.A | Keys.Control)));

			_cms.VisibleChanged += (sender, args) =>
			{
				if (!_cms.Visible) return;

				_cms.Items[0].Enabled = _undoList.Count != 0;
				_cms.Items[1].Visible = _cms.Items[1].Enabled = _redoList.Count != 0;
				_cms.Items[3].Enabled = SelectionHead != -1 && SelectionHead != CaretPosition;
				_cms.Items[4].Enabled = SelectionHead != -1 && SelectionHead != CaretPosition;
				_cms.Items[5].Enabled = Clipboard.ContainsText();
				_cms.Items[6].Enabled = SelectionHead != -1 && SelectionHead != CaretPosition;
				_cms.Items[7].Enabled = Text.Length != 0;
			};
			_cms.Renderer = new WinRenderer();

			_drop.KeyDown += (sender, args) => OnKeyDown(args);
			_drop.KeyPress += (sender, args) => OnKeyPress(args);
			_drop.ItemClicked += (sender, args) =>
			{
				Text = args.ClickedItem.Text;
				CaretPosition = Text.Length;
				if (args.ClickedItem.AccessibleName != "DONOTRAISE") OnAutoCompleteSelected(new EventArgs());
			};

			_blinker.Interval = SystemInformation.CaretBlinkTime;
			_blinker.Tick += (sender, args) =>
			{
				if (Focused) _caretVisible = !CaretVisible;
				else _caretVisible = false;

				Invalidate();
			};
			_blinker.Start();

			_delayed.Interval = TextChangedDelay;
			_delayed.Tick += (sender, args) =>
			{
				if (!_raiseChanged) return;

				_raiseChanged = false;
				OnTextChangedDelayed();
			};
			_delayed.Start();
		}

		#region Properties


		[Description("How the control border looks"), Category("Appearance"), DefaultValue(typeof(TxtBoxBorderStyle), "FixedSingle")]
		public TxtBoxBorderStyle BorderStyle
		{
			get { return _borderStyle; }
			set
			{
				_borderStyle = value;
				OnBorderStyleChanged();
				Invalidate();
			}
		}

		[Description("How some borders will be drawn"), Category("Appearance"), DefaultValue(typeof(ButtonBorderStyle), "Solid")]
		public ButtonBorderStyle BorderLineStyle
		{
			get { return _borderLineStyle; }
			set
			{
				_borderLineStyle = value;
				OnBorderLineStyleChanged();
				Invalidate();
			}
		}

		[Description("If enabled, the selection will be hidden  when it is no longer in focus"), Category("Appearance"), DefaultValue(false)]
		public bool HideSelection
		{
			get { return _hideSelection; }
			set
			{
				_hideSelection = value;
				OnHideSelectionChanged();
				Invalidate();
			}
		}

		[Description("If enabled and a ButtonImage is selected, a button will be drawn inside the control"), Category("Appearance"), DefaultValue(true)]
		public bool ShowButtons
		{
			get { return _showButtons; }
			set
			{
				bool previous = _showButtons && HasButtons;
				_showButtons = value;
				if ((_showButtons && HasButtons) != previous) OnButtonVisibleChanged();
				Invalidate();
			}
		}

		[Description("Chooses how the embedded button's background will be drawn"), Category("Appearance"), DefaultValue(typeof(ButtonStyle), "None")]
		public ButtonStyle ButtonStyle
		{
			get { return _buttonStyle; }
			set
			{
				_buttonStyle = value;//todo: event
				Invalidate();
			}
		}

		[Description("Color of the border"), Category("Colors"), DefaultValue(typeof(Color), "Black")]
		public Color BorderColor
		{
			get { return _borderColor; }
			set
			{
				_borderColor = value;
				OnBorderColorChanged();
				Invalidate();
			}
		}

		[Description("Selects the color of the cursor"), Category("Colors"), DefaultValue(typeof(Color), "Black")]
		public Color CaretColor
		{
			get { return _caretColor; }
			set
			{
				_caretColor = value;
				OnCaretColorChanged();
				Invalidate();
			}
		}

		[Description("Color of the selection"), Category("Colors"), DefaultValue(typeof(Color), "Highlight")]
		public Color SelectionColor
		{
			get { return _selectionColor; }
			set
			{
				_selectionColor = value;
				OnSelectionColorChanged();
				Invalidate();
			}
		}

		[Description("Color of the selection when the control is not on focus"), Category("Colors"), DefaultValue(typeof(Color), "InactiveCaption")]
		public Color InactiveSelection
		{
			get { return _inactiveSelection; }
			set
			{
				_inactiveSelection = value;
				OnInactiveSelectionColorChanged();
				Invalidate();
			}
		}

		[Description("Color of text when disabled"), Category("Colors"), DefaultValue(typeof(Color), "GrayText")]
		public Color DisabledText
		{
			get { return _disabledText; }
			set
			{
				_disabledText = value;
				OnDisabledTextColorChanged();
				Invalidate();
			}
		}

		[Description("Color of highlighted text"), Category("Colors"), DefaultValue(typeof(Color), "White")]
		public Color SelectionForeColor
		{
			get { return _selectionForeColor; }
			set
			{
				_selectionForeColor = value;
				OnSelectionForeColorChanged();
				Invalidate();
			}
		}

		[Description("Color of placeholder text"), Category("Colors"), DefaultValue(typeof(Color), "180, 128, 128, 128")]
		public Color PlaceholderColor
		{
			get { return _placeholderColor; }
			set
			{
				_placeholderColor = value;
				OnPlaceholderColorChanged();
				Invalidate();
			}
		}

		[Description("Color of placeholder text when the control is focused"), Category("Colors"), DefaultValue(typeof(Color), "Gray")]
		public Color FocusedPlaceholderColor
		{
			get { return _focusedPlaceholderColor; }
			set
			{
				_focusedPlaceholderColor = value;
				OnFocusedPlaceholderColorChanged();
				Invalidate();
			}
		}

		[Description("Color of the button if visible"), Category("Colors"), DefaultValue(typeof(Color), "Transparent")]
		public Color ButtonBackColor
		{
			get { return _buttonBackColor; }
			set
			{
				_buttonBackColor = value;
				OnButtonBackColorChanged();
				Invalidate();
			}
		}

		[Description("Color of the button's border"), Category("Colors"), DefaultValue(typeof(Color), "Transparent")]
		public Color ButtonBorderColor
		{
			get { return _buttonBorderColor; }
			set
			{
				_buttonBorderColor = value;
				OnButtonBorderColorChanged();
				Invalidate();
			}
		}

		[Description("Chooses the visible text when the Text property is empty"), Category("Others"), DefaultValue("")]
		public string Placeholder
		{
			get { return _placeholder; }
			set
			{
				_placeholder = value;
				OnPlaceholderChanged();
				Invalidate();
			}
		}
		
		[Browsable(false)]
		public int CaretPosition
		{
			get
			{
				return _caretPosition;
			}
			set
			{
				if (Text.Length < value || value < 0)
				{
					SystemSounds.Beep.Play();
					return;
				}
				if (_selectionMode == TxtBoxSelectionMode.None) SelectionHead = -1;

				LineIndex = 0;
				for (int i = value-1; i > 0; i--)
				{
					if (Text[i] == '\n') LineIndex++;
				}

				_caretPosition = value;
				OnCaretPostitionChanged();

				string visible = VisibleText;

				while (_textPad + MeasureStringWidth(CreateGraphics(), visible == "" ? "" : visible.Substring(0, CaretPosition)) > Width - Padding.Right - ButtonRectangle().Width)
				{
					_textPad -= _selectionMode == TxtBoxSelectionMode.None?(int)(Width * .35):5;
				}
				while (_textPad + MeasureStringWidth(CreateGraphics(), visible == "" ? "" : visible.Substring(0, CaretPosition)) < 0)
				{
					_textPad += _selectionMode == TxtBoxSelectionMode.None ? (int)(Width * .35) : 5;
				}
				if (Focused && _selectionMode == TxtBoxSelectionMode.None) CaretVisible = true;
				Invalidate();
			}
		}

		[Browsable(false)]
		public int SelectionHead
		{
			get { return _selectionHead; }
			set
			{
				if (Text.Length < value || (value < 0 && value != -1)) return;

				_selectionHead = value;
				OnSelectionHeadChanged();
				string visible = VisibleText;

				while (SelectionHead != -1 && _textPad + MeasureStringWidth(CreateGraphics(), visible == "" ? "" : visible.Substring(0, SelectionHead)) > Width - Padding.Horizontal - ButtonRectangle().Width)
				{
					_textPad -= 5;
				}
				while (SelectionHead != -1 && _textPad + MeasureStringWidth(CreateGraphics(), visible == "" ? "" : visible.Substring(0, SelectionHead)) < 0 && _textPad<=-5)
				{
					_textPad += 5;
				}

				Invalidate();
			}
		}

		[Description("Chooses the maximum length of text"), Category("Others"), DefaultValue(32767)]
		public int MaxLength
		{
			get { return _maxLength; }
			set
			{
				_maxLength = value;
				OnMaxLengthChanged();
				if (Text.Length > MaxLength) Text = Text.Remove(MaxLength);
			}
		}

		[Description("Text of the TxtBox"), Category("Others"), DefaultValue("")]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				if (value == Text) return;
				if (value.Length > MaxLength) value = value.Substring(0, MaxLength);

				SelectionHead = -1;
				if (Focused) CaretVisible = true;
				if (CaretPosition > value.Length + 1) CaretPosition = value.Length;
				base.Text = value;

				_raiseChanged = true;
				if (AutoCompleteCustomSource != null && AutoCompleteCustomSource.Count != 0) ShowAutoComplete();

				Invalidate();
			}
		}

		[Description("If not empty, the control will suggest words from this List"), Category("Others"),
		 Editor("System.Windows.Forms.Design.ListControlStringCollectionEditor", typeof(UITypeEditor))]
		public StringCollection AutoCompleteCustomSource
		{
			get { return _autoCompleteCustomSource ?? (_autoCompleteCustomSource = new StringCollection()); }
			set
			{
				_autoCompleteCustomSource = value;
			}
		}

		[Description("Specifies the images for the buttons inside the control"), Category("Others")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public BindingList<TxtButton> Buttons
		{
			get
			{
				if (_buttons != null) return _buttons;

				_buttons = new BindingList<TxtButton>();
				_buttons.ListChanged += (sender, args) => Invalidate();
				return _buttons;
			}
		}

		[Description("Chooses the behavior of auto completion"), Category("Others"), DefaultValue(typeof(AutoCompleteMode), "SuggestAppend")]
		public AutoCompleteMode AutoCompleteMode
		{
			get { return _autoCompleteMode; }
			set
			{
				_autoCompleteMode = value;
				OnAutoCompleteModeChanged();
			}
		}

		[Description("Chooses the source of auto completion"), Category("Others"), DefaultValue(typeof(AutoCompleteSource), "CustomSource")]
		public AutoCompleteSource AutoCompleteSource
		{
			get { return _autoCompleteSource; }
			set
			{
				_autoCompleteSource = value;
				OnAutoCompleteSourceChanged();
			}
		}

		[Description("If enabled, the user will only be able to select and copy but not to edit text"), Category("Others"), DefaultValue(false)]
		public bool ReadOnly
		{
			get { return _readOnly; }
			set
			{
				_readOnly = value;
				OnReadOnlyChanged();
				Invalidate();
			}
		}

		[Description("If not empty, the control will only show this instead of any character to hide the content"), Category("Others"), DefaultValue('\0')]
		public char PasswordChar
		{
			get { return _passwordChar; }
			set
			{
				_passwordChar = value;
				OnPasswordCharChanged();
				Invalidate();
			}
		}

		[Description("If enabled, text will be replaced by the system's password char"), Category("Others"), DefaultValue(false)]
		public bool UseSystemPasswordChar { get; set; }

		[Description("If enabled, right clicking will show the default menu will show up"), Category("Others"), DefaultValue(true)]
		public bool EnabledRightClickMenu
		{
			get { return _enabledRightClickMenu; }
			set { _enabledRightClickMenu = value; }
		}

		[Description("Chooses the duration between the TextChanged and TextChangedDelayed events"), Category("Others"), DefaultValue(250)]
		public int TextChangedDelay
		{
			get { return _delayed.Interval; }
			set
			{
				if(value==0) return;
				_delayed.Interval = value;
			}
		}

		[Description("If the tab key inserts 4 spaces"), Category("Others"), DefaultValue(false)]
		public bool AcceptsTab { get; set; }

		[Description("If the txtbox could contain multiple lines"), Category("Others"), DefaultValue(false)]
		public bool MultiLine
		{
			get { return _multiLine; }
			set
			{
				if(MultiLine== value) return;

				if (!value) Text = Text.Replace("\n", "");
				_multiLine = value;
			}
		}

		[Description("Gets or sets if the autocomplete custom source should allow duplicates")]
		public bool AllowAutoCompleteDuplicates { get; set; }

		[Browsable(false)]
		public string[] Lines
		{
			get { return Text.Split('\n'); }
		}

		[Browsable(false)]
		private int LineIndex
		{
			get { return _lineIndex; }
			set { _lineIndex = value; }
		}

		private string VisibleText
		{
			get
			{
				if (!OnPass) return Text;
				char c = UseSystemPasswordChar ? '•' : PasswordChar;

				string result = "";
				for (int index = 0; index < Text.Length; index++)
					result += c;

				return result;
			}
		}

		private bool OnPass
		{
			get { return PasswordChar != '\0' || UseSystemPasswordChar; }
		}

		private bool CaretVisible
		{
			get { return _caretVisible; }
			set
			{
				_blinker.Stop();
				_blinker.Start();
				_caretVisible = value;
				Invalidate();
			}
		}

		private bool HasButtons
		{
			get { return Buttons.Count(x=>x!=null && x.Image!=null) != 0; }
		}

		#endregion

		#region Event overrides

		protected override void OnKeyDown(KeyEventArgs e)
		{
			int selectionStart = Math.Min(SelectionHead, CaretPosition);
			int selectionLength = Math.Max(SelectionHead, CaretPosition) - selectionStart;

			switch (e.KeyCode)
			{
				case Keys.Right:
					e.Handled = e.SuppressKeyPress = true;
					if (e.Control && e.Shift && !OnPass)
					{
						if (SelectionHead == -1) SelectionHead = CaretPosition;
						SelectionHead = FindBreakAfter(SelectionHead, true);
					}
					else if (e.Control && !OnPass) CaretPosition = FindBreakAfter(CaretPosition, true);
					else if (e.Shift)
					{
						if (SelectionHead == -1) SelectionHead = CaretPosition;
						SelectionHead++;
					}
					else
					{
						if (SelectionHead != -1) CaretPosition = Math.Max(SelectionHead, CaretPosition);
						if (CaretPosition != Text.Length) CaretPosition++;
						SelectionHead = -1;
					}
					break;
				case Keys.Left:
					e.Handled = e.SuppressKeyPress = true;
					if (e.Control && e.Shift && !OnPass)
					{
						if (SelectionHead == 0) break;
						SelectionHead = CaretPosition;
						SelectionHead = FindBreakBefore(SelectionHead, true);
					}
					else if (e.Control && !OnPass) CaretPosition = FindBreakBefore(CaretPosition, true);
					else if (e.Shift)
					{
						if (SelectionHead == 0) break;
						if (SelectionHead == -1) SelectionHead = CaretPosition;
						SelectionHead--;
					}
					else
					{
						if (SelectionHead != -1) CaretPosition = Math.Min(SelectionHead, CaretPosition);
						CaretPosition--;

						SelectionHead = -1;
					}
					break;
				case Keys.PageUp:
				case Keys.Up:
					e.Handled = e.SuppressKeyPress = true;
					if (MultiLine)
					{
						//todo
					}
					else
					{
						if (e.Shift)
						{
							if (SelectionHead == -1) SelectionHead = CaretPosition;
							SelectionHead = 0;
						}
						else CaretPosition = 0;
					}
					break;
				case Keys.PageDown:
				case Keys.Down:
					e.Handled = e.SuppressKeyPress = true;
					if (MultiLine)
					{
						//todo
					}
					else
					{
						if (e.Shift)
						{
							if (SelectionHead == -1) SelectionHead = CaretPosition;
							SelectionHead = Text.Length;
						}
						else CaretPosition = Text.Length;
					}
					break;
				case Keys.Delete:
					if (CaretPosition == Text.Length || ReadOnly)
					{
						SystemSounds.Beep.Play();
						break;
					}

					AddUndoEntry("Delete");
					if (SelectionHead != -1) RemoveSelection();
					else if (e.Control)
					{
						int head = FindBreakAfter(CaretPosition, true);
						Text = Text.Remove(CaretPosition, head - CaretPosition);
					}
					else Text = Text.Remove(CaretPosition, 1);
					break;
				case Keys.Back:
					e.Handled = e.SuppressKeyPress = true;
					if ((CaretPosition == 0 && SelectionHead == -1) || ReadOnly)
					{
						SystemSounds.Beep.Play();
						break;
					}
					AddUndoEntry("Backspace");
					if (SelectionHead != -1) RemoveSelection();
					else if (e.Control)
					{
						int head = FindBreakBefore(CaretPosition, true);
						Text = Text.Remove(head, CaretPosition - head);
						CaretPosition = head;
					}
					else
					{
						Text = Text.Remove(CaretPosition - 1, 1);
						CaretPosition--;
						e.Handled = e.SuppressKeyPress = true;
					}
					break;
				case Keys.X:
					if (!e.Control) break;

					if (ReadOnly || OnPass || SelectionHead == CaretPosition || SelectionHead == -1)
					{
						SystemSounds.Beep.Play();
						break;
					}

					AddUndoEntry("Cut");
					Clipboard.SetText(Text.Substring(selectionStart, selectionLength));
					RemoveSelection();
					break;
				case Keys.V:
					if (!e.Control || !Clipboard.ContainsText() || ReadOnly) break;

					AddUndoEntry("Paste");
					if (e.Control && SelectionHead != CaretPosition && SelectionHead != -1) RemoveSelection();
					Text = Text.Insert(CaretPosition, Clipboard.GetText());
					CaretPosition += Clipboard.GetText().Length;
					break;
				case Keys.C:
					if (e.Control && SelectionHead != CaretPosition && SelectionHead != -1 && !OnPass)
						Clipboard.SetText(Text.Substring(selectionStart, selectionLength));
					break;
				case Keys.A:
					if (!e.Control || Text.Length <= 0) break;

					CaretPosition = 0;
					SelectionHead = Text.Length;
					break;
				case Keys.Z:
					if (e.Control && e.Shift && _redoList.Count != 0)
					{
						SelectionHead = -1;

						Command c = _redoList[0];
						_redoList.RemoveAt(0);
						_undoList.Add(new Command(Text, CaretPosition, SelectionHead, c.Name));
						OnUndoCommandsChanged();

						Text = c.Text;
						CaretPosition = c.CaretPosition;
						SelectionHead = c.SelectionHead;
						OnRedo();
					}
					else if (e.Control && _undoList.Count != 0)
					{
						SelectionHead = -1;

						Command c = _undoList[_undoList.Count - 1];
						_undoList.RemoveAt(_undoList.Count - 1);
						_redoList.Insert(0, new Command(Text, CaretPosition, SelectionHead, c.Name));
						OnRedoCommandsChanged();

						Text = c.Text;
						CaretPosition = c.CaretPosition;
						SelectionHead = c.SelectionHead;
						OnUndo();
					}
					break;
			}
		}

		protected override void OnKeyPress(KeyPressEventArgs e)
		{
			if (ModifierKeys == Keys.Control || ModifierKeys == Keys.Alt || e.KeyChar == (char)27 || (!MultiLine && e.KeyChar == (char)13) || (e.KeyChar == '\t' && !AcceptsTab) || ReadOnly) return;

			AddUndoEntry("KeyPress");

			if (SelectionHead != CaretPosition && SelectionHead != -1) RemoveSelection();

			string temp = Text;

			switch (e.KeyChar)
			{
				case (char)13:
					Text = Text.Insert(CaretPosition, "\n");
					break;
				case '\t':
					Text = Text.Insert(CaretPosition, "    ");
					break;
				default:
					Text = Text.Insert(CaretPosition,e.KeyChar.ToString());
					break;
			}
			
			if(Text!=temp) CaretPosition++;
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			string visible = VisibleText;
			float lineHeight = e.Graphics.MeasureString(" ", Font).Height;
			float textY = MultiLine?Padding.Top:(Height - lineHeight) / 2;
			StringFormat format = StringFormat.GenericDefault;
			Rectangle layoutRectangle = LayoutRectangle();

			format.FormatFlags |= StringFormatFlags.MeasureTrailingSpaces | StringFormatFlags.NoClip;
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;

			e.Graphics.Clear(DesignMode?Color.FromArgb(255, BackColor):BackColor);
			
			if (ReadOnly || !Enabled) e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(150, SystemColors.Control)), e.ClipRectangle);

			DrawCaret(e.Graphics, CaretColor);

			GraphicsPath g = new GraphicsPath();
			g.AddString(visible, Font.FontFamily, (int)Font.Style, e.Graphics.DpiY * Font.Size / 72, layoutRectangle, format);

			Bitmap bmp = new Bitmap((int)MeasureStringWidth(e.Graphics, visible) + 5, (int)lineHeight + 5);
			Graphics gfx = Graphics.FromImage(bmp);
			gfx.Clear(Enabled ? ForeColor : DisabledText);
			DrawCaret(gfx, SelectionForeColor);
			if (SelectionHead != -1 && SelectionHead != CaretPosition && (Focused || !HideSelection))
			{
				SolidBrush b = new SolidBrush(!HideSelection && !Focused ? InactiveSelection : SelectionColor);
				int start = Math.Min(SelectionHead, CaretPosition);
				int end = Math.Max(SelectionHead, CaretPosition);
				List<RectangleF> selections = new List<RectangleF>();
				int index = 0;
				int lineIndex = 0;
				foreach (string line in Lines)
				{
					int lineStart = FindLineStart(index);
					int lineEnd = FindLineEnd(index);

					float realStart;
					float realEnd;

					if (index < start && start < lineEnd)
						realStart = Padding.Left + _textPad + MeasureStringWidth(e.Graphics, visible.Substring(lineStart, start));
					else if 
						(start <= lineStart) realStart = Padding.Left + _textPad;
					else 
						continue;

					if (index < end && end < lineEnd)
						realEnd = Padding.Left + _textPad + MeasureStringWidth(e.Graphics, visible.Substring(lineStart, end-lineStart));
					else if (end >= lineEnd)
						realEnd = Padding.Left + _textPad + MeasureStringWidth(e.Graphics, visible.Substring(lineStart, lineEnd-lineStart));
					else continue;

					selections.Add(new RectangleF(realStart, textY + lineIndex*lineHeight - 2, realEnd - realStart, lineHeight + 4));//todo:fix Y

					index += line.Length;
					lineIndex++;
				}

				if (selections.Count != 0)
				{
					e.Graphics.FillRectangles(b, selections.ToArray());
					gfx.FillRectangles(new SolidBrush(SelectionForeColor), selections.ToArray());
				}

				b.Dispose();
			}

			TextureBrush t = new TextureBrush(bmp);
			e.Graphics.FillPath(t, g);

			t.Dispose();
			g.Dispose();
			gfx.Dispose();
			bmp.Dispose();

			if (Text == "") e.Graphics.DrawString(Placeholder, new Font(Font.FontFamily, Font.Size, FontStyle.Italic), new SolidBrush(Focused ? FocusedPlaceholderColor : PlaceholderColor), layoutRectangle, format);

			if (ShowButtons && HasButtons)
			{
				Rectangle buttons = ButtonRectangle();

				switch (_buttonStyle)
				{
					case ButtonStyle.Minimal:
						for (int i = 0; i < Buttons.Count; i++)
						{
							if (Buttons[i] == null || Buttons[i].Image == null) continue;
							Rectangle bRec = ButtonRectangle(i);
							Rectangle expanded = new Rectangle(bRec.X - 2, bRec.Y - 2, bRec.Width + 4, bRec.Height + 4);
							e.Graphics.FillRectangle(new SolidBrush(ButtonBackColor), expanded);
							ControlPaint.DrawBorder(e.Graphics, expanded, ButtonBorderColor, ButtonBorderStyle.Solid);
						}
						break;
					case ButtonStyle.Button:
						for (int i = 0; i < Buttons.Count; i++)
						{
							if (Buttons[i] == null || Buttons[i].Image == null) continue;
							Rectangle bRec = ButtonRectangle(i);
							Rectangle expanded = new Rectangle(bRec.X-2, bRec.Y-2, bRec.Width+4, bRec.Height+4);
							ButtonRenderer.DrawButton(e.Graphics, expanded, false, Buttons[i].Enabled ? PushButtonState.Default : PushButtonState.Disabled);
						}
						break;
				}

				for (int i = 0; i < Buttons.Count; i++)
					if(Buttons[i] != null && Buttons[i].Image != null)
						e.Graphics.DrawImage(Buttons[i].Image,
							new RectangleF(buttons.X + lineHeight * i +2*i, buttons.Top, lineHeight, lineHeight));
			}

			switch (BorderStyle)
			{
				case TxtBoxBorderStyle.Fixed3D:
					ControlPaint.DrawBorder3D(e.Graphics, ClientRectangle);
					break;
				case TxtBoxBorderStyle.FixedSingle:
					ControlPaint.DrawBorder(e.Graphics, ClientRectangle, BorderColor, BorderLineStyle);
					break;
				case TxtBoxBorderStyle.Underline:
					e.Graphics.DrawLine(new Pen(Color.FromArgb(Focused ? 255 : 50, BorderColor)), 0, Height - 1, Width, Height - 1);
					break;
				case TxtBoxBorderStyle.Minimal:
					e.Graphics.DrawLine(new Pen(Color.FromArgb(Focused ? 255 : 100, BorderColor)), 0, Height - 1, Width, Height - 1);
					e.Graphics.DrawLine(new Pen(Color.FromArgb(Focused ? 255 : 100, BorderColor)), 0, (int)(Height * 0.75), 0, Height);
					e.Graphics.DrawLine(new Pen(Color.FromArgb(Focused ? 255 : 100, BorderColor)), Width - 1, (int)(Height * 0.75),
						Width - 1, Height);
					break;
				case TxtBoxBorderStyle.Halo:
					ControlPaint.DrawBorder(e.Graphics, ClientRectangle, BorderColor, BorderLineStyle);
					if (BorderColor.A > 25)
						ControlPaint.DrawBorder(e.Graphics,
							new Rectangle(ClientRectangle.X + 1, ClientRectangle.Y + 1, ClientRectangle.Width - 2, ClientRectangle.Height - 2),
							Color.FromArgb(BorderColor.A - 25, BorderColor), BorderLineStyle);
					if (BorderColor.A > 50)
						ControlPaint.DrawBorder(e.Graphics,
							new Rectangle(ClientRectangle.X + 2, ClientRectangle.Y + 2, ClientRectangle.Width - 4, ClientRectangle.Height - 4),
							Color.FromArgb(BorderColor.A - 50, BorderColor), BorderLineStyle);
					break;
			}
			base.OnPaint(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);

			if (HasButtons && ShowButtons && ButtonRectangle().Contains(e.X, e.Y)) Cursor = Cursors.Default;
			else Cursor = Cursors.IBeam;

			if (_selectionMode == TxtBoxSelectionMode.None) return;

			int selectionInt = GetCharIndexFromPosition(e.X);
			if (selectionInt == CaretPosition) return;

			if (_selectionMode == TxtBoxSelectionMode.Word && OnPass) return;

			if (_selectionMode == TxtBoxSelectionMode.Word)
			{
				int maxWord = FindBreakAfter(selectionInt);

				int minWord = FindBreakBefore(selectionInt);

				if (selectionInt == SelectionHead) return;

				if (selectionInt > SelectionHead)
				{
					SelectionHead = _wordSelectionStart;
					CaretPosition = maxWord;
				}
				else
				{
					SelectionHead = _wordSelectionEnd;
					CaretPosition = minWord;
				}
			}
			else
			{
				if (SelectionHead == -1) SelectionHead = CaretPosition;

				CaretPosition = selectionInt;
			}
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.OnMouseDown(e);

			Focus();

			if (HasButtons && ShowButtons && ButtonRectangle().Contains(e.X, e.Y)) return;
			if (e.Button != MouseButtons.Left) return;

			_selectionMode = e.Clicks == 1 ? TxtBoxSelectionMode.Character : TxtBoxSelectionMode.Word;

			if ((ModifierKeys & Keys.Shift) == 0) SelectionHead = -1;

			if (SelectionHead == -1 && (ModifierKeys & Keys.Shift) != 0) SelectionHead = CaretPosition;
			CaretPosition = GetCharIndexFromPosition(e.X);

			if (_selectionMode != TxtBoxSelectionMode.Word) return;

			if (OnPass)
			{
				OnKeyDown(new KeyEventArgs(Keys.Control | Keys.A));
				return;
			}

			int maxWord = FindBreakAfter(CaretPosition);

			int minWord = FindBreakBefore(CaretPosition);

			SelectionHead = maxWord;
			CaretPosition = minWord;
			_wordSelectionStart = minWord;
			_wordSelectionEnd = maxWord;
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.OnMouseUp(e);

			if (e.Button == MouseButtons.Left)
				_selectionMode = TxtBoxSelectionMode.None;
			else if (e.Button == MouseButtons.Right && EnabledRightClickMenu && ContextMenuStrip == null)
				_cms.Show(PointToScreen(new Point(e.X, e.Y)));
		}

		protected override void OnMouseClick(MouseEventArgs e)
		{
			base.OnMouseClick(e);
			CaretVisible = true;

			for (int i = 0; i < Buttons.Count; i++)
			{
				if (Buttons[i].Enabled && IsMouseOnButton(e.X, e.Y, i)) OnButtonClicked(Buttons[i], i);
			}
		}

		protected override bool IsInputKey(Keys keyData)
		{
			if( keyData == Keys.Tab && AcceptsTab) return true;

			return (keyData & Keys.Tab) != Keys.Tab;
		}

		protected override void OnGotFocus(EventArgs e)
		{
			base.OnGotFocus(e);
			CaretVisible = true;
		}

		protected override void OnLostFocus(EventArgs e)
		{
			base.OnLostFocus(e);
			CaretVisible = false;
		}

		#endregion

		#region Methods

		float MeasureStringWidth(Graphics graphics, string text)
		{
			if (text == "") return 0;

			if (OnPass)
			{
				string temp = "";
				for (int i = 0; i < text.Length; i++)
				{
					temp += UseSystemPasswordChar ? '•' : PasswordChar;
				}
				text = temp;
			}
			StringFormat format = StringFormat.GenericDefault;

			format.FormatFlags |= StringFormatFlags.MeasureTrailingSpaces;
			return graphics.MeasureString(text, Font, new PointF(Padding.Left, 0), format).Width;
		}

		Rectangle LayoutRectangle()
		{
			string visible = VisibleText;
			float textHeight = CreateGraphics().MeasureString(" ", Font).Height;
			float textWidth = MeasureStringWidth(CreateGraphics(), visible);
			float textY = MultiLine?Padding.Top:(Height - textHeight) / 2;
			Rectangle r = ButtonRectangle();

			Point end = new Point(ShowButtons && HasButtons ? r.Left - 5: Right - Padding.Right, (int)(Height + textHeight) / 2);
			Point start;

			if (textWidth < Width - r.Width && _textPad == 0) start = new Point(Padding.Left, (int)textY);
			else start = new Point(Padding.Left + _textPad, (int)textY);

			return new Rectangle(start.X, start.Y, end.X - start.X, end.Y - start.Y);
		}

		private void DrawCaret(Graphics g, Color c)
		{
			if (_selectionMode != TxtBoxSelectionMode.None || !CaretVisible) return;

			string visible = VisibleText;
			float lineHeight = g.MeasureString(" ", Font).Height;
			int lineStart = FindLineStart(CaretPosition);
			float realPos = _textPad + Padding.Left + MeasureStringWidth(g, visible == "" ? "" : visible.Substring(lineStart, CaretPosition-lineStart));

			if (realPos >= ButtonRectangle().Left && HasButtons) realPos = ButtonRectangle().Left;
			if (CaretPosition != 0 && Text[CaretPosition - 1] == '\n') realPos -= MeasureStringWidth(g, "\n");

			float textY = MultiLine?Padding.Top + LineIndex*lineHeight:(Height - lineHeight) / 2;
			float x = realPos - SystemInformation.CaretWidth / 2F;

			g.DrawLine(new Pen(c, SystemInformation.CaretWidth), x, textY, x, textY+lineHeight);
		}

		int FindLineStart(int before)
		{
			for (int i = before-1; i > 0; i--)
			{
				if (Text[i] == '\n') return i;
			}
			return 0;
		}

		int FindLineEnd(int after)
		{
			for (int i = after; i < Text.Length; i++)
			{
				if (Text[i] == '\n') return i;
			}
			return Text.Length;
		}

		int GetCharIndexFromPosition(int x)
		{
			string visible = VisibleText;
			Graphics g = CreateGraphics();
			if (visible.Length == 0 || (x < Padding.Left + _textPad + MeasureStringWidth(g, visible.Substring(0, 1)))) return 0;

			int index = -1;
			for (int i = 0; i <= visible.Length; i++)
				if (Padding.Left + _textPad + MeasureStringWidth(CreateGraphics(), visible.Substring(0, i)) > x)
				{
					index = i - 1;
					break;
				}

			if (index == -1 || index + 1 == visible.Length) return visible.Length;

			float start = Padding.Left + _textPad + MeasureStringWidth(g, visible.Substring(0, index));
			float middle = start + MeasureStringWidth(g, visible.Substring(index + 1, 1)) / 2;

			return x < middle ? index : index + 1;
		}

		void RemoveSelection()
		{
			int selectionStart = Math.Min(SelectionHead, CaretPosition);
			int selectionLength = Math.Max(SelectionHead, CaretPosition) - selectionStart;

			CaretPosition = selectionStart;
			SelectionHead = -1;

			Text = Text.Remove(selectionStart, selectionLength);
		}

		private int FindBreakAfter(int index, bool skipFirst = false)
		{
			for (int i = index + (skipFirst ? 1 : 0); i < Text.Length; i++)
			{
				if (char.IsLetterOrDigit(Text[i])) continue;

				return i;
			}

			return Text.Length;
		}

		private int FindBreakBefore(int index, bool skipFirst = false)
		{
			for (int i = (index > Text.Length - 1 ? Text.Length - 1 : (skipFirst ? index - 1 : index)); i > 0; i--)
			{
				if (char.IsLetterOrDigit(Text[i])) continue;

				return i;
			}

			return 0;
		}

		private Rectangle ButtonRectangle()
		{
			int buttonCount = Buttons.Count(x=>x!=null && x.Image!=null);

			if (!ShowButtons || buttonCount == 0) return new Rectangle(0, 0, 0, 0);

			float textHeight = CreateGraphics().MeasureString(" ", Font).Height;
			float textY = (Height - textHeight) / 2;
			return Rectangle.Round(new RectangleF(Width - Padding.Right - textHeight*buttonCount -2*buttonCount, textY, textHeight*buttonCount, textHeight));
		}

		private Rectangle ButtonRectangle(int buttonIndex)
		{
			int buttonCount = Buttons.Count(x => x != null && x.Image != null);

			if(buttonIndex>buttonCount || buttonIndex<0 || Buttons[buttonIndex] == null || Buttons[buttonIndex].Image == null) return new Rectangle(0, 0, 0, 0);

			int invalid = 0;

			for (int i = 0; i < buttonIndex; i++)
			{
				if (Buttons[i] == null || Buttons[i].Image == null) invalid++;
			}

			buttonIndex -= invalid;

			float textHeight = CreateGraphics().MeasureString(" ", Font).Height;
			float textY = (Height - textHeight) / 2;
			return Rectangle.Round(new RectangleF(ButtonRectangle().X + textHeight*buttonIndex, textY, textHeight, textHeight));
		}

		private void AddUndoEntry(string name)
		{
			if (_undoList.Count != 0 && _undoList[_undoList.Count - 1].Name == name) return;
			_undoList.Add(new Command(Text, CaretPosition, SelectionHead, name));
			_redoList.Clear();
			OnUndoCommandsChanged();
		}

		bool IsMouseOnButton(int x, int y, int buttonIndex)
		{
			bool result = HasButtons && ShowButtons && ButtonRectangle(buttonIndex).Contains(x, y);
			
			return result;
		}

		private void ShowAutoComplete()
		{
			_drop.Items.Clear();
			_drop.Width = Width;

			if (Text == "" || OnPass)
			{
				_drop.Close();
				return;
			}

			switch (AutoCompleteSource)
			{
				case AutoCompleteSource.None:
					_drop.Close();
					return;
				case AutoCompleteSource.CustomSource:
					PopulateCustomSource();
					break;
				case AutoCompleteSource.FileSystem:
					PopulateFileSystem(true);
					break;
				case AutoCompleteSource.FileSystemDirectories:
					PopulateFileSystem(false);
					break;
			}

			if (_drop.Items.Count == 0)
			{
				_drop.Close();
				return;
			}

			_drop.ResizeDropDown(this);

			if (AutoCompleteMode == AutoCompleteMode.Suggest || AutoCompleteMode == AutoCompleteMode.SuggestAppend)
				_drop.Show(this, 0, Height);
		}

		private void PopulateFileSystem(bool includeFiles)
		{
			try
			{
				string path = Text.Substring(0, Text.LastIndexOf('\\') + 1 == -1 ? Text.Length - 1 : Text.LastIndexOf('\\') + 1);

				foreach (string directory in Directory.GetDirectories(path).Select(f => f).Where(f => (new DirectoryInfo(f).Attributes & FileAttributes.Hidden) == 0))
					if (directory.ToLowerInvariant().StartsWith(Text.ToLowerInvariant()))
						_drop.Items.Add(Path.GetFullPath(directory));

				if (!includeFiles) return;

				foreach (string file in Directory.GetFiles(path).Select(f => f).Where(f => (new FileInfo(f).Attributes & FileAttributes.Hidden) == 0))
					if (file.ToLowerInvariant().StartsWith(Text.ToLowerInvariant()))
						_drop.Items.Add(Path.GetFullPath(file));
			}
			catch (Exception)
			{
				_drop.Items.Clear();
			}
		}

		private void PopulateCustomSource()
		{
			List<KeyValuePair<string, int>> matches = new List<KeyValuePair<string, int>>();
			string[] splitText = Text.ToLowerInvariant().Split(',', '-', ' ', '.', '/', ';', ':', '(', ')', '[', ']', '_');

			foreach (string s in AutoCompleteCustomSource)
			{
				if (!AllowAutoCompleteDuplicates && matches.Exists(x => x.Key == s)) continue;
				string[] keywords = s.ToLowerInvariant().Split(',', '-', ' ', '.', '/', ';', ':', '(', ')', '[', ']', '_');
				int score = 0;
				foreach (string textPart in splitText)
				{
					if(string.IsNullOrEmpty(textPart)) continue;
					bool anyMatch = false;

					foreach (string keyword in keywords)
					{
						if (!keyword.Contains(textPart.ToLowerInvariant())) continue;
						score++;
						anyMatch = true;
					}

					if (anyMatch) continue;
					score = 0;
					break;
				}
				if (score != 0) matches.Add(new KeyValuePair<string, int>(s, score));
			}

			matches.Sort(Compare);

			foreach (KeyValuePair<string, int> match in matches)
				_drop.Items.Add(match.Key);
		}

		int Compare(KeyValuePair<string, int> a, KeyValuePair<string, int> b)
		{
			return a.Value.CompareTo(b.Value) == 0 ? String.Compare(a.Key, b.Key, StringComparison.Ordinal) : a.Value.CompareTo(b.Value);
		}

		#endregion

		#region Events

		[Description("Occurs a while (TextChangedDelay property) after text changes"), Category("Property Changed")]
		public event EventHandler TextChangedDelayed;

		protected virtual void OnTextChangedDelayed()
		{
			EventHandler handler = TextChangedDelayed;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the embedded button is visible and it has been clicked"), Category("Action")]
		public event EventHandler<ButtonEventArgs> ButtonClicked;

		protected virtual void OnButtonClicked(TxtButton caller, int index)
		{
			EventHandler<ButtonEventArgs> handler = ButtonClicked;
			if (handler != null) handler(this, new ButtonEventArgs(caller, index));
		}

		[Description("Occurs when an item inside the auto complete list is selected"), Category("Action")]
		public event AutoCompleteEventHandler AutoCompleteSelected;
		public delegate void AutoCompleteEventHandler(object sender, EventArgs e);

		protected virtual void OnAutoCompleteSelected(EventArgs e)
		{
			AutoCompleteEventHandler handler = AutoCompleteSelected;
			if (handler != null) handler(this, e);
		}

		[Description("Occurs when the auto complete source changes"), Category("Property Changed")]
		public event EventHandler AutoCompleteSourceChanged;

		protected virtual void OnAutoCompleteSourceChanged()
		{
			EventHandler handler = AutoCompleteSourceChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the auto complete mode changes"), Category("Property Changed")]
		public event EventHandler AutoCompleteModeChanged;

		protected virtual void OnAutoCompleteModeChanged()
		{
			EventHandler handler = AutoCompleteModeChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the border line style changes"), Category("Property Changed")]
		public event EventHandler BorderLineStyleChanged;

		protected virtual void OnBorderLineStyleChanged()
		{
			EventHandler handler = BorderLineStyleChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the border style changes"), Category("Property Changed")]
		public event EventHandler BorderStyleChanged;

		protected virtual void OnBorderStyleChanged()
		{
			EventHandler handler = BorderStyleChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}
		
		[Description("Occurs when the caret position changes"), Category("Property Changed")]
		public event EventHandler CaretPostitionChanged;

		protected virtual void OnCaretPostitionChanged()
		{
			EventHandler handler = CaretPostitionChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the selection head changes"), Category("Property Changed")]
		public event EventHandler SelectionHeadChanged;

		protected virtual void OnSelectionHeadChanged()
		{
			EventHandler handler = SelectionHeadChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the max length changes"), Category("Property Changed")]
		public event EventHandler MaxLengthChanged;

		protected virtual void OnMaxLengthChanged()
		{
			EventHandler handler = MaxLengthChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when read only changes"), Category("Property Changed")]
		public event EventHandler ReadOnlyChanged;

		protected virtual void OnReadOnlyChanged()
		{
			EventHandler handler = ReadOnlyChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when hide selection changes"), Category("Property Changed")]
		public event EventHandler HideSelectionChanged;

		protected virtual void OnHideSelectionChanged()
		{
			EventHandler handler = HideSelectionChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the password character changes"), Category("Property Changed")]
		public event EventHandler PasswordCharChanged;

		protected virtual void OnPasswordCharChanged()
		{
			EventHandler handler = PasswordCharChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the selection forecolor changes"), Category("Property Changed")]
		public event EventHandler SelectionForeColorChanged;

		protected virtual void OnSelectionForeColorChanged()
		{
			EventHandler handler = SelectionForeColorChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the disabled text color changes"), Category("Property Changed")]
		public event EventHandler DisabledTextColorChanged;

		protected virtual void OnDisabledTextColorChanged()
		{
			EventHandler handler = DisabledTextColorChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the inactive selection color changes"), Category("Property Changed")]
		public event EventHandler InactiveSelectionColorChanged;

		protected virtual void OnInactiveSelectionColorChanged()
		{
			EventHandler handler = InactiveSelectionColorChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the caret color changes"), Category("Property Changed")]
		public event EventHandler CaretColorChanged;

		protected virtual void OnCaretColorChanged()
		{
			EventHandler handler = CaretColorChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the border color changes"), Category("Property Changed")]
		public event EventHandler BorderColorChanged;

		protected virtual void OnBorderColorChanged()
		{
			EventHandler handler = BorderColorChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the selection color changes"), Category("Property Changed")]
		public event EventHandler SelectionColorChanged;

		protected virtual void OnSelectionColorChanged()
		{
			EventHandler handler = SelectionColorChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the button visibility changes"), Category("Property Changed")]
		public event EventHandler ButtonVisibleChanged;

		protected virtual void OnButtonVisibleChanged()
		{
			EventHandler handler = ButtonVisibleChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the background style changes"), Category("Property Changed")]
		public event EventHandler BackgroundStyleChanged;

		protected virtual void OnBackgroundStyleChanged()
		{
			EventHandler handler = BackgroundStyleChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the placeholder color changes"), Category("Property Changed")]
		public event EventHandler PlaceholderColorChanged;

		protected virtual void OnPlaceholderColorChanged()
		{
			EventHandler handler = PlaceholderColorChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the focused placeholder color changes"), Category("Property Changed")]
		public event EventHandler FocusedPlaceholderColorChanged;

		protected virtual void OnFocusedPlaceholderColorChanged()
		{
			EventHandler handler = FocusedPlaceholderColorChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the placeholder text changes"), Category("Property Changed")]
		public event EventHandler PlaceholderChanged;

		protected virtual void OnPlaceholderChanged()
		{
			EventHandler handler = PlaceholderChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the button backcolor changes"), Category("Property Changed")]
		public event EventHandler ButtonBackColorChanged;

		protected virtual void OnButtonBackColorChanged()
		{
			EventHandler handler = ButtonBackColorChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the button border color changes"), Category("Property Changed")]
		public event EventHandler ButtonBorderColorChanged;

		protected virtual void OnButtonBorderColorChanged()
		{
			EventHandler handler = ButtonBorderColorChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the undo list changes"), Category("Undo/Redo")]
		public event EventHandler UndoCommandsChanged;

		protected virtual void OnUndoCommandsChanged()
		{
			EventHandler handler = UndoCommandsChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when the redo list changes"), Category("Undo/Redo")]
		public event EventHandler RedoCommandsChanged;

		protected virtual void OnRedoCommandsChanged()
		{
			EventHandler handler = RedoCommandsChanged;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when undo occurs"), Category("Undo/Redo")]
		public event EventHandler Undo;

		protected virtual void OnUndo()
		{
			EventHandler handler = Undo;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		[Description("Occurs when redo occurs"), Category("Undo/Redo")]
		public event EventHandler Redo;

		protected virtual void OnRedo()
		{
			EventHandler handler = Redo;
			if (handler != null) handler(this, EventArgs.Empty);
		}

		#endregion

		private class AutoCompleteDropDown : ToolStripDropDown
		{

			ListBox _listBox = new ListBox();
			private ToolStripControlHost _host;

			public AutoCompleteDropDown()
			{
				DropShadowEnabled = true;
				AutoSize = false;
				Padding = new Padding(0);

				_host = new ToolStripControlHost(_listBox);
				_host.Padding = new Padding(0);
				_listBox.Margin = new Padding(0);
				_host.AutoSize = false;

				_listBox.IntegralHeight = true;
				_listBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
				_listBox.SelectionMode = SelectionMode.One;
				_listBox.Margin = new Padding(0);
				_listBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
				_listBox.TabStop = false;
				_listBox.AutoSize = false;
				_listBox.Font = new Font(Font.FontFamily, 10);
				_listBox.MouseClick += (sender, args) =>
				{
					if (_listBox.Items.Count == 0 || _listBox.SelectedItems.Count != 1) return;

					OnItemClicked(new ToolStripItemClickedEventArgs(new ToolStripMenuItem(_listBox.Text)));
					Close();
				};
				_listBox.GotFocus += (sender, args) => Focus();
				_listBox.MouseMove += (sender, args) =>
				{
					int i;
					for (i = 0; i < (_listBox.Items.Count); i++)
					{
						if (!_listBox.GetItemRectangle(i).Contains(_listBox.PointToClient(MousePosition))) continue;

						_listBox.SelectedIndex = i;
						return;
					}
				};

				base.Items.Add(_host);
			}

			protected override bool IsInputKey(Keys keyData)
			{
				switch (keyData)
				{
					case Keys.Up:
						if ((_listBox.SelectedIndex == -1 || _listBox.SelectedIndex == 0) && Items.Count > 0) _listBox.SelectedIndex = Items.Count - 1;
						else if (Items.Count > 0) _listBox.SelectedIndex--;
						else return true;
						break;
					case Keys.Down:
						if ((_listBox.SelectedIndex == -1 || _listBox.SelectedIndex == Items.Count - 1) && Items.Count > 0) _listBox.SelectedIndex = 0;
						else if (Items.Count > 0) _listBox.SelectedIndex++;
						else return true;
						return false;
					case Keys.Tab:
						if (_listBox.Items.Count == 0 || _listBox.SelectedIndex == -1) return true;

						OnItemClicked(new ToolStripItemClickedEventArgs(new ToolStripMenuItem(_listBox.Text) { AccessibleName = "DONOTRAISE" }));
						return false;
					case Keys.Enter:
						if (_listBox.Items.Count == 0 || _listBox.SelectedIndex == -1) return true;

						OnItemClicked(new ToolStripItemClickedEventArgs(new ToolStripMenuItem(_listBox.Text)));
						Close();
						return false;
				}
				return true;
			}

			public new ListBox.ObjectCollection Items
			{
				get { return _listBox.Items; }
			}

			public void ResizeDropDown(TxtBox parent)
			{
				if (Items.Count == 0) Close();
				else
				{
					Width = parent.Width;
					_host.Width = parent.Width;

					int height = 0;

					for (int i = 0; i < Items.Count; i++)
					{
						int temp = _listBox.GetItemHeight(i);

						if (height < 500) height += temp;
					}
					
					Height = height;
					_host.Height = Height+20;
				}
			}
		}

	}

	[Serializable, DesignTimeVisible(false)]
	public class TxtButton : Component, INotifyPropertyChanged
	{
		private Bitmap _image;
		private bool _enabled = true;

		public Bitmap Image
		{
			get { return _image; }
			set
			{
				_image = value;
				OnPropertyChanged("Image");
			}
		}

		public bool Enabled
		{
			get { return _enabled; }
			set { _enabled = value; }
		}

		[field:NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
		}

	}

	struct Command
	{
		public string Text;
		public int CaretPosition;
		public int SelectionHead;
		public string Name;

		public Command(string text, int caretPosition, int selectionHead, string name)
		{
			Text = text;
			CaretPosition = caretPosition;
			SelectionHead = selectionHead;
			Name = name;
		}
	}

	public class ButtonEventArgs : EventArgs
	{

		private TxtButton _caller;
		private int _buttonIndex = -1;

		public ButtonEventArgs(TxtButton caller, int buttonIndex)
		{
			_caller = caller;
			_buttonIndex = buttonIndex;
		}

		public int ButtonIndex
		{
			get { return _buttonIndex; }
		}

		public TxtButton Caller
		{
			get { return _caller; }
		}
	}

	public enum TxtBoxBorderStyle
	{
		None, Fixed3D, FixedSingle, Minimal, Underline, Halo
	}

	enum TxtBoxSelectionMode
	{
		None, Character, Word
	}

	public enum ButtonStyle
	{
		None, Button,
		Minimal
	}

}
