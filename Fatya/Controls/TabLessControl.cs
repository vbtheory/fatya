﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Fatya.Classes;

namespace Fatya.Controls
{
	[System.ComponentModel.DesignerCategory("Code")]
	public class TabLessControl : TabControl
	{
		private bool _showTabs;

		public TabLessControl()
		{
			SetStyle(ControlStyles.DoubleBuffer | ControlStyles.ResizeRedraw, true);
		}

		protected override void OnControlAdded(ControlEventArgs e)
		{
			if (DesignMode) e.Control.Paint += Control_Paint;
			base.OnControlAdded(e);
		}

		protected override void OnControlRemoved(ControlEventArgs e)
		{
			if (DesignMode) e.Control.Paint -= Control_Paint;
			base.OnControlRemoved(e);
		}

		void Control_Paint(object sender, PaintEventArgs e)
		{
			if (!DesignMode) return;

			Font f = new Font(Font.FontFamily, 11f);
			e.Graphics.DrawString(((TabPage)sender).Text + " (" + SelectedIndex + ")", f, Brushes.Black, 0, 0);
		}

		public bool ShowTabs
		{
			get { return _showTabs; }
			set
			{
				if(ShowTabs== value) return;

				_showTabs = value;
				RecreateHandle();
			}
		}

		protected override void WndProc(ref Message m)
		{
			if (m.Msg == 0x1328 && (!ShowTabs || !DesignMode))
				m.Result = (IntPtr)1;
			else
				base.WndProc(ref m);
		}

		protected override void OnKeyDown(KeyEventArgs ke)
		{
			if (((ke.KeyCode & Keys.Tab) == Keys.Tab || (ke.KeyCode & Keys.Tab) == Keys.PageDown || (ke.KeyCode & Keys.Tab) == Keys.PageDown)
				&& ((ke.KeyCode & Keys.Control) == Keys.Control || (ke.KeyCode & Keys.Shift) == Keys.Shift))
			base.OnKeyDown(ke);
		}

		public int DesignIndex
		{
			get { return SelectedIndex; }
			set { if (DesignMode) SelectedIndex = value; }
		}

	}

}