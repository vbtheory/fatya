﻿using System;
using System.Threading;
using System.Windows.Forms;
using Fatya.Classes;
using Transitions;

namespace Fatya
{
	public partial class FrmWait : Form
	{

		public DictionaryRequest DictionaryRequest;
		public StringRequest StringRequest;

		public FrmWait(Func<StringRequest> a)
		{
			InitializeComponent();

			Execute(a);

			Thread t = new Thread(x =>
			{
				Thread.Sleep(1000);
				if (!Disposing && !IsDisposed)
					Transition.run(this, "Opacity", 1D, new TransitionType_CriticalDamping(500));
			});
			t.IsBackground = true;
			t.Start();
		}

		public FrmWait(Func<DictionaryRequest> a)
		{
			InitializeComponent();

			Execute(a);

			Thread t=new Thread(x =>
			{
				Thread.Sleep(1000);
				if (!Disposing && !IsDisposed)
					Transition.run(this, "Opacity", 1D, new TransitionType_CriticalDamping(500));
			});
			t.IsBackground = true;
			t.Start();
		}

		private void Execute(Func<DictionaryRequest> a)
		{
			BgwMain.DoWork += (sender, args) =>
			{
				DictionaryRequest = a.Invoke();
				Close();
			};

			BgwMain.RunWorkerAsync();
		}

		private void Execute(Func<StringRequest> a)
		{
			BgwMain.DoWork += (sender, args) =>
			{
				StringRequest = a.Invoke();
				Close();
			};

			BgwMain.RunWorkerAsync();
		}
		
		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams pm = base.CreateParams;
				pm.ExStyle |= 0x80;
				return pm;
			}
		}

		protected override void WndProc(ref Message m)
		{
			switch (m.Msg)
			{
				case 0x0085: //WM_NCPAINT for the shadow
					if (Environment.OSVersion.Version.Major >= 6)
					{
						int enabled = 0;
						WIN32APIMethods.DwmIsCompositionEnabled(ref enabled);
						if (enabled != 1) return;
					}

					var var = 2;
					WIN32APIMethods.DwmSetWindowAttribute(Handle, 2, ref var, 4);
					MARGINS margins = new MARGINS
					{
						bottomHeight = 1,
						leftWidth = 1,
						rightWidth = 1,
						topHeight = 1
					};
					WIN32APIMethods.DwmExtendFrameIntoClientArea(Handle, ref margins);
					break;
			}
			base.WndProc(ref m);
		}

	}
}
