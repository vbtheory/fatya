﻿using System;
using System.ComponentModel;
using Fatya.Controls;

namespace Fatya
{
	partial class FrmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ColumnHeader ColFormationId;
            System.Windows.Forms.ColumnHeader ColFormationName;
            System.Windows.Forms.ColumnHeader ColFormationDuration;
            System.Windows.Forms.ColumnHeader ColFormationCreation;
            System.Windows.Forms.ColumnHeader ColFormationDescription;
            System.Windows.Forms.ColumnHeader ColClassId;
            System.Windows.Forms.ColumnHeader ColClassStarted;
            System.Windows.Forms.ColumnHeader ColModuleId;
            System.Windows.Forms.ColumnHeader ColModuleName;
            System.Windows.Forms.ColumnHeader ColmoduleCoef;
            System.Windows.Forms.ColumnHeader ColModuleSubjects;
            System.Windows.Forms.ColumnHeader ColModuleClasse;
            System.Windows.Forms.ColumnHeader ColSubjectId;
            System.Windows.Forms.ColumnHeader ColSubjectName;
            System.Windows.Forms.ColumnHeader ColSubjectCoef;
            System.Windows.Forms.ColumnHeader ColSubjectClass;
            System.Windows.Forms.ColumnHeader ColSubjectModule;
            System.Windows.Forms.ColumnHeader ColTeacherId;
            System.Windows.Forms.ColumnHeader ColTeacherCin;
            System.Windows.Forms.ColumnHeader ColTeacherSex;
            System.Windows.Forms.ColumnHeader ColTeacherName;
            System.Windows.Forms.ColumnHeader ColTeacherEmail;
            System.Windows.Forms.ColumnHeader ColTeacherTel;
            System.Windows.Forms.ColumnHeader ColTeacherAddress;
            System.Windows.Forms.ColumnHeader ColId;
            System.Windows.Forms.ColumnHeader ColCin;
            System.Windows.Forms.ColumnHeader ColCne;
            System.Windows.Forms.ColumnHeader ColSex;
            System.Windows.Forms.ColumnHeader ColName;
            System.Windows.Forms.ColumnHeader ColStudentClass;
            System.Windows.Forms.ColumnHeader ColEmail;
            System.Windows.Forms.ColumnHeader ColBirthday;
            System.Windows.Forms.ColumnHeader ColTel;
            System.Windows.Forms.ColumnHeader ColAdress;
            System.Windows.Forms.ColumnHeader ColRequestId;
            System.Windows.Forms.ColumnHeader ColRequestStudent;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.CmsStudent = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CmiStudentFormation = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiStudentClass = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiStudentModules = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiStudentSubjects = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiStudentTeachers = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiStudentRequests = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiStudentAbscence = new System.Windows.Forms.ToolStripMenuItem();
            this.SepStudent = new Fatya.Controls.Sep();
            this.CmiStudentMore = new System.Windows.Forms.ToolStripMenuItem();
            this.CmsTeacher = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CmiTeacherFormations = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiTeachersClasses = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiTeacherSubjects = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiTeacherStudents = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiTeacherAbscences = new System.Windows.Forms.ToolStripMenuItem();
            this.SepTeacher = new Fatya.Controls.Sep();
            this.CmiTeacherMore = new System.Windows.Forms.ToolStripMenuItem();
            this.CmsFormation = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CmiFormationClasses = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiFormationModules = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiFormationSubjects = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiFormationTeachers = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiFormationStudents = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiFormationRequests = new System.Windows.Forms.ToolStripMenuItem();
            this.SepFormation = new Fatya.Controls.Sep();
            this.CmiFormationMore = new System.Windows.Forms.ToolStripMenuItem();
            this.CmsClass = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CmiClassFormation = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiClassModules = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiClassSubjects = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiClassTeachers = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiClassStudents = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiClassRequests = new System.Windows.Forms.ToolStripMenuItem();
            this.SepClass = new Fatya.Controls.Sep();
            this.CmiClassMore = new System.Windows.Forms.ToolStripMenuItem();
            this.CmsRequest = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CmiRequestStudent = new System.Windows.Forms.ToolStripMenuItem();
            this.SepRequest = new Fatya.Controls.Sep();
            this.CmiRequestRespond = new System.Windows.Forms.ToolStripMenuItem();
            this.CmsModule = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CmiModuleFormation = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiModuleClass = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiModuleSubjects = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiModuleTeachers = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiModuleStudents = new System.Windows.Forms.ToolStripMenuItem();
            this.SepModule = new Fatya.Controls.Sep();
            this.CmiModuleMore = new System.Windows.Forms.ToolStripMenuItem();
            this.CmsSubject = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CmiSubjectFormation = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiSubjectClass = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiSubjectModule = new System.Windows.Forms.ToolStripMenuItem();
            this.CmiSubjectTeacher = new System.Windows.Forms.ToolStripMenuItem();
            this.SepSubject = new Fatya.Controls.Sep();
            this.CmiSubjectMore = new System.Windows.Forms.ToolStripMenuItem();
            this.HdrMain = new Fatya.Controls.Header();
            this.HdiFormation = new Fatya.Controls.HeaderItem();
            this.HdiClass = new Fatya.Controls.HeaderItem();
            this.HdiModule = new Fatya.Controls.HeaderItem();
            this.HdiSubject = new Fatya.Controls.HeaderItem();
            this.HdiTeacher = new Fatya.Controls.HeaderItem();
            this.HdiStudent = new Fatya.Controls.HeaderItem();
            this.HdiRequest = new Fatya.Controls.HeaderItem();
            this.HdiMark = new Fatya.Controls.HeaderItem();
            this.HdiAbsence = new Fatya.Controls.HeaderItem();
            this.HdbRefresh = new Fatya.Controls.HeaderItem();
            this.HdbSettings = new Fatya.Controls.HeaderItem();
            this.HdbLock = new Fatya.Controls.HeaderItem();
            this.HdbExit = new Fatya.Controls.HeaderItem();
            this.TlcMain = new Fatya.Controls.TabLessControl();
            this.TbpFormation = new System.Windows.Forms.TabPage();
            this.TxtFormationSearch = new Fatya.Controls.TxtBox();
            this.TxbFormation = new Fatya.Controls.TxtButton();
            this.BtnFormationMisc = new Fatya.Controls.SplitButton();
            this.BtnFormationRemove = new System.Windows.Forms.Button();
            this.BtnFormationEdit = new System.Windows.Forms.Button();
            this.BtnFormationFilter = new System.Windows.Forms.Button();
            this.BtnFormationAdd = new System.Windows.Forms.Button();
            this.LsvFormation = new System.Windows.Forms.ListView();
            this.ColFormationStudents = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColFormationClasses = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TbpClass = new System.Windows.Forms.TabPage();
            this.TxtClassSearch = new Fatya.Controls.TxtBox();
            this.TxbClass = new Fatya.Controls.TxtButton();
            this.BtnClassMisc = new Fatya.Controls.SplitButton();
            this.BtnClassRemove = new System.Windows.Forms.Button();
            this.BtnClassEdit = new System.Windows.Forms.Button();
            this.BtnClassFilter = new System.Windows.Forms.Button();
            this.BtnClassAdd = new System.Windows.Forms.Button();
            this.LsvClass = new System.Windows.Forms.ListView();
            this.ColClassName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColClassModules = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColClassCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TbpModule = new System.Windows.Forms.TabPage();
            this.TxtModuleSearch = new Fatya.Controls.TxtBox();
            this.TxbModule = new Fatya.Controls.TxtButton();
            this.BtnModuleMisc = new Fatya.Controls.SplitButton();
            this.BtnModuleRemove = new System.Windows.Forms.Button();
            this.BtnModuleEdit = new System.Windows.Forms.Button();
            this.BtnModuleFilter = new System.Windows.Forms.Button();
            this.BtnModuleAdd = new System.Windows.Forms.Button();
            this.LsvModule = new System.Windows.Forms.ListView();
            this.ColModuleMin = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TbpSubject = new System.Windows.Forms.TabPage();
            this.TxtSubjectSearch = new Fatya.Controls.TxtBox();
            this.TxbSubject = new Fatya.Controls.TxtButton();
            this.BtnSubjectMisc = new Fatya.Controls.SplitButton();
            this.BtnSubjectRemove = new System.Windows.Forms.Button();
            this.BtnSubjectEdit = new System.Windows.Forms.Button();
            this.BtnSubjectFilter = new System.Windows.Forms.Button();
            this.BtnSubjectAdd = new System.Windows.Forms.Button();
            this.LsvSubject = new System.Windows.Forms.ListView();
            this.ColSubjectMinMark = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColSubjectTeacher = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TbpTeacher = new System.Windows.Forms.TabPage();
            this.TxtTeacherSearch = new Fatya.Controls.TxtBox();
            this.TxbTeacher = new Fatya.Controls.TxtButton();
            this.BtnTeacherMisc = new Fatya.Controls.SplitButton();
            this.BtnTeacherDelete = new System.Windows.Forms.Button();
            this.BtnTeacherEdit = new System.Windows.Forms.Button();
            this.BtnTeacherFilter = new System.Windows.Forms.Button();
            this.BtnTeacherAdd = new System.Windows.Forms.Button();
            this.LsvTeacher = new System.Windows.Forms.ListView();
            this.ColSum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TbpStudent = new System.Windows.Forms.TabPage();
            this.TxtStudentSearch = new Fatya.Controls.TxtBox();
            this.TxbStudent = new Fatya.Controls.TxtButton();
            this.BtnStudentMisc = new Fatya.Controls.SplitButton();
            this.BtnStudentRemove = new System.Windows.Forms.Button();
            this.BtnStudentEdit = new System.Windows.Forms.Button();
            this.BtnStudentFilter = new System.Windows.Forms.Button();
            this.BtnStudentAdd = new System.Windows.Forms.Button();
            this.LsvStudent = new System.Windows.Forms.ListView();
            this.TbpRequest = new System.Windows.Forms.TabPage();
            this.TxtRequestSearch = new Fatya.Controls.TxtBox();
            this.TxbRequest = new Fatya.Controls.TxtButton();
            this.BtnRequestMisc = new Fatya.Controls.SplitButton();
            this.BtnRequestRemove = new System.Windows.Forms.Button();
            this.BtnRequestState = new System.Windows.Forms.Button();
            this.BtnRequestFilter = new System.Windows.Forms.Button();
            this.LsvRequest = new System.Windows.Forms.ListView();
            this.ColRequestType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColRequestState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColRequestClass = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColRequestDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColRequestStart = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColRequestDone = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TbpMarks = new System.Windows.Forms.TabPage();
            this.txtBox1 = new Fatya.Controls.TxtBox();
            this.TxbMarkSearch = new Fatya.Controls.TxtButton();
            this.splitButton1 = new Fatya.Controls.SplitButton();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CbxMarkTest = new System.Windows.Forms.ComboBox();
            this.LsvMark = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button2 = new System.Windows.Forms.Button();
            this.BtnMarkNew = new System.Windows.Forms.Button();
            this.CbxMarkSubject = new System.Windows.Forms.ComboBox();
            this.CbxMarkModule = new System.Windows.Forms.ComboBox();
            this.CbxMarkClass = new System.Windows.Forms.ComboBox();
            this.CbxMarkFormation = new System.Windows.Forms.ComboBox();
            this.TbpAbsence = new System.Windows.Forms.TabPage();
            ColFormationId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColFormationName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColFormationDuration = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColFormationCreation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColFormationDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColClassId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColClassStarted = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColModuleId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColModuleName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColmoduleCoef = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColModuleSubjects = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColModuleClasse = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColSubjectId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColSubjectName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColSubjectCoef = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColSubjectClass = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColSubjectModule = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColTeacherId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColTeacherCin = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColTeacherSex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColTeacherName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColTeacherEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColTeacherTel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColTeacherAddress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColCin = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColCne = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColSex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColStudentClass = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColEmail = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColBirthday = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColTel = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColAdress = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColRequestId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ColRequestStudent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CmsStudent.SuspendLayout();
            this.CmsTeacher.SuspendLayout();
            this.CmsFormation.SuspendLayout();
            this.CmsClass.SuspendLayout();
            this.CmsRequest.SuspendLayout();
            this.CmsModule.SuspendLayout();
            this.CmsSubject.SuspendLayout();
            this.TlcMain.SuspendLayout();
            this.TbpFormation.SuspendLayout();
            this.TbpClass.SuspendLayout();
            this.TbpModule.SuspendLayout();
            this.TbpSubject.SuspendLayout();
            this.TbpTeacher.SuspendLayout();
            this.TbpStudent.SuspendLayout();
            this.TbpRequest.SuspendLayout();
            this.TbpMarks.SuspendLayout();
            this.SuspendLayout();
            // 
            // ColFormationId
            // 
            ColFormationId.Text = "ID";
            // 
            // ColFormationName
            // 
            ColFormationName.Text = "Nom de la formation";
            // 
            // ColFormationDuration
            // 
            ColFormationDuration.Text = "Durée de la fomation";
            // 
            // ColFormationCreation
            // 
            ColFormationCreation.Text = "Date de création";
            // 
            // ColFormationDescription
            // 
            ColFormationDescription.Text = "Description";
            ColFormationDescription.Width = 142;
            // 
            // ColClassId
            // 
            ColClassId.Text = "ID";
            // 
            // ColClassStarted
            // 
            ColClassStarted.Text = "Année de début";
            ColClassStarted.Width = 107;
            // 
            // ColModuleId
            // 
            ColModuleId.Text = "ID";
            // 
            // ColModuleName
            // 
            ColModuleName.Text = "Nom";
            // 
            // ColmoduleCoef
            // 
            ColmoduleCoef.Text = "Coéfficient";
            // 
            // ColModuleSubjects
            // 
            ColModuleSubjects.Text = "Matières";
            // 
            // ColModuleClasse
            // 
            ColModuleClasse.Text = "Classe";
            ColModuleClasse.Width = 142;
            // 
            // ColSubjectId
            // 
            ColSubjectId.Text = "ID";
            // 
            // ColSubjectName
            // 
            ColSubjectName.Text = "Nom";
            // 
            // ColSubjectCoef
            // 
            ColSubjectCoef.Text = "Coéfficient";
            // 
            // ColSubjectClass
            // 
            ColSubjectClass.Text = "Classe";
            ColSubjectClass.Width = 71;
            // 
            // ColSubjectModule
            // 
            ColSubjectModule.Text = "Module";
            // 
            // ColTeacherId
            // 
            ColTeacherId.Text = "ID";
            // 
            // ColTeacherCin
            // 
            ColTeacherCin.Text = "CIN";
            // 
            // ColTeacherSex
            // 
            ColTeacherSex.Text = "Sexe";
            // 
            // ColTeacherName
            // 
            ColTeacherName.Text = "Nom & prénom";
            // 
            // ColTeacherEmail
            // 
            ColTeacherEmail.Text = "Email";
            ColTeacherEmail.Width = 142;
            // 
            // ColTeacherTel
            // 
            ColTeacherTel.Text = "Télephone";
            ColTeacherTel.Width = 86;
            // 
            // ColTeacherAddress
            // 
            ColTeacherAddress.Text = "Adresse";
            ColTeacherAddress.Width = 109;
            // 
            // ColId
            // 
            ColId.Text = "ID";
            // 
            // ColCin
            // 
            ColCin.Text = "CIN";
            // 
            // ColCne
            // 
            ColCne.Text = "CNE";
            // 
            // ColSex
            // 
            ColSex.Text = "Sexe";
            // 
            // ColName
            // 
            ColName.Text = "Nom & prénom";
            ColName.Width = 142;
            // 
            // ColStudentClass
            // 
            ColStudentClass.Text = "Classe";
            // 
            // ColEmail
            // 
            ColEmail.Text = "Email";
            ColEmail.Width = 86;
            // 
            // ColBirthday
            // 
            ColBirthday.Text = "Date de naissance";
            ColBirthday.Width = 109;
            // 
            // ColTel
            // 
            ColTel.Text = "Télephone";
            // 
            // ColAdress
            // 
            ColAdress.Text = "Adresse";
            // 
            // ColRequestId
            // 
            ColRequestId.Text = "ID";
            // 
            // ColRequestStudent
            // 
            ColRequestStudent.Text = "Etudiant";
            ColRequestStudent.Width = 107;
            // 
            // CmsStudent
            // 
            this.CmsStudent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CmiStudentFormation,
            this.CmiStudentClass,
            this.CmiStudentModules,
            this.CmiStudentSubjects,
            this.CmiStudentTeachers,
            this.CmiStudentRequests,
            this.CmiStudentAbscence,
            this.SepStudent,
            this.CmiStudentMore});
            this.CmsStudent.Name = "CmsStudent";
            this.CmsStudent.Size = new System.Drawing.Size(306, 186);
            // 
            // CmiStudentFormation
            // 
            this.CmiStudentFormation.Enabled = false;
            this.CmiStudentFormation.Name = "CmiStudentFormation";
            this.CmiStudentFormation.Size = new System.Drawing.Size(305, 22);
            this.CmiStudentFormation.Text = "Voir formation";
            this.CmiStudentFormation.Click += new System.EventHandler(this.CsiStudentFormation_Click);
            // 
            // CmiStudentClass
            // 
            this.CmiStudentClass.Enabled = false;
            this.CmiStudentClass.Name = "CmiStudentClass";
            this.CmiStudentClass.Size = new System.Drawing.Size(305, 22);
            this.CmiStudentClass.Text = "Voir classe";
            this.CmiStudentClass.Click += new System.EventHandler(this.CsiStudentClass_Click);
            // 
            // CmiStudentModules
            // 
            this.CmiStudentModules.Enabled = false;
            this.CmiStudentModules.Name = "CmiStudentModules";
            this.CmiStudentModules.Size = new System.Drawing.Size(305, 22);
            this.CmiStudentModules.Text = "Voir modules";
            this.CmiStudentModules.Click += new System.EventHandler(this.CmiStudentModules_Click);
            // 
            // CmiStudentSubjects
            // 
            this.CmiStudentSubjects.Enabled = false;
            this.CmiStudentSubjects.Name = "CmiStudentSubjects";
            this.CmiStudentSubjects.Size = new System.Drawing.Size(305, 22);
            this.CmiStudentSubjects.Text = "Voir matières";
            this.CmiStudentSubjects.Click += new System.EventHandler(this.CmiStudentSubjects_Click);
            // 
            // CmiStudentTeachers
            // 
            this.CmiStudentTeachers.Enabled = false;
            this.CmiStudentTeachers.Name = "CmiStudentTeachers";
            this.CmiStudentTeachers.Size = new System.Drawing.Size(305, 22);
            this.CmiStudentTeachers.Text = "Voir enseignants";
            this.CmiStudentTeachers.Click += new System.EventHandler(this.CmiStudentTeachers_Click);
            // 
            // CmiStudentRequests
            // 
            this.CmiStudentRequests.Enabled = false;
            this.CmiStudentRequests.Name = "CmiStudentRequests";
            this.CmiStudentRequests.Size = new System.Drawing.Size(305, 22);
            this.CmiStudentRequests.Text = "Voir demandes";
            this.CmiStudentRequests.Click += new System.EventHandler(this.CsiStudentRequests_Click);
            // 
            // CmiStudentAbscence
            // 
            this.CmiStudentAbscence.Enabled = false;
            this.CmiStudentAbscence.Name = "CmiStudentAbscence";
            this.CmiStudentAbscence.Size = new System.Drawing.Size(305, 22);
            this.CmiStudentAbscence.Text = "Voir absenses";
            this.CmiStudentAbscence.Click += new System.EventHandler(this.CmiStudentAbscence_Click);
            // 
            // SepStudent
            // 
            this.SepStudent.Name = "SepStudent";
            this.SepStudent.Size = new System.Drawing.Size(302, 6);
            // 
            // CmiStudentMore
            // 
            this.CmiStudentMore.Enabled = false;
            this.CmiStudentMore.Name = "CmiStudentMore";
            this.CmiStudentMore.Size = new System.Drawing.Size(305, 22);
            this.CmiStudentMore.Text = "Imprimer la fiche des étudiants sélectionnés";
            this.CmiStudentMore.Click += new System.EventHandler(this.CsiStudentMore_Click);
            // 
            // CmsTeacher
            // 
            this.CmsTeacher.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CmiTeacherFormations,
            this.CmiTeachersClasses,
            this.CmiTeacherSubjects,
            this.CmiTeacherStudents,
            this.CmiTeacherAbscences,
            this.SepTeacher,
            this.CmiTeacherMore});
            this.CmsTeacher.Name = "CmsStudent";
            this.CmsTeacher.Size = new System.Drawing.Size(320, 142);
            // 
            // CmiTeacherFormations
            // 
            this.CmiTeacherFormations.Enabled = false;
            this.CmiTeacherFormations.Name = "CmiTeacherFormations";
            this.CmiTeacherFormations.Size = new System.Drawing.Size(319, 22);
            this.CmiTeacherFormations.Text = "Voir formations";
            this.CmiTeacherFormations.Click += new System.EventHandler(this.CmiTeacherFormations_Click);
            // 
            // CmiTeachersClasses
            // 
            this.CmiTeachersClasses.Enabled = false;
            this.CmiTeachersClasses.Name = "CmiTeachersClasses";
            this.CmiTeachersClasses.Size = new System.Drawing.Size(319, 22);
            this.CmiTeachersClasses.Text = "Voir classes";
            this.CmiTeachersClasses.Click += new System.EventHandler(this.CmiTeachersClasses_Click);
            // 
            // CmiTeacherSubjects
            // 
            this.CmiTeacherSubjects.Enabled = false;
            this.CmiTeacherSubjects.Name = "CmiTeacherSubjects";
            this.CmiTeacherSubjects.Size = new System.Drawing.Size(319, 22);
            this.CmiTeacherSubjects.Text = "Voir matières";
            this.CmiTeacherSubjects.Click += new System.EventHandler(this.CmiTeacherSubjects_Click);
            // 
            // CmiTeacherStudents
            // 
            this.CmiTeacherStudents.Enabled = false;
            this.CmiTeacherStudents.Name = "CmiTeacherStudents";
            this.CmiTeacherStudents.Size = new System.Drawing.Size(319, 22);
            this.CmiTeacherStudents.Text = "Voir étudiants";
            this.CmiTeacherStudents.Click += new System.EventHandler(this.CmiTeacherStudents_Click);
            // 
            // CmiTeacherAbscences
            // 
            this.CmiTeacherAbscences.Enabled = false;
            this.CmiTeacherAbscences.Name = "CmiTeacherAbscences";
            this.CmiTeacherAbscences.Size = new System.Drawing.Size(319, 22);
            this.CmiTeacherAbscences.Text = "Voir absenses";
            this.CmiTeacherAbscences.Click += new System.EventHandler(this.CmiTeacherAbscences_Click);
            // 
            // SepTeacher
            // 
            this.SepTeacher.Name = "SepTeacher";
            this.SepTeacher.Size = new System.Drawing.Size(316, 6);
            // 
            // CmiTeacherMore
            // 
            this.CmiTeacherMore.Enabled = false;
            this.CmiTeacherMore.Name = "CmiTeacherMore";
            this.CmiTeacherMore.Size = new System.Drawing.Size(319, 22);
            this.CmiTeacherMore.Text = "Imprimer la fiche des enseignants sélectionnés";
            this.CmiTeacherMore.Click += new System.EventHandler(this.CsiTeacherMore_Click);
            // 
            // CmsFormation
            // 
            this.CmsFormation.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CmiFormationClasses,
            this.CmiFormationModules,
            this.CmiFormationSubjects,
            this.CmiFormationTeachers,
            this.CmiFormationStudents,
            this.CmiFormationRequests,
            this.SepFormation,
            this.CmiFormationMore});
            this.CmsFormation.Name = "CmsStudent";
            this.CmsFormation.Size = new System.Drawing.Size(321, 164);
            // 
            // CmiFormationClasses
            // 
            this.CmiFormationClasses.Enabled = false;
            this.CmiFormationClasses.Name = "CmiFormationClasses";
            this.CmiFormationClasses.Size = new System.Drawing.Size(320, 22);
            this.CmiFormationClasses.Text = "Voir classes";
            this.CmiFormationClasses.Click += new System.EventHandler(this.CsiFormationClasses_Click);
            // 
            // CmiFormationModules
            // 
            this.CmiFormationModules.Enabled = false;
            this.CmiFormationModules.Name = "CmiFormationModules";
            this.CmiFormationModules.Size = new System.Drawing.Size(320, 22);
            this.CmiFormationModules.Text = "Voir modules";
            this.CmiFormationModules.Click += new System.EventHandler(this.CmiFormationModules_Click);
            // 
            // CmiFormationSubjects
            // 
            this.CmiFormationSubjects.Enabled = false;
            this.CmiFormationSubjects.Name = "CmiFormationSubjects";
            this.CmiFormationSubjects.Size = new System.Drawing.Size(320, 22);
            this.CmiFormationSubjects.Text = "Voir matières";
            this.CmiFormationSubjects.Click += new System.EventHandler(this.CmiFormationSubjects_Click);
            // 
            // CmiFormationTeachers
            // 
            this.CmiFormationTeachers.Enabled = false;
            this.CmiFormationTeachers.Name = "CmiFormationTeachers";
            this.CmiFormationTeachers.Size = new System.Drawing.Size(320, 22);
            this.CmiFormationTeachers.Text = "Voir enseignants";
            this.CmiFormationTeachers.Click += new System.EventHandler(this.CmiFormationTeachers_Click);
            // 
            // CmiFormationStudents
            // 
            this.CmiFormationStudents.Enabled = false;
            this.CmiFormationStudents.Name = "CmiFormationStudents";
            this.CmiFormationStudents.Size = new System.Drawing.Size(320, 22);
            this.CmiFormationStudents.Text = "Voir étudiants";
            this.CmiFormationStudents.Click += new System.EventHandler(this.CsiFormationStudents_Click);
            // 
            // CmiFormationRequests
            // 
            this.CmiFormationRequests.Enabled = false;
            this.CmiFormationRequests.Name = "CmiFormationRequests";
            this.CmiFormationRequests.Size = new System.Drawing.Size(320, 22);
            this.CmiFormationRequests.Text = "Voir demandes";
            this.CmiFormationRequests.Click += new System.EventHandler(this.CmiFormationRequests_Click);
            // 
            // SepFormation
            // 
            this.SepFormation.Name = "SepFormation";
            this.SepFormation.Size = new System.Drawing.Size(317, 6);
            // 
            // CmiFormationMore
            // 
            this.CmiFormationMore.Enabled = false;
            this.CmiFormationMore.Name = "CmiFormationMore";
            this.CmiFormationMore.Size = new System.Drawing.Size(320, 22);
            this.CmiFormationMore.Text = "Imprimer la fiche des formations sélectionnées";
            this.CmiFormationMore.Click += new System.EventHandler(this.CsiFormationMore_Click);
            // 
            // CmsClass
            // 
            this.CmsClass.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CmiClassFormation,
            this.CmiClassModules,
            this.CmiClassSubjects,
            this.CmiClassTeachers,
            this.CmiClassStudents,
            this.CmiClassRequests,
            this.SepClass,
            this.CmiClassMore});
            this.CmsClass.Name = "CmsStudent";
            this.CmsClass.Size = new System.Drawing.Size(299, 164);
            // 
            // CmiClassFormation
            // 
            this.CmiClassFormation.Enabled = false;
            this.CmiClassFormation.Name = "CmiClassFormation";
            this.CmiClassFormation.Size = new System.Drawing.Size(298, 22);
            this.CmiClassFormation.Text = "Voir formation";
            this.CmiClassFormation.Click += new System.EventHandler(this.CmiClassFormation_Click);
            // 
            // CmiClassModules
            // 
            this.CmiClassModules.Enabled = false;
            this.CmiClassModules.Name = "CmiClassModules";
            this.CmiClassModules.Size = new System.Drawing.Size(298, 22);
            this.CmiClassModules.Text = "Voir modules";
            this.CmiClassModules.Click += new System.EventHandler(this.CmiClassModules_Click);
            // 
            // CmiClassSubjects
            // 
            this.CmiClassSubjects.Enabled = false;
            this.CmiClassSubjects.Name = "CmiClassSubjects";
            this.CmiClassSubjects.Size = new System.Drawing.Size(298, 22);
            this.CmiClassSubjects.Text = "Voir matières";
            this.CmiClassSubjects.Click += new System.EventHandler(this.CmiClassSubjects_Click);
            // 
            // CmiClassTeachers
            // 
            this.CmiClassTeachers.Enabled = false;
            this.CmiClassTeachers.Name = "CmiClassTeachers";
            this.CmiClassTeachers.Size = new System.Drawing.Size(298, 22);
            this.CmiClassTeachers.Text = "Voir enseignants";
            this.CmiClassTeachers.Click += new System.EventHandler(this.CmiClassTeachers_Click);
            // 
            // CmiClassStudents
            // 
            this.CmiClassStudents.Enabled = false;
            this.CmiClassStudents.Name = "CmiClassStudents";
            this.CmiClassStudents.Size = new System.Drawing.Size(298, 22);
            this.CmiClassStudents.Text = "Voir étudiants";
            this.CmiClassStudents.Click += new System.EventHandler(this.CmiClassStudents_Click);
            // 
            // CmiClassRequests
            // 
            this.CmiClassRequests.Enabled = false;
            this.CmiClassRequests.Name = "CmiClassRequests";
            this.CmiClassRequests.Size = new System.Drawing.Size(298, 22);
            this.CmiClassRequests.Text = "Voir demandes";
            this.CmiClassRequests.Click += new System.EventHandler(this.CmiClassRequests_Click);
            // 
            // SepClass
            // 
            this.SepClass.Name = "SepClass";
            this.SepClass.Size = new System.Drawing.Size(295, 6);
            // 
            // CmiClassMore
            // 
            this.CmiClassMore.Enabled = false;
            this.CmiClassMore.Name = "CmiClassMore";
            this.CmiClassMore.Size = new System.Drawing.Size(298, 22);
            this.CmiClassMore.Text = "Imprimer la fiche des classes sélectionnées";
            // 
            // CmsRequest
            // 
            this.CmsRequest.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CmiRequestStudent,
            this.SepRequest,
            this.CmiRequestRespond});
            this.CmsRequest.Name = "CmsStudent";
            this.CmsRequest.Size = new System.Drawing.Size(189, 54);
            // 
            // CmiRequestStudent
            // 
            this.CmiRequestStudent.Enabled = false;
            this.CmiRequestStudent.Name = "CmiRequestStudent";
            this.CmiRequestStudent.Size = new System.Drawing.Size(188, 22);
            this.CmiRequestStudent.Text = "Voir étudiant";
            this.CmiRequestStudent.Click += new System.EventHandler(this.CmiRequestStudent_Click);
            // 
            // SepRequest
            // 
            this.SepRequest.Name = "SepRequest";
            this.SepRequest.Size = new System.Drawing.Size(185, 6);
            // 
            // CmiRequestRespond
            // 
            this.CmiRequestRespond.Enabled = false;
            this.CmiRequestRespond.Name = "CmiRequestRespond";
            this.CmiRequestRespond.Size = new System.Drawing.Size(188, 22);
            this.CmiRequestRespond.Text = "Imprimer la demande";
            // 
            // CmsModule
            // 
            this.CmsModule.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CmiModuleFormation,
            this.CmiModuleClass,
            this.CmiModuleSubjects,
            this.CmiModuleTeachers,
            this.CmiModuleStudents,
            this.SepModule,
            this.CmiModuleMore});
            this.CmsModule.Name = "CmsStudent";
            this.CmsModule.Size = new System.Drawing.Size(162, 142);
            // 
            // CmiModuleFormation
            // 
            this.CmiModuleFormation.Enabled = false;
            this.CmiModuleFormation.Name = "CmiModuleFormation";
            this.CmiModuleFormation.Size = new System.Drawing.Size(161, 22);
            this.CmiModuleFormation.Text = "Voir formation";
            this.CmiModuleFormation.Click += new System.EventHandler(this.CmiModuleFormation_Click);
            // 
            // CmiModuleClass
            // 
            this.CmiModuleClass.Enabled = false;
            this.CmiModuleClass.Name = "CmiModuleClass";
            this.CmiModuleClass.Size = new System.Drawing.Size(161, 22);
            this.CmiModuleClass.Text = "Voir classe";
            this.CmiModuleClass.Click += new System.EventHandler(this.CmiModuleClass_Click);
            // 
            // CmiModuleSubjects
            // 
            this.CmiModuleSubjects.Enabled = false;
            this.CmiModuleSubjects.Name = "CmiModuleSubjects";
            this.CmiModuleSubjects.Size = new System.Drawing.Size(161, 22);
            this.CmiModuleSubjects.Text = "Voir matières";
            this.CmiModuleSubjects.Click += new System.EventHandler(this.CmiModuleSubjects_Click);
            // 
            // CmiModuleTeachers
            // 
            this.CmiModuleTeachers.Enabled = false;
            this.CmiModuleTeachers.Name = "CmiModuleTeachers";
            this.CmiModuleTeachers.Size = new System.Drawing.Size(161, 22);
            this.CmiModuleTeachers.Text = "Voir enseignants";
            this.CmiModuleTeachers.Click += new System.EventHandler(this.CmiModuleTeachers_Click);
            // 
            // CmiModuleStudents
            // 
            this.CmiModuleStudents.Enabled = false;
            this.CmiModuleStudents.Name = "CmiModuleStudents";
            this.CmiModuleStudents.Size = new System.Drawing.Size(161, 22);
            this.CmiModuleStudents.Text = "Voir étudiants";
            this.CmiModuleStudents.Click += new System.EventHandler(this.CmiModuleStudents_Click);
            // 
            // SepModule
            // 
            this.SepModule.Name = "SepModule";
            this.SepModule.Size = new System.Drawing.Size(158, 6);
            // 
            // CmiModuleMore
            // 
            this.CmiModuleMore.Enabled = false;
            this.CmiModuleMore.Name = "CmiModuleMore";
            this.CmiModuleMore.Size = new System.Drawing.Size(161, 22);
            this.CmiModuleMore.Text = "//";
            // 
            // CmsSubject
            // 
            this.CmsSubject.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CmiSubjectFormation,
            this.CmiSubjectClass,
            this.CmiSubjectModule,
            this.CmiSubjectTeacher,
            this.SepSubject,
            this.CmiSubjectMore});
            this.CmsSubject.Name = "CmsStudent";
            this.CmsSubject.Size = new System.Drawing.Size(157, 120);
            // 
            // CmiSubjectFormation
            // 
            this.CmiSubjectFormation.Enabled = false;
            this.CmiSubjectFormation.Name = "CmiSubjectFormation";
            this.CmiSubjectFormation.Size = new System.Drawing.Size(156, 22);
            this.CmiSubjectFormation.Text = "Voir formation";
            this.CmiSubjectFormation.Click += new System.EventHandler(this.CmiSubjectFormation_Click);
            // 
            // CmiSubjectClass
            // 
            this.CmiSubjectClass.Enabled = false;
            this.CmiSubjectClass.Name = "CmiSubjectClass";
            this.CmiSubjectClass.Size = new System.Drawing.Size(156, 22);
            this.CmiSubjectClass.Text = "Voir classe";
            this.CmiSubjectClass.Click += new System.EventHandler(this.CmiSubjectClass_Click);
            // 
            // CmiSubjectModule
            // 
            this.CmiSubjectModule.Enabled = false;
            this.CmiSubjectModule.Name = "CmiSubjectModule";
            this.CmiSubjectModule.Size = new System.Drawing.Size(156, 22);
            this.CmiSubjectModule.Text = "Voir module";
            this.CmiSubjectModule.Click += new System.EventHandler(this.CmiSubjectModule_Click);
            // 
            // CmiSubjectTeacher
            // 
            this.CmiSubjectTeacher.Enabled = false;
            this.CmiSubjectTeacher.Name = "CmiSubjectTeacher";
            this.CmiSubjectTeacher.Size = new System.Drawing.Size(156, 22);
            this.CmiSubjectTeacher.Text = "Voir enseignant";
            this.CmiSubjectTeacher.Click += new System.EventHandler(this.CmiSubjectTeacher_Click);
            // 
            // SepSubject
            // 
            this.SepSubject.Name = "SepSubject";
            this.SepSubject.Size = new System.Drawing.Size(153, 6);
            // 
            // CmiSubjectMore
            // 
            this.CmiSubjectMore.Enabled = false;
            this.CmiSubjectMore.Name = "CmiSubjectMore";
            this.CmiSubjectMore.Size = new System.Drawing.Size(156, 22);
            this.CmiSubjectMore.Text = "//";
            // 
            // HdrMain
            // 
            this.HdrMain.BackColor = System.Drawing.SystemColors.Window;
            this.HdrMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.HdrMain.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HdrMain.Items.Add(this.HdiFormation);
            this.HdrMain.Items.Add(this.HdiClass);
            this.HdrMain.Items.Add(this.HdiModule);
            this.HdrMain.Items.Add(this.HdiSubject);
            this.HdrMain.Items.Add(this.HdiTeacher);
            this.HdrMain.Items.Add(this.HdiStudent);
            this.HdrMain.Items.Add(this.HdiRequest);
            this.HdrMain.Items.Add(this.HdiMark);
            this.HdrMain.Items.Add(this.HdiAbsence);
            this.HdrMain.Items.Add(this.HdbRefresh);
            this.HdrMain.Items.Add(this.HdbSettings);
            this.HdrMain.Items.Add(this.HdbLock);
            this.HdrMain.Items.Add(this.HdbExit);
            this.HdrMain.Location = new System.Drawing.Point(0, 0);
            this.HdrMain.Name = "HdrMain";
            this.HdrMain.SelectedIndex = 0;
            this.HdrMain.SelectedItem = this.HdiFormation;
            this.HdrMain.Size = new System.Drawing.Size(944, 111);
            this.HdrMain.TabIndex = 0;
            this.HdrMain.TabStop = false;
            this.HdrMain.Text = "Formations";
            this.HdrMain.SelectedIndexChanged += new System.EventHandler<Fatya.Controls.HeaderEventArgs>(this.HdrMain_SelectedIndexChanged);
            this.HdrMain.ButtonClicked += new System.EventHandler<Fatya.Controls.HeaderEventArgs>(this.HdrMain_ButtonClicked);
            // 
            // HdiFormation
            // 
            this.HdiFormation.Image = global::Fatya.Properties.Resources.Formation;
            this.HdiFormation.Text = "Formations";
            // 
            // HdiClass
            // 
            this.HdiClass.Image = global::Fatya.Properties.Resources.Class;
            this.HdiClass.Text = "Classes";
            // 
            // HdiModule
            // 
            this.HdiModule.Image = global::Fatya.Properties.Resources.Module_;
            this.HdiModule.Text = "Modules";
            // 
            // HdiSubject
            // 
            this.HdiSubject.Image = global::Fatya.Properties.Resources.Subject;
            this.HdiSubject.Text = "Matières";
            // 
            // HdiTeacher
            // 
            this.HdiTeacher.Image = global::Fatya.Properties.Resources.Teacher;
            this.HdiTeacher.StartGroup = true;
            this.HdiTeacher.Text = "Enseignants";
            // 
            // HdiStudent
            // 
            this.HdiStudent.Image = global::Fatya.Properties.Resources.Student;
            this.HdiStudent.Text = "Etudiants";
            // 
            // HdiRequest
            // 
            this.HdiRequest.Image = global::Fatya.Properties.Resources.Request;
            this.HdiRequest.StartGroup = true;
            this.HdiRequest.Text = "Demandes";
            // 
            // HdiMark
            // 
            this.HdiMark.Image = global::Fatya.Properties.Resources.Marks;
            this.HdiMark.Text = "Notes";
            // 
            // HdiAbsence
            // 
            this.HdiAbsence.Image = global::Fatya.Properties.Resources.Absence;
            this.HdiAbsence.Text = "Absence";
            // 
            // HdbRefresh
            // 
            this.HdbRefresh.Image = global::Fatya.Properties.Resources.Refresh;
            this.HdbRefresh.IsButton = true;
            this.HdbRefresh.StartGroup = true;
            this.HdbRefresh.Text = "Recharger";
            // 
            // HdbSettings
            // 
            this.HdbSettings.Image = global::Fatya.Properties.Resources.Settings;
            this.HdbSettings.IsButton = true;
            this.HdbSettings.StartGroup = true;
            this.HdbSettings.Text = "Paramètres";
            // 
            // HdbLock
            // 
            this.HdbLock.Image = global::Fatya.Properties.Resources.Lock;
            this.HdbLock.IsButton = true;
            this.HdbLock.Text = "Verrouiller";
            // 
            // HdbExit
            // 
            this.HdbExit.Image = global::Fatya.Properties.Resources.Logout;
            this.HdbExit.IsButton = true;
            this.HdbExit.Text = "Quitter";
            // 
            // TlcMain
            // 
            this.TlcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TlcMain.Controls.Add(this.TbpFormation);
            this.TlcMain.Controls.Add(this.TbpClass);
            this.TlcMain.Controls.Add(this.TbpModule);
            this.TlcMain.Controls.Add(this.TbpSubject);
            this.TlcMain.Controls.Add(this.TbpTeacher);
            this.TlcMain.Controls.Add(this.TbpStudent);
            this.TlcMain.Controls.Add(this.TbpRequest);
            this.TlcMain.Controls.Add(this.TbpMarks);
            this.TlcMain.Controls.Add(this.TbpAbsence);
            this.TlcMain.DesignIndex = 7;
            this.TlcMain.Location = new System.Drawing.Point(0, 109);
            this.TlcMain.Name = "TlcMain";
            this.TlcMain.SelectedIndex = 0;
            this.TlcMain.ShowTabs = true;
            this.TlcMain.Size = new System.Drawing.Size(944, 448);
            this.TlcMain.TabIndex = 1;
            // 
            // TbpFormation
            // 
            this.TbpFormation.Controls.Add(this.TxtFormationSearch);
            this.TbpFormation.Controls.Add(this.BtnFormationMisc);
            this.TbpFormation.Controls.Add(this.BtnFormationRemove);
            this.TbpFormation.Controls.Add(this.BtnFormationEdit);
            this.TbpFormation.Controls.Add(this.BtnFormationFilter);
            this.TbpFormation.Controls.Add(this.BtnFormationAdd);
            this.TbpFormation.Controls.Add(this.LsvFormation);
            this.TbpFormation.Location = new System.Drawing.Point(4, 22);
            this.TbpFormation.Name = "TbpFormation";
            this.TbpFormation.Padding = new System.Windows.Forms.Padding(3);
            this.TbpFormation.Size = new System.Drawing.Size(936, 422);
            this.TbpFormation.TabIndex = 3;
            this.TbpFormation.Text = "Formation";
            // 
            // TxtFormationSearch
            // 
            this.TxtFormationSearch.AllowAutoCompleteDuplicates = false;
            this.TxtFormationSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtFormationSearch.AutoCompleteCustomSource = ((System.Collections.Specialized.StringCollection)(resources.GetObject("TxtFormationSearch.AutoCompleteCustomSource")));
            this.TxtFormationSearch.BackColor = System.Drawing.SystemColors.Window;
            this.TxtFormationSearch.Buttons.Add(this.TxbFormation);
            this.TxtFormationSearch.CaretPosition = 0;
            this.TxtFormationSearch.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtFormationSearch.Location = new System.Drawing.Point(620, 7);
            this.TxtFormationSearch.Name = "TxtFormationSearch";
            this.TxtFormationSearch.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TxtFormationSearch.Placeholder = "Rechercher";
            this.TxtFormationSearch.SelectionForeColor = System.Drawing.Color.Black;
            this.TxtFormationSearch.SelectionHead = -1;
            this.TxtFormationSearch.Size = new System.Drawing.Size(312, 29);
            this.TxtFormationSearch.TabIndex = 14;
            this.TxtFormationSearch.TextChangedDelay = 500;
            this.TxtFormationSearch.TextChangedDelayed += new System.EventHandler(this.TxtFormationSearch_TextChangedDelayed);
            this.TxtFormationSearch.ButtonClicked += new System.EventHandler<Fatya.Controls.ButtonEventArgs>(this.TxtFormationSearch_ButtonClicked);
            this.TxtFormationSearch.AutoCompleteSelected += new Fatya.Controls.TxtBox.AutoCompleteEventHandler(this.TxtFormationSearch_AutoCompleteSelected);
            // 
            // TxbFormation
            // 
            this.TxbFormation.Enabled = true;
            this.TxbFormation.Image = global::Fatya.Properties.Resources.Search;
            // 
            // BtnFormationMisc
            // 
            this.BtnFormationMisc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnFormationMisc.AutoSize = true;
            this.BtnFormationMisc.ContextMenuStrip = this.CmsFormation;
            this.BtnFormationMisc.Enabled = false;
            this.BtnFormationMisc.IsDropDown = true;
            this.BtnFormationMisc.Location = new System.Drawing.Point(673, 393);
            this.BtnFormationMisc.Name = "BtnFormationMisc";
            this.BtnFormationMisc.Size = new System.Drawing.Size(102, 29);
            this.BtnFormationMisc.SplitMenuStrip = this.CmsFormation;
            this.BtnFormationMisc.TabIndex = 19;
            this.BtnFormationMisc.Text = "Autres";
            this.BtnFormationMisc.UseVisualStyleBackColor = true;
            // 
            // BtnFormationRemove
            // 
            this.BtnFormationRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnFormationRemove.Enabled = false;
            this.BtnFormationRemove.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnFormationRemove.Location = new System.Drawing.Point(453, 393);
            this.BtnFormationRemove.Name = "BtnFormationRemove";
            this.BtnFormationRemove.Size = new System.Drawing.Size(214, 29);
            this.BtnFormationRemove.TabIndex = 18;
            this.BtnFormationRemove.Text = "Supprimer les formations sélectionnées";
            this.BtnFormationRemove.UseVisualStyleBackColor = true;
            this.BtnFormationRemove.Click += new System.EventHandler(this.BtnFormationRemove_Click);
            // 
            // BtnFormationEdit
            // 
            this.BtnFormationEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnFormationEdit.Enabled = false;
            this.BtnFormationEdit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnFormationEdit.Location = new System.Drawing.Point(233, 393);
            this.BtnFormationEdit.Name = "BtnFormationEdit";
            this.BtnFormationEdit.Size = new System.Drawing.Size(214, 29);
            this.BtnFormationEdit.TabIndex = 17;
            this.BtnFormationEdit.Text = "Modifier les formations sélectionnées";
            this.BtnFormationEdit.UseVisualStyleBackColor = true;
            this.BtnFormationEdit.Click += new System.EventHandler(this.BtnFormationEdit_Click);
            // 
            // BtnFormationFilter
            // 
            this.BtnFormationFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnFormationFilter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnFormationFilter.Location = new System.Drawing.Point(856, 393);
            this.BtnFormationFilter.Name = "BtnFormationFilter";
            this.BtnFormationFilter.Size = new System.Drawing.Size(76, 29);
            this.BtnFormationFilter.TabIndex = 20;
            this.BtnFormationFilter.Text = "Filtrer...";
            this.BtnFormationFilter.UseVisualStyleBackColor = true;
            this.BtnFormationFilter.Click += new System.EventHandler(this.BtnFormationFilter_Click);
            // 
            // BtnFormationAdd
            // 
            this.BtnFormationAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnFormationAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnFormationAdd.Location = new System.Drawing.Point(13, 393);
            this.BtnFormationAdd.Name = "BtnFormationAdd";
            this.BtnFormationAdd.Size = new System.Drawing.Size(214, 29);
            this.BtnFormationAdd.TabIndex = 16;
            this.BtnFormationAdd.Text = "Ajouter une formation";
            this.BtnFormationAdd.UseVisualStyleBackColor = true;
            this.BtnFormationAdd.Click += new System.EventHandler(this.BtnFormationAdd_Click);
            // 
            // LsvFormation
            // 
            this.LsvFormation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LsvFormation.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            ColFormationId,
            ColFormationName,
            ColFormationDuration,
            this.ColFormationStudents,
            this.ColFormationClasses,
            ColFormationCreation,
            ColFormationDescription});
            this.LsvFormation.ContextMenuStrip = this.CmsFormation;
            this.LsvFormation.FullRowSelect = true;
            this.LsvFormation.HideSelection = false;
            this.LsvFormation.Location = new System.Drawing.Point(12, 42);
            this.LsvFormation.Name = "LsvFormation";
            this.LsvFormation.Size = new System.Drawing.Size(920, 345);
            this.LsvFormation.TabIndex = 15;
            this.LsvFormation.UseCompatibleStateImageBehavior = false;
            this.LsvFormation.View = System.Windows.Forms.View.Details;
            this.LsvFormation.SelectedIndexChanged += new System.EventHandler(this.LsvFormation_SelectedIndexChanged);
            this.LsvFormation.DoubleClick += new System.EventHandler(this.LsvFormation_DoubleClick);
            // 
            // ColFormationStudents
            // 
            this.ColFormationStudents.Text = "Effectif";
            // 
            // ColFormationClasses
            // 
            this.ColFormationClasses.Text = "Classes";
            // 
            // TbpClass
            // 
            this.TbpClass.Controls.Add(this.TxtClassSearch);
            this.TbpClass.Controls.Add(this.BtnClassMisc);
            this.TbpClass.Controls.Add(this.BtnClassRemove);
            this.TbpClass.Controls.Add(this.BtnClassEdit);
            this.TbpClass.Controls.Add(this.BtnClassFilter);
            this.TbpClass.Controls.Add(this.BtnClassAdd);
            this.TbpClass.Controls.Add(this.LsvClass);
            this.TbpClass.Location = new System.Drawing.Point(4, 22);
            this.TbpClass.Name = "TbpClass";
            this.TbpClass.Padding = new System.Windows.Forms.Padding(3);
            this.TbpClass.Size = new System.Drawing.Size(936, 422);
            this.TbpClass.TabIndex = 2;
            this.TbpClass.Text = "Class";
            // 
            // TxtClassSearch
            // 
            this.TxtClassSearch.AllowAutoCompleteDuplicates = false;
            this.TxtClassSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtClassSearch.AutoCompleteCustomSource = ((System.Collections.Specialized.StringCollection)(resources.GetObject("TxtClassSearch.AutoCompleteCustomSource")));
            this.TxtClassSearch.BackColor = System.Drawing.SystemColors.Window;
            this.TxtClassSearch.Buttons.Add(this.TxbClass);
            this.TxtClassSearch.CaretPosition = 0;
            this.TxtClassSearch.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtClassSearch.Location = new System.Drawing.Point(620, 7);
            this.TxtClassSearch.Name = "TxtClassSearch";
            this.TxtClassSearch.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TxtClassSearch.Placeholder = "Rechercher";
            this.TxtClassSearch.SelectionForeColor = System.Drawing.Color.Black;
            this.TxtClassSearch.SelectionHead = -1;
            this.TxtClassSearch.Size = new System.Drawing.Size(312, 29);
            this.TxtClassSearch.TabIndex = 21;
            this.TxtClassSearch.TextChangedDelay = 500;
            this.TxtClassSearch.TextChangedDelayed += new System.EventHandler(this.TxtClassSearch_TextChangedDelayed);
            this.TxtClassSearch.ButtonClicked += new System.EventHandler<Fatya.Controls.ButtonEventArgs>(this.TxtClassSearch_ButtonClicked);
            this.TxtClassSearch.AutoCompleteSelected += new Fatya.Controls.TxtBox.AutoCompleteEventHandler(this.TxtClassSearch_AutoCompleteSelected);
            // 
            // TxbClass
            // 
            this.TxbClass.Enabled = true;
            this.TxbClass.Image = global::Fatya.Properties.Resources.Search;
            // 
            // BtnClassMisc
            // 
            this.BtnClassMisc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnClassMisc.AutoSize = true;
            this.BtnClassMisc.ContextMenuStrip = this.CmsClass;
            this.BtnClassMisc.Enabled = false;
            this.BtnClassMisc.IsDropDown = true;
            this.BtnClassMisc.Location = new System.Drawing.Point(673, 393);
            this.BtnClassMisc.Name = "BtnClassMisc";
            this.BtnClassMisc.Size = new System.Drawing.Size(102, 29);
            this.BtnClassMisc.SplitMenuStrip = this.CmsClass;
            this.BtnClassMisc.TabIndex = 26;
            this.BtnClassMisc.Text = "Autres";
            this.BtnClassMisc.UseVisualStyleBackColor = true;
            // 
            // BtnClassRemove
            // 
            this.BtnClassRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnClassRemove.Enabled = false;
            this.BtnClassRemove.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnClassRemove.Location = new System.Drawing.Point(453, 393);
            this.BtnClassRemove.Name = "BtnClassRemove";
            this.BtnClassRemove.Size = new System.Drawing.Size(214, 29);
            this.BtnClassRemove.TabIndex = 25;
            this.BtnClassRemove.Text = "Supprimer les classes sélectionnées";
            this.BtnClassRemove.UseVisualStyleBackColor = true;
            this.BtnClassRemove.Click += new System.EventHandler(this.BtnClassRemove_Click);
            // 
            // BtnClassEdit
            // 
            this.BtnClassEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnClassEdit.Enabled = false;
            this.BtnClassEdit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnClassEdit.Location = new System.Drawing.Point(233, 393);
            this.BtnClassEdit.Name = "BtnClassEdit";
            this.BtnClassEdit.Size = new System.Drawing.Size(214, 29);
            this.BtnClassEdit.TabIndex = 24;
            this.BtnClassEdit.Text = "Modifier les classes sélectionnées";
            this.BtnClassEdit.UseVisualStyleBackColor = true;
            this.BtnClassEdit.Click += new System.EventHandler(this.BtnClassEdit_Click);
            // 
            // BtnClassFilter
            // 
            this.BtnClassFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnClassFilter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnClassFilter.Location = new System.Drawing.Point(856, 393);
            this.BtnClassFilter.Name = "BtnClassFilter";
            this.BtnClassFilter.Size = new System.Drawing.Size(76, 29);
            this.BtnClassFilter.TabIndex = 27;
            this.BtnClassFilter.Text = "Filtrer...";
            this.BtnClassFilter.UseVisualStyleBackColor = true;
            this.BtnClassFilter.Click += new System.EventHandler(this.BtnClassFilter_Click);
            // 
            // BtnClassAdd
            // 
            this.BtnClassAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnClassAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnClassAdd.Location = new System.Drawing.Point(13, 393);
            this.BtnClassAdd.Name = "BtnClassAdd";
            this.BtnClassAdd.Size = new System.Drawing.Size(214, 29);
            this.BtnClassAdd.TabIndex = 23;
            this.BtnClassAdd.Text = "Ajouter une classe";
            this.BtnClassAdd.UseVisualStyleBackColor = true;
            this.BtnClassAdd.Click += new System.EventHandler(this.BtnClassAdd_Click);
            // 
            // LsvClass
            // 
            this.LsvClass.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LsvClass.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            ColClassId,
            this.ColClassName,
            ColClassStarted,
            this.ColClassModules,
            this.ColClassCount});
            this.LsvClass.ContextMenuStrip = this.CmsClass;
            this.LsvClass.FullRowSelect = true;
            this.LsvClass.HideSelection = false;
            this.LsvClass.Location = new System.Drawing.Point(12, 42);
            this.LsvClass.Name = "LsvClass";
            this.LsvClass.Size = new System.Drawing.Size(920, 345);
            this.LsvClass.TabIndex = 22;
            this.LsvClass.UseCompatibleStateImageBehavior = false;
            this.LsvClass.View = System.Windows.Forms.View.Details;
            this.LsvClass.SelectedIndexChanged += new System.EventHandler(this.LsvClass_SelectedIndexChanged);
            this.LsvClass.DoubleClick += new System.EventHandler(this.LsvClass_DoubleClick);
            // 
            // ColClassName
            // 
            this.ColClassName.Text = "Nom";
            // 
            // ColClassModules
            // 
            this.ColClassModules.Text = "Modules";
            // 
            // ColClassCount
            // 
            this.ColClassCount.Text = "Effectif";
            // 
            // TbpModule
            // 
            this.TbpModule.Controls.Add(this.TxtModuleSearch);
            this.TbpModule.Controls.Add(this.BtnModuleMisc);
            this.TbpModule.Controls.Add(this.BtnModuleRemove);
            this.TbpModule.Controls.Add(this.BtnModuleEdit);
            this.TbpModule.Controls.Add(this.BtnModuleFilter);
            this.TbpModule.Controls.Add(this.BtnModuleAdd);
            this.TbpModule.Controls.Add(this.LsvModule);
            this.TbpModule.Location = new System.Drawing.Point(4, 22);
            this.TbpModule.Name = "TbpModule";
            this.TbpModule.Size = new System.Drawing.Size(936, 422);
            this.TbpModule.TabIndex = 5;
            this.TbpModule.Text = "Module";
            // 
            // TxtModuleSearch
            // 
            this.TxtModuleSearch.AllowAutoCompleteDuplicates = false;
            this.TxtModuleSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtModuleSearch.AutoCompleteCustomSource = ((System.Collections.Specialized.StringCollection)(resources.GetObject("TxtModuleSearch.AutoCompleteCustomSource")));
            this.TxtModuleSearch.BackColor = System.Drawing.SystemColors.Window;
            this.TxtModuleSearch.Buttons.Add(this.TxbModule);
            this.TxtModuleSearch.CaretPosition = 0;
            this.TxtModuleSearch.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtModuleSearch.Location = new System.Drawing.Point(620, 7);
            this.TxtModuleSearch.Name = "TxtModuleSearch";
            this.TxtModuleSearch.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TxtModuleSearch.Placeholder = "Rechercher";
            this.TxtModuleSearch.SelectionForeColor = System.Drawing.Color.Black;
            this.TxtModuleSearch.SelectionHead = -1;
            this.TxtModuleSearch.Size = new System.Drawing.Size(312, 29);
            this.TxtModuleSearch.TabIndex = 14;
            this.TxtModuleSearch.TextChangedDelay = 500;
            this.TxtModuleSearch.TextChangedDelayed += new System.EventHandler(this.TxtModuleSearch_TextChangedDelayed);
            this.TxtModuleSearch.ButtonClicked += new System.EventHandler<Fatya.Controls.ButtonEventArgs>(this.TxtModuleSearch_ButtonClicked);
            this.TxtModuleSearch.AutoCompleteSelected += new Fatya.Controls.TxtBox.AutoCompleteEventHandler(this.TxtModuleSearch_AutoCompleteSelected);
            // 
            // TxbModule
            // 
            this.TxbModule.Enabled = true;
            this.TxbModule.Image = global::Fatya.Properties.Resources.Search;
            // 
            // BtnModuleMisc
            // 
            this.BtnModuleMisc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnModuleMisc.AutoSize = true;
            this.BtnModuleMisc.ContextMenuStrip = this.CmsModule;
            this.BtnModuleMisc.Enabled = false;
            this.BtnModuleMisc.IsDropDown = true;
            this.BtnModuleMisc.Location = new System.Drawing.Point(613, 389);
            this.BtnModuleMisc.Name = "BtnModuleMisc";
            this.BtnModuleMisc.Size = new System.Drawing.Size(102, 29);
            this.BtnModuleMisc.SplitMenuStrip = this.CmsModule;
            this.BtnModuleMisc.TabIndex = 19;
            this.BtnModuleMisc.Text = "Autres";
            this.BtnModuleMisc.UseVisualStyleBackColor = true;
            // 
            // BtnModuleRemove
            // 
            this.BtnModuleRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnModuleRemove.Enabled = false;
            this.BtnModuleRemove.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnModuleRemove.Location = new System.Drawing.Point(413, 389);
            this.BtnModuleRemove.Name = "BtnModuleRemove";
            this.BtnModuleRemove.Size = new System.Drawing.Size(194, 29);
            this.BtnModuleRemove.TabIndex = 18;
            this.BtnModuleRemove.Text = "Supprimer les modules sélectionnés";
            this.BtnModuleRemove.UseVisualStyleBackColor = true;
            this.BtnModuleRemove.Click += new System.EventHandler(this.BtnModuleRemove_Click);
            // 
            // BtnModuleEdit
            // 
            this.BtnModuleEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnModuleEdit.Enabled = false;
            this.BtnModuleEdit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnModuleEdit.Location = new System.Drawing.Point(213, 389);
            this.BtnModuleEdit.Name = "BtnModuleEdit";
            this.BtnModuleEdit.Size = new System.Drawing.Size(194, 29);
            this.BtnModuleEdit.TabIndex = 17;
            this.BtnModuleEdit.Text = "Modier les modules sélectionnés";
            this.BtnModuleEdit.UseVisualStyleBackColor = true;
            this.BtnModuleEdit.Click += new System.EventHandler(this.BtnModuleEdit_Click);
            // 
            // BtnModuleFilter
            // 
            this.BtnModuleFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnModuleFilter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnModuleFilter.Location = new System.Drawing.Point(856, 389);
            this.BtnModuleFilter.Name = "BtnModuleFilter";
            this.BtnModuleFilter.Size = new System.Drawing.Size(76, 29);
            this.BtnModuleFilter.TabIndex = 20;
            this.BtnModuleFilter.Text = "Filtrer...";
            this.BtnModuleFilter.UseVisualStyleBackColor = true;
            this.BtnModuleFilter.Click += new System.EventHandler(this.BtnModuleFilter_Click);
            // 
            // BtnModuleAdd
            // 
            this.BtnModuleAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnModuleAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnModuleAdd.Location = new System.Drawing.Point(13, 389);
            this.BtnModuleAdd.Name = "BtnModuleAdd";
            this.BtnModuleAdd.Size = new System.Drawing.Size(194, 29);
            this.BtnModuleAdd.TabIndex = 16;
            this.BtnModuleAdd.Text = "Ajouter un module";
            this.BtnModuleAdd.UseVisualStyleBackColor = true;
            this.BtnModuleAdd.Click += new System.EventHandler(this.BtnModuleAdd_Click);
            // 
            // LsvModule
            // 
            this.LsvModule.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LsvModule.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            ColModuleId,
            ColModuleName,
            this.ColModuleMin,
            ColmoduleCoef,
            ColModuleSubjects,
            ColModuleClasse});
            this.LsvModule.ContextMenuStrip = this.CmsModule;
            this.LsvModule.FullRowSelect = true;
            this.LsvModule.HideSelection = false;
            this.LsvModule.Location = new System.Drawing.Point(12, 42);
            this.LsvModule.Name = "LsvModule";
            this.LsvModule.Size = new System.Drawing.Size(920, 341);
            this.LsvModule.TabIndex = 15;
            this.LsvModule.UseCompatibleStateImageBehavior = false;
            this.LsvModule.View = System.Windows.Forms.View.Details;
            this.LsvModule.SelectedIndexChanged += new System.EventHandler(this.LsvModule_SelectedIndexChanged);
            this.LsvModule.DoubleClick += new System.EventHandler(this.LsvModule_DoubleClick);
            // 
            // ColModuleMin
            // 
            this.ColModuleMin.Text = "Note éliminatoire";
            // 
            // TbpSubject
            // 
            this.TbpSubject.Controls.Add(this.TxtSubjectSearch);
            this.TbpSubject.Controls.Add(this.BtnSubjectMisc);
            this.TbpSubject.Controls.Add(this.BtnSubjectRemove);
            this.TbpSubject.Controls.Add(this.BtnSubjectEdit);
            this.TbpSubject.Controls.Add(this.BtnSubjectFilter);
            this.TbpSubject.Controls.Add(this.BtnSubjectAdd);
            this.TbpSubject.Controls.Add(this.LsvSubject);
            this.TbpSubject.Location = new System.Drawing.Point(4, 22);
            this.TbpSubject.Name = "TbpSubject";
            this.TbpSubject.Size = new System.Drawing.Size(936, 422);
            this.TbpSubject.TabIndex = 6;
            this.TbpSubject.Text = "Subject";
            // 
            // TxtSubjectSearch
            // 
            this.TxtSubjectSearch.AllowAutoCompleteDuplicates = false;
            this.TxtSubjectSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtSubjectSearch.AutoCompleteCustomSource = ((System.Collections.Specialized.StringCollection)(resources.GetObject("TxtSubjectSearch.AutoCompleteCustomSource")));
            this.TxtSubjectSearch.BackColor = System.Drawing.SystemColors.Window;
            this.TxtSubjectSearch.Buttons.Add(this.TxbSubject);
            this.TxtSubjectSearch.CaretPosition = 0;
            this.TxtSubjectSearch.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtSubjectSearch.Location = new System.Drawing.Point(620, 7);
            this.TxtSubjectSearch.Name = "TxtSubjectSearch";
            this.TxtSubjectSearch.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TxtSubjectSearch.Placeholder = "Rechercher";
            this.TxtSubjectSearch.SelectionForeColor = System.Drawing.Color.Black;
            this.TxtSubjectSearch.SelectionHead = -1;
            this.TxtSubjectSearch.Size = new System.Drawing.Size(312, 29);
            this.TxtSubjectSearch.TabIndex = 21;
            this.TxtSubjectSearch.TextChangedDelay = 500;
            this.TxtSubjectSearch.TextChangedDelayed += new System.EventHandler(this.TxtSubjectSearch_TextChangedDelayed);
            this.TxtSubjectSearch.ButtonClicked += new System.EventHandler<Fatya.Controls.ButtonEventArgs>(this.TxtSubjectSearch_ButtonClicked);
            this.TxtSubjectSearch.AutoCompleteSelected += new Fatya.Controls.TxtBox.AutoCompleteEventHandler(this.TxtSubjectSearch_AutoCompleteSelected);
            // 
            // TxbSubject
            // 
            this.TxbSubject.Enabled = true;
            this.TxbSubject.Image = global::Fatya.Properties.Resources.Search;
            // 
            // BtnSubjectMisc
            // 
            this.BtnSubjectMisc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnSubjectMisc.AutoSize = true;
            this.BtnSubjectMisc.ContextMenuStrip = this.CmsSubject;
            this.BtnSubjectMisc.Enabled = false;
            this.BtnSubjectMisc.IsDropDown = true;
            this.BtnSubjectMisc.Location = new System.Drawing.Point(638, 393);
            this.BtnSubjectMisc.Name = "BtnSubjectMisc";
            this.BtnSubjectMisc.Size = new System.Drawing.Size(102, 29);
            this.BtnSubjectMisc.SplitMenuStrip = this.CmsSubject;
            this.BtnSubjectMisc.TabIndex = 26;
            this.BtnSubjectMisc.Text = "Autres";
            this.BtnSubjectMisc.UseVisualStyleBackColor = true;
            // 
            // BtnSubjectRemove
            // 
            this.BtnSubjectRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnSubjectRemove.Enabled = false;
            this.BtnSubjectRemove.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnSubjectRemove.Location = new System.Drawing.Point(429, 393);
            this.BtnSubjectRemove.Name = "BtnSubjectRemove";
            this.BtnSubjectRemove.Size = new System.Drawing.Size(203, 29);
            this.BtnSubjectRemove.TabIndex = 25;
            this.BtnSubjectRemove.Text = "Supprimer les matières sélectionnées";
            this.BtnSubjectRemove.UseVisualStyleBackColor = true;
            this.BtnSubjectRemove.Click += new System.EventHandler(this.BtnSubjectRemove_Click);
            // 
            // BtnSubjectEdit
            // 
            this.BtnSubjectEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnSubjectEdit.Enabled = false;
            this.BtnSubjectEdit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnSubjectEdit.Location = new System.Drawing.Point(220, 393);
            this.BtnSubjectEdit.Name = "BtnSubjectEdit";
            this.BtnSubjectEdit.Size = new System.Drawing.Size(203, 29);
            this.BtnSubjectEdit.TabIndex = 24;
            this.BtnSubjectEdit.Text = "Modier les matières sélectionnées";
            this.BtnSubjectEdit.UseVisualStyleBackColor = true;
            this.BtnSubjectEdit.Click += new System.EventHandler(this.BtnSubjectEdit_Click);
            // 
            // BtnSubjectFilter
            // 
            this.BtnSubjectFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnSubjectFilter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnSubjectFilter.Location = new System.Drawing.Point(856, 393);
            this.BtnSubjectFilter.Name = "BtnSubjectFilter";
            this.BtnSubjectFilter.Size = new System.Drawing.Size(76, 29);
            this.BtnSubjectFilter.TabIndex = 27;
            this.BtnSubjectFilter.Text = "Filtrer...";
            this.BtnSubjectFilter.UseVisualStyleBackColor = true;
            this.BtnSubjectFilter.Click += new System.EventHandler(this.BtnSubjectFilter_Click);
            // 
            // BtnSubjectAdd
            // 
            this.BtnSubjectAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnSubjectAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnSubjectAdd.Location = new System.Drawing.Point(13, 393);
            this.BtnSubjectAdd.Name = "BtnSubjectAdd";
            this.BtnSubjectAdd.Size = new System.Drawing.Size(203, 29);
            this.BtnSubjectAdd.TabIndex = 23;
            this.BtnSubjectAdd.Text = "Ajouter une matière";
            this.BtnSubjectAdd.UseVisualStyleBackColor = true;
            this.BtnSubjectAdd.Click += new System.EventHandler(this.BtnSubjectAdd_Click);
            // 
            // LsvSubject
            // 
            this.LsvSubject.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LsvSubject.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            ColSubjectId,
            ColSubjectName,
            this.ColSubjectMinMark,
            ColSubjectCoef,
            this.ColSubjectTeacher,
            ColSubjectClass,
            ColSubjectModule});
            this.LsvSubject.ContextMenuStrip = this.CmsSubject;
            this.LsvSubject.FullRowSelect = true;
            this.LsvSubject.HideSelection = false;
            this.LsvSubject.Location = new System.Drawing.Point(12, 42);
            this.LsvSubject.Name = "LsvSubject";
            this.LsvSubject.Size = new System.Drawing.Size(920, 345);
            this.LsvSubject.TabIndex = 22;
            this.LsvSubject.UseCompatibleStateImageBehavior = false;
            this.LsvSubject.View = System.Windows.Forms.View.Details;
            this.LsvSubject.SelectedIndexChanged += new System.EventHandler(this.LsvSubject_SelectedIndexChanged);
            this.LsvSubject.DoubleClick += new System.EventHandler(this.LsvSubject_DoubleClick);
            // 
            // ColSubjectMinMark
            // 
            this.ColSubjectMinMark.Text = "Note éliminatoire";
            // 
            // ColSubjectTeacher
            // 
            this.ColSubjectTeacher.Text = "Enseignant";
            // 
            // TbpTeacher
            // 
            this.TbpTeacher.Controls.Add(this.TxtTeacherSearch);
            this.TbpTeacher.Controls.Add(this.BtnTeacherMisc);
            this.TbpTeacher.Controls.Add(this.BtnTeacherDelete);
            this.TbpTeacher.Controls.Add(this.BtnTeacherEdit);
            this.TbpTeacher.Controls.Add(this.BtnTeacherFilter);
            this.TbpTeacher.Controls.Add(this.BtnTeacherAdd);
            this.TbpTeacher.Controls.Add(this.LsvTeacher);
            this.TbpTeacher.Location = new System.Drawing.Point(4, 22);
            this.TbpTeacher.Name = "TbpTeacher";
            this.TbpTeacher.Padding = new System.Windows.Forms.Padding(3);
            this.TbpTeacher.Size = new System.Drawing.Size(936, 422);
            this.TbpTeacher.TabIndex = 1;
            this.TbpTeacher.Text = "Teacher";
            // 
            // TxtTeacherSearch
            // 
            this.TxtTeacherSearch.AllowAutoCompleteDuplicates = false;
            this.TxtTeacherSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtTeacherSearch.AutoCompleteCustomSource = ((System.Collections.Specialized.StringCollection)(resources.GetObject("TxtTeacherSearch.AutoCompleteCustomSource")));
            this.TxtTeacherSearch.BackColor = System.Drawing.SystemColors.Window;
            this.TxtTeacherSearch.Buttons.Add(this.TxbTeacher);
            this.TxtTeacherSearch.CaretPosition = 0;
            this.TxtTeacherSearch.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtTeacherSearch.Location = new System.Drawing.Point(620, 7);
            this.TxtTeacherSearch.Name = "TxtTeacherSearch";
            this.TxtTeacherSearch.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TxtTeacherSearch.Placeholder = "Rechercher";
            this.TxtTeacherSearch.SelectionForeColor = System.Drawing.Color.Black;
            this.TxtTeacherSearch.SelectionHead = -1;
            this.TxtTeacherSearch.Size = new System.Drawing.Size(312, 29);
            this.TxtTeacherSearch.TabIndex = 7;
            this.TxtTeacherSearch.TextChangedDelay = 500;
            this.TxtTeacherSearch.TextChangedDelayed += new System.EventHandler(this.TxtTeacherSearch_TextChangedDelayed);
            this.TxtTeacherSearch.ButtonClicked += new System.EventHandler<Fatya.Controls.ButtonEventArgs>(this.TxtTeacherSearch_ButtonClicked);
            this.TxtTeacherSearch.AutoCompleteSelected += new Fatya.Controls.TxtBox.AutoCompleteEventHandler(this.TxtTeacherSearch_AutoCompleteSelected);
            // 
            // TxbTeacher
            // 
            this.TxbTeacher.Enabled = true;
            this.TxbTeacher.Image = global::Fatya.Properties.Resources.Search;
            // 
            // BtnTeacherMisc
            // 
            this.BtnTeacherMisc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnTeacherMisc.AutoSize = true;
            this.BtnTeacherMisc.ContextMenuStrip = this.CmsTeacher;
            this.BtnTeacherMisc.Enabled = false;
            this.BtnTeacherMisc.IsDropDown = true;
            this.BtnTeacherMisc.Location = new System.Drawing.Point(673, 393);
            this.BtnTeacherMisc.Name = "BtnTeacherMisc";
            this.BtnTeacherMisc.Size = new System.Drawing.Size(102, 29);
            this.BtnTeacherMisc.SplitMenuStrip = this.CmsTeacher;
            this.BtnTeacherMisc.TabIndex = 12;
            this.BtnTeacherMisc.Text = "Autres";
            this.BtnTeacherMisc.UseVisualStyleBackColor = true;
            // 
            // BtnTeacherDelete
            // 
            this.BtnTeacherDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnTeacherDelete.Enabled = false;
            this.BtnTeacherDelete.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnTeacherDelete.Location = new System.Drawing.Point(453, 393);
            this.BtnTeacherDelete.Name = "BtnTeacherDelete";
            this.BtnTeacherDelete.Size = new System.Drawing.Size(214, 29);
            this.BtnTeacherDelete.TabIndex = 11;
            this.BtnTeacherDelete.Text = "Supprimer les enseignants sélectionnés";
            this.BtnTeacherDelete.UseVisualStyleBackColor = true;
            this.BtnTeacherDelete.Click += new System.EventHandler(this.BtnTeacherDelete_Click);
            // 
            // BtnTeacherEdit
            // 
            this.BtnTeacherEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnTeacherEdit.Enabled = false;
            this.BtnTeacherEdit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnTeacherEdit.Location = new System.Drawing.Point(233, 393);
            this.BtnTeacherEdit.Name = "BtnTeacherEdit";
            this.BtnTeacherEdit.Size = new System.Drawing.Size(214, 29);
            this.BtnTeacherEdit.TabIndex = 10;
            this.BtnTeacherEdit.Text = "Modifier les enseignants sélectionnés";
            this.BtnTeacherEdit.UseVisualStyleBackColor = true;
            this.BtnTeacherEdit.Click += new System.EventHandler(this.BtnTeacherEdit_Click);
            // 
            // BtnTeacherFilter
            // 
            this.BtnTeacherFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnTeacherFilter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnTeacherFilter.Location = new System.Drawing.Point(856, 393);
            this.BtnTeacherFilter.Name = "BtnTeacherFilter";
            this.BtnTeacherFilter.Size = new System.Drawing.Size(76, 29);
            this.BtnTeacherFilter.TabIndex = 13;
            this.BtnTeacherFilter.Text = "Filtrer...";
            this.BtnTeacherFilter.UseVisualStyleBackColor = true;
            this.BtnTeacherFilter.Click += new System.EventHandler(this.BtnTeacherFilter_Click);
            // 
            // BtnTeacherAdd
            // 
            this.BtnTeacherAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnTeacherAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnTeacherAdd.Location = new System.Drawing.Point(13, 393);
            this.BtnTeacherAdd.Name = "BtnTeacherAdd";
            this.BtnTeacherAdd.Size = new System.Drawing.Size(214, 29);
            this.BtnTeacherAdd.TabIndex = 9;
            this.BtnTeacherAdd.Text = "Ajouter un enseignant";
            this.BtnTeacherAdd.UseVisualStyleBackColor = true;
            this.BtnTeacherAdd.Click += new System.EventHandler(this.BtnTeacherAdd_Click);
            // 
            // LsvTeacher
            // 
            this.LsvTeacher.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LsvTeacher.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            ColTeacherId,
            ColTeacherCin,
            this.ColSum,
            ColTeacherSex,
            ColTeacherName,
            ColTeacherEmail,
            ColTeacherTel,
            ColTeacherAddress});
            this.LsvTeacher.ContextMenuStrip = this.CmsTeacher;
            this.LsvTeacher.FullRowSelect = true;
            this.LsvTeacher.HideSelection = false;
            this.LsvTeacher.Location = new System.Drawing.Point(12, 42);
            this.LsvTeacher.Name = "LsvTeacher";
            this.LsvTeacher.Size = new System.Drawing.Size(920, 345);
            this.LsvTeacher.TabIndex = 8;
            this.LsvTeacher.UseCompatibleStateImageBehavior = false;
            this.LsvTeacher.View = System.Windows.Forms.View.Details;
            this.LsvTeacher.SelectedIndexChanged += new System.EventHandler(this.LsvTeacher_SelectedIndexChanged);
            this.LsvTeacher.DoubleClick += new System.EventHandler(this.LsvTeacher_DoubleClick);
            // 
            // ColSum
            // 
            this.ColSum.Text = "N° de somme";
            // 
            // TbpStudent
            // 
            this.TbpStudent.Controls.Add(this.TxtStudentSearch);
            this.TbpStudent.Controls.Add(this.BtnStudentMisc);
            this.TbpStudent.Controls.Add(this.BtnStudentRemove);
            this.TbpStudent.Controls.Add(this.BtnStudentEdit);
            this.TbpStudent.Controls.Add(this.BtnStudentFilter);
            this.TbpStudent.Controls.Add(this.BtnStudentAdd);
            this.TbpStudent.Controls.Add(this.LsvStudent);
            this.TbpStudent.Location = new System.Drawing.Point(4, 22);
            this.TbpStudent.Name = "TbpStudent";
            this.TbpStudent.Padding = new System.Windows.Forms.Padding(3);
            this.TbpStudent.Size = new System.Drawing.Size(936, 422);
            this.TbpStudent.TabIndex = 0;
            this.TbpStudent.Text = "Student";
            this.TbpStudent.Click += new System.EventHandler(this.TbpStudent_Click);
            // 
            // TxtStudentSearch
            // 
            this.TxtStudentSearch.AllowAutoCompleteDuplicates = false;
            this.TxtStudentSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtStudentSearch.AutoCompleteCustomSource = ((System.Collections.Specialized.StringCollection)(resources.GetObject("TxtStudentSearch.AutoCompleteCustomSource")));
            this.TxtStudentSearch.BackColor = System.Drawing.SystemColors.Window;
            this.TxtStudentSearch.Buttons.Add(this.TxbStudent);
            this.TxtStudentSearch.CaretPosition = 0;
            this.TxtStudentSearch.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtStudentSearch.Location = new System.Drawing.Point(620, 7);
            this.TxtStudentSearch.Name = "TxtStudentSearch";
            this.TxtStudentSearch.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TxtStudentSearch.Placeholder = "Rechercher";
            this.TxtStudentSearch.SelectionForeColor = System.Drawing.Color.Black;
            this.TxtStudentSearch.SelectionHead = -1;
            this.TxtStudentSearch.Size = new System.Drawing.Size(312, 29);
            this.TxtStudentSearch.TabIndex = 0;
            this.TxtStudentSearch.TextChangedDelay = 500;
            this.TxtStudentSearch.TextChangedDelayed += new System.EventHandler(this.TxtStudentSearch_TextChangedDelayed);
            this.TxtStudentSearch.ButtonClicked += new System.EventHandler<Fatya.Controls.ButtonEventArgs>(this.TxtStudentSearch_ButtonClicked);
            this.TxtStudentSearch.AutoCompleteSelected += new Fatya.Controls.TxtBox.AutoCompleteEventHandler(this.TxtStudentSearch_AutoCompleteSelected);
            // 
            // TxbStudent
            // 
            this.TxbStudent.Enabled = true;
            this.TxbStudent.Image = global::Fatya.Properties.Resources.Search;
            // 
            // BtnStudentMisc
            // 
            this.BtnStudentMisc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnStudentMisc.AutoSize = true;
            this.BtnStudentMisc.ContextMenuStrip = this.CmsStudent;
            this.BtnStudentMisc.Enabled = false;
            this.BtnStudentMisc.IsDropDown = true;
            this.BtnStudentMisc.Location = new System.Drawing.Point(646, 393);
            this.BtnStudentMisc.Name = "BtnStudentMisc";
            this.BtnStudentMisc.Size = new System.Drawing.Size(102, 29);
            this.BtnStudentMisc.SplitMenuStrip = this.CmsStudent;
            this.BtnStudentMisc.TabIndex = 5;
            this.BtnStudentMisc.Text = "Autres";
            this.BtnStudentMisc.UseVisualStyleBackColor = true;
            // 
            // BtnStudentRemove
            // 
            this.BtnStudentRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnStudentRemove.Enabled = false;
            this.BtnStudentRemove.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnStudentRemove.Location = new System.Drawing.Point(435, 393);
            this.BtnStudentRemove.Name = "BtnStudentRemove";
            this.BtnStudentRemove.Size = new System.Drawing.Size(205, 29);
            this.BtnStudentRemove.TabIndex = 4;
            this.BtnStudentRemove.Text = "Supprimer les étudiants sélectionnés";
            this.BtnStudentRemove.UseVisualStyleBackColor = true;
            this.BtnStudentRemove.Click += new System.EventHandler(this.BtnStudentRemove_Click);
            // 
            // BtnStudentEdit
            // 
            this.BtnStudentEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnStudentEdit.Enabled = false;
            this.BtnStudentEdit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnStudentEdit.Location = new System.Drawing.Point(224, 393);
            this.BtnStudentEdit.Name = "BtnStudentEdit";
            this.BtnStudentEdit.Size = new System.Drawing.Size(205, 29);
            this.BtnStudentEdit.TabIndex = 3;
            this.BtnStudentEdit.Text = "Modifier les étudiants sélectionnés";
            this.BtnStudentEdit.UseVisualStyleBackColor = true;
            this.BtnStudentEdit.Click += new System.EventHandler(this.BtnStudentEdit_Click);
            // 
            // BtnStudentFilter
            // 
            this.BtnStudentFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnStudentFilter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnStudentFilter.Location = new System.Drawing.Point(856, 393);
            this.BtnStudentFilter.Name = "BtnStudentFilter";
            this.BtnStudentFilter.Size = new System.Drawing.Size(76, 29);
            this.BtnStudentFilter.TabIndex = 6;
            this.BtnStudentFilter.Text = "Filtrer...";
            this.BtnStudentFilter.UseVisualStyleBackColor = true;
            this.BtnStudentFilter.Click += new System.EventHandler(this.BtnStudentFilter_Click);
            // 
            // BtnStudentAdd
            // 
            this.BtnStudentAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnStudentAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnStudentAdd.Location = new System.Drawing.Point(13, 393);
            this.BtnStudentAdd.Name = "BtnStudentAdd";
            this.BtnStudentAdd.Size = new System.Drawing.Size(205, 29);
            this.BtnStudentAdd.TabIndex = 2;
            this.BtnStudentAdd.Text = "Ajouter un étudiant";
            this.BtnStudentAdd.UseVisualStyleBackColor = true;
            this.BtnStudentAdd.Click += new System.EventHandler(this.BtnStudentAdd_Click);
            // 
            // LsvStudent
            // 
            this.LsvStudent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LsvStudent.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            ColId,
            ColCin,
            ColCne,
            ColSex,
            ColName,
            ColStudentClass,
            ColEmail,
            ColBirthday,
            ColTel,
            ColAdress});
            this.LsvStudent.ContextMenuStrip = this.CmsStudent;
            this.LsvStudent.FullRowSelect = true;
            this.LsvStudent.HideSelection = false;
            this.LsvStudent.Location = new System.Drawing.Point(12, 42);
            this.LsvStudent.Name = "LsvStudent";
            this.LsvStudent.Size = new System.Drawing.Size(920, 345);
            this.LsvStudent.TabIndex = 1;
            this.LsvStudent.UseCompatibleStateImageBehavior = false;
            this.LsvStudent.View = System.Windows.Forms.View.Details;
            this.LsvStudent.SelectedIndexChanged += new System.EventHandler(this.LsvStudent_SelectedIndexChanged);
            this.LsvStudent.DoubleClick += new System.EventHandler(this.LsvStudent_DoubleClick);
            // 
            // TbpRequest
            // 
            this.TbpRequest.Controls.Add(this.TxtRequestSearch);
            this.TbpRequest.Controls.Add(this.BtnRequestMisc);
            this.TbpRequest.Controls.Add(this.BtnRequestRemove);
            this.TbpRequest.Controls.Add(this.BtnRequestState);
            this.TbpRequest.Controls.Add(this.BtnRequestFilter);
            this.TbpRequest.Controls.Add(this.LsvRequest);
            this.TbpRequest.Location = new System.Drawing.Point(4, 22);
            this.TbpRequest.Name = "TbpRequest";
            this.TbpRequest.Padding = new System.Windows.Forms.Padding(3);
            this.TbpRequest.Size = new System.Drawing.Size(936, 422);
            this.TbpRequest.TabIndex = 4;
            this.TbpRequest.Text = "Request";
            // 
            // TxtRequestSearch
            // 
            this.TxtRequestSearch.AllowAutoCompleteDuplicates = false;
            this.TxtRequestSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtRequestSearch.AutoCompleteCustomSource = ((System.Collections.Specialized.StringCollection)(resources.GetObject("TxtRequestSearch.AutoCompleteCustomSource")));
            this.TxtRequestSearch.BackColor = System.Drawing.SystemColors.Window;
            this.TxtRequestSearch.Buttons.Add(this.TxbRequest);
            this.TxtRequestSearch.CaretPosition = 0;
            this.TxtRequestSearch.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TxtRequestSearch.Location = new System.Drawing.Point(620, 7);
            this.TxtRequestSearch.Name = "TxtRequestSearch";
            this.TxtRequestSearch.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TxtRequestSearch.Placeholder = "Rechercher";
            this.TxtRequestSearch.SelectionForeColor = System.Drawing.Color.Black;
            this.TxtRequestSearch.SelectionHead = -1;
            this.TxtRequestSearch.Size = new System.Drawing.Size(312, 29);
            this.TxtRequestSearch.TabIndex = 28;
            this.TxtRequestSearch.TextChangedDelay = 500;
            this.TxtRequestSearch.TextChangedDelayed += new System.EventHandler(this.TxtRequestSearch_TextChangedDelayed);
            this.TxtRequestSearch.ButtonClicked += new System.EventHandler<Fatya.Controls.ButtonEventArgs>(this.TxtRequestSearch_ButtonClicked);
            this.TxtRequestSearch.AutoCompleteSelected += new Fatya.Controls.TxtBox.AutoCompleteEventHandler(this.TxtRequestSearch_AutoCompleteSelected);
            // 
            // TxbRequest
            // 
            this.TxbRequest.Enabled = true;
            this.TxbRequest.Image = global::Fatya.Properties.Resources.Search;
            // 
            // BtnRequestMisc
            // 
            this.BtnRequestMisc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnRequestMisc.AutoSize = true;
            this.BtnRequestMisc.ContextMenuStrip = this.CmsRequest;
            this.BtnRequestMisc.Enabled = false;
            this.BtnRequestMisc.IsDropDown = true;
            this.BtnRequestMisc.Location = new System.Drawing.Point(474, 393);
            this.BtnRequestMisc.Name = "BtnRequestMisc";
            this.BtnRequestMisc.Size = new System.Drawing.Size(102, 29);
            this.BtnRequestMisc.SplitMenuStrip = this.CmsRequest;
            this.BtnRequestMisc.TabIndex = 33;
            this.BtnRequestMisc.Text = "Autres";
            this.BtnRequestMisc.UseVisualStyleBackColor = true;
            // 
            // BtnRequestRemove
            // 
            this.BtnRequestRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnRequestRemove.Enabled = false;
            this.BtnRequestRemove.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnRequestRemove.Location = new System.Drawing.Point(254, 393);
            this.BtnRequestRemove.Name = "BtnRequestRemove";
            this.BtnRequestRemove.Size = new System.Drawing.Size(214, 29);
            this.BtnRequestRemove.TabIndex = 32;
            this.BtnRequestRemove.Text = "Supprimer les demandes sélectionnées";
            this.BtnRequestRemove.UseVisualStyleBackColor = true;
            this.BtnRequestRemove.Click += new System.EventHandler(this.BtnRequestRemove_Click);
            // 
            // BtnRequestState
            // 
            this.BtnRequestState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BtnRequestState.Enabled = false;
            this.BtnRequestState.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnRequestState.Location = new System.Drawing.Point(12, 393);
            this.BtnRequestState.Name = "BtnRequestState";
            this.BtnRequestState.Size = new System.Drawing.Size(236, 29);
            this.BtnRequestState.TabIndex = 31;
            this.BtnRequestState.Text = "Changer l\'état des demandes sélectionnées";
            this.BtnRequestState.UseVisualStyleBackColor = true;
            this.BtnRequestState.Click += new System.EventHandler(this.BtnRequestState_Click);
            // 
            // BtnRequestFilter
            // 
            this.BtnRequestFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnRequestFilter.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.BtnRequestFilter.Location = new System.Drawing.Point(856, 393);
            this.BtnRequestFilter.Name = "BtnRequestFilter";
            this.BtnRequestFilter.Size = new System.Drawing.Size(76, 29);
            this.BtnRequestFilter.TabIndex = 34;
            this.BtnRequestFilter.Text = "Filtrer...";
            this.BtnRequestFilter.UseVisualStyleBackColor = true;
            this.BtnRequestFilter.Click += new System.EventHandler(this.BtnRequestFilter_Click);
            // 
            // LsvRequest
            // 
            this.LsvRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LsvRequest.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            ColRequestId,
            this.ColRequestType,
            this.ColRequestState,
            ColRequestStudent,
            this.ColRequestClass,
            this.ColRequestDescription,
            this.ColRequestStart,
            this.ColRequestDone});
            this.LsvRequest.ContextMenuStrip = this.CmsRequest;
            this.LsvRequest.FullRowSelect = true;
            this.LsvRequest.HideSelection = false;
            this.LsvRequest.Location = new System.Drawing.Point(12, 42);
            this.LsvRequest.Name = "LsvRequest";
            this.LsvRequest.Size = new System.Drawing.Size(920, 345);
            this.LsvRequest.TabIndex = 29;
            this.LsvRequest.UseCompatibleStateImageBehavior = false;
            this.LsvRequest.View = System.Windows.Forms.View.Details;
            this.LsvRequest.SelectedIndexChanged += new System.EventHandler(this.LsvRequest_SelectedIndexChanged);
            this.LsvRequest.DoubleClick += new System.EventHandler(this.LsvRequest_DoubleClick);
            // 
            // ColRequestType
            // 
            this.ColRequestType.Text = "Type";
            // 
            // ColRequestState
            // 
            this.ColRequestState.Text = "Etat";
            // 
            // ColRequestClass
            // 
            this.ColRequestClass.Text = "Classe";
            // 
            // ColRequestDescription
            // 
            this.ColRequestDescription.Text = "Description";
            // 
            // ColRequestStart
            // 
            this.ColRequestStart.Text = "Date de la demande";
            this.ColRequestStart.Width = 107;
            // 
            // ColRequestDone
            // 
            this.ColRequestDone.Text = "Achevé le";
            // 
            // TbpMarks
            // 
            this.TbpMarks.Controls.Add(this.txtBox1);
            this.TbpMarks.Controls.Add(this.splitButton1);
            this.TbpMarks.Controls.Add(this.button4);
            this.TbpMarks.Controls.Add(this.button5);
            this.TbpMarks.Controls.Add(this.button6);
            this.TbpMarks.Controls.Add(this.button7);
            this.TbpMarks.Controls.Add(this.label5);
            this.TbpMarks.Controls.Add(this.label4);
            this.TbpMarks.Controls.Add(this.label3);
            this.TbpMarks.Controls.Add(this.label2);
            this.TbpMarks.Controls.Add(this.label1);
            this.TbpMarks.Controls.Add(this.CbxMarkTest);
            this.TbpMarks.Controls.Add(this.LsvMark);
            this.TbpMarks.Controls.Add(this.button2);
            this.TbpMarks.Controls.Add(this.BtnMarkNew);
            this.TbpMarks.Controls.Add(this.CbxMarkSubject);
            this.TbpMarks.Controls.Add(this.CbxMarkModule);
            this.TbpMarks.Controls.Add(this.CbxMarkClass);
            this.TbpMarks.Controls.Add(this.CbxMarkFormation);
            this.TbpMarks.Location = new System.Drawing.Point(4, 22);
            this.TbpMarks.Name = "TbpMarks";
            this.TbpMarks.Padding = new System.Windows.Forms.Padding(3);
            this.TbpMarks.Size = new System.Drawing.Size(936, 422);
            this.TbpMarks.TabIndex = 7;
            this.TbpMarks.Text = "Notes";
            this.TbpMarks.UseVisualStyleBackColor = true;
            this.TbpMarks.Click += new System.EventHandler(this.TbpMarks_Click);
            // 
            // txtBox1
            // 
            this.txtBox1.AllowAutoCompleteDuplicates = false;
            this.txtBox1.AutoCompleteCustomSource = ((System.Collections.Specialized.StringCollection)(resources.GetObject("txtBox1.AutoCompleteCustomSource")));
            this.txtBox1.BackColor = System.Drawing.SystemColors.Window;
            this.txtBox1.Buttons.Add(this.TxbMarkSearch);
            this.txtBox1.CaretPosition = 0;
            this.txtBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtBox1.Location = new System.Drawing.Point(645, 67);
            this.txtBox1.Name = "txtBox1";
            this.txtBox1.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.txtBox1.Placeholder = null;
            this.txtBox1.SelectionForeColor = System.Drawing.Color.Black;
            this.txtBox1.SelectionHead = -1;
            this.txtBox1.Size = new System.Drawing.Size(283, 21);
            this.txtBox1.TabIndex = 26;
            this.txtBox1.Text = "txtBox1";
            this.txtBox1.TextChangedDelay = 100;
            // 
            // TxbMarkSearch
            // 
            this.TxbMarkSearch.Enabled = true;
            this.TxbMarkSearch.Image = global::Fatya.Properties.Resources.Search;
            // 
            // splitButton1
            // 
            this.splitButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.splitButton1.AutoSize = true;
            this.splitButton1.ContextMenuStrip = this.CmsFormation;
            this.splitButton1.Enabled = false;
            this.splitButton1.IsDropDown = true;
            this.splitButton1.Location = new System.Drawing.Point(671, 387);
            this.splitButton1.Name = "splitButton1";
            this.splitButton1.Size = new System.Drawing.Size(102, 29);
            this.splitButton1.SplitMenuStrip = this.CmsFormation;
            this.splitButton1.TabIndex = 24;
            this.splitButton1.Text = "Autres";
            this.splitButton1.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button4.Enabled = false;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button4.Location = new System.Drawing.Point(451, 387);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(214, 29);
            this.button4.TabIndex = 23;
            this.button4.Text = "Importer ";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button5.Enabled = false;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button5.Location = new System.Drawing.Point(231, 387);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(214, 29);
            this.button5.TabIndex = 22;
            this.button5.Text = "Exporter";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button6.Location = new System.Drawing.Point(854, 387);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(76, 29);
            this.button6.TabIndex = 25;
            this.button6.Text = "Filtrer...";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button7.Location = new System.Drawing.Point(11, 387);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(214, 29);
            this.button7.TabIndex = 21;
            this.button7.Text = "Modifier la note";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(609, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "label5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(461, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(307, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "label3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(156, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "label2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "label1";
            // 
            // CbxMarkTest
            // 
            this.CbxMarkTest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxMarkTest.FormattingEnabled = true;
            this.CbxMarkTest.Location = new System.Drawing.Point(612, 36);
            this.CbxMarkTest.Name = "CbxMarkTest";
            this.CbxMarkTest.Size = new System.Drawing.Size(145, 21);
            this.CbxMarkTest.TabIndex = 4;
            this.CbxMarkTest.SelectedIndexChanged += new System.EventHandler(this.CbxMarkTest_SelectedIndexChanged);
            // 
            // LsvMark
            // 
            this.LsvMark.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LsvMark.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.LsvMark.Location = new System.Drawing.Point(8, 94);
            this.LsvMark.Name = "LsvMark";
            this.LsvMark.Size = new System.Drawing.Size(920, 287);
            this.LsvMark.TabIndex = 2;
            this.LsvMark.UseCompatibleStateImageBehavior = false;
            this.LsvMark.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Nom";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Note";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(763, 34);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Supprimer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // BtnMarkNew
            // 
            this.BtnMarkNew.Location = new System.Drawing.Point(844, 34);
            this.BtnMarkNew.Name = "BtnMarkNew";
            this.BtnMarkNew.Size = new System.Drawing.Size(75, 23);
            this.BtnMarkNew.TabIndex = 1;
            this.BtnMarkNew.Text = "Nouveau";
            this.BtnMarkNew.UseVisualStyleBackColor = true;
            this.BtnMarkNew.Click += new System.EventHandler(this.BtnMarkNew_Click);
            // 
            // CbxMarkSubject
            // 
            this.CbxMarkSubject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxMarkSubject.FormattingEnabled = true;
            this.CbxMarkSubject.Location = new System.Drawing.Point(461, 36);
            this.CbxMarkSubject.Name = "CbxMarkSubject";
            this.CbxMarkSubject.Size = new System.Drawing.Size(145, 21);
            this.CbxMarkSubject.TabIndex = 0;
            this.CbxMarkSubject.SelectedIndexChanged += new System.EventHandler(this.CbxMarkSubject_SelectedIndexChanged);
            // 
            // CbxMarkModule
            // 
            this.CbxMarkModule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxMarkModule.FormattingEnabled = true;
            this.CbxMarkModule.Location = new System.Drawing.Point(310, 36);
            this.CbxMarkModule.Name = "CbxMarkModule";
            this.CbxMarkModule.Size = new System.Drawing.Size(145, 21);
            this.CbxMarkModule.TabIndex = 0;
            this.CbxMarkModule.SelectedIndexChanged += new System.EventHandler(this.CbxMarkModule_SelectedIndexChanged);
            // 
            // CbxMarkClass
            // 
            this.CbxMarkClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxMarkClass.FormattingEnabled = true;
            this.CbxMarkClass.Location = new System.Drawing.Point(159, 36);
            this.CbxMarkClass.Name = "CbxMarkClass";
            this.CbxMarkClass.Size = new System.Drawing.Size(145, 21);
            this.CbxMarkClass.TabIndex = 0;
            this.CbxMarkClass.SelectedIndexChanged += new System.EventHandler(this.CB_note_classe_SelectedIndexChanged);
            // 
            // CbxMarkFormation
            // 
            this.CbxMarkFormation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbxMarkFormation.FormattingEnabled = true;
            this.CbxMarkFormation.Location = new System.Drawing.Point(8, 36);
            this.CbxMarkFormation.Name = "CbxMarkFormation";
            this.CbxMarkFormation.Size = new System.Drawing.Size(145, 21);
            this.CbxMarkFormation.TabIndex = 0;
            this.CbxMarkFormation.SelectedIndexChanged += new System.EventHandler(this.CbxMarkFormation_SelectedIndexChanged);
            // 
            // TbpAbsence
            // 
            this.TbpAbsence.Location = new System.Drawing.Point(4, 22);
            this.TbpAbsence.Name = "TbpAbsence";
            this.TbpAbsence.Padding = new System.Windows.Forms.Padding(3);
            this.TbpAbsence.Size = new System.Drawing.Size(936, 422);
            this.TbpAbsence.TabIndex = 8;
            this.TbpAbsence.Text = "Absence";
            this.TbpAbsence.UseVisualStyleBackColor = true;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(944, 565);
            this.Controls.Add(this.HdrMain);
            this.Controls.Add(this.TlcMain);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(952, 596);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fatya 0.5";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.CmsStudent.ResumeLayout(false);
            this.CmsTeacher.ResumeLayout(false);
            this.CmsFormation.ResumeLayout(false);
            this.CmsClass.ResumeLayout(false);
            this.CmsRequest.ResumeLayout(false);
            this.CmsModule.ResumeLayout(false);
            this.CmsSubject.ResumeLayout(false);
            this.TlcMain.ResumeLayout(false);
            this.TbpFormation.ResumeLayout(false);
            this.TbpFormation.PerformLayout();
            this.TbpClass.ResumeLayout(false);
            this.TbpClass.PerformLayout();
            this.TbpModule.ResumeLayout(false);
            this.TbpModule.PerformLayout();
            this.TbpSubject.ResumeLayout(false);
            this.TbpSubject.PerformLayout();
            this.TbpTeacher.ResumeLayout(false);
            this.TbpTeacher.PerformLayout();
            this.TbpStudent.ResumeLayout(false);
            this.TbpStudent.PerformLayout();
            this.TbpRequest.ResumeLayout(false);
            this.TbpRequest.PerformLayout();
            this.TbpMarks.ResumeLayout(false);
            this.TbpMarks.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private TabLessControl TlcMain;
		private System.Windows.Forms.ListView LsvStudent;
		private System.Windows.Forms.Button BtnStudentAdd;
		private System.Windows.Forms.Button BtnStudentRemove;
		private System.Windows.Forms.Button BtnStudentEdit;
		private System.Windows.Forms.Button BtnStudentFilter;
		private SplitButton BtnStudentMisc;
		private System.Windows.Forms.ContextMenuStrip CmsStudent;
		private System.Windows.Forms.ToolStripMenuItem CmiStudentTeachers;
		private System.Windows.Forms.ToolStripMenuItem CmiStudentMore;
		private Header HdrMain;
		private System.Windows.Forms.TabPage TbpStudent;
		private System.Windows.Forms.TabPage TbpTeacher;
		private System.Windows.Forms.TabPage TbpClass;
		private System.Windows.Forms.TabPage TbpFormation;
		private System.Windows.Forms.TabPage TbpRequest;
		private TxtBox TxtStudentSearch;
		private System.Windows.Forms.ToolStripMenuItem CmiStudentFormation;
		private System.Windows.Forms.ToolStripMenuItem CmiStudentClass;
		private System.Windows.Forms.ToolStripMenuItem CmiStudentAbscence;
		private System.Windows.Forms.ToolStripMenuItem CmiStudentRequests;
		private Sep SepStudent;
		private TxtBox TxtTeacherSearch;
		private SplitButton BtnTeacherMisc;
		private System.Windows.Forms.Button BtnTeacherDelete;
		private System.Windows.Forms.Button BtnTeacherEdit;
		private System.Windows.Forms.Button BtnTeacherFilter;
		private System.Windows.Forms.Button BtnTeacherAdd;
		private System.Windows.Forms.ListView LsvTeacher;
		private System.Windows.Forms.ContextMenuStrip CmsTeacher;
		private System.Windows.Forms.ToolStripMenuItem CmiTeacherStudents;
		private System.Windows.Forms.ToolStripMenuItem CmiTeachersClasses;
		private System.Windows.Forms.ToolStripMenuItem CmiTeacherAbscences;
		private Sep SepTeacher;
		private System.Windows.Forms.ToolStripMenuItem CmiTeacherMore;
		private TxtButton TxbTeacher;
		private TxtButton TxbStudent;
		private TxtBox TxtFormationSearch;
		private SplitButton BtnFormationMisc;
		private System.Windows.Forms.Button BtnFormationRemove;
		private System.Windows.Forms.Button BtnFormationEdit;
		private System.Windows.Forms.Button BtnFormationFilter;
		private System.Windows.Forms.Button BtnFormationAdd;
		private System.Windows.Forms.ListView LsvFormation;
		private TxtButton TxbFormation;
		private System.Windows.Forms.ContextMenuStrip CmsFormation;
		private System.Windows.Forms.ToolStripMenuItem CmiFormationClasses;
		private System.Windows.Forms.ToolStripMenuItem CmiFormationStudents;
		private Sep SepFormation;
		private System.Windows.Forms.ToolStripMenuItem CmiFormationMore;
		private System.Windows.Forms.ColumnHeader ColSum;
		private TxtBox TxtClassSearch;
		private SplitButton BtnClassMisc;
		private System.Windows.Forms.Button BtnClassRemove;
		private System.Windows.Forms.Button BtnClassEdit;
		private System.Windows.Forms.Button BtnClassFilter;
		private System.Windows.Forms.Button BtnClassAdd;
		private System.Windows.Forms.ListView LsvClass;
		private TxtButton TxbClass;
		private System.Windows.Forms.ContextMenuStrip CmsClass;
		private System.Windows.Forms.ToolStripMenuItem CmiClassFormation;
		private Sep SepClass;
		private System.Windows.Forms.ToolStripMenuItem CmiClassMore;
		private System.Windows.Forms.ToolStripMenuItem CmiClassStudents;
		private System.Windows.Forms.ColumnHeader ColClassName;
		private System.Windows.Forms.ColumnHeader ColClassCount;
		private HeaderItem HdiStudent;
		private HeaderItem HdiTeacher;
		private HeaderItem HdiFormation;
		private HeaderItem HdiClass;
		private HeaderItem HdiRequest;
		private TxtBox TxtRequestSearch;
		private SplitButton BtnRequestMisc;
		private System.Windows.Forms.Button BtnRequestRemove;
		private System.Windows.Forms.Button BtnRequestFilter;
		private System.Windows.Forms.ListView LsvRequest;
		private System.Windows.Forms.ColumnHeader ColRequestType;
		private System.Windows.Forms.ColumnHeader ColRequestClass;
		private TxtButton TxbRequest;
		private System.Windows.Forms.ContextMenuStrip CmsRequest;
		private System.Windows.Forms.ToolStripMenuItem CmiRequestStudent;
		private Sep SepRequest;
		private System.Windows.Forms.ToolStripMenuItem CmiRequestRespond;
		private System.Windows.Forms.ColumnHeader ColRequestDescription;
		private System.Windows.Forms.ColumnHeader ColRequestStart;
		private System.Windows.Forms.ColumnHeader ColRequestDone;
		private System.Windows.Forms.Button BtnRequestState;
		private HeaderItem HdiSubject;
		private HeaderItem HdbSettings;
		private HeaderItem HdbLock;
		private HeaderItem HdbExit;
		private System.Windows.Forms.TabPage TbpModule;
		private System.Windows.Forms.TabPage TbpSubject;
		private HeaderItem HdiModule;
		private System.Windows.Forms.ColumnHeader ColRequestState;
		private System.Windows.Forms.ColumnHeader ColFormationStudents;
		private System.Windows.Forms.ColumnHeader ColFormationClasses;
		private TxtBox TxtModuleSearch;
		private SplitButton BtnModuleMisc;
		private System.Windows.Forms.Button BtnModuleRemove;
		private System.Windows.Forms.Button BtnModuleEdit;
		private System.Windows.Forms.Button BtnModuleFilter;
		private System.Windows.Forms.Button BtnModuleAdd;
		private System.Windows.Forms.ListView LsvModule;
		private System.Windows.Forms.ColumnHeader ColModuleMin;
		private TxtButton TxbModule;
		private System.Windows.Forms.ContextMenuStrip CmsModule;
		private System.Windows.Forms.ToolStripMenuItem CmiModuleFormation;
		private System.Windows.Forms.ToolStripMenuItem CmiModuleTeachers;
		private System.Windows.Forms.ToolStripMenuItem CmiModuleClass;
		private System.Windows.Forms.ToolStripMenuItem CmiModuleSubjects;
		private Sep SepModule;
		private System.Windows.Forms.ToolStripMenuItem CmiModuleMore;
		private TxtBox TxtSubjectSearch;
		private SplitButton BtnSubjectMisc;
		private System.Windows.Forms.Button BtnSubjectRemove;
		private System.Windows.Forms.Button BtnSubjectEdit;
		private System.Windows.Forms.Button BtnSubjectFilter;
		private System.Windows.Forms.Button BtnSubjectAdd;
		private System.Windows.Forms.ListView LsvSubject;
		private System.Windows.Forms.ColumnHeader ColSubjectMinMark;
		private System.Windows.Forms.ColumnHeader ColSubjectTeacher;
		private TxtButton TxbSubject;
		private System.Windows.Forms.ContextMenuStrip CmsSubject;
		private System.Windows.Forms.ToolStripMenuItem CmiSubjectFormation;
		private System.Windows.Forms.ToolStripMenuItem CmiSubjectClass;
		private System.Windows.Forms.ToolStripMenuItem CmiSubjectModule;
		private System.Windows.Forms.ToolStripMenuItem CmiSubjectTeacher;
		private Sep SepSubject;
		private System.Windows.Forms.ToolStripMenuItem CmiSubjectMore;
		private System.Windows.Forms.ToolStripMenuItem CmiTeacherSubjects;
		private System.Windows.Forms.ToolStripMenuItem CmiClassModules;
		private System.Windows.Forms.ToolStripMenuItem CmiClassTeachers;
		private System.Windows.Forms.ColumnHeader ColClassModules;
		private System.Windows.Forms.ToolStripMenuItem CmiStudentModules;
		private System.Windows.Forms.ToolStripMenuItem CmiStudentSubjects;
		private System.Windows.Forms.ToolStripMenuItem CmiTeacherFormations;
		private System.Windows.Forms.ToolStripMenuItem CmiFormationModules;
		private System.Windows.Forms.ToolStripMenuItem CmiFormationSubjects;
		private System.Windows.Forms.ToolStripMenuItem CmiFormationTeachers;
		private System.Windows.Forms.ToolStripMenuItem CmiFormationRequests;
		private System.Windows.Forms.ToolStripMenuItem CmiClassSubjects;
		private System.Windows.Forms.ToolStripMenuItem CmiClassRequests;
		private System.Windows.Forms.ToolStripMenuItem CmiModuleStudents;
		private HeaderItem HdiAbsence;
		private HeaderItem HdiMark;
		private System.Windows.Forms.TabPage TbpMarks;
		private System.Windows.Forms.ListView LsvMark;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button BtnMarkNew;
		private System.Windows.Forms.ComboBox CbxMarkSubject;
		private System.Windows.Forms.ComboBox CbxMarkModule;
		private System.Windows.Forms.ComboBox CbxMarkClass;
        private System.Windows.Forms.ComboBox CbxMarkFormation;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private HeaderItem HdbRefresh;
		private System.Windows.Forms.TabPage TbpAbsence;
        private System.Windows.Forms.ComboBox CbxMarkTest;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private SplitButton splitButton1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private TxtBox txtBox1;
        private TxtButton TxbMarkSearch;
	}
}

