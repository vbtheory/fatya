﻿namespace Fatya
{
	partial class FrmWait
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.LblWait = new System.Windows.Forms.Label();
			this.BgwMain = new System.ComponentModel.BackgroundWorker();
			this.PbrWait = new System.Windows.Forms.ProgressBar();
			this.SuspendLayout();
			// 
			// LblWait
			// 
			this.LblWait.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.LblWait.AutoSize = true;
			this.LblWait.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblWait.Location = new System.Drawing.Point(24, 26);
			this.LblWait.Name = "LblWait";
			this.LblWait.Size = new System.Drawing.Size(149, 21);
			this.LblWait.TabIndex = 0;
			this.LblWait.Text = "Veuillez patientez...";
			// 
			// PbrWait
			// 
			this.PbrWait.Location = new System.Drawing.Point(-1, 65);
			this.PbrWait.MarqueeAnimationSpeed = 20;
			this.PbrWait.Name = "PbrWait";
			this.PbrWait.Size = new System.Drawing.Size(199, 8);
			this.PbrWait.Step = 1;
			this.PbrWait.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			this.PbrWait.TabIndex = 1;
			// 
			// FrmWait
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(196, 73);
			this.ControlBox = false;
			this.Controls.Add(this.PbrWait);
			this.Controls.Add(this.LblWait);
			this.DoubleBuffered = true;
			this.Font = new System.Drawing.Font("Segoe UI Semilight", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FrmWait";
			this.Opacity = 0D;
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LblWait;
		private System.ComponentModel.BackgroundWorker BgwMain;
		private System.Windows.Forms.ProgressBar PbrWait;
	}
}