﻿namespace Fatya
{
	partial class FrmLogin
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin));
			this.LblTitle = new System.Windows.Forms.Label();
			this.BtnLogin = new System.Windows.Forms.Button();
			this.BtnCancel = new System.Windows.Forms.Button();
			this.LlbPassReset = new System.Windows.Forms.LinkLabel();
			this.RbxPassword = new Fatya.Controls.RuleBox();
			this.TxbPass = new Fatya.Controls.TxtButton();
			this.SuspendLayout();
			// 
			// LblTitle
			// 
			this.LblTitle.AutoSize = true;
			this.LblTitle.Font = new System.Drawing.Font("Segoe UI Semilight", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LblTitle.Location = new System.Drawing.Point(12, 9);
			this.LblTitle.Name = "LblTitle";
			this.LblTitle.Size = new System.Drawing.Size(155, 13);
			this.LblTitle.TabIndex = 0;
			this.LblTitle.Text = "Veuillez insérer le mot de passe:";
			// 
			// BtnLogin
			// 
			this.BtnLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnLogin.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BtnLogin.Enabled = false;
			this.BtnLogin.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnLogin.Location = new System.Drawing.Point(158, 85);
			this.BtnLogin.Name = "BtnLogin";
			this.BtnLogin.Size = new System.Drawing.Size(75, 23);
			this.BtnLogin.TabIndex = 2;
			this.BtnLogin.Text = "Connecter";
			this.BtnLogin.UseVisualStyleBackColor = true;
			this.BtnLogin.Click += new System.EventHandler(this.BtnLogin_Click);
			// 
			// BtnCancel
			// 
			this.BtnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.BtnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.BtnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.BtnCancel.Location = new System.Drawing.Point(239, 85);
			this.BtnCancel.Name = "BtnCancel";
			this.BtnCancel.Size = new System.Drawing.Size(75, 23);
			this.BtnCancel.TabIndex = 2;
			this.BtnCancel.Text = "Annuler";
			this.BtnCancel.UseVisualStyleBackColor = true;
			this.BtnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
			// 
			// LlbPassReset
			// 
			this.LlbPassReset.ActiveLinkColor = System.Drawing.Color.SteelBlue;
			this.LlbPassReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.LlbPassReset.AutoSize = true;
			this.LlbPassReset.LinkColor = System.Drawing.Color.DeepSkyBlue;
			this.LlbPassReset.Location = new System.Drawing.Point(12, 90);
			this.LlbPassReset.Name = "LlbPassReset";
			this.LlbPassReset.Size = new System.Drawing.Size(117, 13);
			this.LlbPassReset.TabIndex = 3;
			this.LlbPassReset.TabStop = true;
			this.LlbPassReset.Text = "Mot de passe oublié?";
			this.LlbPassReset.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
			// 
			// RbxPassword
			// 
			this.RbxPassword.AllowAutoCompleteDuplicates = false;
			this.RbxPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.RbxPassword.AutoCompleteCustomSource = ((System.Collections.Specialized.StringCollection)(resources.GetObject("RbxPassword.AutoCompleteCustomSource")));
			this.RbxPassword.BackColor = System.Drawing.SystemColors.Window;
			this.RbxPassword.BorderColor = System.Drawing.SystemColors.ControlText;
			this.RbxPassword.BorderStyle = Fatya.Controls.TxtBoxBorderStyle.Minimal;
			this.RbxPassword.Buttons.Add(this.TxbPass);
			this.RbxPassword.CaretPosition = 0;
			this.RbxPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.RbxPassword.FixedLength = 3;
			this.RbxPassword.Location = new System.Drawing.Point(15, 29);
			this.RbxPassword.Name = "RbxPassword";
			this.RbxPassword.NumericOnly = false;
			this.RbxPassword.Padding = new System.Windows.Forms.Padding(5, 0, 5, 0);
			this.RbxPassword.Placeholder = "Mot de passe";
			this.RbxPassword.SelectionForeColor = System.Drawing.Color.Black;
			this.RbxPassword.SelectionHead = -1;
			this.RbxPassword.Size = new System.Drawing.Size(299, 31);
			this.RbxPassword.TabIndex = 1;
			this.RbxPassword.TextChangedDelay = 100;
			this.RbxPassword.UseSystemPasswordChar = true;
			this.RbxPassword.ValidationMethod = Fatya.Controls.Validation.MinLength;
			this.RbxPassword.TextChangedDelayed += new System.EventHandler(this.RbxPassword_TextChangedDelayed);
			this.RbxPassword.ButtonClicked += new System.EventHandler<Fatya.Controls.ButtonEventArgs>(this.RbxPassword_ButtonClicked);
			this.RbxPassword.TextChanged += new System.EventHandler(this.RbxPassword_TextChanged);
			// 
			// TxbPass
			// 
			this.TxbPass.Enabled = true;
			this.TxbPass.Image = global::Fatya.Properties.Resources.View;
			// 
			// FrmLogin
			// 
			this.AcceptButton = this.BtnLogin;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.CancelButton = this.BtnCancel;
			this.ClientSize = new System.Drawing.Size(326, 120);
			this.Controls.Add(this.LlbPassReset);
			this.Controls.Add(this.BtnCancel);
			this.Controls.Add(this.BtnLogin);
			this.Controls.Add(this.RbxPassword);
			this.Controls.Add(this.LblTitle);
			this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FrmLogin";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Identification";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmLogin_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label LblTitle;
		private Controls.RuleBox RbxPassword;
		private System.Windows.Forms.Button BtnLogin;
		private System.Windows.Forms.Button BtnCancel;
		private System.Windows.Forms.LinkLabel LlbPassReset;
		private Controls.TxtButton TxbPass;
	}
}