﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Fatya.Classes;
using Fatya.Controls;

namespace Fatya
{
	public partial class FrmLogin : Form
	{

		private bool _canExit;
		private bool _cancelExit;

		private int _count;

		public FrmLogin()
		{
			InitializeComponent();
		}

		private void BtnLogin_Click(object sender, EventArgs e)
		{
			try
			{
				StringRequest r = Server.OpenConnectionAsync(RbxPassword.Text);

				if (!string.IsNullOrEmpty(r.Exception)) throw new Exception(r.Exception);

				DialogResult = DialogResult.OK;
				_canExit = true;
				Close();
			}
			catch (Exception ex)
			{
				_cancelExit = true;
				if (Alert.Show(ex.Message, "", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
					BtnLogin_Click(null, null);
			}
		}

		private void BtnCancel_Click(object sender, EventArgs e)
		{
			_canExit = false;
			Close();
		}

		private void FrmLogin_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_canExit) return;
			if (_cancelExit)
			{
				e.Cancel = true;
				_cancelExit = false;
				return;
			}

			Alert a = new Alert("Êtes-vous sûr de vouloir quitter?", MessageBoxButtons.YesNo);
			a.Btn2Highlight = Highlight.Red;

			if (a.Show() == DialogResult.Yes)
			{
				_canExit = true;
				Process.GetCurrentProcess().Kill();
			}
			else e.Cancel = true;
		}

		private void RbxPassword_TextChanged(object sender, EventArgs e)
		{
			BtnLogin.Enabled = false;
		}

		private void RbxPassword_TextChangedDelayed(object sender, EventArgs e)
		{
			BtnLogin.Enabled = RbxPassword.IsValid;
		}

		private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			AutoForm f = new AutoForm("Récupération de mot de passe");
			f.AddEntry("email", new RuleEntry("Email", "", Validation.Email, true));

			bool exit = false;
			while (!exit)
			{
				if(_count==4) f.AddEntry("captcha", new CaptchaEntry("", CaptchaStyle.Equation));

				if (f.ShowDialog() != DialogResult.OK) break;

				DictionaryRequest response = Server.ReadJSONAsync("recup=" + f.Entries["email"].GetValue());

				if (!string.IsNullOrEmpty(response.Exception))
				{
					Alert.Show(response.Exception, "", MessageBoxIcon.Error);
					continue;
				}

				if (response.Response["recup"].ToLower() == "true")
				{
					Alert.Show("Vous allez recevoir un message sur votre boîte de réception pour récupérer votre mot de passe");
					exit = true;
				}
				else
				{
					Alert.Show("Email incorrect", "", MessageBoxIcon.Warning);
					_count++;
				}
			}

			_count = 0;
		}

		private void RbxPassword_ButtonClicked(object sender, ButtonEventArgs e)
		{
			RbxPassword.UseSystemPasswordChar = !RbxPassword.UseSystemPasswordChar;
		}

	}
}
