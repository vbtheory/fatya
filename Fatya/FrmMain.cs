﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using ExcelLibrary.SpreadSheet;
using Fatya.Classes;
using System.IO;
using Fatya.Controls;
using Fatya.Properties;
using Newtonsoft.Json.Linq;
using Module = Fatya.Classes.Module;

namespace Fatya
{
	public partial class FrmMain : Form
	{

		#region Fields

		public static FrmMain Frm;

		public List<Formation> Formations = new List<Formation>();
		FormationFilter _formationFilter = new FormationFilter();
		private bool _formationsLoaded;

		public List<Class> Classes = new List<Class>();
		ClassFilter _classFilter = new ClassFilter();
		private bool _classesLoaded;

		public List<Module> Modules = new List<Module>();
		ModuleFilter _moduleFilter = new ModuleFilter();
		private bool _modulesLoaded;

		public List<Subject> Subjects = new List<Subject>();
		SubjectFilter _subjectFilter = new SubjectFilter();
		private bool _subjectsLoaded;

		public List<Teacher> Teachers = new List<Teacher>();
		TeacherFilter _teacherFilter = new TeacherFilter();
		private bool _teachersLoaded;

		public List<Student> Students = new List<Student>();
		StudentFilter _studentFilter = new StudentFilter();
		private bool _studentsLoaded;

		public List<Request> Requests = new List<Request>();
		RequestFilter _requestsFilter = new RequestFilter();
		private bool _requestsLoaded;

        public List<Test> Tests = new List<Test>(); 
        private bool _marksLoaded;

        public List<Mark> Marks = new List<Mark>(); 

		#endregion

		#region UI

		public FrmMain()
		{
			Frm = this;
			CheckForIllegalCrossThreadCalls = false; //remove the InvalidThreadOperationException
			//todo remove debugger check
			if (!Debugger.IsAttached && new FrmLogin().ShowDialog() != DialogResult.OK) Close(); //opens login and if the user doesn't login, close this form

			InitializeComponent();

			//get all databases
			//todo remove debugger check
			if(!Debugger.IsAttached)DownloadFormations();

            for (int i = 0; i < 10; i++ )
            {
                Formation f = new Formation(i);

                f.Name = Path.GetRandomFileName();

                Formations.Add(f);

            }

            Random r = new Random();

            for (int i = 0; i < 20; i++)
            {
                Class c = new Class(i);

                c.IdFormation = r.Next(0, 11);

                Classes.Add(c);

            }

            for (int i = 0; i < 20; i++)
            {
                Module c = new Module(i);

                c.IdClass = r.Next(0, 21);
                c.Name = Path.GetRandomFileName();

                Modules.Add(c);

            }
            for (int i = 0; i < 20; i++)
            {
                Subject s = new Subject(i);

                s.IdModule = r.Next(0, 21);
                s.IdTeacher = r.Next(0, 21);
                s.Name = Path.GetRandomFileName();

                Subjects.Add(s);

            }
            for (int i = 0; i < 20; i++)
            {
                Teacher p = new Teacher(i);

               
                p.Fname = Path.GetRandomFileName();

                Teachers.Add(p);

            }
            for (int i = 0; i < 20; i++)
            {
                Student e = new Student(i);

                e.IdClass = r.Next(0, 21);
                e.Fname = Path.GetRandomFileName();

                Students.Add(e);

            }

            //Load all formations to listview
            LoadFormations();

			//Sets up WinRenderer for context menu strips
			SetTheme();

			//check if the there are any notifications
			CheckNotifications();

			Notifier.Show("Bienvenue à Fatya!"); //show welcome notification
		}

		private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			e.Cancel = true;
			HdrMain_ButtonClicked(null, new HeaderEventArgs(0, HdbExit));
		}

		private void HdrMain_SelectedIndexChanged(object sender, HeaderEventArgs e)
		{
			TlcMain.SelectedIndex = e.SelectedIndex; //change the tab control index

			//load listview according to selected item
			if (e.Item == HdiStudent && !_studentsLoaded)
				LoadStudents();
			else if (e.Item == HdiTeacher && !_teachersLoaded)
				LoadTeachers();
			else if (e.Item == HdiFormation && !_formationsLoaded)
				LoadFormations();
			else if (e.Item == HdiClass && !_classesLoaded)
				LoadClasses();
			else if (e.Item == HdiRequest && !_requestsLoaded)
				LoadRequests();
			else if (e.Item == HdiModule && !_modulesLoaded)
				LoadModules();
			else if (e.Item == HdiSubject && !_subjectsLoaded)
				LoadSubjects();
            else if (e.Item == HdiMark && !_marksLoaded)
                LoadMarks();
		}

		private void HdrMain_ButtonClicked(object sender, HeaderEventArgs e)
		{
			if (e.Item == HdbSettings && ModifierKeys == Keys.Alt) HdrMain.RunEaster();
			else if (e.Item == HdbLock) //button lock clicked
			{
				Server.CloseConnection(); //close connection
				Opacity = 0;
				ShowInTaskbar = false;
				new FrmLogin().ShowDialog(); //show login
				ShowInTaskbar = true;
				Opacity = 1;
				Activate();
			}
			else if (e.Item == HdbExit && //button exit clicked
					 Alert.Show("Êtes-vous sûr de vouloir quitter?", MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				Server.CloseConnection();

				Process.GetCurrentProcess().Kill();
			}
			else if (e.Item == HdbRefresh)
			{
				//todo add all date from db
				DownloadFormations();
			}
		}

		#endregion

		#region Formations

		public void DownloadFormations()
		{
			Loop:

			const string command = "GET_ALL_FORMATION";

			ApiRequest a = new ApiRequest();
			a.Add(command, Param("", ""));
			ApiResponseAlt response = Server.ReadApiQuery(a);
			if (!string.IsNullOrEmpty(response.Exception))
			{
				if (Alert.Show(response.Exception, MessageBoxButtons.RetryCancel) == DialogResult.Cancel)
				{
					Process.GetCurrentProcess().Kill();
				}
				else goto Loop;
			}

			for (int i = 0; i < response.Response[command].Count(); i++)
			{
				for (int j = 0; j < response.Response[command][i].Count(); j++)
				{
					JToken token = response.Response[command][i][j];

					Formation f = new Formation(Convert.ToInt32(token["id"]));

					f.Name = token["name"].ToString();
					f.NameAbr = token["name_abr"].ToString();
					f.Description = token["description"].ToString();
					f.CreationDate = DateTime.ParseExact(token["creation_date"].ToString(), "yyyy-MM-dd", CultureInfo.CurrentCulture);
					f.NYear = Convert.ToInt32(token["nYear"]);
					f.Icon = token["icon"].ToString();

					Formations.Add(f);
				}
			}
		}

		/// <summary>
		/// Loads formations from the Formations list to the listview
		/// </summary>
		private void LoadFormations()
		{
			_formationsLoaded = true;
			LsvFormation.BeginUpdate(); //called to help with the performance
			LsvFormation.Items.Clear(); //remove all current items
			TxtFormationSearch.AutoCompleteCustomSource.Clear();

			foreach (Formation formation in Formations)//iterate through all formations
			{
				LoadFormation(formation, -1);
			}

			//resize all columns first to the content then to the header
			ResizeColumns(LsvFormation);

			LsvFormation.EndUpdate(); //called to resume painting the listview

			CheckNotifications();
		}
		/// <summary>
		/// Adds a formation to the listview
		/// </summary>
		/// <param name="formation">Formation to add</param>
		/// <param name="def">If def != -1, the formation will be inserted at that index</param>
		private void LoadFormation(Formation formation, int def)
		{
			if (!_formationFilter.AppliesTo(formation)) return;

			//calculate some info from the class
			string id = formation.Id.ToString();
			string name = formation.Name;
			string duration = formation.NYear.IsPlural("an");
			string studentCount = formation.StudentCount.IsPlural("étudiant");
			string classCount = formation.ClassCount.IsPlural("classe");
			string date = formation.CreationDate.ToString("dd MMMM yyyy");
			string description = formation.Description;

			//we need to minimize the text from description
			if (description.Split('\n').Any())
			{
				string[] descSplit = description.Split('\n');

				//only get the first line of the description
				description = descSplit[0] + (descSplit.Count() > 1 ? "..." : "");
				if (description.Length > 50) description = description.Substring(0, 50) + "...";
			}

			if (def == -1) def = LsvFormation.Items.Count;

			//add new item to listview
			ListViewItem l = new ListViewItem(new[] { id, name, duration, studentCount, classCount, date, description });
			if (formation.NeedsAttention) l.BackColor = Color.Wheat;

			LsvFormation.Items.Insert(def, l);

			TxtFormationSearch.AutoCompleteCustomSource.Insert(def, name);
		}

		private void LsvFormation_SelectedIndexChanged(object sender, EventArgs e)
		{
			BtnFormationMisc.Enabled =
				BtnFormationEdit.Enabled = BtnFormationRemove.Enabled = CmiFormationMore.Enabled = LsvFormation.SelectedIndices.Count != 0;

			foreach (var item in CmsFormation.Items)
			{
				if(item is Sep) return;

				((ToolStripItem) item).Enabled = LsvFormation.SelectedItems.Count == 1;
			}
		}

		private bool OpenFormationDialog(Formation formation, bool edit, int def)
		{
			//create autoform instance (if edit==true, title is "Modifier name" else "Ajouter une formation")
			AutoForm form = new AutoForm(edit ? "Modifier " + formation.Name : "Ajouter une formation");

			form.AddEntry("name", new TextEntry("Nom de la formation", formation.Name, true));
			form.AddEntry("duration", new IntegerEntry("Durée de la formation", formation.NYear, 1, 6));
			form.AddEntry("date", new DateEntry("Date de création", formation.CreationDate));
			form.AddEntry("desc", new TextEntry("Description", formation.Description, true, true));

			SHOWFORM:

			//show form and make sure the user clicked yes
			if (form.ShowDialog() != DialogResult.OK) return false;

			//get values from form
			string name = form.GetValue("name").ToString();
			int duration = (int)form.GetValue("duration");
			DateTime date = DateTime.ParseExact(form.GetValue("date").ToString(), "d", CultureInfo.CurrentCulture);
			string desc = form.GetValue("desc").ToString();

			int index = -1;

			do
			{
				try
				{
					string command = edit ? "EDIT_FORMATION" : "ADD_FORMATION";
					ApiRequest a = new ApiRequest();
					
					if(edit)
						a.Add(command, Param("name", name), Param("name_abr", "abc"), Param("description", desc), Param("creation_date", date.ToString("yyyy-MM-dd")), Param("nYear", duration), Param("icon", "trophy"), Param("id", formation.Id));
					else
						a.Add(command, Param("name", name), Param("name_abr", "abc"), Param("description", desc), Param("creation_date", date.ToString("yyyy-MM-dd")), Param("nYear", duration), Param("icon", "trophy"));
					
					ApiResponse res = Server.ReadApiCommit(a);
					
					if(!string.IsNullOrEmpty(res.Exception)) throw new Exception(res.Exception);

					index = Convert.ToInt32(res.Response[command][0]);

					if (index==-1) throw new Exception(string.Format("Erreur lors de la {0} de la formation", edit?"modification": "création"));

					break;
				}
				catch (Exception ex)
				{
					if (Alert.Show(ex.Message, MessageBoxButtons.RetryCancel) == DialogResult.Cancel)
						goto SHOWFORM;
				}
			} while (true);

			int id = edit ? formation.Id : index;

			//create formation from form 
			Formation f = new Formation(id, name, "", duration, date, desc, "trophy"); //todo: fix
			Formations.Add(f);

			//add formation to listview
			LoadFormation(f, def);

			//resize all columns
			ResizeColumns(LsvFormation);

			return true;
		}

		private void BtnFormationAdd_Click(object sender, EventArgs e)
		{
			OpenFormationDialog(new Formation(0), false, -1);
		}

		private void BtnFormationEdit_Click(object sender, EventArgs e)
		{
			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvFormation.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			for (int i = 0; i < orderedIndices.Count; i++) //iterate through selected items
			{
				//get the list index of the formation
				int formationIndex = Formations.FindIndex(x => x.Id.ToString() == LsvFormation.Items[orderedIndices[i]].Text);

				if (OpenFormationDialog(Formations[formationIndex], true, orderedIndices[i])) //if the edit window is validated, remove the previous one
				{
					Formations.RemoveAt(formationIndex);
					LsvFormation.Items.RemoveAt(orderedIndices[i] + 1);
					TxtFormationSearch.AutoCompleteCustomSource.RemoveAt(orderedIndices[i] + 1);
					continue;
				}
				//if not and this is not the last item, ask for confirmation to continue editing
				if (orderedIndices.Count <= 1 || i == orderedIndices.Count - 1) continue;

				Alert a = new Alert("Voulez vous continuer à modifier les formations?", "", MessageBoxButtons.YesNo);

				a.Btn2Text = "&Continuer";
				a.Btn3Text = "&Annuler";
				//if the user chooses to stop editing, exit
				if (a.Show() != DialogResult.Yes) break;
			}

			//refresh related windows
			LoadFormationChildren();

			//resize all columns
			ResizeColumns(LsvFormation);
		}

		private void LsvFormation_DoubleClick(object sender, EventArgs e)
		{
			//if no item is selected, do nothing
			if (LsvFormation.SelectedIndices.Count != 1) return;

			//find item index in its list
			int index =
				Formations.FindIndex(
					x => _formationFilter.AppliesTo(x) && x.Id.ToString() == LsvFormation.Items[LsvFormation.SelectedIndices[0]].Text);

			//create Formation instance
			Formation s = Formations[index];

			//create form an show it
			AutoForm f = new AutoForm("Plus d'informations");
			f.OkOnly = true;

			f.AddEntry("pct", new PictureEntry(Resources.Fatya, s.Name));
			f.AddEntry("duration", new LabelEntry("Durée de formation", s.NYear.ToString()));
			f.AddEntry("created", new LabelEntry("Date de création", s.CreationDate.ToString("dd MMMM yyyy")));
			f.AddEntry("desc", new LabelEntry("Description", s.Description));

			f.ShowDialog();
		}

		private void BtnFormationRemove_Click(object sender, EventArgs e)
		{
			//create an alert message box
			Alert a = new Alert("Voulez-vous vraiment supprimer " + LsvFormation.SelectedIndices.Count.IsPlural("formation") + "?", MessageBoxButtons.YesNo);

			//set buttons' text and colors
			a.Btn2Highlight = Highlight.Red;
			a.Btn2Text = "&Supprimer";
			a.Btn3Text = "&Annuler";

			if (a.Show() != DialogResult.Yes) return;

			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvFormation.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			RETRY:

			ApiRequest r = new ApiRequest();

			foreach (int t in orderedIndices)
			{
				int id = Formations.Find(x => _formationFilter.AppliesTo(x) && x.Id.ToString() == LsvFormation.Items[t].Text).Id;
				r.Add("DELETE_FORMATION", Param("id", id));
			}

			ApiResponse response = Server.ReadApiCommit(r);
			if (!string.IsNullOrEmpty(response.Exception))
			{
				if (Alert.Show(response.Exception, MessageBoxButtons.RetryCancel) == DialogResult.Retry)
					goto RETRY;
				return;
			}

			foreach (int t in orderedIndices)
			{
				int id = Formations.FindIndex(x => _formationFilter.AppliesTo(x) && x.Id.ToString() == LsvFormation.Items[t].Text);
				Formations.RemoveAt(id);
				LsvFormation.Items.RemoveAt(t);
				TxtFormationSearch.AutoCompleteCustomSource.RemoveAt(t);
			}

			//refresh related windows
			LoadFormationChildren();

			//resize all columns
			ResizeColumns(LsvFormation);
		}

		private void CsiFormationMore_Click(object sender, EventArgs e)
		{
			//todo: add student total

			//create SaveFileDialog and set filter and title
			SaveFileDialog s = new SaveFileDialog();
			s.Filter = "Fiche Excel (.xls)|*.xls";
			s.Title = "Choisir un emplacement";
			s.FileName = "Formations.xls";

			//if the user doesn't press OK, do nothing
			if (s.ShowDialog() != DialogResult.OK) return;

			//create work book and sheet instances
			Workbook book = new Workbook();
			Worksheet sheet = new Worksheet("Formations");

			//fill header columns
			sheet.Cells[0, 0] = new Cell("ID");
			sheet.Cells[0, 1] = new Cell("Nom");
			sheet.Cells[0, 2] = new Cell("Durée");
			sheet.Cells[0, 3] = new Cell("Date de création");
			sheet.Cells[0, 4] = new Cell("Description");

			int row = 1;
			foreach (int t in LsvFormation.SelectedIndices)//fill rows
			{
				Formation subject = Formations.First(x => x.Id.ToString() == LsvFormation.Items[t].Text); //get formation

				//store data
				sheet.Cells[row, 0] = new Cell(subject.Id);
				sheet.Cells[row, 1] = new Cell(subject.Name);
				sheet.Cells[row, 2] = new Cell(subject.NYear.IsPlural("an"));
				sheet.Cells[row, 3] = new Cell(subject.CreationDate.ToString("dd MMMM yyyy"));
				sheet.Cells[row, 4] = new Cell(subject.Description);

				row++;
			}

			//fill sheet with white spaces to avoid corruption
			for (int i = 0; i < 150; i++)
			{
				int col = 1;
				for (int j = 0; j < 10; j++)
				{
					sheet.Cells[row, col] = new Cell(" ");
					col++;
				}
				row++;
			}

			//save to file
			book.Worksheets.Add(sheet);
			book.Save(s.FileName);

			if (Alert.Show("Voulez-vous ouvrir le fichier?", MessageBoxButtons.YesNo) == DialogResult.Yes)
				Process.Start(s.FileName);
		}

		private void BtnFormationFilter_Click(object sender, EventArgs e)
		{
			//todo:add more filters
			//create auto form
			AutoForm form = new AutoForm("Filtrer les formations");

			//create list of formations
			List<string> formations = new List<string>();
			formations.Add("Pas de filtre");
			formations.AddRange(Formations.AsEnumerable().Select(x => x.Name));

			form.AddEntry("isActive", new CheckBoxEntry("Activer le filtre", _formationFilter.IsActive, false));
			form.AddEntry("idFormation", new ComboEntry("Formation", Formations.FindIndex(x => x.Id == _formationFilter.Id) + 1, false, formations.ToArray()));
			form.AddEntry("duration", new IntegerEntry("Durée de la formation", _formationFilter.Duration, 0, 6)); //todo fix filter activation

			if (form.ShowDialog() != DialogResult.OK) return; //if the user doesn't click ok, do nothing

			//save filter
			_formationFilter.IsActive = (bool)form.GetValue("isActive");
			_formationFilter.Duration = (int)form.GetValue("duration");
			int id = ((int)form.GetValue("idFormation")) - 1;

			_formationFilter.Id = id < 0 ? id : Formations[id].Id;

			//refresh listview
			LoadFormations();
		}

		private void CsiFormationClasses_Click(object sender, EventArgs e)
		{
			//find formation index
			int i = Formations.FindIndex(x => x.Id.ToString() == LsvFormation.SelectedItems[0].Text);

			if (i == -1)
			{
				Notifier.Show("Pas de formation dont l'ID est " + LsvFormation.SelectedItems[0].Text);
				return;
			}

			Formation f = Formations[i];

			//remove previous class filter
			_classFilter = new ClassFilter();
			_classFilter.IsActive = true;
			_classFilter.IdFormation = f.Id;

			LoadClasses();

			HdrMain.SelectedItem = HdiClass;
		}

		private void CsiFormationStudents_Click(object sender, EventArgs e)
		{
			//find formation index
			int i = Formations.FindIndex(x => x.Id.ToString() == LsvFormation.SelectedItems[0].Text);

			if (i == -1)
			{
				Notifier.Show("Pas de formation dont l'ID est " + LsvFormation.SelectedItems[0].Text);
				return;
			}

			Formation f = Formations[i];

			//remove previous student filter
			_studentFilter = new StudentFilter();
			_studentFilter.IsActive = true;
			_studentFilter.IdFormation = f.Id;

			LoadStudents();

			HdrMain.SelectedItem = HdiStudent;
		}

		private void TxtFormationSearch_AutoCompleteSelected(object sender, EventArgs e)
		{
			//reset the filter
			_formationFilter = new FormationFilter();

			//set name filter and activate it
			_formationFilter.Search = TxtFormationSearch.Text;
			_formationFilter.IsActive = true;

			//set the "clear" image instead of "search" in txtbox
			TxbFormation.Image = Resources.Delete;

			//refresh listview
			LoadFormations();
		}

		private void TxtFormationSearch_ButtonClicked(object sender, ButtonEventArgs e)
		{
			if (!string.IsNullOrEmpty(_formationFilter.Search))
			{
				TxtFormationSearch.Text = _formationFilter.Search = "";

				LoadFormations();

				TxbFormation.Image = Resources.Search;
			}
			else if (!string.IsNullOrEmpty(TxtFormationSearch.Text))
				TxtFormationSearch_AutoCompleteSelected(null, null);
		}

		private void TxtFormationSearch_TextChangedDelayed(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(TxtFormationSearch.Text))
				TxtFormationSearch_AutoCompleteSelected(null, null);
			else
			{
				_formationFilter.Search = "";

				LoadFormations();

				TxbFormation.Image = Resources.Search;
			}
		}

		public void LoadFormationChildren()
		{
            LoadMarks();
			LoadClasses();
			LoadModules();
			LoadSubjects();
			LoadStudents();
			LoadRequests();
		}

		#endregion

		#region Classes

		/// <summary>
		/// Loads classes from the Classes list to the listview
		/// </summary>
		private void LoadClasses()
		{
			_classesLoaded = true;

			//pause listview drawing to save resources
			LsvClass.BeginUpdate();

			//clear all previous items
			LsvClass.Items.Clear();
			TxtClassSearch.AutoCompleteCustomSource.Clear();

			foreach (Class klass in Classes)
			{
				LoadClass(klass, -1);
			}

			//resize all columns
			ResizeColumns(LsvClass);

			//resume listview drawing
			LsvClass.EndUpdate();

			CheckNotifications(); //todo add notifier message
		}
		/// <summary>
		/// Adds class to listview
		/// </summary>
		/// <param name="klass">Class to add</param>
		/// <param name="def">Index to where to insert the item</param>
		private void LoadClass(Class klass, int def)
		{
			if (!_classFilter.AppliesTo(klass)) return;

			//load info from class
			string id = klass.Id.ToString();
			string name = klass.Name;
			string start = klass.StartedAt.ToString("yyyy");
			string modules = klass.ModuleCount.IsPlural("module");
			string effectif = klass.StudentCount.IsPlural("étudiant");

			//if the index where to put the item == -1, set it as the last one
			if (def == -1) def = LsvClass.Items.Count;

			//add item to listview and to search autocomplete
			ListViewItem l = new ListViewItem(new[] { id, name, start, modules, effectif });
			if (klass.NeedsAttention) l.BackColor = Color.Wheat;

			LsvClass.Items.Insert(def, l);

			TxtClassSearch.AutoCompleteCustomSource.Insert(def, name);
		}

		private void LsvClass_SelectedIndexChanged(object sender, EventArgs e)
		{
			BtnClassMisc.Enabled = BtnClassEdit.Enabled = BtnClassRemove.Enabled = CmiClassMore.Enabled = LsvClass.SelectedIndices.Count != 0;

			foreach (var item in CmsClass.Items)
			{
				if (item is Sep) return;

				((ToolStripItem)item).Enabled = LsvClass.SelectedItems.Count == 1;
			}
		}

		private bool OpenClassDialog(Class klass, bool edit, int def)
		{
			//find the index of the formation in the list (not the id)
			int formationIndex = Formations.FindIndex(x => x.Id == klass.IdFormation);

			//create autoform instance (if edit==true, title is "Modifier classname" else "Ajouter une classe")
			AutoForm form = new AutoForm(edit ? "Modifier " + klass.Name : "Ajouter une classe");

			form.AddEntry("started", new DateEntry("Date de début", klass.StartedAt));
			form.AddEntry("nyear", new IntegerEntry("Année", klass.Nyear, 1, 6));
			form.AddEntry("idFormation", new ComboEntry("Formation", formationIndex, true, Formations.ToArray()));

			//show form and make sure the user clicked yes
			if (form.ShowDialog() != DialogResult.OK) return false;

			//get values from form
			int id = edit ? klass.Id : Classes.Max(x => x.Id) + 1;
			DateTime started = DateTime.ParseExact(form.GetValue("started").ToString(), "d", CultureInfo.CurrentCulture);
			int nyear = (int)form.GetValue("nyear");
			int idFormation = Formations[(int)form.GetValue("idFormation")].Id;

			//todo:send request and verify result

			//create class from form 
			Class c = new Class(id, started, nyear, idFormation);
			Classes.Add(c);

			LoadClassChildren();

			//add class to listview
			LoadClass(c, def);

			//resize all columns
			ResizeColumns(LsvClass);

			return true;
		}

		private void BtnClassAdd_Click(object sender, EventArgs e)
		{
			OpenClassDialog(new Class(0), false, -1);
		}

		private void BtnClassEdit_Click(object sender, EventArgs e)
		{
			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvClass.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			for (int i = 0; i < orderedIndices.Count; i++) //iterate through selected items
			{
				//get the list index of the class
				int classIndex = Classes.FindIndex(x => _classFilter.AppliesTo(x) && x.Id.ToString() == LsvClass.Items[orderedIndices[i]].Text);

				if (OpenClassDialog(Classes[classIndex], true, orderedIndices[i])) //if the edit window is validated, remove the previous one
				{
					Classes.RemoveAt(classIndex);
					LsvClass.Items.RemoveAt(orderedIndices[i] + 1);
					TxtClassSearch.AutoCompleteCustomSource.RemoveAt(orderedIndices[i] + 1);
					continue;
				}
				//if not and this is not the last item, ask for confirmation to continue editing
				if (orderedIndices.Count <= 1 || i == orderedIndices.Count - 1) continue;

				Alert a = new Alert("Voulez vous continuer à modifier les classes?", "", MessageBoxButtons.YesNo);

				a.Btn2Text = "&Continuer";
				a.Btn3Text = "&Annuler";
				//if the user chooses to stop editing, exit
				if (a.Show() != DialogResult.Yes) break;
			}

			//resize all columns
			ResizeColumns(LsvClass);
		}

		private void LsvClass_DoubleClick(object sender, EventArgs e)
		{
			//if no item is selected, do nothing
			if (LsvClass.SelectedIndices.Count != 1) return;

			//find item index in its list
			int index =
				Classes.FindIndex(
					x => _classFilter.AppliesTo(x) && x.Id.ToString() == LsvClass.Items[LsvClass.SelectedIndices[0]].Text);

			//create Formation instance
			Class c = Classes[index];

			//create form an show it
			AutoForm f = new AutoForm("Plus d'informations");
			f.OkOnly = true;

			f.AddEntry("name", new LabelEntry("Nom", c.Name));
			f.AddEntry("started", new LabelEntry("Année de début", c.StartedAt.ToString("yyyy")));
			f.AddEntry("nyear", new LabelEntry("Année", c.Nyear.ToLetters(false)));
			f.AddEntry("studentCount", new LabelEntry("Effectif", c.StudentCount.IsPlural("étudiant")));

			f.ShowDialog();
		}

		private void BtnClassRemove_Click(object sender, EventArgs e)
		{
			//create an alert message box
			Alert a = new Alert("Voulez-vous vraiment supprimer " + LsvClass.SelectedIndices.Count.IsPlural("classe") + "?", MessageBoxButtons.YesNo);

			//set buttons' text and colors
			a.Btn2Highlight = Highlight.Red;
			a.Btn2Text = "&Supprimer";
			a.Btn3Text = "&Annuler";

			if (a.Show() != DialogResult.Yes) return;

			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvClass.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			foreach (int t in orderedIndices)
			{
				//todo: send request and check state
				Classes.RemoveAt(Classes.FindIndex(x => _classFilter.AppliesTo(x) && x.Id.ToString() == LsvClass.Items[t].Text));
				LsvClass.Items.RemoveAt(t);
				TxtClassSearch.AutoCompleteCustomSource.RemoveAt(t);
			}

			LoadClassChildren();

			//resize all columns
			ResizeColumns(LsvClass);
		}

		private void BtnClassFilter_Click(object sender, EventArgs e)
		{
			//todo:add more filters
			//create auto form
			AutoForm form = new AutoForm("Filtrer les classes");

			//create a list of all classes
			List<string> classes = new List<string>();
			classes.Add("Pas de filtre");
			classes.AddRange(Classes.Select(x => x.Name));

			//create a list of all formations
			List<string> formations = new List<string>();
			formations.Add("Pas de filtre");
			formations.Add("Aucune");
			formations.AddRange(Formations.Select(x => x.Name));

			form.AddEntry("isActive", new CheckBoxEntry("Activer le filtre", _classFilter.IsActive, false));
			form.AddEntry("id", new ComboEntry("Classe", Classes.FindIndex(x => x.Id == _classFilter.Id) + 1, false, classes.ToArray()));
			form.AddEntry("idFormation", new ComboEntry("Formation", Formations.FindId(x => x.Id == _classFilter.IdFormation), false, formations.ToArray()));
			form.AddEntry("nYear", new IntegerEntry("Année", _classFilter.Nyear, 0, 6));
			form.AddEntry("studentCount", new IntegerEntry("Effectif", _classFilter.StudentCount, -1, 30)); //todo fix filter activation

			if (form.ShowDialog() != DialogResult.OK) return; //if the user doesn't click ok, do nothing

			//save filter
			_classFilter.IsActive = (bool)form.GetValue("isActive");
			int id = (int)form.GetValue("id") - 1;
			int idFormation = (int)form.GetValue("idFormation") - 2;
			_classFilter.Nyear = (int)form.GetValue("nYear");
			_classFilter.StudentCount = (int)form.GetValue("studentCount");

			_classFilter.Id = id < 0 ? id : Classes[id].Id;
			_classFilter.IdFormation = idFormation < 0 ? idFormation : Formations[idFormation].Id;

			//refresh listview
			LoadClasses();
		}

		private void TxtClassSearch_AutoCompleteSelected(object sender, EventArgs e)
		{
			//reset the filter
			_classFilter = new ClassFilter();

			//set name filter and activate it
			_classFilter.Search = TxtClassSearch.Text;
			_classFilter.IsActive = true;

			//set the "clear" image instead of "search" in txtbox
			TxbClass.Image = Resources.Delete;

			//refresh listview
			LoadClasses();
		}

		private void TxtClassSearch_ButtonClicked(object sender, ButtonEventArgs e)
		{
			if (!string.IsNullOrEmpty(_classFilter.Search))
			{
				TxtClassSearch.Text = _classFilter.Search = "";

				LoadClasses();

				TxbClass.Image = Resources.Search;
			}
			else if (!string.IsNullOrEmpty(TxtClassSearch.Text))
				TxtClassSearch_AutoCompleteSelected(null, null);
		}

		private void TxtClassSearch_TextChangedDelayed(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(TxtClassSearch.Text))
				TxtClassSearch_AutoCompleteSelected(null, null);
			else
			{
				_classFilter.Search = "";

				LoadClasses();

				TxbClass.Image = Resources.Search;
			}
		}

		private void CmiClassFormation_Click(object sender, EventArgs e)
		{
			//find class index
			int i = Classes.FindIndex(x => x.Id.ToString() == LsvClass.SelectedItems[0].Text);

			if (i == -1)
			{
				Notifier.Show("Pas de classe dont l'ID est " + LsvClass.SelectedItems[0].Text);
				return;
			}

			Class c = Classes[i];

			//remove previous formation filter
			_formationFilter = new FormationFilter();
			_formationFilter.IsActive = true;
			_formationFilter.Id = c.IdFormation;

			LoadFormations();

			HdrMain.SelectedItem = HdiFormation;
		}

		private void CmiClassStudents_Click(object sender, EventArgs e)
		{
			//find class index
			int i = Classes.FindIndex(x => x.Id.ToString() == LsvClass.SelectedItems[0].Text);

			if (i == -1)
			{
				Notifier.Show("Pas de classe dont l'ID est " + LsvClass.SelectedItems[0].Text);
				return;
			}

			Class c = Classes[i];

			//remove previous student filter
			_studentFilter = new StudentFilter();
			_studentFilter.IsActive = true;
			_studentFilter.IdClass = c.Id;

			LoadStudents();

			HdrMain.SelectedItem = HdiStudent;
		}

		/// <summary>
		/// Gets called when changes happen on classes to update related listviews
		/// </summary>
		private void LoadClassChildren()
        {
            LoadMarks();
			LoadFormations();
			LoadStudents();
			LoadRequests();
			LoadModules();
		}

		#endregion

		#region Modules
		/// <summary>
		/// Loads students from the Students list to the listview
		/// </summary>
		private void LoadModules()
		{
			_modulesLoaded = true;
			LsvModule.BeginUpdate(); //called to help with the performance
			LsvModule.Items.Clear(); //remove all current items
			TxtModuleSearch.AutoCompleteCustomSource.Clear();

			foreach (Module module in Modules)//iterate through all modules
			{
				LoadModule(module, -1);
			}

			//resize all columns first to the content then to the header
			ResizeColumns(LsvModule);

			LsvModule.EndUpdate(); //called to resume painting the listview

			CheckNotifications();//todo add notifier message
		}
		/// <summary>
		/// Adds a module to the listview
		/// </summary>
		/// <param name="module">Module to add</param>
		/// <param name="def">If def != -1, the module will be inserted at that index</param>
		private void LoadModule(Module module, int def)
		{
			if (!_moduleFilter.AppliesTo(module)) return;

			//calculate some info from the class
			string id = module.Id.ToString();
			string name = module.Name;
			string minMark = module.MinMark.ToString();
			string coef = module.Coefficient.ToString();
			string subjectCount = module.SubjectCount.IsPlural("matière");
			string className = module.ClassName;

			if (def == -1) def = LsvModule.Items.Count;

			//add new item to listview
			ListViewItem l = new ListViewItem(new[] { id, name, minMark, coef, subjectCount, className });
			if (module.NeedsAttention) l.BackColor = Color.Wheat;

			LsvModule.Items.Insert(def, l);

			TxtModuleSearch.AutoCompleteCustomSource.Insert(def, name);
		}

		private void LsvModule_SelectedIndexChanged(object sender, EventArgs e)
		{
			//enable edit/remove/misc buttons if there are selected items
			CmiModuleMore.Enabled = BtnModuleEdit.Enabled = BtnModuleRemove.Enabled = BtnModuleRemove.Enabled = BtnModuleMisc.Enabled = LsvModule.SelectedIndices.Count > 0;

			foreach (var item in CmsModule.Items)
			{
				if (item is Sep) return;

				((ToolStripItem)item).Enabled = LsvModule.SelectedItems.Count == 1;
			}
		}

		private bool OpenModuleDialog(Module module, bool edit, int def)
		{
			//create autoform instance (if edit==true, title is "Modifier name" else "Ajouter un module")
			AutoForm form = new AutoForm(edit ? "Modifier " + module.Name : "Ajouter un module");

			//create classes list
			List<string> classes = new List<string>();
			classes.Add("Aucune");
			classes.AddRange(Classes.Select(x => x.Name));

			form.AddEntry("name", new TextEntry("Name", module.Name, true));
			form.AddEntry("minMark", new IntegerEntry("Note éliminatoire", module.MinMark, 1, 19));
			form.AddEntry("coef", new IntegerEntry("Coéfficient", module.Coefficient, 1, 100));
			form.AddEntry("class", new ComboEntry("Classe", module.ClassExists ? module.IdClass + 1 : 0, true, classes.ToArray()));

			//show form and make sure the user clicked yes
			if (form.ShowDialog() != DialogResult.OK) return false;

			//get values from form
            int id = edit ? module.Id : (Modules.Count == 0 ? 0 : Modules.Max(x => x.Id) + 1);
			string name = form.GetValue("name").ToString();
			int minMark = (int)form.GetValue("minMark");
			int coef = (int)form.GetValue("coef");
			int klass = (int)form.GetValue("class") - 1;
            
			//todo:send request and verify result

			//create module from form 
			Module m = new Module(id, name, minMark, coef, klass);
			Modules.Add(m);

			LoadModuleChildren();

			//add Module to listview
			LoadModule(m, def);

			//resize all columns
			ResizeColumns(LsvModule);

			return true;
		}

		private void BtnModuleAdd_Click(object sender, EventArgs e)
		{
			OpenModuleDialog(new Module(0), false, -1);
		}

		private void BtnModuleEdit_Click(object sender, EventArgs e)
		{
			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvModule.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			for (int i = 0; i < orderedIndices.Count; i++) //iterate through selected items
			{
				//get the list index of the student
				int moduleIndex = Modules.FindIndex(x => _moduleFilter.AppliesTo(x) && x.Id.ToString() == LsvModule.Items[orderedIndices[i]].Text);

				if (OpenModuleDialog(Modules[moduleIndex], true, orderedIndices[i])) //if the edit window is validated, remove the previous one
				{
					Modules.RemoveAt(moduleIndex);
					LsvModule.Items.RemoveAt(orderedIndices[i] + 1);
					TxtModuleSearch.AutoCompleteCustomSource.RemoveAt(orderedIndices[i] + 1);
					continue;
				}
				//if not and this is not the last item, ask for confirmation to continue editing
				if (orderedIndices.Count <= 1 || i == orderedIndices.Count - 1) continue;

				Alert a = new Alert("Voulez vous continuer à modifier les modules?", "", MessageBoxButtons.YesNo);

				a.Btn2Text = "&Continuer";
				a.Btn3Text = "&Annuler";
				//if the user chooses to stop editing, exit
				if (a.Show() != DialogResult.Yes) break;
			}

			//resize all columns
			ResizeColumns(LsvModule);
		}

		private void LsvModule_DoubleClick(object sender, EventArgs e)
		{
			//if no item is selected, do nothing
			if (LsvModule.SelectedIndices.Count != 1) return;

			//find item index in its list
			int index =
				Modules.FindIndex(
					x => _moduleFilter.AppliesTo(x) && x.Id.ToString() == LsvModule.Items[LsvModule.SelectedIndices[0]].Text);

			//create module instance
			Module s = Modules[index];

			//create form an show it
			AutoForm f = new AutoForm("Plus d'informations");
			f.OkOnly = true;

			f.AddEntry("name", new LabelEntry("Nom", s.ClassName));
			f.AddEntry("minmark", new LabelEntry("Note éliminatoire", s.MinMark.ToString()));
			f.AddEntry("coef", new LabelEntry("Coéfficient", s.Coefficient.ToString()));
			f.AddEntry("class", new LabelEntry("Classe", s.ClassName));
			f.AddEntry("subjectCount", new LabelEntry("Matières", s.SubjectCount.IsPlural("matière")));

			f.ShowDialog();
		}

		private void BtnModuleRemove_Click(object sender, EventArgs e)
		{
			//create an alert message box
			Alert a = new Alert("Voulez-vous vraiment supprimer " + LsvModule.SelectedIndices.Count.IsPlural("module") + "?", MessageBoxButtons.YesNo);

			//set buttons' text and colors
			a.Btn2Highlight = Highlight.Red;
			a.Btn2Text = "&Supprimer";
			a.Btn3Text = "&Annuler";

			if (a.Show() != DialogResult.Yes) return;

			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvModule.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			foreach (int t in orderedIndices)
			{
				//todo: send request and check state
				Modules.RemoveAt(Modules.FindIndex(x => _moduleFilter.AppliesTo(x) && x.Id.ToString() == LsvModule.Items[t].Text));
				LsvModule.Items.RemoveAt(t);
				TxtModuleSearch.AutoCompleteCustomSource.RemoveAt(t);
			}

			CheckNotifications();

			//resize all columns
			ResizeColumns(LsvModule);
		}

		private void BtnModuleFilter_Click(object sender, EventArgs e)
		{
			//todo:add more filters
			//create auto form
			AutoForm form = new AutoForm("Filtrer les modules");

			//create a list of classes
			List<string> classes = new List<string>();
			classes.Add("Pas de filtre");
			classes.Add("Aucune");
			classes.AddRange(Classes.AsEnumerable().Select(x => x.Name));

			//create a list of formations
			List<string> formations = new List<string>();
			formations.Add("Pas de filtre");
			formations.Add("Aucune");
			formations.AddRange(Formations.AsEnumerable().Select(x => x.Name));

			//create a list of modules
			List<string> modules = new List<string>();
			modules.Add("Pas de filtre");
			modules.AddRange(Modules.AsEnumerable().Select(x => x.Name));

			form.AddEntry("isActive", new CheckBoxEntry("Activer le filtre", _moduleFilter.IsActive, false));
			form.AddEntry("id", new ComboEntry("Module", Modules.FindIndex(x=>x.Id == _moduleFilter.Id)+1, false, modules.ToArray()));
			form.AddEntry("idClass", new ComboEntry("Classe", Classes.FindId(x => x.Id == _moduleFilter.IdClass), false, classes.ToArray()));
			form.AddEntry("idFormation", new ComboEntry("Formation", Formations.FindId(x=>x.Id == _moduleFilter.IdFormation), false, formations.ToArray()));

			if (form.ShowDialog() != DialogResult.OK) return; //if the user doesn't click ok, do nothing

			//save filter
			_moduleFilter.IsActive = (bool)form.GetValue("isActive");
			int id = ((int)form.GetValue("id")) - 1;
			int idClass = ((int)form.GetValue("idClass")) - 2;
			int idFormation = ((int)form.GetValue("idFormation")) - 2;

			_moduleFilter.Id = id < 0 ? id : Modules[id].Id;
			_moduleFilter.IdClass = idClass < 0 ? idClass : Classes[idClass].Id;
			_moduleFilter.IdFormation = idFormation < 0 ? idFormation : Formations[idFormation].Id;

			//refresh listview
			LoadModules();
		}

		private void TxtModuleSearch_AutoCompleteSelected(object sender, EventArgs e)
		{
			//reset the filter
			_moduleFilter = new ModuleFilter();

			//set name filter and activate it
			_moduleFilter.Search = TxtModuleSearch.Text;
			_moduleFilter.IsActive = true;

			//set the "clear" image instead of "search" in txtbox
			TxbModule.Image = Resources.Delete;

			//refresh listview
			LoadModules();
		}

		private void TxtModuleSearch_ButtonClicked(object sender, ButtonEventArgs e)
		{
			if (!string.IsNullOrEmpty(_moduleFilter.Search))
			{
				TxtModuleSearch.Text = _moduleFilter.Search = "";

				LoadModules();

				TxbModule.Image = Resources.Search;
			}
			else if (!string.IsNullOrEmpty(TxtModuleSearch.Text))
				TxtModuleSearch_AutoCompleteSelected(null, null);
		}

		private void TxtModuleSearch_TextChangedDelayed(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(TxtModuleSearch.Text))
				TxtModuleSearch_AutoCompleteSelected(null, null);
			else
			{
				_moduleFilter.Search = "";

				LoadModules();

				TxbModule.Image = Resources.Search;
			}
		}

		private void CmiModuleFormation_Click(object sender, EventArgs e)
		{
			int index = Modules.FindIndex(x => x.Id.ToString() == LsvModule.SelectedItems[0].Text);

			if (index == -1 || !Modules[index].FormationExists) return;

			_formationFilter = new FormationFilter();
			_formationFilter.IsActive = true;
			_formationFilter.Id = Modules[index].IdFormation;

			LoadFormations();

			HdrMain.SelectedItem = HdiFormation;
		}

		private void CmiModuleClass_Click(object sender, EventArgs e)
		{
			int index = Modules.FindIndex(x => x.Id.ToString() == LsvModule.SelectedItems[0].Text);

			if (index == -1 || !Modules[index].ClassExists) return;

			_classFilter = new ClassFilter();
			_classFilter.IsActive = true;
			_classFilter.Id = Modules[index].IdClass;

			LoadClasses();

			HdrMain.SelectedItem = HdiClass;
		}

		private void LoadModuleChildren()
        {
            LoadMarks();
            LoadSubjects();
		}

		#endregion
        
		#region Subjects

		private void LoadSubjects()
		{
			_subjectsLoaded = true;
			LsvSubject.BeginUpdate(); //called to help with the performance
			LsvSubject.Items.Clear(); //remove all current items
			TxtSubjectSearch.AutoCompleteCustomSource.Clear();

			foreach (Subject subject in Subjects)//iterate through all subjects
			{
				LoadSubject(subject, -1);
			}

			//resize all columns first to the content then to the header
			ResizeColumns(LsvSubject);

			LsvSubject.EndUpdate(); //called to resume painting the listview

			CheckNotifications();//todo add notifier message
		}

		private void LoadSubject(Subject subject, int def)
		{
			if (!_subjectFilter.AppliesTo(subject)) return;

			//calculate some info from the class
			string id = subject.Id.ToString();
			string name = subject.Name;
			string minMark = subject.MinMark.ToString();
			string coef = subject.Coefficient.ToString();
			string teacher = subject.TeacherName;
			string klass = subject.ClassName;
			string module = subject.ModuleName;

			if (def == -1) def = LsvSubject.Items.Count;

			//add new item to listview
			ListViewItem l = new ListViewItem(new[] { id, name, minMark, coef, teacher, klass, module });
			if (subject.NeedsAttention) l.BackColor = Color.Wheat;

			LsvSubject.Items.Insert(def, l);

			TxtSubjectSearch.AutoCompleteCustomSource.Insert(def, name);
		}

		private void LsvSubject_SelectedIndexChanged(object sender, EventArgs e)
		{
			BtnSubjectEdit.Enabled = BtnSubjectRemove.Enabled = BtnSubjectMisc.Enabled = CmiSubjectMore.Enabled = LsvSubject.SelectedIndices.Count > 0;
			
			foreach (var item in CmsSubject.Items)
			{
				if (item is Sep) return;

				((ToolStripItem)item).Enabled = LsvSubject.SelectedItems.Count == 1;
			}
		}

		private bool OpenSubjectDialog(Subject subject, bool edit, int def)
		{
			//create autoform instance (if edit==true, title is "Modifier name" else "Ajouter une matière")
			AutoForm form = new AutoForm(edit ? "Modifier " + subject.Name : "Ajouter une matière");

			//create classes list
			List<string> teachers = new List<string>();
			teachers.Add("Aucun");
			teachers.AddRange(Teachers.Select(x => x.GetFullName()));

			//create modules list
			List<string> modules = new List<string>();
			modules.Add("Aucun");
			modules.AddRange(Modules.Select(x => x.Name));

			form.AddEntry("name", new TextEntry("Name", subject.Name, true));
			form.AddEntry("minMark", new IntegerEntry("Note éliminatoire", subject.MinMark, 1, 19));
			form.AddEntry("coef", new IntegerEntry("Coéfficient", subject.Coefficient, 1, 100));
			form.AddEntry("teacher", new ComboEntry("Enseignant", subject.TeacherExists ? subject.IdTeacher + 1 : 0, true, teachers.ToArray()));
			form.AddEntry("module", new ComboEntry("Module", subject.ModuleExists ? subject.IdModule + 1 : 0, true, modules.ToArray()));

			//show form and make sure the user clicked yes
			if (form.ShowDialog() != DialogResult.OK) return false;

			//get values from form
			int id = edit ? subject.Id : Subjects.Max(x => x.Id) + 1;
			string name = form.GetValue("name").ToString();
			int minMark = (int)form.GetValue("minMark");
			int coef = (int)form.GetValue("coef");
			int teacher = (int)form.GetValue("teacher") - 1;
			int module = (int)form.GetValue("module") - 1;

			//todo:send request and verify result

			//create module from form 
			Subject m = new Subject(id, name, minMark, coef, module, teacher);
			Subjects.Add(m);

			LoadSubjectChildren();

			//add Subject to listview
			LoadSubject(m, def);

			//resize all columns
			ResizeColumns(LsvSubject);

			return true;
		}

		private void BtnSubjectAdd_Click(object sender, EventArgs e)
		{
			OpenSubjectDialog(new Subject(0), false, -1);
		}

		private void BtnSubjectEdit_Click(object sender, EventArgs e)
		{
			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvSubject.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			for (int i = 0; i < orderedIndices.Count; i++) //iterate through selected items
			{
				//get the list index of the subject
				int subjectIndex = Subjects.FindIndex(x => _subjectFilter.AppliesTo(x) && x.Id.ToString() == LsvSubject.Items[orderedIndices[i]].Text);

				if (OpenSubjectDialog(Subjects[subjectIndex], true, orderedIndices[i])) //if the edit window is validated, remove the previous one
				{
					Subjects.RemoveAt(subjectIndex);
					LsvSubject.Items.RemoveAt(orderedIndices[i] + 1); //Additional information: InvalidArgument=Value of '2' is not valid for 'index'.
					TxtSubjectSearch.AutoCompleteCustomSource.RemoveAt(orderedIndices[i] + 1);
					continue;
				}
				//if not and this is not the last item, ask for confirmation to continue editing
				if (orderedIndices.Count <= 1 || i == orderedIndices.Count - 1) continue;

				Alert a = new Alert("Voulez vous continuer à modifier les matières?", "", MessageBoxButtons.YesNo);

				a.Btn2Text = "&Continuer";
				a.Btn3Text = "&Annuler";
				//if the user chooses to stop editing, exit
				if (a.Show() != DialogResult.Yes) break;
			}

			//resize all columns
			ResizeColumns(LsvSubject);
		}

		private void LsvSubject_DoubleClick(object sender, EventArgs e)
		{
			//if no item is selected, do nothing
			if (LsvSubject.SelectedIndices.Count != 1) return;

			//find item index in its list
			int index =
				Subjects.FindIndex(
					x => _subjectFilter.AppliesTo(x) && x.Id.ToString() == LsvSubject.Items[LsvSubject.SelectedIndices[0]].Text);

			//create stubject instance
			Subject s = Subjects[index];

			//create form an show it
			AutoForm f = new AutoForm("Plus d'informations");
			f.OkOnly = true;

			f.AddEntry("name", new LabelEntry("Nom", s.ClassName));
			f.AddEntry("minmark", new LabelEntry("Note éliminatoire", s.MinMark.ToString()));
			f.AddEntry("coef", new LabelEntry("Coéfficient", s.Coefficient.ToString()));
			f.AddEntry("teacher", new LabelEntry("Enseignant", s.TeacherName));
			f.AddEntry("module", new LabelEntry("Module", s.ModuleName));
			f.AddEntry("class", new LabelEntry("Classe", s.ClassName));

			f.ShowDialog();
		}

		private void BtnSubjectRemove_Click(object sender, EventArgs e)
		{
			//create an alert message box
			Alert a = new Alert("Voulez-vous vraiment supprimer " + LsvSubject.SelectedIndices.Count.IsPlural("matière") + "?", MessageBoxButtons.YesNo);

			//set buttons' text and colors
			a.Btn2Highlight = Highlight.Red;
			a.Btn2Text = "&Supprimer";
			a.Btn3Text = "&Annuler";

			if (a.Show() != DialogResult.Yes) return;

			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvSubject.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			foreach (int t in orderedIndices)
			{
				//todo: send request and check state
				Subjects.RemoveAt(Subjects.FindIndex(x => _subjectFilter.AppliesTo(x) && x.Id.ToString() == LsvSubject.Items[t].Text));
				LsvSubject.Items.RemoveAt(t);
				TxtSubjectSearch.AutoCompleteCustomSource.RemoveAt(t);
			}

			CheckNotifications();

			//resize all columns
			ResizeColumns(LsvSubject);
		}

		private void BtnSubjectFilter_Click(object sender, EventArgs e)
		{
			//todo:add more filters
			//create auto form
			AutoForm form = new AutoForm("Filtrer les matières");

			//create a list of classes
			List<string> classes = new List<string>();
			classes.Add("Pas de filtre");
			classes.Add("Aucune");
			classes.AddRange(Classes.AsEnumerable().Select(x => x.Name));

			//create a list of formations
			List<string> formations = new List<string>();
			formations.Add("Pas de filtre");
			formations.Add("Aucune");
			formations.AddRange(Formations.AsEnumerable().Select(x => x.Name));

			//create a list of modules
			List<string> modules = new List<string>();
			modules.Add("Pas de filtre");
			modules.Add("Aucun");
			modules.AddRange(Modules.AsEnumerable().Select(x => x.Name));

			//create a list of teachers
			List<string> teachers = new List<string>();
			teachers.Add("Pas de filtre");
			teachers.Add("Aucun");
			teachers.AddRange(Teachers.AsEnumerable().Select(x => x.GetFullName()));

			form.AddEntry("isActive", new CheckBoxEntry("Activer le filtre", _subjectFilter.IsActive, false));
			form.AddEntry("id", new ComboEntry("Module", Modules.FindId(x => x.Id == _subjectFilter.IdModule), false, modules.ToArray()));
			form.AddEntry("idFormation", new ComboEntry("Formation", Formations.FindId(x => x.Id == _subjectFilter.IdFormation), false, formations.ToArray()));
			form.AddEntry("idClass", new ComboEntry("Classe", Classes.FindId(x => x.Id == _subjectFilter.IdClass), false, classes.ToArray()));
			form.AddEntry("idTeacher", new ComboEntry("Enseignant", Teachers.FindId(x => x.Id == _subjectFilter.IdTeacher), false, teachers.ToArray()));

			if (form.ShowDialog() != DialogResult.OK) return; //if the user doesn't click ok, do nothing

			//save filter
			_subjectFilter.IsActive = (bool)form.GetValue("isActive");
			int idModule = ((int)form.GetValue("id")) - 2;
			int idClass = ((int)form.GetValue("idClass")) - 2;
			int idFormation = ((int)form.GetValue("idFormation")) - 2;
			int idTeacher = ((int)form.GetValue("idTeacher")) - 2;

			_subjectFilter.IdModule = idModule < 0 ? idModule : Modules[idModule].Id;
			_subjectFilter.IdClass = idClass < 0 ? idClass : Classes[idClass].Id;
			_subjectFilter.IdFormation = idFormation < 0 ? idFormation : Formations[idFormation].Id;
			_subjectFilter.IdTeacher = idTeacher < 0 ? idTeacher : Teachers[idTeacher].Id;

			//refresh listview
			LoadSubjects();
		}

		private void TxtSubjectSearch_AutoCompleteSelected(object sender, EventArgs e)
		{
			//reset the filter
			_subjectFilter = new SubjectFilter();

			//set name filter and activate it
			_subjectFilter.Search = TxtSubjectSearch.Text;
			_subjectFilter.IsActive = true;

			//set the "clear" image instead of "search" in txtbox
			TxbSubject.Image = Resources.Delete;

			//refresh listview
			LoadSubjects();
		}

		private void TxtSubjectSearch_ButtonClicked(object sender, ButtonEventArgs e)
		{
			if (!string.IsNullOrEmpty(_subjectFilter.Search))
			{
				TxtSubjectSearch.Text = _subjectFilter.Search = "";

				LoadSubjects();

				TxbSubject.Image = Resources.Search;
			}
			else if (!string.IsNullOrEmpty(TxtSubjectSearch.Text))
				TxtSubjectSearch_AutoCompleteSelected(null, null);
		}

		private void TxtSubjectSearch_TextChangedDelayed(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(TxtSubjectSearch.Text))
				TxtSubjectSearch_AutoCompleteSelected(null, null);
			else
			{
				_subjectFilter.Search = "";

				LoadSubjects();

				TxbSubject.Image = Resources.Search;
			}
		}

		private void CmiSubjectFormation_Click(object sender, EventArgs e)
		{
			int index = Subjects.FindIndex(x => x.Id.ToString() == LsvSubject.SelectedItems[0].Text);

			if (index == -1 || !Subjects[index].FormationExists) return;

			_formationFilter = new FormationFilter();
			_formationFilter.IsActive = true;
			_formationFilter.Id = Subjects[index].IdFormation;

			LoadFormations();

			HdrMain.SelectedItem = HdiFormation;
		}

		private void CmiSubjectClass_Click(object sender, EventArgs e)
		{
			int index = Subjects.FindIndex(x => x.Id.ToString() == LsvSubject.SelectedItems[0].Text);

			if (index == -1 || !Subjects[index].ClassExists) return;

			_classFilter = new ClassFilter();
			_classFilter.IsActive = true;
			_classFilter.Id = Subjects[index].IdClass;

			LoadClasses();

			HdrMain.SelectedItem = HdiClass;
		}

		private void CmiSubjectModule_Click(object sender, EventArgs e)
		{
			int index = Subjects.FindIndex(x => x.Id.ToString() == LsvSubject.SelectedItems[0].Text);

			if (index == -1 || !Subjects[index].ModuleExists) return;

			_moduleFilter = new ModuleFilter();
			_moduleFilter.IsActive = true;
			_moduleFilter.Id = Subjects[index].IdModule;

			LoadModules();

			HdrMain.SelectedItem = HdiModule;
		}

		private void CmiSubjectTeacher_Click(object sender, EventArgs e)
		{
			int index = Subjects.FindIndex(x => x.Id.ToString() == LsvSubject.SelectedItems[0].Text);

			if (index == -1 || !Subjects[index].TeacherExists) return;

			_teacherFilter = new TeacherFilter();
			_teacherFilter.IsActive = true;
			_teacherFilter.Id = Subjects[index].IdTeacher;

			LoadTeachers();

			HdrMain.SelectedItem = HdiTeacher;
		}

		public void LoadSubjectChildren()
        {
            LoadMarks();
		}

		#endregion

		#region Teachers

		/// <summary>
		/// Loads teachers from the Teachers list to the listview
		/// </summary>
		private void LoadTeachers()
		{
			_teachersLoaded = true;

			//pause listview drawing to save resources
			LsvTeacher.BeginUpdate();

			//clear all previous items
			LsvTeacher.Items.Clear();
			TxtTeacherSearch.AutoCompleteCustomSource.Clear();

			foreach (Teacher teacher in Teachers)
			{
				LoadTeacher(teacher, -1);
			}

			//resize all columns
			ResizeColumns(LsvTeacher);

			//resume listview drawing
			LsvTeacher.EndUpdate();

			//todo:HdiTeacher.Notify = Teachers.Count(x => x.ClassName == "N/A");
		}
		/// <summary>
		/// Adds teacher to listview
		/// </summary>
		/// <param name="teacher">Teacher to add</param>
		/// <param name="def">Index to where to insert the item</param>
		private void LoadTeacher(Teacher teacher, int def)
		{
			if (!_teacherFilter.AppliesTo(teacher)) return;

			//load info from class
			string id = teacher.Id.ToString();
			string cin = teacher.Cin;
			string nsum = teacher.Nsum;
			string sex = teacher.Gender.ToString();
			string fullname = teacher.GetFullName();
			string subjectCount = teacher.SubjectCount.IsPlural("matière");
			string email = teacher.Email;
			string tel = teacher.Telephone;
			string address = teacher.Address;

			//if the index where to put the item == -1, set it as the last one
			if (def == -1) def = LsvTeacher.Items.Count;

			//add item to listview and to search autocomplete
			ListViewItem l = new ListViewItem(new[] {id, cin, nsum, sex, fullname, subjectCount, email, tel, address});
			if (teacher.NeedsAttention) l.BackColor = Color.Wheat;
			
			LsvTeacher.Items.Insert(def, l);
			TxtTeacherSearch.AutoCompleteCustomSource.Insert(def, fullname);
		}

		private bool OpenTeacherDialog(Teacher teacher, bool edit, int def)
		{
			//create autoform instance (if edit==true, title is "Modifier name LNAME" else "Ajouter un enseignant")
			AutoForm form = new AutoForm(edit ? "Modifier " + teacher.GetFullName() : "Ajouter un enseignant");

			form.AddEntry("cin", new RuleEntry("CIN", teacher.Cin, Validation.FixedLength, true, 7));
			form.AddEntry("nsum", new RuleEntry("Numéro de somme", teacher.Nsum, Validation.FixedLength, true, 6, true));
			form.AddEntry("fname", new TextEntry("Prénom", teacher.Fname, true));
			form.AddEntry("lname", new TextEntry("Nom", teacher.Lname, true));
			form.AddEntry("email", new RuleEntry("Email", teacher.Email, Validation.Email, true));
			form.AddEntry("telephone", new RuleEntry("Télephone", teacher.Telephone, Validation.FixedLength, true, 10, true));
			form.AddEntry("Gender", new ComboEntry("Sexe", (int)teacher.Gender, true, "Masculin", "Féminin"));
			form.AddEntry("address", new RuleEntry("Adresse", teacher.Address, Validation.MaxLength, true, 50));

			//show form and make sure the user clicked yes
			if (form.ShowDialog() != DialogResult.OK) return false;

			//get values from form
			int id = edit ? teacher.Id : Teachers.Max(x => x.Id) + 1;
			string cin = form.GetValue("cin").ToString();
			string nsum = form.GetValue("nsum").ToString();
			string fname = form.GetValue("fname").ToString();
			string lname = form.GetValue("lname").ToString();
			string email = form.GetValue("email").ToString();
			string tel = form.GetValue("telephone").ToString();
			Gender gender = (Gender)form.GetValue("Gender");
			string address = form.GetValue("address").ToString();

			//todo:send request and verify result

			//create teacher from form 
			Teacher t = new Teacher(id, fname, lname, teacher.Password, email, teacher.Bio, tel, address, nsum, cin, gender);
			Teachers.Add(t);

			//add teacher to listview
			LoadTeacher(t, def);

			CheckNotifications();

			//resize all columns
			ResizeColumns(LsvTeacher);

			return true;
		}

		private void BtnTeacherAdd_Click(object sender, EventArgs e)
		{
			OpenTeacherDialog(new Teacher(0), false, -1);
		}

		private void BtnTeacherEdit_Click(object sender, EventArgs e)
		{
			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvTeacher.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			for (int i = 0; i < orderedIndices.Count; i++) //iterate through selected items
			{
				//get the list index of the teacher
				int teacherIndex = Teachers.FindIndex(x => _teacherFilter.AppliesTo(x) && x.Id.ToString() == LsvTeacher.Items[orderedIndices[i]].Text);

				if (OpenTeacherDialog(Teachers[teacherIndex], true, orderedIndices[i])) //if the edit window is validated, remove the previous one
				{
					Teachers.RemoveAt(teacherIndex);
					LsvTeacher.Items.RemoveAt(orderedIndices[i] + 1);
					TxtTeacherSearch.AutoCompleteCustomSource.RemoveAt(orderedIndices[i] + 1);
					continue;
				}
				//if not and this is not the last item, ask for confirmation to continue editing
				if (orderedIndices.Count <= 1 || i == orderedIndices.Count - 1) continue;

				Alert a = new Alert("Voulez vous continuer à modifier les enseignants?", "", MessageBoxButtons.YesNo);

				a.Btn2Text = "&Continuer";
				a.Btn3Text = "&Annuler";
				//if the user chooses to stop editing, exit
				if (a.Show() != DialogResult.Yes) break;
			}

			//resize all columns
			ResizeColumns(LsvTeacher);
		}

		private void LsvTeacher_DoubleClick(object sender, EventArgs e)
		{
			//if no item is selected, do nothing
			if (LsvTeacher.SelectedIndices.Count != 1) return;

			//find item index in its list
			int index =
				Teachers.FindIndex(
					x => _teacherFilter.AppliesTo(x) && x.Id.ToString() == LsvTeacher.Items[LsvTeacher.SelectedIndices[0]].Text);

			//create teacher instance
			Teacher t = Teachers[index];

			//create form an show it
			AutoForm f = new AutoForm("Plus d'informations");
			f.OkOnly = true;

			f.AddEntry("pct", new PictureEntry(Resources.Fatya, t.GetFullName()));
			f.AddEntry("Gender", new LabelEntry("Sexe", new[] { "Aucun", "Masculin", "Féminin" }[(int)t.Gender]));
			f.AddEntry("email", new LabelEntry("Email", t.Email));
			f.AddEntry("tel", new LabelEntry("Télephone", t.Telephone));
			f.AddEntry("adr", new LabelEntry("Adresse", t.Address));
			f.AddEntry("cin", new LabelEntry("CIN", t.Cin));
			f.AddEntry("nsum", new LabelEntry("Numéro de somme", t.Nsum));

			f.ShowDialog();
		}

		private void LsvTeacher_SelectedIndexChanged(object sender, EventArgs e)
		{
			BtnTeacherMisc.Enabled = BtnTeacherEdit.Enabled = BtnTeacherDelete.Enabled = CmiTeacherMore.Enabled = LsvTeacher.SelectedIndices.Count != 0;
			
			foreach (var item in CmsTeacher.Items)
			{
				if (item is Sep) return;

				((ToolStripItem)item).Enabled = LsvTeacher.SelectedItems.Count == 1;
			}
		}

		private void BtnTeacherDelete_Click(object sender, EventArgs e)
		{
			//create an alert message box
			Alert a = new Alert("Voulez-vous vraiment supprimer " + LsvTeacher.SelectedIndices.Count.IsPlural("enseignant") + "?", MessageBoxButtons.YesNo);

			//set buttons' text and colors
			a.Btn2Highlight = Highlight.Red;
			a.Btn2Text = "&Supprimer";
			a.Btn3Text = "&Annuler";

			if (a.Show() != DialogResult.Yes) return;

			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvTeacher.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			foreach (int t in orderedIndices)
			{
				//todo: send request and check state
				Teachers.RemoveAt(Teachers.FindIndex(x => _teacherFilter.AppliesTo(x) && x.Id.ToString() == LsvTeacher.Items[t].Text));
				LsvTeacher.Items.RemoveAt(t);
				TxtTeacherSearch.AutoCompleteCustomSource.RemoveAt(t);
			}

			CheckNotifications();

			//resize all columns
			ResizeColumns(LsvTeacher);
		}

		private void BtnTeacherFilter_Click(object sender, EventArgs e)
		{
			//todo:add more filters
			//create auto form
			AutoForm form = new AutoForm("Filtrer les enseignants");

			//create a list of teachers
			List<string> teachers = new List<string>();
			teachers.Add("Pas de filtre");
			teachers.AddRange(Teachers.Select(x => x.GetFullName()));

			//create a list of classes
			List<string> classes = new List<string>();
			classes.Add("Pas de filtre");
			classes.Add("Aucune");
			classes.AddRange(Classes.Select(x => x.Name));

			form.AddEntry("isActive", new CheckBoxEntry("Activer le filtre", _teacherFilter.IsActive, false));
			form.AddEntry("id", new ComboEntry("Enseignant", Teachers.FindIndex(x => x.Id == _teacherFilter.Id) + 1, false, teachers.ToArray()));
			form.AddEntry("Gender", new ComboEntry("Sexe", (int)_teacherFilter.Gender, false, "Aucun", "Masculin", "Féminin"));
			form.AddEntry("class", new ComboEntry("Classe", Classes.FindId(x => x.Id == _teacherFilter.IdClass), false, classes.ToArray()));

			if (form.ShowDialog() != DialogResult.OK) return; //if the user doesn't click ok, do nothing

			//save filter
			_teacherFilter.IsActive = (bool)form.GetValue("isActive");
			int id = (int)form.GetValue("id") -1;
			_teacherFilter.Gender = (Gender)form.GetValue("Gender");
			int idClass = (int)form.GetValue("class") - 2 < 0 ? (int)form.GetValue("class") - 2 : Teachers[(int)form.GetValue("class") - 2].Id;

			_teacherFilter.Id = id < 0 ? id : Teachers[id].Id;
			_teacherFilter.IdClass = idClass < 0 ? idClass : Classes[idClass].Id;

			//refresh listview
			LoadTeachers();
		}

		private void TxtTeacherSearch_AutoCompleteSelected(object sender, EventArgs e)
		{
			//reset the filter
			_teacherFilter = new TeacherFilter();

			//set name filter and activate it
			_teacherFilter.Search = TxtTeacherSearch.Text;
			_teacherFilter.IsActive = true;

			//set the "clear" image instead of "search" in txtbox
			TxbTeacher.Image = Resources.Delete;

			//refresh listview
			LoadTeachers();
		}

		private void CsiTeacherMore_Click(object sender, EventArgs e)
		{
			//todo: add formation & class info

			//create SaveFileDialog and set filter and title
			SaveFileDialog s = new SaveFileDialog();
			s.Filter = "Fiche Excel (.xls)|*.xls";
			s.Title = "Choisir un emplacement";
			s.FileName = "Enseignants.xls";

			//if the user doesn't press OK, do nothing
			if (s.ShowDialog() != DialogResult.OK) return;

			//create work book and sheet instances
			Workbook book = new Workbook();
			Worksheet sheet = new Worksheet("Enseignants");

			//fill header columns
			sheet.Cells[0, 0] = new Cell("ID");
			sheet.Cells[0, 1] = new Cell("Prénom");
			sheet.Cells[0, 2] = new Cell("Nom");
			sheet.Cells[0, 3] = new Cell("Email");
			sheet.Cells[0, 4] = new Cell("Télephone");
			sheet.Cells[0, 5] = new Cell("Adresse");
			sheet.Cells[0, 6] = new Cell("Numéro de somme");
			sheet.Cells[0, 7] = new Cell("CIN");
			sheet.Cells[0, 8] = new Cell("Sexe");

			int row = 1;
			foreach (int t in LsvTeacher.SelectedIndices)//fill rows
			{
				Teacher subject = Teachers.First(x => x.Id.ToString() == LsvTeacher.Items[t].Text); //get student

				//store data
				sheet.Cells[row, 0] = new Cell(subject.Id);
				sheet.Cells[row, 1] = new Cell(subject.Fname);
				sheet.Cells[row, 2] = new Cell(subject.Lname);
				sheet.Cells[row, 3] = new Cell(subject.Email);
				sheet.Cells[row, 4] = new Cell(subject.Telephone);
				sheet.Cells[row, 5] = new Cell(subject.Address);
				sheet.Cells[row, 6] = new Cell(subject.Nsum);
				sheet.Cells[row, 7] = new Cell(subject.Cin);
				sheet.Cells[row, 8] = new Cell(subject.Gender.ToString());

				row++;
			}

			//fill sheet with white spaces to avoid corruption
			for (int i = 0; i < 150; i++)
			{
				int col = 1;
				for (int j = 0; j < 10; j++)
				{
					sheet.Cells[row, col] = new Cell(" ");
					col++;
				}
				row++;
			}

			//save to file
			book.Worksheets.Add(sheet);
			book.Save(s.FileName);

			if (Alert.Show("Voulez-vous ouvrir le fichier?", MessageBoxButtons.YesNo) == DialogResult.Yes)
				Process.Start(s.FileName);
		}

		private void TxtTeacherSearch_ButtonClicked(object sender, ButtonEventArgs e)
		{
			if (!string.IsNullOrEmpty(_teacherFilter.Search))
			{
				TxtTeacherSearch.Text = _teacherFilter.Search = "";

				LoadTeachers();

				TxbTeacher.Image = Resources.Search;
			}
			else if (!string.IsNullOrEmpty(TxtStudentSearch.Text))
				TxtTeacherSearch_AutoCompleteSelected(null, null);
		}

		private void TxtTeacherSearch_TextChangedDelayed(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(TxtTeacherSearch.Text))
				TxtTeacherSearch_AutoCompleteSelected(null, null);
			else
			{
				_teacherFilter.Search = "";

				LoadTeachers();

				TxbTeacher.Image = Resources.Search;
			}
		}

		#endregion

		#region Students

		/// <summary>
		/// Loads students from the Students list to the listview
		/// </summary>
		private void LoadStudents()
		{
			_studentsLoaded = true;
			LsvStudent.BeginUpdate(); //called to help with the performance
			LsvStudent.Items.Clear(); //remove all current items
			TxtStudentSearch.AutoCompleteCustomSource.Clear();

			foreach (Student student in Students)//iterate through all students
			{
				LoadStudent(student, -1);
			}

			//resize all columns first to the content then to the header
			ResizeColumns(LsvStudent);

			LsvStudent.EndUpdate(); //called to resume painting the listview

			CheckNotifications();//todo add notifier message
		}

		/// <summary>
		/// Adds a student to the listview
		/// </summary>
		/// <param name="student">Student to add</param>
		/// <param name="def">If def != -1, the student will be inserted at that index</param>
		private void LoadStudent(Student student, int def)
		{
			if (!_studentFilter.AppliesTo(student)) return;

			//calculate some info from the class
			string id = student.Id.ToString();
			string cin = student.Cin;
			string cne = student.Cne;
			string sex = student.Gender.ToString();
			string fullname = student.GetFullName();
			string klass = student.ClassName;
			string email = student.Email;
			string bday = student.Bday.ToString("dd MMMM yyyy");
			string tel = student.Telephone;
			string address = student.Address;

			if (def == -1) def = LsvStudent.Items.Count;

			//add new item to listview
			ListViewItem l = new ListViewItem(new[] { id, cin, cne, sex, fullname, klass, email, bday, tel, address });
			if (student.NeedsAttention) l.BackColor = Color.Wheat;

			LsvStudent.Items.Insert(def, l);

			TxtStudentSearch.AutoCompleteCustomSource.Insert(def, fullname);
		}

		private void LsvStudent_SelectedIndexChanged(object sender, EventArgs e)
		{
			//enable edit/remove/misc buttons if there are selected items
			CmiStudentMore.Enabled = BtnStudentEdit.Enabled = BtnStudentRemove.Enabled = BtnStudentMisc.Enabled = CmiStudentMore.Enabled = LsvStudent.SelectedIndices.Count > 0;
			
			foreach (var item in CmsStudent.Items)
			{
				if (item is Sep) return;

				((ToolStripItem)item).Enabled = LsvStudent.SelectedItems.Count == 1;
			}
		}

		private bool OpenStudentDialog(Student student, bool edit, int def)
		{
			//create autoform instance (if edit==true, title is "Modifier name LNAME" else "Ajouter un étudiant")
			AutoForm form = new AutoForm(edit ? "Modifier " + student.GetFullName() : "Ajouter un étudiant");

			//create classes list
			List<string> classes = new List<string>();
			classes.Add("Aucune");
			classes.AddRange(Classes.Select(x => x.Name));

			form.AddEntry("cin", new RuleEntry("CIN", student.Cin, Validation.FixedLength, true, 7));
			form.AddEntry("cne", new RuleEntry("CNE", student.Cne, Validation.FixedLength, true, 10, true));
			form.AddEntry("fname", new TextEntry("Prénom", student.Fname, true));
			form.AddEntry("lname", new TextEntry("Nom", student.Lname, true));
			form.AddEntry("email", new RuleEntry("Email", student.Email, Validation.Email, true));
			form.AddEntry("bac", new RuleEntry("Baccalauréat", student.Bac, Validation.MaxLength, true, 15));
			form.AddEntry("telephone", new RuleEntry("Télephone", student.Telephone, Validation.FixedLength, true, 10, true));
			form.AddEntry("Gender", new ComboEntry("Sexe", (int)student.Gender - 1, true, "Masculin", "Féminin"));
			form.AddEntry("bday", new DateEntry("Date de naissance", student.Bday));
			form.AddEntry("address", new RuleEntry("Adresse", student.Address, Validation.MaxLength, true, 50));
			form.AddEntry("class", new ComboEntry("Classe", student.ClassExists ? student.IdClass + 1 : 0, true, classes.ToArray()));

			//show form and make sure the user clicked yes
			if (form.ShowDialog() != DialogResult.OK) return false;

			//get values from form
			int id = edit ? student.Id : Students.Max(x => x.Id) + 1;
			string cin = form.GetValue("cin").ToString();
			string cne = form.GetValue("cne").ToString();
			string fname = form.GetValue("fname").ToString();
			string lname = form.GetValue("lname").ToString();
			string email = form.GetValue("email").ToString();
			string bac = form.GetValue("bac").ToString();
			string tel = form.GetValue("telephone").ToString();
			Gender gender = (Gender)((int)form.GetValue("Gender") + 1);
			DateTime bday = DateTime.ParseExact(form.GetValue("bday").ToString(), "d", CultureInfo.CurrentCulture);
			string address = form.GetValue("address").ToString();
			int idClass = (int)form.GetValue("class") - 1;

			//todo:send request and verify result

			//create student from form 
			Student s = new Student(id, fname, lname, student.Password, email, student.Bio, bac, tel, address, cne, cin, gender,
				bday, idClass);
			Students.Add(s);

			LoadStudentChildren();

			//add student to listview
			LoadStudent(s, def);

			//resize all columns
			ResizeColumns(LsvStudent);

			return true;
		}

		private void BtnStudentAdd_Click(object sender, EventArgs e)
		{
			OpenStudentDialog(new Student(0), false, -1);
		}

		private void BtnStudentEdit_Click(object sender, EventArgs e)
		{
			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvStudent.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			for (int i = 0; i < orderedIndices.Count; i++) //iterate through selected items
			{
				//get the list index of the student
				int studentIndex = Students.FindIndex(x => _studentFilter.AppliesTo(x) && x.Id.ToString() == LsvStudent.Items[orderedIndices[i]].Text);

				if (OpenStudentDialog(Students[studentIndex], true, orderedIndices[i])) //if the edit window is validated, remove the previous one
				{
					Students.RemoveAt(studentIndex);
					LsvStudent.Items.RemoveAt(orderedIndices[i] + 1);
					TxtStudentSearch.AutoCompleteCustomSource.RemoveAt(orderedIndices[i] + 1);
					continue;
				}
				//if not and this is not the last item, ask for confirmation to continue editing
				if (orderedIndices.Count <= 1 || i == orderedIndices.Count - 1) continue;

				Alert a = new Alert("Voulez vous continuer à modifier les étudiants?", "", MessageBoxButtons.YesNo);

				a.Btn2Text = "&Continuer";
				a.Btn3Text = "&Annuler";
				//if the user chooses to stop editing, exit
				if (a.Show() != DialogResult.Yes) break;
			}

			//resize all columns
			ResizeColumns(LsvStudent);
		}

		private void LsvStudent_DoubleClick(object sender, EventArgs e)
		{
			//if no item is selected, do nothing
			if (LsvStudent.SelectedIndices.Count != 1) return;

			//find item index in its list
			int index =
				Students.FindIndex(
					x => _studentFilter.AppliesTo(x) && x.Id.ToString() == LsvStudent.Items[LsvStudent.SelectedIndices[0]].Text);

			//create student instance
			Student s = Students[index];

			//create form an show it
			AutoForm f = new AutoForm("Plus d'informations");
			f.OkOnly = true;

			f.AddEntry("pct", new PictureEntry(Resources.Fatya, s.GetFullName()));
			f.AddEntry("Gender", new LabelEntry("Sexe", new[] { "Aucun", "Masculin", "Féminin" }[(int)s.Gender]));//todo fix Gender litteral
			f.AddEntry("class", new LabelEntry("Classe", s.ClassName));
			f.AddEntry("email", new LabelEntry("Email", s.Email));
			f.AddEntry("tel", new LabelEntry("Télephone", s.Telephone));
			f.AddEntry("adr", new LabelEntry("Adresse", s.Address));
			f.AddEntry("cin", new LabelEntry("CIN", s.Cin));
			f.AddEntry("cne", new LabelEntry("CNE", s.Cne));
			f.AddEntry("bac", new LabelEntry("Baccalauréat", s.Bac));
			f.AddEntry("bday", new LabelEntry("Date de naissance", s.Bday.ToString("dd MMMM yyyy")));

			f.ShowDialog();
		}

		private void BtnStudentRemove_Click(object sender, EventArgs e)
		{
			//create an alert message box
			Alert a = new Alert("Voulez-vous vraiment supprimer " + LsvStudent.SelectedIndices.Count.IsPlural("étudiant") + "?", MessageBoxButtons.YesNo);

			//set buttons' text and colors
			a.Btn2Highlight = Highlight.Red;
			a.Btn2Text = "&Supprimer";
			a.Btn3Text = "&Annuler";

			if (a.Show() != DialogResult.Yes) return;

			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvStudent.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			foreach (int t in orderedIndices)
			{
				//todo: send request and check state
				Students.RemoveAt(Students.FindIndex(x => _studentFilter.AppliesTo(x) && x.Id.ToString() == LsvStudent.Items[t].Text));
				LsvStudent.Items.RemoveAt(t);
				TxtStudentSearch.AutoCompleteCustomSource.RemoveAt(t);
			}

			LoadStudentChildren();

			//resize all columns
			ResizeColumns(LsvStudent);
		}

		private void CsiStudentMore_Click(object sender, EventArgs e)
		{
			//todo: add formation & class info

			//create SaveFileDialog and set filter and title
			SaveFileDialog s = new SaveFileDialog();
			s.Filter = "Fiche Excel (.xls)|*.xls";
			s.Title = "Choisir un emplacement";
			s.FileName = "Etudiants.xls";

			//if the user doesn't press OK, do nothing
			if (s.ShowDialog() != DialogResult.OK) return;

			//create work book and sheet instances
			Workbook book = new Workbook();
			Worksheet sheet = new Worksheet("Etudiants");

			//fill header columns
			sheet.Cells[0, 0] = new Cell("ID");
			sheet.Cells[0, 1] = new Cell("Prénom");
			sheet.Cells[0, 2] = new Cell("Nom");
			sheet.Cells[0, 3] = new Cell("Email");
			sheet.Cells[0, 4] = new Cell("Baccalauréat");
			sheet.Cells[0, 5] = new Cell("Télephone");
			sheet.Cells[0, 6] = new Cell("Adresse");
			sheet.Cells[0, 7] = new Cell("CNE");
			sheet.Cells[0, 8] = new Cell("CIN");
			sheet.Cells[0, 9] = new Cell("Sexe");
			sheet.Cells[0, 10] = new Cell("Date de naissance");

			int row = 1;
			foreach (int t in LsvStudent.SelectedIndices)//fill rows
			{
				Student subject = Students.First(x => x.Id.ToString() == LsvStudent.Items[t].Text); //get student

				//store data
				sheet.Cells[row, 0] = new Cell(subject.Id);
				sheet.Cells[row, 1] = new Cell(subject.Fname);
				sheet.Cells[row, 2] = new Cell(subject.Lname);
				sheet.Cells[row, 3] = new Cell(subject.Email);
				sheet.Cells[row, 4] = new Cell(subject.Bac);
				sheet.Cells[row, 5] = new Cell(subject.Telephone);
				sheet.Cells[row, 6] = new Cell(subject.Address);
				sheet.Cells[row, 7] = new Cell(subject.Cne);
				sheet.Cells[row, 8] = new Cell(subject.Cin);
				sheet.Cells[row, 9] = new Cell(subject.Gender.ToString());
				sheet.Cells[row, 10] = new Cell(subject.Bday.ToString("dd MMMM yyyy"));

				row++;
			}

			//fill sheet with white spaces to avoid corruption
			for (int i = 0; i < 150; i++)
			{
				int col = 1;
				for (int j = 0; j < 10; j++)
				{
					sheet.Cells[row, col] = new Cell(" ");
					col++;
				}
				row++;
			}

			//save to file
			book.Worksheets.Add(sheet);
			book.Save(s.FileName);

			if (Alert.Show("Voulez-vous ouvrir le fichier?", MessageBoxButtons.YesNo) == DialogResult.Yes)
				Process.Start(s.FileName);
		}

		private void BtnStudentFilter_Click(object sender, EventArgs e)
		{
			//todo:add more filters
			//create auto form
			AutoForm form = new AutoForm("Filtrer les étudiants");

			//create a list of classes
			List<string> classes = new List<string>();
			classes.Add("Pas de filtre");
			classes.Add("Aucune");
			classes.AddRange(Classes.AsEnumerable().Select(x => x.Name));

			//create a list of formations
			List<string> formations = new List<string>();
			formations.Add("Pas de filtre");
			formations.Add("Aucune");
			formations.AddRange(Formations.AsEnumerable().Select(x => x.Name));

			//create a list of students
			List<string> students = new List<string>();
			students.Add("Pas de filtre");
			students.AddRange(Students.AsEnumerable().Select(x => x.GetFullName()));

			form.AddEntry("isActive", new CheckBoxEntry("Activer le filtre", _studentFilter.IsActive, false));
			form.AddEntry("Gender", new ComboEntry("Sexe", (int)_studentFilter.Gender, false, "Aucun", "Masculin", "Féminin"));
			form.AddEntry("bac", new TextEntry("Baccalauréat", _studentFilter.Bac, false));
			form.AddEntry("id", new ComboEntry("Etudiant", Students.FindIndex(x=>x.Id==_studentFilter.Id) + 1, false, students.ToArray()));
			form.AddEntry("idClass", new ComboEntry("Classe", Classes.FindId(x => x.Id == _studentFilter.IdClass), false, classes.ToArray()));
			form.AddEntry("idFormation", new ComboEntry("Formation", Formations.FindId(x => x.Id == _studentFilter.IdFormation), false, formations.ToArray()));

			if (form.ShowDialog() != DialogResult.OK) return; //if the user doesn't click ok, do nothing

			//save filter
			_studentFilter.IsActive = (bool)form.GetValue("isActive");
			_studentFilter.Gender = (Gender)form.GetValue("Gender");
			_studentFilter.Bac = (string)form.GetValue("bac");
			int id = ((int)form.GetValue("id")) - 1;
			int idClass = ((int)form.GetValue("idClass")) - 2;
			int idFormation = ((int)form.GetValue("idFormation")) - 2;

			_studentFilter.Id = id < 0 ? id : Students[id].Id;
			_studentFilter.IdClass = idClass < 0 ? idClass : Classes[id].Id;
			_studentFilter.IdFormation = idFormation < 0 ? idFormation : Formations[idFormation].Id;

			//refresh listview
			LoadStudents();
		}

		private void TxtStudentSearch_AutoCompleteSelected(object sender, EventArgs e)
		{
			//reset the filter
			_studentFilter = new StudentFilter();

			//set name filter and activate it
			_studentFilter.Search = TxtStudentSearch.Text;
			_studentFilter.IsActive = true;

			//set the "clear" image instead of "search" in txtbox
			TxbStudent.Image = Resources.Delete;

			//refresh listview
			LoadStudents();
		}

		private void TxtStudentSearch_ButtonClicked(object sender, ButtonEventArgs e)
		{
			if (!string.IsNullOrEmpty(_studentFilter.Search))
			{
				TxtStudentSearch.Text = _studentFilter.Search = "";

				LoadStudents();

				TxbStudent.Image = Resources.Search;
			}
			else if (!string.IsNullOrEmpty(TxtStudentSearch.Text))
				TxtStudentSearch_AutoCompleteSelected(null, null);
		}

		private void TxtStudentSearch_TextChangedDelayed(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(TxtStudentSearch.Text))
				TxtStudentSearch_AutoCompleteSelected(null, null);
			else
			{
				_studentFilter.Search = "";

				LoadStudents();

				TxbStudent.Image = Resources.Search;
			}
		}

		private void CsiStudentFormation_Click(object sender, EventArgs e)
		{
			//find student index
			int i = Students.FindIndex(x => x.Id.ToString() == LsvStudent.SelectedItems[0].Text);

			if (i == -1)
			{
				Notifier.Show("Pas d'étudiant dont l'ID est " + LsvStudent.SelectedItems[0].Text);
				return;
			}

			Student s = Students[i];

			//remove previous formation filter
			_formationFilter = new FormationFilter();
			_formationFilter.IsActive = true;
			_formationFilter.Id = s.IdFormation;

			LoadFormations();

			HdrMain.SelectedItem = HdiFormation;
		}

		private void CsiStudentClass_Click(object sender, EventArgs e)
		{
			//find student index
			int i = Students.FindIndex(x => x.Id.ToString() == LsvStudent.SelectedItems[0].Text);

			if (i == -1)
			{
				Notifier.Show("Pas d'étudiant dont l'ID est " + LsvStudent.SelectedItems[0].Text);
				return;
			}

			Student s = Students[i];

			//remove previous class filter
			_classFilter = new ClassFilter();
			_classFilter.IsActive = true;
			_classFilter.Id = s.IdClass;

			LoadClasses();

			HdrMain.SelectedItem = HdiClass;
		}

		private void CsiStudentRequests_Click(object sender, EventArgs e)
		{
			//find student index
			int i = Students.FindIndex(x => x.Id.ToString() == LsvStudent.SelectedItems[0].Text);

			if (i == -1)
			{
				Notifier.Show("Pas d'étudiant dont l'ID est " + LsvStudent.SelectedItems[0].Text);
				return;
			}

			Student s = Students[i];

			//remove previous request filter
			_requestsFilter = new RequestFilter();
			_requestsFilter.IsActive = true;
			_requestsFilter.IdStudent = s.Id;

			LoadRequests();

			HdrMain.SelectedItem = HdiRequest;
		}

		private void LoadStudentChildren()
        {
            LoadMarks();
			LoadModules();
			LoadClasses();
			LoadRequests();
		}

		#endregion

		#region Requests
    

		private void LoadRequests()
		{
			_requestsLoaded = true;
			LsvRequest.BeginUpdate(); //called to help with the performance
			LsvRequest.Items.Clear(); //remove all current items
			TxtRequestSearch.AutoCompleteCustomSource.Clear();

			foreach (Request request in Requests)//iterate through all requests
			{
				LoadRequest(request, -1);
			}

			//resize all columns first to the content then to the header
			ResizeColumns(LsvRequest);

			LsvRequest.EndUpdate(); //called to resume painting the listview

			CheckNotifications();//todo add notifier message
		}

		private void LoadRequest(Request request, int def)
		{
			if (!_requestsFilter.AppliesTo(request)) return;

			//calculate some info from the class
			string id = request.Id.ToString();
			string type = request.TypeLitteral();
			string state = request.StateLitteral();
			string student = request.StudentName;
			string klass = request.StudentClass;
			string desc = request.Description;
			string start = request.RequestedAt.ToString("dd MMMM yyyy");
			string end = request.DoneLitteral();

			if (def == -1) def = LsvRequest.Items.Count;

			//add new item to listview
			ListViewItem l = new ListViewItem(new[] { id, type, state, student, klass, desc, start, end });
			if (request.NeedsAttention) l.BackColor = Color.Wheat;

			LsvRequest.Items.Insert(def, l);

			TxtRequestSearch.AutoCompleteCustomSource.Insert(def, type);
		}

		private void LsvRequest_SelectedIndexChanged(object sender, EventArgs e)
		{
			BtnRequestState.Enabled = BtnRequestRemove.Enabled = BtnRequestMisc.Enabled = CmiRequestRespond.Enabled = LsvRequest.SelectedIndices.Count > 0;
			
			foreach (var item in CmsRequest.Items)
			{
				if (item is Sep) return;

				((ToolStripItem)item).Enabled = LsvRequest.SelectedItems.Count == 1;
			}
		}

		private bool OpenRequestDialog(ref int index)
		{
			//create autoform instance (if edit==true, title is "Modifier name LNAME" else "Ajouter un étudiant")
			AutoForm form = new AutoForm("Modifier l'état de la demande");

			form.AddEntry("state", new ComboEntry("Etat", index, true, new[] { "Validée", "En attente", "Rejetée" }));

			//show form and make sure the user clicked yes
			if (form.ShowDialog() != DialogResult.OK) return false;

			//get values from form
			index = (int)form.GetValue("state");

			return true;
		}

		private void BtnRequestState_Click(object sender, EventArgs e)
		{
			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvRequest.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();
			int stateIndex = 0;

			for (int i = 0; i < orderedIndices.Count; i++) //iterate through selected items
			{
				//get the list index of the request
				int requestIndex = Requests.FindIndex(x => _requestsFilter.AppliesTo(x) && x.Id.ToString() == LsvRequest.Items[orderedIndices[i]].Text);

				if (i == 0) stateIndex = (int)Requests[requestIndex].RequestState;
				else if (stateIndex != (int)Requests[requestIndex].RequestState) stateIndex = -1;
			}

			if (!OpenRequestDialog(ref stateIndex)) return; //if the edit window is validated, remove the previous one

			foreach (int t in orderedIndices)
			{
				//get the list index of the request
				int requestIndex = Requests.FindIndex(x => _requestsFilter.AppliesTo(x) && x.Id.ToString() == LsvRequest.Items[t].Text);
				Request r = Requests[requestIndex];
				RequestState newState = (RequestState)stateIndex;

				//if the request state changed, change the done at time
				if (newState != r.RequestState && (newState == RequestState.Done || newState == RequestState.Pending))
					r.DoneAt = DateTime.Now;

				r.RequestState = (RequestState)stateIndex;

				LoadRequest(r, t);
				Requests.RemoveAt(requestIndex);
				LsvRequest.Items.RemoveAt(t + 1);
				TxtRequestSearch.AutoCompleteCustomSource.RemoveAt(t + 1);
			}

			//resize all columns
			ResizeColumns(LsvRequest);
		}

		private void LsvRequest_DoubleClick(object sender, EventArgs e)
		{
			//if no item is selected, do nothing
			if (LsvRequest.SelectedIndices.Count != 1) return;

			//find item index in its list
			int index =
				Requests.FindIndex(
					x => _requestsFilter.AppliesTo(x) && x.Id.ToString() == LsvRequest.Items[LsvRequest.SelectedIndices[0]].Text);

			//create request instance
			Request r = Requests[index];

			//create form an show it
			AutoForm f = new AutoForm("Plus d'informations");
			f.OkOnly = true;

			f.AddEntry("name", new LabelEntry("Nom de l'étudiant", r.StudentName));
			f.AddEntry("class", new LabelEntry("Classe", r.StudentClass));
			f.AddEntry("type", new LabelEntry("Type", r.TypeLitteral()));
			f.AddEntry("desc", new LabelEntry("Description", r.Description));
			f.AddEntry("state", new LabelEntry("Etat", r.StateLitteral()));
			f.AddEntry("requested", new LabelEntry("Date de la demande", r.RequestedAt.ToString("dd MMMM yyyy")));
			if (r.RequestState == RequestState.Done || r.RequestState == RequestState.Rejected) f.AddEntry("done", new LabelEntry("Achevée", r.DoneLitteral()));

			f.ShowDialog();
		}

		private void BtnRequestRemove_Click(object sender, EventArgs e)
		{
			//create an alert message box
			Alert a = new Alert("Voulez-vous vraiment supprimer " + LsvRequest.SelectedIndices.Count.IsPlural("demande") + "?", MessageBoxButtons.YesNo);

			//set buttons' text and colors
			a.Btn2Highlight = Highlight.Red;
			a.Btn2Text = "&Supprimer";
			a.Btn3Text = "&Annuler";

			if (a.Show() != DialogResult.Yes) return;

			//order selected indexes from greatest to lowest
			List<int> orderedIndices = LsvRequest.SelectedIndices.Cast<int>().ToList();
			orderedIndices = orderedIndices.OrderByDescending(x => x).ToList();

			foreach (int t in orderedIndices)
			{
				//todo: send request and check state
				Requests.RemoveAt(Requests.FindIndex(x => _requestsFilter.AppliesTo(x) && x.Id.ToString() == LsvRequest.Items[t].Text));
				LsvRequest.Items.RemoveAt(t);
				TxtRequestSearch.AutoCompleteCustomSource.RemoveAt(t);
			}

			CheckNotifications();

			//resize all columns
			ResizeColumns(LsvRequest);
		}

		private void BtnRequestFilter_Click(object sender, EventArgs e)
		{
			//todo:add more filters
			//create auto form
			AutoForm form = new AutoForm("Filtrer les demandes");

			//create a list of classes
			List<string> classes = new List<string>();
			classes.Add("Pas de filtre");
			classes.Add("Aucune");
			classes.AddRange(Classes.AsEnumerable().Select(x => x.Name));

			//create a list of students
			List<string> students = new List<string>();
			students.Add("Pas de filtre");
			students.Add("Aucun");
			students.AddRange(Students.AsEnumerable().Select(x => x.GetFullName()));

			//create a list of states
			List<string> states = new List<string>();
			states.Add("Pas de filtre");
			states.Add("Validée");
			states.Add("En attente");
			states.Add("Rejetée");

			form.AddEntry("isActive", new CheckBoxEntry("Activer le filtre", _requestsFilter.IsActive, false));
			form.AddEntry("idClass", new ComboEntry("Classe", Classes.FindId(x => x.Id == _requestsFilter.IdClass), false, classes.ToArray()));
			form.AddEntry("idStudent", new ComboEntry("Etudiant", Students.FindId(x => x.Id == _requestsFilter.IdStudent), false, students.ToArray()));
			form.AddEntry("state", new ComboEntry("Etat de la demande", (int)_requestsFilter.RequestState + 1, false, states.ToArray()));

			if (form.ShowDialog() != DialogResult.OK) return; //if the user doesn't click ok, do nothing

			//save filter
			_requestsFilter.IsActive = (bool)form.GetValue("isActive");
			int idClass = ((int)form.GetValue("idClass")) - 2;
			int idStudent = ((int)form.GetValue("idStudent")) - 2;
			_requestsFilter.RequestState = (RequestState)((int)form.GetValue("state") - 1);

			_requestsFilter.IdClass = idClass < 0 ? idClass : Classes[idClass].Id;
			_requestsFilter.IdStudent = idStudent < 0 ? idStudent : Students[idStudent].Id;

			//refresh listview
			LoadRequests();
		}

		private void CmiRequestStudent_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Requests.FindIndex(x => x.Id.ToString() == LsvRequest.SelectedItems[0].Text);

			if (i == -1) return;

			Request f = Requests[i];

			//remove previous student filter
			_studentFilter = new StudentFilter();
			_studentFilter.IsActive = true;
			_studentFilter.Id = f.IdStudent;

			LoadStudents();

			HdrMain.SelectedItem = HdiStudent;
		}

		private void TxtRequestSearch_AutoCompleteSelected(object sender, EventArgs e)
		{
			//reset the filter
			_requestsFilter = new RequestFilter();

			//set name filter and activate it
			_requestsFilter.Search = TxtRequestSearch.Text;
			_requestsFilter.IsActive = true;

			//set the "clear" image instead of "search" in txtbox
			TxbRequest.Image = Resources.Delete;

			//refresh listview
			LoadRequests();
		}

		private void TxtRequestSearch_ButtonClicked(object sender, ButtonEventArgs e)
		{
			if (!string.IsNullOrEmpty(_requestsFilter.Search))
			{
				TxtRequestSearch.Text = _requestsFilter.Search = "";

				LoadRequests();

				TxbRequest.Image = Resources.Search;
			}
			else if (!string.IsNullOrEmpty(TxtRequestSearch.Text))
				TxtRequestSearch_AutoCompleteSelected(null, null);
		}

		private void TxtRequestSearch_TextChangedDelayed(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(TxtRequestSearch.Text))
				TxtRequestSearch_AutoCompleteSelected(null, null);
			else
			{
				_requestsFilter.Search = "";

				LoadRequests();

				TxbRequest.Image = Resources.Search;
			}
		}

		#endregion

        #region Marks_BELCAID

	    private void LoadMarks()
	    {
	        _marksLoaded = true;

            CbxMarkFormation.Items.Clear();
            CbxMarkClass.Items.Clear();
            CbxMarkModule.Items.Clear();
            CbxMarkSubject.Items.Clear();
            CbxMarkTest.Items.Clear();
            
	        foreach (Formation f in Formations.Where(x=>x.StudentCount!=0))
	        {
	            CbxMarkFormation.Items.Add(f.Id + " - " + f.Name);
	        }

	        if (CbxMarkFormation.Items.Count > 0) CbxMarkFormation.SelectedIndex = 0;

	        CheckComboEnabling();
	    }

	    #endregion

        #region Other methods

        private void ResizeColumns(ListView listView)
		{
			listView.BeginUpdate();//avoids jitter

			listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
			listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

			listView.EndUpdate();
		}

		public void SetTheme()
		{
			foreach (TabPage t in TlcMain.TabPages)
			{
				foreach (Control c in t.Controls)
				{
					if (!(c is ListView)) continue;

					PropertyInfo aProp = typeof(Control).GetProperty("DoubleBuffered", BindingFlags.NonPublic | BindingFlags.Instance);
					aProp.SetValue(c, true, null);
					SetWindowTheme(c.Handle, "explorer", null);
				}
			}

			WinRenderer w = new WinRenderer();

			CmsFormation.Renderer = w;
			CmsClass.Renderer = w;
			CmsRequest.Renderer = w;
			CmsModule.Renderer = w;
			CmsSubject.Renderer = w;
			CmsTeacher.Renderer = w;
			CmsStudent.Renderer = w;
		}

		public void CheckNotifications()
		{
			HdiFormation.Notify = Formations.Any(x => x.NeedsAttention);
			HdiClass.Notify = Classes.Any(x => x.NeedsAttention);
			HdiModule.Notify = Modules.Any(x => x.NeedsAttention);
			HdiSubject.Notify = Subjects.Any(x => x.NeedsAttention);
			HdiStudent.Notify = Students.Any(x => x.NeedsAttention);
			HdiTeacher.Notify = Teachers.Any(x => x.NeedsAttention);
			HdiRequest.Notify = Requests.Any(x => x.NeedsAttention);
		}

		private KeyValuePair<string, string> Param(string key, object value)
		{
			return new KeyValuePair<string, string>(key, value.ToString());
		}

		[DllImport("uxtheme.dll")]
		public extern static int SetWindowTheme(
			IntPtr hWnd,
			[MarshalAs(UnmanagedType.LPWStr)] string pszSubAppName,
			[MarshalAs(UnmanagedType.LPWStr)] string pszSubIdList);

		#endregion

		private void CmiClassTeachers_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Classes.FindIndex(x => x.Id.ToString() == LsvClass.SelectedItems[0].Text);

			if (i == -1) return;

			Class c = Classes[i];

			//remove previous student filter
			_teacherFilter = new TeacherFilter();
			_teacherFilter.IsActive = true;
			_teacherFilter.IdClass = c.Id;

			LoadTeachers();

			HdrMain.SelectedItem = HdiTeacher;
		}

		private void CmiModuleTeachers_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Modules.FindIndex(x => x.Id.ToString() == LsvModule.SelectedItems[0].Text);

			if (i == -1) return;

			Module m = Modules[i];

			//remove previous teacher filter
			_teacherFilter = new TeacherFilter();
			_teacherFilter.IsActive = true;
			_teacherFilter.IdModule = m.Id;

			LoadTeachers();

			HdrMain.SelectedItem = HdiTeacher;
		}

		private void CmiModuleSubjects_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Modules.FindIndex(x => x.Id.ToString() == LsvModule.SelectedItems[0].Text);

			if (i == -1) return;

			Module m = Modules[i];

			//remove previous subject filter
			_subjectFilter = new SubjectFilter();
			_subjectFilter.IsActive = true;
			_subjectFilter.IdModule = m.Id;

			LoadSubjects();

			HdrMain.SelectedItem = HdiSubject;
		}

		private void CmiTeachersClasses_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Teachers.FindIndex(x => x.Id.ToString() == LsvTeacher.SelectedItems[0].Text);

			if (i == -1) return;

			Teacher t = Teachers[i];

			//remove previous class filter
			_classFilter = new ClassFilter();
			_classFilter.IsActive = true;
			_classFilter.IdTeacher = t.Id;

			LoadClasses();

			HdrMain.SelectedItem = HdiClass;
		}

		private void CmiTeacherStudents_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Teachers.FindIndex(x => x.Id.ToString() == LsvTeacher.SelectedItems[0].Text);

			if (i == -1) return;

			Teacher t = Teachers[i];

			//remove previous student filter
			_studentFilter = new StudentFilter();
			_studentFilter.IsActive = true;
			_studentFilter.IdTeacher = t.Id;

			LoadStudents();

			HdrMain.SelectedItem = HdiStudent;
		}

		private void CmiStudentTeachers_Click(object sender, EventArgs e)
		{

		}

		private void CmiTeacherSubjects_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Teachers.FindIndex(x => x.Id.ToString() == LsvTeacher.SelectedItems[0].Text);

			if (i == -1) return;

			Teacher t = Teachers[i];

			//remove previous class filter
			_subjectFilter = new SubjectFilter();
			_subjectFilter.IsActive = true;
			_subjectFilter.IdTeacher = t.Id;

			LoadSubjects();

			HdrMain.SelectedItem = HdiSubject;
		}

		private void CmiClassModules_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Classes.FindIndex(x => x.Id.ToString() == LsvClass.SelectedItems[0].Text);

			if (i == -1) return;

			Class c = Classes[i];

			//remove previous module filter
			_moduleFilter = new ModuleFilter();
			_moduleFilter.IsActive = true;
			_moduleFilter.IdClass = c.Id;

			LoadModules();

			HdrMain.SelectedItem = HdiModule;
		}

		private void CmiFormationModules_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Formations.FindIndex(x => x.Id.ToString() == LsvFormation.SelectedItems[0].Text);

			if (i == -1) return;

			Formation c = Formations[i];

			//remove previous module filter
			_moduleFilter = new ModuleFilter();
			_moduleFilter.IsActive = true;
			_moduleFilter.IdFormation = c.Id;

			LoadModules();

			HdrMain.SelectedItem = HdiModule;
		}

		private void CmiFormationSubjects_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Formations.FindIndex(x => x.Id.ToString() == LsvFormation.SelectedItems[0].Text);

			if (i == -1) return;

			Formation c = Formations[i];

			//remove previous subject filter
			_subjectFilter = new SubjectFilter();
			_subjectFilter.IsActive = true;
			_subjectFilter.IdFormation = c.Id;

			LoadSubjects();

			HdrMain.SelectedItem = HdiSubject;
		}
		private void CmiFormationTeachers_Click(object sender, EventArgs e)

		{
			//find request index
			int i = Formations.FindIndex(x => x.Id.ToString() == LsvFormation.SelectedItems[0].Text);

			if (i == -1) return;

			Formation c = Formations[i];

			//remove previous subject filter
			_teacherFilter = new TeacherFilter();
			_teacherFilter.IsActive = true;
			_teacherFilter.IdFormation = c.Id;

			LoadTeachers();

			HdrMain.SelectedItem = HdiTeacher;
		}

		private void CmiFormationRequests_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Formations.FindIndex(x => x.Id.ToString() == LsvFormation.SelectedItems[0].Text);

			if (i == -1) return;

			Formation c = Formations[i];

			//remove previous subject filter
			_requestsFilter = new RequestFilter();
			_requestsFilter.IsActive = true;
			_requestsFilter.IdFormation = c.Id;

			LoadRequests();

			HdrMain.SelectedItem = HdiRequest;
		}

		private void CmiClassSubjects_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Classes.FindIndex(x => x.Id.ToString() == LsvClass.SelectedItems[0].Text);

			if (i == -1) return;

			Class c = Classes[i];

			//remove previous subject filter
			_subjectFilter = new SubjectFilter();
			_subjectFilter.IsActive = true;
			_subjectFilter.IdClass = c.Id;

			LoadClasses();

			HdrMain.SelectedItem = HdiClass;
		}

		private void CmiClassRequests_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Classes.FindIndex(x => x.Id.ToString() == LsvClass.SelectedItems[0].Text);

			if (i == -1) return;

			Class c = Classes[i];

			//remove previous subject filter
			_requestsFilter = new RequestFilter();
			_requestsFilter.IsActive = true;
			_requestsFilter.IdClass = c.Id;

			LoadRequests();

			HdrMain.SelectedItem = HdiRequest;
		}

		private void CmiModuleStudents_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Modules.FindIndex(x => x.Id.ToString() == LsvModule.SelectedItems[0].Text);

			if (i == -1) return;

			Module c = Modules[i];

			//remove previous subject filter
			_studentFilter = new StudentFilter();
			_studentFilter.IsActive = true;
			_studentFilter.IdModule = c.Id;

			LoadStudents();

			HdrMain.SelectedItem = HdiStudent;
		}

		private void CmiTeacherFormations_Click(object sender, EventArgs e)
		{
			//find request index
			int i = Teachers.FindIndex(x => x.Id.ToString() == LsvTeacher.SelectedItems[0].Text);

			if (i == -1) return;

			Teacher t = Teachers[i];

			//remove previous class filter
			_formationFilter = new FormationFilter();
			_formationFilter.IsActive = true;
			_formationFilter.IdTeacher = t.Id;

			LoadFormations();

			HdrMain.SelectedItem = HdiFormation;
		}

		private void CmiTeacherAbscences_Click(object sender, EventArgs e)
		{

		}

		private void CmiStudentModules_Click(object sender, EventArgs e)
		{

		}

		private void CmiStudentSubjects_Click(object sender, EventArgs e)
		{

		}

		private void CmiStudentAbscence_Click(object sender, EventArgs e)
		{

		}

        private void TbpStudent_Click(object sender, EventArgs e)
        {

        }

        private void TbpMarks_Click(object sender, EventArgs e)
        {

        }
        private void CbxMarkFormation_SelectedIndexChanged(object sender, EventArgs e)
        {
            CbxMarkClass.Items.Clear();
            CbxMarkModule.Items.Clear();
            CbxMarkSubject.Items.Clear();
            CbxMarkTest.Items.Clear();

            if (CbxMarkFormation.SelectedIndex == -1) return;

            int id = Convert.ToInt32(CbxMarkFormation.Text.Split('-')[0]);
            int idFormation = Formations.FindIndex(x => x.Id == id);

            if (idFormation == -1) return;

            foreach (Class c in Classes.Where(x => x.IdFormation == idFormation))
            {
                CbxMarkClass.Items.Add(c.Id + " - " + c.Name);
            }

            if (CbxMarkClass.Items.Count > 0) CbxMarkClass.SelectedIndex = 0;

            CheckComboEnabling();
        }

        private void CB_note_classe_SelectedIndexChanged(object sender, EventArgs e)
        {
            CbxMarkModule.Items.Clear();
            CbxMarkSubject.Items.Clear();
            CbxMarkTest.Items.Clear();

            if (CbxMarkFormation.SelectedIndex == -1) return;

            int id = Convert.ToInt32(CbxMarkClass.Text.Split('-')[0]);
            int idClass = Classes.FindIndex(x => x.Id == id);

            if (idClass == -1) return;

            foreach (Module m in Modules.Where(module => module.IdClass == idClass))
            {
                CbxMarkModule.Items.Add(m.Id + " - " + m.Name);
            }

            if (CbxMarkModule.Items.Count > 0) CbxMarkModule.SelectedIndex = 0;

            CheckComboEnabling();
        }
        private void CbxMarkModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            CbxMarkSubject.Items.Clear();
            CbxMarkTest.Items.Clear();            

            if (CbxMarkModule.SelectedIndex == -1) return;

            int id = Convert.ToInt32(CbxMarkModule.Text.Split('-')[0]);
            int idModule = Modules.FindIndex(x => x.Id == id);

            if (idModule == -1) return;

            foreach (Subject s in Subjects.Where(subject => subject.IdModule == idModule))
            {
                CbxMarkSubject.Items.Add(s.Id + " - " + s.Name);
            }

            if (CbxMarkSubject.Items.Count > 0) CbxMarkSubject.SelectedIndex = 0;

            CheckComboEnabling();
        }
	   

        private void CbxMarkSubject_SelectedIndexChanged(object sender, EventArgs e)
        {
            CbxMarkTest.Items.Clear();

            if (CbxMarkModule.SelectedIndex == -1) return;

            int id = Convert.ToInt32(CbxMarkSubject.Text.Split('-')[0]);
            int idSubject = Subjects.FindIndex(x => x.Id == id);

            if (idSubject == -1) return;

            foreach (Test t in Tests.Where(test => test.IdSubject == idSubject))
            {
                CbxMarkTest.Items.Add(t.Id + " - " + t.Name);
            }

            if (CbxMarkTest.Items.Count > 0) CbxMarkTest.SelectedIndex = 0;

            CheckComboEnabling();
        }

	    private void CheckComboEnabling()
	    {
            CbxMarkTest.Enabled = CbxMarkTest.Items.Count != 0;
            CbxMarkSubject.Enabled = CbxMarkSubject.Items.Count != 0;
            CbxMarkModule.Enabled = CbxMarkModule.Items.Count != 0;
            CbxMarkClass.Enabled = CbxMarkClass.Items.Count != 0;
            CbxMarkFormation.Enabled = CbxMarkFormation.Items.Count != 0;
	    }

        private void CbxMarkTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            LsvMark.Items.Clear();

            if (CbxMarkTest.SelectedIndex == -1 || CbxMarkClass.SelectedIndex == -1) return;

            int id = Convert.ToInt32(CbxMarkTest.Text.Split('-')[0]);
            int idClass = Convert.ToInt32(CbxMarkClass.Text.Split('-')[0]);
            int idSubject = Subjects.FindIndex(x => x.Id == id);

            if (idSubject == -1) return;

            LsvMark.BeginUpdate();

            foreach (Student student in Students.Where(x=>x.IdClass== idClass))
            {
                ListViewItem l = new ListViewItem(student.Id.ToString());

                l.SubItems.Add(student.GetFullName());

                int idMark = Marks.FindIndex(x => x.IdStudent == student.Id && x.IdTest == id);

                if (idMark == -1)
                {
                    l.SubItems.Add("Pas de note");
                    l.BackColor = Color.Wheat;
                }
                else
                {
                    l.SubItems.Add(Marks[idMark].MarkLitteral);

                    if (Marks[idMark].Mark_ == -1) l.BackColor = Color.Wheat;
                }

                LsvMark.Items.Add(l);
            }

            ResizeColumns(LsvMark);

            LsvMark.EndUpdate();
        }

        private void BtnMarkNew_Click(object sender, EventArgs e)
        {
            AutoForm a = new AutoForm("Ajouter un control");

            a.AddEntry("name", new RuleEntry("Nom du control", "", Validation.MinLength, true, 5));
            a.AddEntry("dstart", new DateEntry("Date de début",DateTime.Now));
            a.AddEntry("dend", new DateEntry("Date de fin", DateTime.Now));

            if (a.ShowDialog() != DialogResult.OK) return;

            Test t = new Test(0);

            t.Name = a.GetValue("name").ToString();
            t.datetime_end = DateTime.ParseExact(a.GetValue("dend").ToString(), "d", CultureInfo.CurrentCulture);
            t.datetime_start = DateTime.ParseExact(a.GetValue("dstart").ToString(), "d", CultureInfo.CurrentCulture);

            int id = Convert.ToInt32(CbxMarkSubject.Text.Split('-')[0]);
            t.IdSubject = Subjects.FindIndex(x => x.Id == id);

            Tests.Add(t);

            CbxMarkSubject_SelectedIndexChanged(null, null);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Alert a = new Alert("Êtes-vous sûr de vouloir supprimer le control?", MessageBoxButtons.YesNo);

            a.Btn2Text = "Supprimer";
            a.Btn2Highlight= Highlight.Red;
            a.Btn3Text = "Annuler";

            if(a.ShowDialog() != DialogResult.Yes) return;

            Alert.Show("test");
        }

	}
}
